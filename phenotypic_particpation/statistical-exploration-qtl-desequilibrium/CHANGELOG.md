# Carnet de bord

## Overlapping cleaning problem

The problem comes from the fact that the statistics are made on a dataset containing QTLs physically mapped on the genome and cleaned by the overlap system but also unmapped QTLs which do not undergo this cleaning.
If we use only the QTLS that have been cleaned we end up with only the pair 13-16 that passes the statistics.

### Solution

Need to add more QTLs to gain in statistical power:

- Try to use data/markers/whole_datasets/all_markers_malus_GDR_cleaned_file.csv.cleaned. But this file, gather 3069281 marker which can help to assign more QTL. But this makes execution time exploding.
  **Solution**: make parallele computing to purge this huge dataset, deleting markers that are not associated with a QTL.
  **Results**: Using data/markers/whole_datasets/all_markers_malus_GDR_cleaned_file.csv.cleaned insted of
  data/markers/whole_datasets/whole_markers_mapped_on_GDDH13_from_GDR_cleaned_file.csv.cleaned, the dataset goes from **3888** markers to **5802** markers.
  This make that 951 QTLs are mapped to physical genome, making after overlap cleaning a dataset with 394 QTL used for statistic validation

- Try to use the genetic map to map non mappable markers to the nearest marker that is mapped
  Deleting marker with positions on old genome (MD...) makes a dataset from **5802** to **4934**

- Use of genetic map allow to map **4515** instead of **3881** markers. Blasting all SSR sequences allows to be more precise, and avoid to map to much QTL to same positions, avoiding overllaping filtering to be too much stringent

### Improvements

In: _2ec5d31ae628a1ad4cf526534a9d40b4e77b7ada_

Instead of using only markers associated to a QTL in GDR dataset, all marker with at least a start position (genetic position) or associated to a map is keep. It gather **515990** markers.
All SSR markers associated to a forward and reverse primers are Blasted (**520** SSR markers). It makes found **1040** records in query, **1015** have hits, making **25** misses. After finding a consensus between hits, position of **358** SSR markers are mapped on GDDH13. SSR markers non mapped on GDDH13: **184**. This approach permit to find new positions for markers with not any way to map them. Hence, not mapped markers can be mapped using closest mapped marker position's in associated genetic map.
This allow to get **510974** markers instead of **504484**.
This improvements allows to get **829** QTL mapped which makes, after cleaning, **394** available for statitiscs analysis.

Update in _47586950eeaf3c0afdb328681f7e38c9ee2cb430_:

- Mapping was erroneous due to bugs. New numbers are following:
- Mapped marker before using genetic maps: **504484**
- Mapped marker after using genetic maps: **513996**
- Afer removing duplicated marker (even if map are different beacause a this point, map is name become useless): **500943**
- About QTL: **855** before cleaning, **405** after

### Use of ontology

Update in: _49fecbd6a8703987ecf74c7f1bec65a18b466ad0_
By using a new ontology system for describe category we hope to save more QTL during cleaning of overlapping QTLs.
By doing so we get get from **855** QTLs mapped to **452** after cleaning. Became **462** even if cleaning overlap with 10000 around each QTL (Update in:).
Fixing some typos in categories allowing to keep **476** QTLs. (Update in:_3408e81202e510a443902d1ecb2799562b1549ee_)

## Refactor all code

Updates in: _670348ee2df7b6c7e03100482e3eb02f12a9e0d6_ in branch **refactor_code**
Because of many improvements added to code, scripts became too large to be easy maintainable. I decided to split QTL mapping and statistical analysis in two different scripts and associated librabry.
Tests are splitted too. This work take me near to two days.

## Add real synteny blocks data

The third projects allows to parse DAG chainer results coming from SynMap. A synteny block data file is produced and can be added to project to compare exact syntenic blocks, allowing to be more pecise and to compare all blocks not only entire ohnologous chromosomes.

This integration leads me to split statiscal exploration into two files and associated libraries into multiple files. All those modifications make a new refactoring needed.

### Algorithm

Updates in: _ae2cf070c1a12549ea739d2c8b1cfecf94291984_ in branch **includ_syntenic_blocks**

Globally algorithm is runned as following:

1. Load all files
2. Assign QTLs to each syntenic blocks and count them
3. Add to each QTL their associated syntenic blocks
4. Group QTL dataset following chrA and chrB columns allowing to create syntenic pairs
5. Compute all tests

### Results

Updates in: _ae2cf070c1a12549ea739d2c8b1cfecf94291984_ in branch **includ_syntenic_blocks**

Using not any particular sorts or thresholds following results are significative for a threshold at 5%.

|     | chromosome | statistical_value |     p_value | test_statistics       | origin          | Sort_on | Threshold       | Count_first_chromosome | Count_second_chromosome | Total_count |
| --: | :--------- | ----------------: | ----------: | :-------------------- | :-------------- | :------ | :-------------- | ---------------------: | ----------------------: | ----------: |
|   0 | [1, 7]     |           28.5946 | 8.92309e-08 | proportions_chisquare | Biparental-GWAS | No sort | No Row dropping |                     30 |                       7 |          37 |
|   1 | [1, 7]     |           5.34739 | 8.92309e-08 | proportions_ztest     | Biparental-GWAS | No sort | No Row dropping |                     30 |                       7 |          37 |
|   2 | [1, 7]     |                 7 | 0.000191076 | mcnemar_test          | Biparental-GWAS | No sort | No Row dropping |                     30 |                       7 |          37 |
|   3 | [1, 7]     |               nan | 9.55382e-05 | binomial_test         | Biparental-GWAS | No sort | No Row dropping |                     30 |                       7 |          37 |
|  20 | [3, 11]    |           6.89796 |  0.00862942 | proportions_chisquare | Biparental-GWAS | No sort | No Row dropping |                     31 |                      18 |          49 |
|  21 | [3, 11]    |            2.6264 |  0.00862942 | proportions_ztest     | Biparental-GWAS | No sort | No Row dropping |                     31 |                      18 |          49 |
|  23 | [3, 11]    |               nan |   0.0427166 | binomial_test         | Biparental-GWAS | No sort | No Row dropping |                     31 |                      18 |          49 |
|  40 | [6, 14]    |           7.14286 |  0.00752632 | proportions_chisquare | Biparental-GWAS | No sort | No Row dropping |                      9 |                      19 |          28 |
|  41 | [6, 14]    |          -2.67261 |  0.00752632 | proportions_ztest     | Biparental-GWAS | No sort | No Row dropping |                      9 |                      19 |          28 |
|  44 | [6, 14]    |               nan |   0.0435793 | binomial_test         | Biparental-GWAS | No sort | No Row dropping |                      9 |                      19 |          28 |
|  50 | [9, 17]    |           4.26667 |   0.0388671 | proportions_chisquare | Biparental-GWAS | No sort | No Row dropping |                     11 |                      19 |          30 |
|  51 | [9, 17]    |          -2.06559 |   0.0388671 | proportions_ztest     | Biparental-GWAS | No sort | No Row dropping |                     11 |                      19 |          30 |
|  60 | [13, 16]   |           17.9259 | 2.29671e-05 | proportions_chisquare | Biparental-GWAS | No sort | No Row dropping |                     16 |                      38 |          54 |
|  61 | [13, 16]   |           -4.2339 | 2.29671e-05 | proportions_ztest     | Biparental-GWAS | No sort | No Row dropping |                     16 |                      38 |          54 |
|  62 | [13, 16]   |                16 |  0.00383827 | mcnemar_test          | Biparental-GWAS | No sort | No Row dropping |                     16 |                      38 |          54 |
|  64 | [13, 16]   |               nan |  0.00191913 | binomial_test         | Biparental-GWAS | No sort | No Row dropping |                     16 |                      38 |          54 |

Updates in: _54d007b0a63042f0cd769f6eb1c38d417a33b65c_ in branch **includ_syntenic_blocks**

Add the other statistical tests.

Updates in: _78986ab75f240e8382c0fd13d60003d7d0685683_ in branch **includ_syntenic_blocks**

Add the categorie statistical tests.

### Some fixes on statistical tests

All test are runned using following informations:

- Pair: [1,7]
- 30 QTLs on chr1
- 7 QTLs on chr7

#### McNemar

Updates in: _09eaff8efc8752ced645d16dcf88af6496e6e062_ in branch **includ_syntenic_blocks**
This test is runned on chi-square distribution (exact=False) with a continuity correction (correction=True).
Initally following contingency table was constructed:

|                   |                 | First Chromosome |                |
| ----------------- | --------------- | ---------------- | -------------- |
|                   |                 | Presence of QTL  | Absence of QTL |
| Second Chromosome | Presence of QTL | 30               | 7              |
|                   | Absence of QTL  | 37               | 37             |

Which is erroneous. NOw following table is constructed

|                   |                 | First Chromosome |                |
| ----------------- | --------------- | ---------------- | -------------- |
|                   |                 | Presence of QTL  | Absence of QTL |
| Second Chromosome | Presence of QTL | 37               | 7              |
|                   | Absence of QTL  | 30               | 0              |

Giving following results:

- Method : McNemar's Chi-squared test with continuity correction
- Observed statistic Qobs : 13.081081081081
- p-value : 0.00029829324714202

New results:

| chromosome | statistical_value | p_value   | test_statistics | origin                | Sort_on         | Threshold | Count_first_chromosome | Count_second_chromosome | Total_count |     |
| ---------- | ----------------- | --------- | --------------- | --------------------- | --------------- | --------- | ---------------------- | ----------------------- | ----------- | --- |
| 0          | [1, 7]            | 28.594595 | 8.923093e-08    | proportions_chisquare | Biparental-GWAS | No sort   | No Row dropping        | 30                      | 7           | 37  |
| 1          | [1, 7]            | 5.347391  | 8.923093e-08    | proportions_ztest     | Biparental-GWAS | No sort   | No Row dropping        | 30                      | 7           | 37  |
| 2          | [1, 7]            | 13.081081 | 2.982932e-04    | mcnemar_test          | Biparental-GWAS | No sort   | No Row dropping        | 30                      | 7           | 37  |
| 3          | [1, 7]            | NaN       | 9.553821e-05    | binomial_test         | Biparental-GWAS | No sort   | No Row dropping        | 30                      | 7           | 37  |
| 20         | [3, 11]           | 7.840000  | 5.110261e-03    | proportions_chisquare | Biparental-GWAS | No sort   | No Row dropping        | 32                      | 18          | 50  |
| 21         | [3, 11]           | 2.800000  | 5.110261e-03    | proportions_ztest     | Biparental-GWAS | No sort   | No Row dropping        | 32                      | 18          | 50  |
| 23         | [3, 11]           | NaN       | 3.245432e-02    | binomial_test         | Biparental-GWAS | No sort   | No Row dropping        | 32                      | 18          | 50  |
| 40         | [6, 14]           | 7.142857  | 7.526315e-03    | proportions_chisquare | Biparental-GWAS | No sort   | No Row dropping        | 9                       | 19          | 28  |
| 41         | [6, 14]           | -2.672612 | 7.526315e-03    | proportions_ztest     | Biparental-GWAS | No sort   | No Row dropping        | 9                       | 19          | 28  |
| 44         | [6, 14]           | NaN       | 4.357928e-02    | binomial_test         | Biparental-GWAS | No sort   | No Row dropping        | 9                       | 19          | 28  |
| 60         | [13, 16]          | 24.711864 | 6.657309e-07    | proportions_chisquare | Biparental-GWAS | No sort   | No Row dropping        | 16                      | 43          | 59  |
| 61         | [13, 16]          | -4.971103 | 6.657309e-07    | proportions_ztest     | Biparental-GWAS | No sort   | No Row dropping        | 16                      | 43          | 59  |
| 62         | [13, 16]          | 11.457627 | 7.120116e-04    | mcnemar_test          | Biparental-GWAS | No sort   | No Row dropping        | 16                      | 43          | 59  |
| 64         | [13, 16]          | NaN       | 2.921823e-04    | binomial_test         | Biparental-GWAS | No sort   | No Row dropping        | 16                      | 43          | 59  |

#### Proportion z test

Checked manually, this test is fine.

#### Proportions_chisquare

#### Binomial test

Using previous test conditions, this test is fine. manually calculated:

- The probability of exactly 30 (K) out of 37 (n) is p = .00007491.
- The probability of exactly, or fewer than, 30 (K) out of 37 (n) is p = .99997937.
- The probability of exactly, or more than, 30 (K) out of 37 (n) is p = .00009554.

Which is fine with current results

## Analysis of results

A this point, results can be analysed as following.
Globally following pairs are clearly desequilibred:

- 1 and 7
- 3 and 11
- 6 and 14
- 13 and 16

Considering only by categories those syntenic blocks:

- 1 and 7
- 13 and 16
- 2 and 15
- 3 and 11
- 4 and 12
- 5 and 10
- 6 and 14

Associated results all computed using proportions_chisquare:

|      | chromosome | statistical_value | p_value      | Sort_on                | Threshold                             | Count_first_chr | Count_second_chr | Total_count |
| ---- | ---------- | ----------------- | ------------ | ---------------------- | ------------------------------------- | --------------- | ---------------- | ----------- |
| 0    | [1, 7]     | 23.120000         | 1.521993e-06 | Trait Category Level 3 | Biochemical trait                     | 21              | 4                | 25          |
| 167  | [1, 7]     | 14.000000         | 1.828106e-04 | Trait Category Level 3 | Fruit quality trait                   | 7               | 0                | 7           |
| 19   | [1, 7]     | 12.000000         | 5.320055e-04 | Trait Category Level 2 | Fruit quality trait                   | 6               | 0                | 6           |
| 51   | [1, 7]     | 30.421053         | 3.477337e-08 | Trait Category Level 2 | Phenolic                              | 18              | 1                | 19          |
| 182  | [2, 15]    | 5.555556          | 1.842213e-02 | Trait Category Level 3 | Fruit quality trait                   | 7               | 2                | 9           |
| 20   | [3, 11]    | 16.133333         | 5.903578e-05 | Trait Category Level 3 | Biochemical trait                     | 13              | 2                | 15          |
| 71   | [3, 11]    | 10.888889         | 9.674285e-04 | Trait Category Level 2 | Phenolic                              | 8               | 1                | 9           |
| 30   | [4, 12]    | 7.142857          | 7.526315e-03 | Trait Category Level 3 | Biochemical trait                     | 1               | 6                | 7           |
| 35   | [5, 10]    | 3.846154          | 4.986020e-02 | Trait Category Level 3 | Biochemical trait                     | 9               | 4                | 13          |
| 1062 | [5, 10]    | 5.555556          | 0.018422     | Categorie              | Texture                               | 7               | 2                | 9           |
| 1547 | [5, 10]    | 9.000000          | 0.002700     | Categorie              | resistance to Botryosphaeria dothidea | 1               | 7                | 8           |
| 1552 | [6, 14]    | 4.000000          | 0.045500     | Categorie              | resistance to Botryosphaeria dothidea | 2               | 6                | 8           |
| 309  | [6, 14]    | 4.000000          | 4.550026e-02 | Trait Category Level 3 | Stress trait                          | 2               | 6                | 8           |
| 570  | [6, 14]    | 4.000000          | 4.550026e-02 | Trait Category Level 2 | Biotic stress                         | 2               | 6                | 8           |
| 213  | [13, 16]   | 12.000000         | 5.320055e-04 | Trait Category Level 2 | Acid                                  | 0               | 6                | 6           |
| 111  | [13, 16]   | 28.444444         | 9.642607e-08 | Trait Category Level 2 | Phenolic                              | 1               | 17               | 18          |
| 107  | [13, 16]   | 14.000000         | 0.000183     | Categorie              | Procyanidin                           | 0               | 7                | 7           |
| 1087 | [13, 16]   | 12.000000         | 0.000532     | Categorie              | Texture                               | 0               | 6                | 6           |
| 60   | [13, 16]   | 43.103448         | 5.192067e-11 | Trait Category Level 3 | Biochemical trait                     | 2               | 27               | 29          |

### Work on visualizations

Now use altair library that allows interactive plotting aswell as more possibilities.

#### Visualisations implemented

Using altair foolowing visualisations are implemented:

- Stripplots to represent QTL and their genomic positions:
  - ![Stripplots to represent QTL](img/stripplot_altair.png)
- A stacked Bart chart to represent number of QTL on each chromosome
  - ![A stacked Bart chart](img/stacked_bar_chart.png)
- A Scatter plot to represent all statistical results
  - ![A Scatter plot]()

### Corrections

#### Improvements after meeting

- Increases the size of the overlap cleaning to **100000** to each size. DONE: _a93d404a54555fba933f2aeb277d28880f46d7c6_
- Add a limit to includ neightboring marker to **5 cM**. DONE: _a93d404a54555fba933f2aeb277d28880f46d7c6_
- Mapping via the nearest marker around 5 cM
- Color circos using p-value
- Check where are QTLs non associated with a syntenic block. DONE: _6dda31df7f9c1649fc591092197cd24b656cfb8d_
- Remove QTLs non associated with syntenic block. DONE: _6dda31df7f9c1649fc591092197cd24b656cfb8d_
- Improve visualisations:
  - Sort stacked bar chart. DONE: _acf3f516fa523702e11bfc28d2bdb89bef0e2802_
  - Order bar. DONE: _85139df93df6f2773916cebc5475478a0beeeba2_
  - Opacity of bar following orientation of imbalance. DONE: _acf3f516fa523702e11bfc28d2bdb89bef0e2802_

#### Results

Increases the size of the overlap cleaning to **100000** to each size. Add a limit to includ neightboring marker to **5 cM**.
**New results**:

Total number of before purging dataset markers:3069159
Total number of afer purging dataset markers:515990
Blast : found 1040 records in query, 1015 have hits, making 25 misses
Number of unique SSR primers before dropping: 1015
Number of unique SSR primers after dropping: 1015
SSR markers mapped on GDDH13: 344
SSR markers non mapped on GDDH13: 198
Mapped marker before using genetic maps: 504484
Mapped marker after using genetic maps: 513821
Number of QTLs before dropping all non mapped: (1079, 24)
Number of QTLs after dropping all non mapped: (705, 24)
Number of QTLs before dropping overlapping QTLs: 840
Number of QTLs after dropping overlapping QTLs: 474

Completly rework marker mapping. Update in: _687e4bd7ff75109f5f71a39ec30fccc2068d7a52_
**New results**:
Total number of before purging dataset markers:3069159
Total number of afer purging dataset markers:515990
Blast : found 1040 records in query, 1015 have hits, making 25 misses
Number of unique SSR primers before dropping: 1015
Number of unique SSR primers after dropping: 1015
SSR markers mapped on GDDH13: 347
SSR markers non mapped on GDDH13: 177
Number of QTLs before dropping all non mapped: (1079, 24)
Number of QTLs after dropping all non mapped: (873, 24)

Number of QTLs before dropping overlapping QTLs: 1008
Number of QTLs after dropping overlapping QTLs: 538
