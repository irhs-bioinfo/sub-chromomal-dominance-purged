#!/bin/bash
docker build --tag qtl-desiquilibrium-dockerized .
docker run qtl-desiquilibrium-dockerized:latest /bin/bash -c 'cd src ; python map_to_chromosome.py'
docker cp qtl-desiquilibrium:/usr/src/app/output .


#docker run -it --rm --entrypoint=/bin/bash qtl-desiquilibrium-dockerized:latest
# To build on forge.mia
# docker build -t registry.forgemia.inra.fr/tanguy.lallemand/qtl-desequilibrium .
# docker push registry.forgemia.inra.fr/tanguy.lallemand/qtl-desequilibrium
