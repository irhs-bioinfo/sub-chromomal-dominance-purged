import numpy as np
import pandas as pd
import pytest

from src.lib.compute_all_tests import save_in_data_frame
from tests.conftest import (getSampleChromosomeData, getSampleMarkers,
                            getSampleQtlFromGdr, getSampleQtlFromGwas)

def test_save_in_data_frame(getSampleQtlFromGwas):
  resultDataFrame = pd.DataFrame()
  testedParameters = {'Sort_on': 'R2', 'Threshold': 8}
  numberOfSuccesses = {'chromosome_3': 0, 'chromosome_11': 1}
  totalSize = 0
  blocksCoordinates = {3: 0, 11: 0}
  resultDataFrame = save_in_data_frame([3, 11], 5.2, 0.01, 'mc_nemar', 'GWAS',
                                       resultDataFrame, testedParameters,
                                       numberOfSuccesses, totalSize,
                                       blocksCoordinates)
  expectedResults = pd.DataFrame(
      data=[[{
          3: 0,
          11: 0
      }, 0, 1, 0.0, 'R2', 8.0, 0, [3, 11], 'GWAS', 0.01, 5.2, 'mc_nemar']],
      columns=[
          'Blocks_coordinates', 'Count_first_chromosome',
          'Count_second_chromosome', 'Orientation_of_imbalance', 'Sort_on',
          'Threshold', 'Total_count', 'chromosome', 'origin', 'p_value',
          'statistical_value', 'test_statistics'
      ])
  pd.testing.assert_frame_equal(
      resultDataFrame,
      expectedResults,
      check_dtype=False,
      check_index_type=False)