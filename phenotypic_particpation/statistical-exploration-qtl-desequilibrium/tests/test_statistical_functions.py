import numpy as np
import pandas as pd
import pytest
from scipy import stats
from statsmodels.stats.contingency_tables import mcnemar
from statsmodels.stats.proportion import (proportions_chisquare,
                                          proportions_ztest)

from src.lib.statistical_functions import (
    execute_mcnemar_test, execute_proportions_chisquare,
    execute_proportions_ztest, significative_p_value)
from tests.conftest import (getSampleChromosomeData, getSampleMarkers,
                            getSampleQtlFromGdr, getSampleQtlFromGwas)
from src.lib.dichotomic_table import generate_contingency_table, generate_dichotomic_table, generate_sucesses_table


@pytest.mark.parametrize('chromosomeNumber, expected', [(3, 0.25), (11, 1)])
def test_execute_binomial_test(chromosomeNumber, expected,
                               getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table([3, 11], neededColumns, qtlData)
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  pValue = stats.binom_test(
      numberOfSuccesses['chromosome_' + str(chromosomeNumber)],
      totalSize,
      0.5,
      alternative='greater')
  assert pValue == expected


@pytest.mark.parametrize('chromosomeNumber, expected',
                         [(3, 0.47950012218695337), (11, 0.47950012218695337)])
def test_execute_mcnemar_test(chromosomeNumber, expected, getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table([3, 11], neededColumns, qtlData)
  # McNemar test of homogeneity.
  mcnemarResults = execute_mcnemar_test(dichotomicTable, [3, 11])
  assert mcnemarResults.pvalue == expected


@pytest.mark.parametrize('chromosomeNumbers, expected',
                         [([3, 11], 0.04550026389635839)])
def test_execute_proportions_ztest(chromosomeNumbers, expected,
                                   getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table(chromosomeNumbers, neededColumns,
                                              qtlData)
  stat, pval = execute_proportions_ztest(dichotomicTable, chromosomeNumbers)
  assert pval == expected


@pytest.mark.parametrize('chromosomeNumbers, expected',
                         [([3, 11], 0.04550026389635857)])
def test_execute_proportions_chisquare(chromosomeNumbers, expected,
                                       getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table(chromosomeNumbers, neededColumns,
                                              qtlData)
  stat, pval, table = execute_proportions_chisquare(dichotomicTable,
                                                    chromosomeNumbers)
  assert pval == expected


@pytest.mark.parametrize('pValue, threshold, expected', [(0.01, 0.05, True),
                                                         (0.1, 0.05, None)])
def test_significative_p_value(pValue, threshold, expected):
  assert significative_p_value(pValue, threshold) == expected
