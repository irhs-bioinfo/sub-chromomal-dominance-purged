import pytest
import pandas as pd
import os
import sys

def pytest_addoption(parser):
  # py.test has an issue where the cwd is not in the PYTHONPATH. Fix it here.
  if os.getcwd() not in sys.path:
    sys.path.insert(0, os.getcwd())


@pytest.fixture(scope='function', autouse=True)
def getSampleQtlFromGdr():
  # Read qtl data and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/sample_whole_qtl_from_GDR.csv')


@pytest.fixture(scope='function', autouse=True)
def getSampleQtlFromGwas():
  # Read qtl data and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/sample_qtl_from_GWAS.csv')


@pytest.fixture(scope='function', autouse=True)
def getSampleChromosomeData():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/chromosomes_caracteritics.csv')


@pytest.fixture(scope='function', autouse=True)
def getSampleMarkers():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/sample_marker_from_GDR.csv')


@pytest.fixture(scope='function', autouse=True)
def getAllQtlPhysicalyMappedDf():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/sample_all_qtl_physicaly_mapped_df.csv'
  )


@pytest.fixture(scope='function', autouse=True)
def getSyntenicBlocksDf():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/syntenic_blocks.csv')


@pytest.fixture(scope='function', autouse=True)
def getAllQtlPhysicalyMappedDfWholeDf():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/allQtlPhysicalyMappedDf.csv')


@pytest.fixture(scope='function', autouse=True)
def getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks():
  # Read chromosome characteritics and put it in pandas Data Frame
  return pd.read_csv(
      'tests/data_samples/all_qtl_with_syntenic_blocks.csv')
