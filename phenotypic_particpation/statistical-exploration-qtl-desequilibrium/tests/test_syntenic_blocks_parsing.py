import numpy as np
import pandas as pd
import pytest

from src.lib.syntenic_blocks_parsing import add_associated_syntenic_block_to_qtl_df, group_by_syntenic_pair, get_ohnologous_syntenic_groups, compute_all_tests_syntenic_blocks
from tests.conftest import (getSampleChromosomeData, getSampleMarkers,
                            getSampleQtlFromGdr, getSampleQtlFromGwas,
                            getSyntenicBlocksDf)


@pytest.mark.parametrize('chromosomeNumber, expected', [(1, 34), (11, 20),
                                                        (5, 29), (10, 33)])
def test_add_associated_syntenic_block_to_qtl_df(
    chromosomeNumber, expected, getSyntenicBlocksDf,
    getAllQtlPhysicalyMappedDfWholeDf):
  syntenicBlocks = getSyntenicBlocksDf
  allQtlPhysicalyMappedDf = getAllQtlPhysicalyMappedDfWholeDf
  # Delete chr substring and retype column as a int64
  syntenicBlocks['chrA'] = syntenicBlocks['chrA'].str.replace(
      r'Chr', '').astype('int64')
  syntenicBlocks['chrB'] = syntenicBlocks['chrB'].str.replace(
      r'Chr', '').astype('int64')

  allQtlPhysicalyMappedDf[['chrA', 'startA', 'endA', 'chrB', 'startB',
                           'endB']] = allQtlPhysicalyMappedDf.apply(
                               add_associated_syntenic_block_to_qtl_df,
                               axis=1,
                               args=(syntenicBlocks,))
  numberOnChr1 = allQtlPhysicalyMappedDf.loc[allQtlPhysicalyMappedDf['chrA'] ==
                                             chromosomeNumber]
  assert numberOnChr1.shape[0] == expected


def test_group_by_syntenic_pair(
    getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks):
  allQtlPhysicalyMappedDf = getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks
  groupedAllQtlPhysicalyMappedDf = group_by_syntenic_pair(
      allQtlPhysicalyMappedDf)
  assert groupedAllQtlPhysicalyMappedDf.ngroups == 45

def test_get_ohnologous_syntenic_groups(getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks):
  allQtlPhysicalyMappedDf = getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks
  groupedAllQtlPhysicalyMappedDf = group_by_syntenic_pair(
      allQtlPhysicalyMappedDf)
  listOfSyntenicPairs = get_ohnologous_syntenic_groups(
      groupedAllQtlPhysicalyMappedDf)
  assert len(listOfSyntenicPairs) == 13

def test_compute_all_tests_syntenic_blocks(getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks):
  allQtlPhysicalyMappedDf = getAllQtlPhysicalyMappedDfWholeDfAndSyntenicBlocks
  groupedAllQtlPhysicalyMappedDf = group_by_syntenic_pair(
      allQtlPhysicalyMappedDf)
  listOfSyntenicPairs = get_ohnologous_syntenic_groups(
      groupedAllQtlPhysicalyMappedDf)
  resultDataFrame = compute_all_tests_syntenic_blocks(
      listOfSyntenicPairs, groupedAllQtlPhysicalyMappedDf, ['Label', 'Origin'], '',
      0.05)
  row = resultDataFrame.iloc[1]
  assert row['statistical_value'] == 5.3473913822156875 and row[
      'Count_first_chromosome'] == 30
