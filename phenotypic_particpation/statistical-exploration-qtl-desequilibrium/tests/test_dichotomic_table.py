import numpy as np
import pandas as pd
import pytest

from src.lib.dichotomic_table import (generate_contingency_table,
                                                generate_dichotomic_table,
                                                generate_sucesses_table)
from tests.conftest import (getSampleChromosomeData, getSampleMarkers,
                            getSampleQtlFromGdr, getSampleQtlFromGwas)

# TODO: test to implements
# def test_compute_all_tests(chromosomePairs, qtlDataset, neededColumns, sortingColumn,
#                   threshold):

# def test_summary_of_needed_column(neededColumns, dichotomicTable):
# def test_sump_up_columns(neededColumns, dichotomicTable, chromosomeNumbers,
#                     tempDataFrame):
# def test_subset_qtl_data_data_of_one_chromosome(qtlData, chromosomeNumber,
#                                            neededColumns):
# def test_sort_and_cut_dichotomic_table(dichotomicTable, sortingColumn, threshold):
# def test_check_for_nan(numberOfSuccesses, totalSize, chromosomeNumbers):


def test_generate_contingency_table(getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table([3, 10], neededColumns, qtlData)
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  contingencyTable = generate_contingency_table(dichotomicTable, [3, 10])
  assert contingencyTable.loc[0][1] == 1 and contingencyTable.loc[1][
      0] == 2 and contingencyTable.loc[0][1] + contingencyTable.loc[1][0] == 3


def test_generate_sucesses_table(getSampleQtlFromGwas):
  qtlData = getSampleQtlFromGwas
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table([3, 11], neededColumns, qtlData)
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  assert numberOfSuccesses.chromosome_3 == 2 and numberOfSuccesses.chromosome_11 == 0 and totalSize == 2


def test_generate_dichotomic_table(getSampleQtlFromGwas):

  qtlData = getSampleQtlFromGwas
  chromosomeNumbers = [3, 11]
  neededColumns = ['Label']
  dichotomicTable = generate_dichotomic_table(chromosomeNumbers, neededColumns,
                                              qtlData)
  expectedResults = pd.DataFrame(
      data=[[1, 0, 'SNP.3-3 and SNP.3-4'], [1, 0, 'SNP.3-6 and SNP.3-7']],
      columns=['chromosome_3', 'chromosome_11', 'Label'],
      index=dichotomicTable.index)
  pd.testing.assert_frame_equal(
      dichotomicTable,
      expectedResults,
      check_dtype=False,
      check_index_type=False)
