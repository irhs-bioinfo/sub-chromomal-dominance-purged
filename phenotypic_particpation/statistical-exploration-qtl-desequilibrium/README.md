# 1. statistical-exploration-qtl-desequilibrium

[![pipeline status](https://forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium/badges/master/pipeline.svg)](https://forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium/-/commits/master)
[![coverage report](https://forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium/badges/master/coverage.svg)](https://forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium/-/commits/master)

## 1.1. Aims

The purpose of this script is to statistically explore a possible imbalance in the distribution of QTLs between different syntenic peers.

## 1.2. Installation

### 1.2.1. From Docker image

First login to container registry using

    docker login registry.forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium -u login -p access_token

Continue by pulling image

    docker pull registry.forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium:latest

You can rename image with a more readable one

    docker image tag registry.forgemia.inra.fr/tanguy.lallemand/statistical-exploration-qtl-desequilibrium:latest statistical-exploration-qtl-desequilibrium:latest

Start a container. This command allows to binds on 8898 port of local machine with jupyter port of container. It binds a local directory to ouput directory of container, allowing to save results on local machine. To finish it dynammicaly launch container with current user on local machine.

    docker run --rm -d  -p 8898:8888/tcp -v "$(pwd)"/local_directory_to_bind:/app/output statistical-exploration-qtl-desequilibrium:latest

finally go to http://localhost:8895 and start to use notebook from src directory

### 1.2.2. From virtual environnement

Begin by getting all files by cloning repository

#### 1.2.2.1. Installation of dependencies

- Pipenv

Go at root of the repository

    cd statistical-exploration-qtl-desequilibrium
    pipenv install

- Other environnements

Go to your virtual environnement and:
pip install -r requirements.txt

#### 1.2.2.2. Run it

run:

    pipenv run jupyter-notebook

and follow the link displayed in console

    pipenv run python statistical_exploration_synteny_blocks.py

## 1.3. File architecture

The project is divided into different folders. The src folder contains all scripts and associated function libraries. Each script exists in two versions, a first version in python and a second version in jupyter notebook. The tests folder contains all the unit tests associated with the project. The output folder will contain all the output files. The folder data will contain all the input files and in particular:

- The set of markers, all with physical poistions in the apple tree, coming from qtl-desequilibrium script. This file is stored at: **data/QTLs/allQtlPhysicalyMappedDf.csv.gz**
- The set of QTLs mapped on the apple tree, coming from qtl-desequilibrium script. This file is stored at: **data/qtl/whole_qtl_from_GDR.csv.gz**
- A file containing the coordinates of the syntenic blocks existing within the genome. This file was generated from the results of SynMap. This file is stored at: **data/syntenic_blocks/syntenic_blocks.csv**

## 1.4. Algorithm

This script starts by unpacking and loading the necessary files.
The first step will consist in analyzing the blocks of synténies in order to build an object allowing an optimal exploitation in term of storage and computing time. Then each QTL is associated to a syntax block. The QTLs are then grouped according to their synteny blocks (i.e. a pair of chromosome parts). A series of statistical tests are then run for each group. The statistical tests used are the McNemar test, the binomial test, the ztest proportion test and the chi2 proportion test.
To follow, tests are launched by sorting the groups differently and in particular according to their origin (GWAS or biparental) or the category of trait associated with the QTL.
Finally, a series of visualizations are constructed.

## 1.5. Results

A few synenic peers are therefore quite unbalanced. The test results are placed in the following directory: **output/statistics**.
To summarize, following pairs have desequilibred QTLs:
[1,7], [13,16], [2,15], [3,11], [4,12], [5,10], [6,14]
In addition, different associated visualizations have been built and placed here after.

![This plot aims to represent count of QTLs on each chromosome. On x-axis is represented chromosome and y axis is the number of QTLs. Color represent category of trait associated to each QTL.](output/figures/synteny_block_scale/stacked_bar_chart.png)
This plot aims to represent count of QTLs on each chromosome. On x-axis is represented chromosome and y axis is the number of QTLs. Color represent category of trait associated to each QTL.
![](output/figures/synteny_block_scale/stripplot_altair.png)
This set of plots represent location of QTLs. Each subplot is associated to a chromosome.On y axis are the location of pb so the strip is the size of QTL on chromosome. Color is associated to category trait.
![This plot represent all significative results. The first striplot represent on x axis syntenic pair and on the y-axis, the associated p-value. The second plot is constitued by a set of bar chart. Each subplot is associated to a syntenic block. Opacity represent direction of imbalance, thus if all bar are of the same opacity then the imbalance is always in the same direction. Color represent tested threshold. X-axis represent a count of significative test retrieved.](output/figures/synteny_block_scale/allSignificativeResults.png)
This plot represent all significative results. The first striplot represent on x axis syntenic pair and on the y-axis, the associated p-value. The second plot is constitued by a set of bar chart. Each subplot is associated to a syntenic block. Opacity represent direction of imbalance, thus if all bar are of the same opacity then the imbalance is always in the same direction. Color represent tested threshold. X-axis represent a count of significative test retrieved.
