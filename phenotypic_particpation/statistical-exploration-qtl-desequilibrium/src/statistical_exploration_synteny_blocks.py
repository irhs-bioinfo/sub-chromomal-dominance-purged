# %% [markdown]
"""The purpose of this script is to statistically explore a possible imbalance in the distribution of QTLs between different syntenic peers.
"""
# %%
##############################################################################
# Import needed modules and packages
##############################################################################

import os

import altair as alt
import altair_viewer
import ipywidgets as widgets
import numpy as np
import pandas as pd
from altair_saver import save
from IPython.display import Image, Markdown, display
from ipywidgets import fixed, interact

from lib.config_parser import config_parser
from lib.misc import (delete_temporary_files, description_of_datasets,
                      output_dataframe_in_csv,
                      reorder_chromosome_pair_for_visualisation)
from lib.parsing_statistical_results import (
    interactive_exploration, interactive_exploration_R2,
    interactive_exploration_vanilla_stats, interactive_results)

from lib.syntenic_blocks_parsing import (
    add_associated_syntenic_block_to_qtl_df, compute_all_tests_syntenic_blocks,
    count_qtls_on_syntenic_blocks, get_all_results,
    get_ohnologous_syntenic_groups, group_by_syntenic_pair,
    size_of_syntenic_blocks)
from lib.visualization import (generate_stacked_bar_chart,
                               generate_stripplot_with_altair,
                               generate_visualizations, heatmapOfResults,
                               summary_of_results)

# %%
pd.set_option('display.max_rows', 1000)

# Load configuration
conf = config_parser(
    './conf/config_statistical_exploration_synteny_blocks.yaml')
##############################################################################
# Load datasets and initialize some variables
##############################################################################
# Read input files and put it in pandas Data Frame
# QTL related files
allQtlPhysicalyMappedDf = pd.read_csv(conf['QTL']['AllQtlPhysicalyMappedDf'], compression='infer')

# Markers and physical location related files
chromosomeData = pd.read_csv(conf['SynthenicBlocks']['chromosomeData'], compression='infer')

# Synthenic blocks
syntenicBlocksDf = pd.read_csv(conf['SynthenicBlocks']['synthenicBlocks'], compression='infer')
# Variable initialization
verbose = conf['Variable']['verbose']
outputAsFile = conf['Variable']['outputAsFile']

# Purge temporary files
delete_temporary_files([
    conf['Statistics']['outputPathStatsVanilla'],
    conf['Statistics']['outputPathStatsR2Effect'],
    conf['Statistics']['outputPathOriginEffect'],
    conf['Statistics']['outputPathCategoryEffect'],
    conf['Statistics']['outputPathTraitCategoryLevel2Effect'],
    conf['Statistics']['outputPathTraitCategoryLevel3Effect']
])
# Recreate file architecture if needed
for name in conf['Figure']:
  os.makedirs(os.path.dirname(conf['Figure'][name]), exist_ok=True)

#%%

# Delete chr substring and retype column as a int64
for chrColumn in ['chrA', 'chrB']:
  # Delete chr substring and retype column as a int64
  syntenicBlocksDf[chrColumn] = syntenicBlocksDf[chrColumn].str.replace(
      r'Chr', '').astype('int64')
# Make a copy of QTL dataset to keep a trace of non assigned QTL
nonAssignedToSyntenicBlock = allQtlPhysicalyMappedDf.copy()
# Init a new column
syntenicBlocksDf['assignedQtlToA'] = 0
# Get count of QTL associated to each syntenic block
syntenicBlocksDf['assignedQtlToA'] = syntenicBlocksDf.apply(
    count_qtls_on_syntenic_blocks,
    axis=1,
    args=(nonAssignedToSyntenicBlock, 'A'))

syntenicBlocksDf['sizeBlockA'] = syntenicBlocksDf.apply(
    size_of_syntenic_blocks, axis=1)

percentageCoverage = (syntenicBlocksDf['sizeBlockA'].sum() / 624851326) * 100
print('Percentage of coverage of apple genome by syntenic blocks: ' +
      f'{percentageCoverage:.2f}')
# Add associated syntenic block to each QTL
allQtlPhysicalyMappedDf[['chrA', 'startA', 'endA', 'chrB', 'startB',
                         'endB']] = allQtlPhysicalyMappedDf.apply(
                             add_associated_syntenic_block_to_qtl_df,
                             axis=1,
                             args=(syntenicBlocksDf,))
qtlNonassignedToSyntenicGroup = allQtlPhysicalyMappedDf[
    allQtlPhysicalyMappedDf['chrA'].isnull()]
allQtlPhysicalyMappedDf = allQtlPhysicalyMappedDf[
    ~allQtlPhysicalyMappedDf['chrA'].isnull()]
# Make group by chrA and chr B allowing to get each syntenic pair
groupedAllQtlPhysicalyMappedDf = group_by_syntenic_pair(allQtlPhysicalyMappedDf)

# Get all existent syntenic pairs in a list
listOfSyntenicPairs = get_ohnologous_syntenic_groups(
    groupedAllQtlPhysicalyMappedDf)

# %% [markdown]
"""
## Some meta values
"""
#%%
# Display Circos in jupyter notebook
display(Image(filename='../img/circos.png'))

if verbose:
  print('Description of mapped QTLs:')
  display(description_of_datasets(allQtlPhysicalyMappedDf))
  print('Number of QTL assigned to syntenic block: ' +
        str(syntenicBlocksDf['assignedQtlToA'].sum()))
  print('Syntenic block tested:')
  neededColumns = ['Label', 'Origin']
  resultDataFrame = compute_all_tests_syntenic_blocks(
      listOfSyntenicPairs, groupedAllQtlPhysicalyMappedDf, neededColumns, '',
      0.05)
  display(
      Markdown(
          pd.Series(np.unique(resultDataFrame['chromosome'])).to_markdown()))



#%%

neededColumns = ['Label', 'Origin']

resultDataFrameStatsVanilla = compute_all_tests_syntenic_blocks(
    listOfSyntenicPairs, groupedAllQtlPhysicalyMappedDf, neededColumns, '',
    0.05)

pd.set_option('display.max_rows', resultDataFrameStatsVanilla.shape[0] + 1)
# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)

interact(
    interactive_exploration_vanilla_stats,
    resultDataFrame=fixed(resultDataFrameStatsVanilla),
    significative=checkBox)

# %% [markdown]
"""
## Exploration on effect of R2 on statistical validation.

Then we'll look at the effect of R2 on QTL imbalance. We will therefore apply a threshold to the data with a value of R2
"""
# %%
neededColumns = ['Label', 'Origin', 'R2']
testedPercentages = np.arange(0.0, 0.2, 0.15)

appendedData = []
for testedPercentage in testedPercentages:
  appendedData.append(
      compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                        groupedAllQtlPhysicalyMappedDf,
                                        neededColumns, 'R2', testedPercentage))
resultDataFrameStatsR2Effect = pd.concat(appendedData, axis=0)

interact(
    interactive_exploration_R2,
    resultDataFrame=fixed(resultDataFrameStatsR2Effect),
    column=['Threshold'],
    threshold=(0.0, 0.2, 0.15),
    significative=checkBox)

# %% [markdown]
"""
## Exploration on effect of origin on statistical validation.

Then we'll look at the effect of the origin of QTLS on the imbalance of QTLs. We will then select the data on their origin (biparental, GWAs or both).
"""

# %%
neededColumns = ['Label', 'Origin', 'R2']
testedOrigins = ['Biparental', 'GWAS', 0]
appendedData = []
for testedOrigin in testedOrigins:
  if testedOrigin == 0:
    appendedData.append(
        compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                          groupedAllQtlPhysicalyMappedDf,
                                          neededColumns, '', 0))
  else:
    appendedData.append(
        compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                          groupedAllQtlPhysicalyMappedDf,
                                          neededColumns, 'Origin',
                                          testedOrigin))
resultDataFrameOriginEffect = pd.concat(appendedData, axis=0)

# Initalize a selector to select origin
select = widgets.Select(
    options=['Biparental', 'GWAS', 'Biparental-GWAS', r'(.*?)'],
    value=r'(.*?)',
    description='Origin:',
    disabled=False)

interact(
    interactive_exploration,
    resultDataFrame=fixed(resultDataFrameOriginEffect),
    categorie=select,
    significative=checkBox,
    column='Threshold')

# %% [markdown]
"""
## Exploration on effect of category on statistical validation.

Then we will look at the effect of the category to which the QTLSs belong on the imbalance of the QTLs. We will therefore select the QTLs by category and apply the set of tests

### Using GDR categories (Top level of created ontologie)
"""
# %%

neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]

appendedData = []
for testedCategorie in allQtlPhysicalyMappedDf['Categorie'].unique():
  appendedData.append(
      compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                        groupedAllQtlPhysicalyMappedDf,
                                        neededColumns, 'Categorie',
                                        testedCategorie))
resultDataFrameCategoryEffect = pd.concat(appendedData, axis=0)

# Initalize a selctor to select origin
select = widgets.Select(
    options=np.append(
        sorted(allQtlPhysicalyMappedDf['Categorie'].unique()), r'(.*?)'),
    value=r'(.*?)',
    description='Categorie:',
    disabled=False)

interact(
    interactive_exploration,
    resultDataFrame=fixed(resultDataFrameCategoryEffect),
    categorie=select,
    significative=checkBox,
    column='Threshold')

# %% [markdown]
"""
### Using Ontologie (Level  2 of created ontologie)
"""
#%%
neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]

appendedData = []

for testedCategorie in allQtlPhysicalyMappedDf['Trait Category Level 2'].unique(
):
  appendedData.append(
      compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                        groupedAllQtlPhysicalyMappedDf,
                                        neededColumns, 'Trait Category Level 2',
                                        testedCategorie))
resultDataFrameTraitCategoryLevel2Effect = pd.concat(appendedData, axis=0)

# Initalize a selctor to select origin
select = widgets.Select(
    options=np.append(
        sorted(allQtlPhysicalyMappedDf['Trait Category Level 2'].unique()),
        r'(.*?)'),
    value=r'(.*?)',
    description='Categorie:',
    disabled=False)

interact(
    interactive_exploration,
    resultDataFrame=fixed(resultDataFrameTraitCategoryLevel2Effect),
    categorie=select,
    significative=checkBox,
    column='Threshold')

# %% [markdown]
"""
### Using Ontologie (Level  3 of created ontologie)
"""
#%%
neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]
appendedData = []

for testedCategorie in allQtlPhysicalyMappedDf['Trait Category Level 3'].unique(
):
  appendedData.append(
      compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                        groupedAllQtlPhysicalyMappedDf,
                                        neededColumns, 'Trait Category Level 3',
                                        testedCategorie))
resultDataFrameTraitCategoryLevel3Effect = pd.concat(appendedData, axis=0)

# Initalize a selector to select origin
select = widgets.Select(
    options=np.append(
        sorted(allQtlPhysicalyMappedDf['Trait Category Level 3'].unique()),
        r'(.*?)'),
    value=r'(.*?)',
    description='Categorie:',
    disabled=False)

# Define interactive widget to interact with result table
interact(
    interactive_exploration,
    resultDataFrame=fixed(resultDataFrameTraitCategoryLevel3Effect),
    categorie=select,
    significative=checkBox,
    column='Threshold')

#%%
# If asked, save result data frame as a CSV file
if outputAsFile:
  for resultName in [
      'resultDataFrameTraitCategoryLevel3Effect',
      'resultDataFrameTraitCategoryLevel2Effect',
      'resultDataFrameStatsR2Effect',
      'resultDataFrameCategoryEffect', 'resultDataFrameStatsVanilla',
      'resultDataFrameOriginEffect'
  ]:
    resultFile = eval(resultName)
    resultName = resultName.replace('resultDataFrame', 'outputPath')
    output_dataframe_in_csv(conf['Statistics'][resultName], resultFile, True)

# %% [markdown]
"""
### Analysing results
"""
#%%
files = [
    conf['Statistics']['outputPathStatsVanilla'],
    conf['Statistics']['outputPathStatsR2Effect'],
    conf['Statistics']['outputPathOriginEffect'],
    conf['Statistics']['outputPathCategoryEffect'],
    conf['Statistics']['outputPathTraitCategoryLevel2Effect'],
    conf['Statistics']['outputPathTraitCategoryLevel3Effect']
]

allResults = get_all_results(files)
allResults = allResults.loc[allResults['Total_count'] >= 5]
allResults = allResults.drop_duplicates()
allResults = allResults.sort_values(by=['chromosome'])

# Initalize a selctor to select origin
test_statistics = widgets.Select(
    options=np.append(allResults['test_statistics'].unique(), r'(.*?)'),
    value=r'(.*?)',
    description='Used Test:')
Sort_on = widgets.Select(
    options=np.append(allResults['Sort_on'].unique(), r'(.*?)'),
    value=r'(.*?)',
    description='Sort_on:')
chromosomePair = widgets.Select(
    options=np.append(allResults['chromosome'].unique(), r'(.*?)'),
    value=r'(.*?)',
    description='chromosomePair:')

# Define interactive widget to interact with result table
interact(
    interactive_results,
    resultDataFrame=fixed(allResults),
    test_statistics=test_statistics,
    Sort_on=Sort_on,
    chromosomePair=chromosomePair,
    significative=checkBox)

output_dataframe_in_csv(conf['Statistics']['outputPathAllSignificative'], allResults, False)
# %% [markdown]
"""
## Visualizations

To finish, a visualisation of every QTLs on each chromosome is generated.
"""

# %%
##############################################################################
# Construct visualizations
##############################################################################

colOrder = [3, 11, 5, 10, 9, 17, 13, 16, 4, 12, 1, 2, 6, 7, 8, 14, 15]

##############################################################################
# Construct visualizations using Seaborn
##############################################################################
generate_visualizations(allQtlPhysicalyMappedDf, colOrder, conf['Figure']['outputPathStripplotWithPositions'])

##############################################################################
# Construct visualizations using Altair
##############################################################################

alt.renderers.enable('default', inline=True)
# Make a deep copy to modify it freely
qtlDf = allQtlPhysicalyMappedDf.copy(deep=True)
# Generate graphs
stripPlot = generate_stripplot_with_altair(qtlDf, colOrder)
stackedBarChart = generate_stacked_bar_chart(qtlDf, colOrder)
nonAssignedStackedBarChart = generate_stacked_bar_chart(nonAssignedToSyntenicBlock, colOrder)

# Making a parsing test type
allResultsBinom, significative_binom = reorder_chromosome_pair_for_visualisation(allResults, 'binomial_test')
allResultsMcNemar, significative_mcnemar = reorder_chromosome_pair_for_visualisation(allResults, 'mcnemar_test')
allResultsProportions_ztest, significative_ztest = reorder_chromosome_pair_for_visualisation(allResults, 'proportions_ztest')
# Making a heatmap test type
allResultsPlotsHeatmapProportionsZtest = heatmapOfResults(allResultsProportions_ztest)
allResultsPlotsHeatmapBinom = heatmapOfResults(allResultsBinom)
allResultsPlotsHeatmapMcNemar = heatmapOfResults(allResultsMcNemar)

allResultsPlots = summary_of_results(allResultsProportions_ztest)
allSignificativeResultsPlots = summary_of_results(significative_ztest)


for plot in [stripPlot, stackedBarChart, allSignificativeResultsPlots, allResultsPlotsHeatmapProportionsZtest, nonAssignedStackedBarChart]:
  altair_viewer.display(plot, inline=True)

# Save results in files
for extension in ['.png', '.svg']:
  save(stripPlot, conf['Figure']['outputPathStripplotAltair'] + extension)
  save(stackedBarChart, conf['Figure']['outputPathStackedBarChart'] + extension)
  save(allResultsPlotsHeatmapProportionsZtest, conf['Figure']['outputPathAllResultsHeatmapZtest'] + extension)
  save(allResultsPlots, conf['Figure']['outputPathAllResults'] + extension)
  save(allSignificativeResultsPlots, conf['Figure']['outputPathAllSignificativeResults'] + extension)
  save(allResultsPlotsHeatmapBinom, conf['Figure']['outputPathAllResultsHeatmapBinom'] + extension)
  save(allResultsPlotsHeatmapMcNemar, conf['Figure']['outputPathAllResultsHeatmapMcNemar'] + extension)
  save(nonAssignedStackedBarChart, conf['Figure']['outputPathNonAssignedStackedBarChart'] + extension)
# %%
