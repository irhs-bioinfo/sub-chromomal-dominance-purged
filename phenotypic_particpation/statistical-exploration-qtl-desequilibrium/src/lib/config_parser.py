import yaml


def config_parser(configPath):
  with open(configPath) as configFile:
    configObject = yaml.safe_load(configFile)
  return configObject
