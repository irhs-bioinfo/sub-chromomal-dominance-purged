
import numpy as np
from scipy import stats
from statsmodels.stats.contingency_tables import mcnemar
from statsmodels.stats.proportion import (proportions_chisquare,
                                          proportions_ztest)

from .dichotomic_table import (check_for_nan, generate_dichotomic_table,
                               generate_sucesses_table)

def compute_verbose_binomial_test(chromosomePairs, qtlDataset, verbose,
                                  neededColumns):
  """
  A function to compute a binomial test and output in terminal results values

  Parameters
  ----------
  chromosomePairs : array
    Stores the numbers of the chromosomes on which the test is performed.
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  verbose : bool
    Indicate if statistical values should be ouputed in terminal
  neededColumns : array
    Label of columns wanted in dichotomic table
  """
  for chromosomePair in chromosomePairs:
    # Generate a table containing two columns gathering the labels (or another
    # column) of the QTL present on the chromosomes whose number is given in
    # argument
    dichotomicTable = generate_dichotomic_table(chromosomePair, neededColumns,
                                                qtlDataset)
    # Calculate associated p-value using a binomial test
    # H_0: QTL are equaly repartited on each chromosome ( 50% of QTL on each chromosome)
    # H_1: QTL are preferentially repartited on current chromosome (more than
    # 50%)
    pValueBinomial, numberOfSuccesses, totalSize = execute_binomial_test(
        dichotomicTable, chromosomePair)
    # If verbose is asked, display it in terminal
    if verbose:
      print('Current tested pair: ' + str(chromosomePair))
      print('Binomial test')
      print('Table: ' + str(numberOfSuccesses.iloc[0:2]))
      print('Nobs: ' + str(totalSize))
      print('p-val: ' + str(pValueBinomial))
      print('\n')


def compute_verbose_mcnemar_test(chromosomePairs, qtlDataset, verbose,
                                 neededColumns, significative):
  """
  A function to compute a mc Nemar test and output in terminal results values

  Parameters
  ----------
  chromosomePairs : array
    Stores the numbers of the chromosomes on which the test is performed.
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  verbose : bool
    Indicate if statistical values should be ouputed in terminal
  neededColumns : array
    Label of columns wanted in dichotomic table
  """
  for chromosomePair in chromosomePairs:
    # Generate a table containing two columns gathering the labels (or another
    # column) of the QTL present on the chromosomes whose number is given in
    # argument
    dichotomicTable = generate_dichotomic_table(chromosomePair, neededColumns,
                                                qtlDataset)
    # McNemar test of homogeneity.
    mcnemarResults = execute_mcnemar_test(dichotomicTable, chromosomePair)
    # If verbose is asked, display it in terminal
    if verbose and significative:
      if significative_p_value(mcnemarResults.pvalue, 0.05):
        print('Current tested pair: ' + str(chromosomePair))
        print('McNemar test of homogeneity')
        print(mcnemarResults)
        print('\n')


def compute_verbose_proportions_ztest(chromosomePairs, qtlDataset, verbose,
                                      neededColumns, significative):
  """
  A function to compute a proportions based on normal (z) test and output in terminal results values

  Parameters
  ----------
  chromosomePairs : array
    Stores the numbers of the chromosomes on which the test is performed.
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  verbose : bool
    Indicate if statistical values should be ouputed in terminal
  neededColumns : array
    Label of columns wanted in dichotomic table
  """
  for chromosomePair in chromosomePairs:
    # Generate a table containing two columns gathering the labels (or another
    # column) of the QTL present on the chromosomes whose number is given in
    # argument
    dichotomicTable = generate_dichotomic_table(chromosomePair, neededColumns,
                                                qtlDataset)
    # Test for proportions based on normal (z) test
    stat, pval = execute_proportions_ztest(dichotomicTable, chromosomePair)
    # If verbose is asked, display it in terminal
    if verbose and significative:
      if significative_p_value(pval, 0.05):
        print('Current tested pair: ' + str(chromosomePair))
        print('Test for proportions based on normal (z) test')
        print('stat: ' + str(stat))
        print('pval: ' + str(pval))
        print('\n')


def compute_verbose_proportions_chisquare(chromosomePairs, qtlDataset, verbose,
                                          neededColumns, significative):
  """
  A function to compute a proportions based on chisquare test and output in terminal results values

  Parameters
  ----------
  chromosomePairs : array
    Stores the numbers of the chromosomes on which the test is performed.
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  verbose : bool
    Indicate if statistical values should be ouputed in terminal
  neededColumns : array
    Label of columns wanted in dichotomic table
  """
  for chromosomePair in chromosomePairs:
    # Generate a table containing two columns gathering the labels (or another
    # column) of the QTL present on the chromosomes whose number is given in
    # argument
    dichotomicTable = generate_dichotomic_table(chromosomePair, neededColumns,
                                                qtlDataset)
    # Test for proportions based on chisquare test
    stat, pval, table = execute_proportions_chisquare(dichotomicTable,
                                                      chromosomePair)
    # If verbose is asked, display it in terminal
    if verbose and significative:
      if significative_p_value(pval, 0.05):
        print('Current tested pair: ' + str(chromosomePair))
        print('test for proportions based on chisquare test')
        print('table: ' + str(table))
        print('chi2stat: ' + str(stat))
        print('pval: ' + str(pval))
        print('\n')