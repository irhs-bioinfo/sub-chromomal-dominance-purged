import numpy as np
import pandas as pd

from .compute_all_tests import summary_of_needed_column, orientation_of_imbalance
from .dichotomic_table import (generate_dichotomic_table,
                               sort_and_cut_dichotomic_table,
                               generate_sucesses_table)
from .statistical_functions import (execute_binomial_test, execute_mcnemar_test,
                                    execute_proportions_chisquare,
                                    execute_proportions_ztest)

###############################################################################
# Test Wrapper
###############################################################################


def compute_all_tests(chromosomePairs, qtlDataset, neededColumns, sortingColumn,
                      threshold):
  """
  [summary]

  [extended_summary]

  Parameters
  ----------

  chromosomePairs : array
    Stores the numbers of the chromosomes on which the test is performed.
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  neededColumns : array
    Label of columns wanted in dichotomic table
  sortingColumn : string
    Name of column used to sort data frame
  threshold : float , string
    threshold used to slice Data Frame


  Returns
  -------
  pandas.core.frame.DataFrame
      A data Frame storing all results and metadata associated to statistical tests
  """
  resultDataFrame = pd.DataFrame(columns=[
      'chromosome', 'statistical_value', 'p_value', 'test_statistics', 'origin',
      'Sort_on', 'Threshold', 'Count_first_chromosome',
      'Count_second_chromosome', 'Total_count'
  ])
  for chromosomePair in chromosomePairs:
    # Generate a table containing two columns gathering the labels (or another
    # column) of the QTL present on the chromosomes whose number is given in
    # argument
    dichotomicTable = generate_dichotomic_table(chromosomePair, neededColumns,
                                                qtlDataset)
    if sortingColumn != '':
      if isinstance(threshold, float):
        testedParameters = {
            'Sort_on': sortingColumn,
            'Threshold': round(threshold, 2)
        }
      else:
        testedParameters = {'Sort_on': sortingColumn, 'Threshold': threshold}
      dichotomicTable = sort_and_cut_dichotomic_table(dichotomicTable,
                                                      sortingColumn, threshold)
    else:
      testedParameters = {'Sort_on': 'No sort', 'Threshold': 'No Row dropping'}
    if not dichotomicTable.empty:
      summaryOfNeededColumns = summary_of_needed_column(neededColumns,
                                                        dichotomicTable)
      numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
      # Computation of statistical tests
      statisticalValue, pValue, table = execute_proportions_chisquare(
          dichotomicTable, chromosomePair)
      resultDataFrame = save_in_data_frame(chromosomePair, statisticalValue,
                                           pValue, 'proportions_chisquare',
                                           summaryOfNeededColumns['Origin'],
                                           resultDataFrame, testedParameters,
                                           numberOfSuccesses, totalSize)
      statisticalValue, pValue = execute_proportions_ztest(
          dichotomicTable, chromosomePair)
      resultDataFrame = save_in_data_frame(chromosomePair, statisticalValue,
                                           pValue, 'proportions_ztest',
                                           summaryOfNeededColumns['Origin'],
                                           resultDataFrame, testedParameters,
                                           numberOfSuccesses, totalSize)
      mcnemarResults = execute_mcnemar_test(dichotomicTable, chromosomePair)
      resultDataFrame = save_in_data_frame(
          chromosomePair, mcnemarResults.statistic, mcnemarResults.pvalue,
          'mcnemar_test', summaryOfNeededColumns['Origin'], resultDataFrame,
          testedParameters, numberOfSuccesses, totalSize)
      pValue, numberOfSuccesses, totalSize = execute_binomial_test(
          dichotomicTable, chromosomePair)
      # Duplicate line because there is two ouput p-value
      for chromosome in chromosomePair:
        resultDataFrame = save_in_data_frame(chromosomePair, np.nan,
                                             pValue['chr_' + str(chromosome)],
                                             'binomial_test',
                                             summaryOfNeededColumns['Origin'],
                                             resultDataFrame, testedParameters,
                                             numberOfSuccesses, totalSize)
    else:
      numberOfSuccesses = {
          'chromosome_' + str(chromosomePair[0]): 0,
          'chromosome_' + str(chromosomePair[1]): 0
      }
      totalSize = 0
      resultDataFrame = save_in_data_frame(chromosomePair, np.nan, np.nan,
                                           np.nan, np.nan, resultDataFrame,
                                           testedParameters, numberOfSuccesses,
                                           totalSize)
  return resultDataFrame


def save_in_data_frame(syntenicPair, statisticalValue, pValue, testStatistics,
                       origin, resultDataFrame, testedParameters,
                       numberOfSuccesses, totalCount):
  """
  [summary]

  [extended_summary]

  Parameters
  ----------
  syntenicPair : array
      Pair of chromosome studied
  statisticalValue : float
      statistic of test
  pValue : float
      Associated p-value of statistical test
  testStatistics : string
      Which test was applied
  origin : string
      A string from 'Origin' column from QTL dataset
      indicating from which origin each QTL comes from
  resultDataFrame : pandas.core.frame.DataFrame
      A data Frame storing all results and metadata associated to statistical tests
  testedParameters : dictionnary
    Sum up tested parameter
  numberOfSuccesses: dictionnary
    Dictionnary storing number of QTLs on each chromosome
  totalCount : Integer
    Total number of QTLs considered
  Returns
  -------
  pandas.core.frame.DataFrame
      A data Frame storing all results and metadata associated to statistical tests
  """
  orientationOfImbalance = orientation_of_imbalance(
      numberOfSuccesses['chromosome_' + str(syntenicPair[0])],
      numberOfSuccesses['chromosome_' + str(syntenicPair[1])])
  return resultDataFrame.append(
      {
          'chromosome':
              syntenicPair,
          'statistical_value':
              statisticalValue,
          'p_value':
              pValue,
          'test_statistics':
              testStatistics,
          'origin':
              origin,
          'Sort_on':
              testedParameters['Sort_on'],
          'Threshold':
              testedParameters['Threshold'],
          'Count_first_chromosome':
              numberOfSuccesses['chromosome_' + str(syntenicPair[0])],
          'Count_second_chromosome':
              numberOfSuccesses['chromosome_' + str(syntenicPair[1])],
          'Total_count':
              totalCount,
          'Orientation_of_imbalance':
              orientationOfImbalance
      },
      ignore_index=True)