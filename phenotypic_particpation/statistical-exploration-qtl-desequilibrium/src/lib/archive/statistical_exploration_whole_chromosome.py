# %% [markdown]
"""This tool aims to study the QTL disequilibrium within the duplicated apple genome.


"""
# %%
##############################################################################
# Import needed modules and packages
##############################################################################

import ipywidgets as widgets
import numpy as np
import pandas as pd
from IPython.display import display
from ipywidgets import interact

from lib.config_parser import config_parser
from lib.misc import (delete_temporary_files, description_of_datasets,
                      output_dataframe_in_csv)
from lib.statistical_functions import (
    compute_verbose_binomial_test, compute_verbose_mcnemar_test,
    compute_verbose_proportions_chisquare, compute_verbose_proportions_ztest)
from lib.visualization import generate_visualizations
from lib.whole_chromosomes import compute_all_tests
import os

# %%
# Load configuration
conf = config_parser('./conf/config_statistical_exploration.yaml')
##############################################################################
# Load datasets and initialize some variables
##############################################################################
# Read input files and put it in pandas Data Frame
# QTL related files
allQtlPhysicalyMappedDf = pd.read_csv(conf['QTL']['AllQtlPhysicalyMappedDf'], compression='infer')
allQtlDf = pd.read_csv(conf['QTL']['AllQtlDf'], compression='infer')
# Markers and physical location related files
chromosomeData = pd.read_csv(conf['SynthenicBlocks']['chromosomeData'])
mappedMarkers = pd.read_csv(conf['Markers']['mappedMarkers'], compression='infer')
# Synthenic blocks
syntenicBlocksDf = pd.read_csv(conf['SynthenicBlocks']['synthenicBlocks'])
# Variable initialization
verbose = conf['Variable']['verbose']
outputAsFile = conf['Variable']['outputAsFile']

# Result outputed files
# Tables
outputPathAllQtlPhysicalyMappedDf = conf['Statistics'][
    'outputPathAllQtlPhysicalyMappedDf']
outputPathAllQtlDf = conf['Statistics']['outputPathAllQtlDf']
outputPathStatsVanilla = conf['Statistics']['outputPathStatsVanilla']
outputPathSatsR2Effect = conf['Statistics']['outputPathSatsR2Effect']
outputPathAllOriginEffect = conf['Statistics']['outputPathAllOriginEffect']
outputPathCategoryEffect = conf['Statistics']['outputPathCategoryEffect']
outputPathTraitCategoryLevel2Effect = conf['Statistics'][
    'outputPathTraitCategoryLevel2Effect']
outputPathTraitCategoryLevel3Effect = conf['Statistics'][
    'outputPathTraitCategoryLevel3Effect']
# Figure
outputPathStripplotWithLimits = conf['Figure']['outputPathStripplotWithLimits']
outputPathStripplotWithPositions = conf['Figure'][
    'outputPathStripplotWithPositions']

# Recreate file architecture if needed
for name in conf['Figure']:
  os.makedirs(os.path.dirname(conf['Figure'][name]), exist_ok=True)

descriptionOfDatasetsAllQtlPhysicalyMappedDf = description_of_datasets(
    allQtlPhysicalyMappedDf)
descriptionOfDatasetsAllQtlDf = description_of_datasets(allQtlDf)

display(descriptionOfDatasetsAllQtlPhysicalyMappedDf)

# %% [markdown]
"""
# Statistical exploration
## Some documentation:
https://stats.stackexchange.com/questions/76875/what-is-the-difference-between-mcnemars-test-and-the-chi-squared-test-and-how
https://stats.stackexchange.com/questions/158459/test-of-proportions-or-chi-squared
https://stats.stackexchange.com/questions/78459/difference-estimate-confidence-intervals-for-chi2-test-between-2-proportio
https://stats.stackexchange.com/questions/173415/at-what-level-is-a-chi2-test-mathematically-identical-to-a-z-test-of-propo
https://stats.stackexchange.com/questions/329465/assessing-a-b-test-results-using-python
## Implemented tests:

We seek to show that for a pair of ohnological chromosomes, the distribution of QTLs is not homogeneous between them.
To do this, a set of QTLs were recovered. Statistical tests are then set up to find out whether or not the QTLs are equally distributed between the chromosomes.
Thus, the following tests are applied:
- Binomial test
- Mc Nemar test
- Proportions ztest
- proportions_chisquare

Details fo those tests are presented in sections below.

### Binomial test

  The binomial test is an exact test, trying to test the statistical significance of deviations from a theoretically expected distribution of observations into two categories.
  Statistical value in two sided way, is calculated using following formula:

  $\sum_{i=0}^{c_1} B(i|p_0,n) \leq \alpha/2$

  In this study, we consider a distribution of 50% on each chromosomes. This implies as hypothesis:

  - H_0: QTL are equaly repartited on each chromosome ( 50% of QTL on each chromosome)
  - H_1: QTL are preferentially repartited on considered chromosome (more than 50% of QTLs on this chromosome)

### Mc Nemar test

  In this test only the discordant values are taken into account, i.e. those that have a different value between the two chromosomes.
  Let A be the proportion of values having had the value X at the first chromosome and the value Y at the second chromosome and B the proportion of values having had the value Y at the first chromosome and the value X at the second chromosome.
  The test statistic noted K is therefore:

   $K={\frac {(A-B)^{2}}{A+B}}$.

  This statistic is then compared to the threshold value in the Chi-square law table with a degree of freedom of 1.

  This test can be used only if data are paired (like Before/After treatment)

### Proportions ztest

  Test for proportions based on normal (z) test.
  This tests can test for a difference in proportions. A two proportion z-test allows you to compare two proportions to see if they are the same. This implies as hypothesis:

  - The null hypothesis (H0) for the test is that the proportions are the same.
  - The alternate hypothesis (H1) is that the proportions are not the same.

  The test statistic is calculated using following formula:

  $z = \frac{p_A-p_B}{\sqrt{pq/n_A+pq/n_B}}$

#### Conditions of test

  - Your sample size is greater than 30. Otherwise, use a t test.
  - Data points should be independent from each other. In other words, one data point isn’t related or doesn’t affect another data point.
  - Your data should be normally distributed. However, for large sample sizes (over 30)this doesn’t always matter.
  - Your data should be randomly selected from a population, where each item has an equal chance of being selected.
  - Sample sizes should be equal if at all possible.

### proportions_chisquare

  Identical to a z-test. In fact, Z squared is the chi-square statistic.
  In fact if we take a 2x2 frequency table where columns are the two onhologous chromosome and the rows are "presence of QTL" and "absence of QTL".
  Expected frequencies of the chi-square test in a given column is the weighted (by the groups' N) average column (group) profile multiplied by that group's N.
  Thus, chi-square test is the deviation of each of the two groups profiles from this average group profile. This test is equivalent to test the groups profiles difference from each other which correspond to a z-test of proportions.


## Compute all test with all verbose for following paires of ohnologous chromosomes:

  | Chromosome 1 | Chromosome 2 |
  |--------------|--------------|
  |       3      |      11      |
  |       5      |      10      |
  |       9      |      17      |
  |      13      |      16      |

Here is all statistics of significative test (alpha risk fixed at 5%):
"""
# %%
##############################################################################
# Calculate associated statistics
##############################################################################
chromosomePairs = [[3, 11], [5, 10], [9, 17], [13, 16]]
neededColumns = ['Label', 'Origin']
if verbose:
  compute_verbose_binomial_test(chromosomePairs, allQtlPhysicalyMappedDf, True,
                                neededColumns)
  compute_verbose_mcnemar_test(chromosomePairs, allQtlPhysicalyMappedDf, True,
                               neededColumns, True)
  compute_verbose_proportions_ztest(chromosomePairs, allQtlPhysicalyMappedDf,
                                    True, neededColumns, True)
  compute_verbose_proportions_chisquare(chromosomePairs,
                                        allQtlPhysicalyMappedDf, True,
                                        neededColumns, True)
# %% [markdown]
"""
First of all, the tests are carried out on the 4 most important pairs of ohnologous chromosomes without any preconceived ideas about the importance of the data. We are not going to make any sort or threshold on the data.
Those results can be summarized in a table:
"""
# %%
neededColumns = ['Label', 'Origin']
# Compute all test and ghet results in a data Frame
resultDataFrame = compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf,
                                    neededColumns, '', 0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathStatsVanilla, resultDataFrame, verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_exploration_vanilla_stats(significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame.loc[(resultDataFrame['p_value'] <=
                              significativeThreshold)]


# %% [markdown]
"""
## Exploration on effect of R2 on statistical validation.

Then we'll look at the effect of R2 on QTL imbalance. We will therefore apply a threshold to the data with a value of R2
"""
# %%
neededColumns = ['Label', 'Origin', 'R2']
testedPercentages = np.arange(0.0, 1.0, 0.1)
appendedData = []
for testedPercentage in testedPercentages:
  appendedData.append(
      compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf, neededColumns,
                        'R2', testedPercentage))
resultDataFrame = pd.concat(appendedData, axis=0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathSatsR2Effect, resultDataFrame, verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_exploration_R2(column=['Threshold'],
                               threshold=(0, 1, 0.1),
                               significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame.loc[
      (resultDataFrame[column] >= threshold)
      & (resultDataFrame['p_value'] <= significativeThreshold)]


# %% [markdown]
"""
## Exploration on effect of origin on statistical validation.

Then we'll look at the effect of the origin of QTLS on the imbalance of QTLs. We will then select the data on their origin (biparental, GWAs or both).
"""

# %%
neededColumns = ['Label', 'Origin', 'R2']
testedOrigins = ['Biparental', 'GWAS', 0]
appendedData = []
for testedOrigin in testedOrigins:
  if testedOrigin == 0:
    appendedData.append(
        compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf,
                          neededColumns, '', 0))
  else:
    appendedData.append(
        compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf,
                          neededColumns, 'Origin', testedOrigin))
resultDataFrame = pd.concat(appendedData, axis=0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathAllOriginEffect, resultDataFrame, verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)
# Initalize a selctor to select origin
select = widgets.Select(
    options=['Biparental', 'GWAS', 'Biparental-GWAS'],
    value='Biparental',
    description='Origin:',
    disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_origin_exploration(origin=select, significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame[(resultDataFrame['origin'].str.match(origin))
                         &
                         (resultDataFrame['p_value'] <= significativeThreshold)]


# %% [markdown]
"""
## Exploration on effect of category on statistical validation.

Then we will look at the effect of the category to which the QTLSs belong on the imbalance of the QTLs. We will therefore select the QTLs by category and apply the set of tests

### Using GDR categories (Top level of created ontologie)
"""
# %%

neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]
testedCategories = allQtlPhysicalyMappedDf['Categorie'].unique()
appendedData = []

for testedCategorie in testedCategories:
  appendedData.append(
      compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf, neededColumns,
                        'Categorie', testedCategorie))
resultDataFrame = pd.concat(appendedData, axis=0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathCategoryEffect, resultDataFrame, verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)
# Initalize a selctor to select origin
select = widgets.Select(
    options=sorted(testedCategories),
    value='Flavanols',
    description='Categorie:',
    disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_categorie_level_1(categorie=select, significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame[(resultDataFrame['Threshold'].str.match(categorie))
                         &
                         (resultDataFrame['p_value'] <= significativeThreshold)]


# %% [markdown]
"""
### Using Ontologie (Level  2 of created ontologie)
"""
#%%
neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]
testedCategories = allQtlPhysicalyMappedDf['Trait Category Level 2'].unique()
appendedData = []

for testedCategorie in testedCategories:
  appendedData.append(
      compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf, neededColumns,
                        'Trait Category Level 2', testedCategorie))
resultDataFrame = pd.concat(appendedData, axis=0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathTraitCategoryLevel2Effect, resultDataFrame,
                          verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)
# Initalize a selctor to select origin
select = widgets.Select(
    options=sorted(testedCategories),
    value='Sugar',
    description='Categorie:',
    disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_categorie_level_2(categorie=select, significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame[(resultDataFrame['Threshold'].str.match(categorie))
                         &
                         (resultDataFrame['p_value'] <= significativeThreshold)]


# %% [markdown]
"""
### Using Ontologie (Level  3 of created ontologie)
"""
#%%
neededColumns = [
    'Label', 'Origin', 'Categorie', 'Trait Category Level 2',
    'Trait Category Level 3'
]
testedCategories = allQtlPhysicalyMappedDf['Trait Category Level 3'].unique()
appendedData = []

for testedCategorie in testedCategories:
  appendedData.append(
      compute_all_tests(chromosomePairs, allQtlPhysicalyMappedDf, neededColumns,
                        'Trait Category Level 3', testedCategorie))
resultDataFrame = pd.concat(appendedData, axis=0)
# If asked, save result data frame as a CSV file
if outputAsFile:
  output_dataframe_in_csv(outputPathTraitCategoryLevel3Effect, resultDataFrame,
                          verbose)

# Initialize a check box
checkBox = widgets.Checkbox(
    value=True, description='Significative at 5%', disabled=False)
# Initalize a selctor to select origin
select = widgets.Select(
    options=sorted(testedCategories),
    value='Biochemical trait',
    description='Categorie:',
    disabled=False)


# Define interactive widget to interact with result table
@interact
def interactive_categorie_level_3(categorie=select, significative=checkBox):
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame[(resultDataFrame['Threshold'].str.match(categorie))
                         &
                         (resultDataFrame['p_value'] <= significativeThreshold)]


# %% [markdown]
"""
## Visualizations

To finish, a visualisation of every QTLs on each chromosome is generated.
"""

# %%
##############################################################################
# Construct visualizations
##############################################################################
colOrder = [3, 11, 5, 10, 9, 17, 13, 16, 4, 12, 1, 2, 6, 7, 8, 14, 15]

generate_visualizations(allQtlDf, allQtlPhysicalyMappedDf, colOrder,
                        outputPathStripplotWithLimits,
                        outputPathStripplotWithPositions)
# %%
