"""
Statistical test using syntenic blocks

This module seeks to set up the parsing necessary to perform statistical tests to verify a possible QTL imbalance between the different synenic blocks within the genome.
"""

import numpy as np
import pandas as pd

from .compute_all_tests import save_in_data_frame, summary_of_needed_column
from .dichotomic_table import (generate_dichotomic_table,
                               sort_and_cut_dichotomic_table,
                               generate_sucesses_table,
                               get_coordinates_of_compared_blocks)
from .statistical_functions import (execute_binomial_test, execute_mcnemar_test,
                                    execute_proportions_chisquare,
                                    execute_proportions_ztest)


def count_qtls_on_syntenic_blocks(syntenicBlocksRow, allQtlPhysicalyMappedDf,
                                  chromosome):
  """
  This function tries to count the number of QTLs associated to each of the syntenic blocks.

  To call it:
  syntenicBlocksDf['assignedQtlToA'] = 0
  syntenicBlocksDf['assignedQtlToA'] = syntenicBlocksDf.apply(
      count_qtls_on_syntenic_blocks, axis=1, args=(nonAssignedToSyntenicBlock, 'A'))

  Parameters
  ----------
  syntenicBlocksRow : pandas.Series
      A row of syntenicBlocks Data Frame
      An exempale of available columns:
        chrA,startA,endA,chrB,startB,endB
        Chr00,930092,987973,Chr00,942597,995429
  allQtlPhysicalyMappedDf : pandas.core.frame.DataFrame
      Qtl Dataset with physical locations
  chromosome : char
      Current studied chromosome (A or B)

  Returns
  -------
  pandas.Series
      SyntenicBlock row with a column more assignedQtlToA or assignedQtlToB storing number of QTL associated with this syntenic block
  """

  belongsToChrOfSyntenicBlock = allQtlPhysicalyMappedDf.loc[
      allQtlPhysicalyMappedDf['Chromosome_Number'] == syntenicBlocksRow[
          'chr' + chromosome]]
  numberOfOverllapingQTLWithSyntenicBlock = belongsToChrOfSyntenicBlock.loc[
      (belongsToChrOfSyntenicBlock['Start_trust_interval']
       .between(syntenicBlocksRow['start' +
                                  chromosome]-200000, syntenicBlocksRow['end' +
                                                                 chromosome]+200000)) |
      (belongsToChrOfSyntenicBlock['End_trust_interval']
       .between(syntenicBlocksRow['start' +
                                  chromosome]-200000, syntenicBlocksRow['end' +
                                                                 chromosome]+200000))]
  allQtlPhysicalyMappedDf.drop(
      numberOfOverllapingQTLWithSyntenicBlock.index, inplace=True)
  return syntenicBlocksRow[
      'assignedQtlTo' +
      chromosome] + numberOfOverllapingQTLWithSyntenicBlock.shape[0]


def add_associated_syntenic_block_to_qtl_df(allQtlPhysicalyMappedRow,
                                            syntenicBlocksDf):
  """
  This function aims to add to each mapped QTL associated syntenic block

  Parameters
  ----------
  allQtlPhysicalyMappedRow : pandas.Series
      A row of allQtlPhysicalyMappedDf Data Frame gathering Qtl Dataset with physical locations
  syntenicBlocksDf : pandas.core.frame.DataFrame
      A syntenicBlocks Data Frame
      An exempale of available columns:
        chrA,startA,endA,chrB,startB,endB
        Chr00,930092,987973,Chr00,942597,995429
  Returns
  -------
  pandas.core.frame.DataFrame
      A row of allQtlPhysicalyMappedDf Data Frame with associated syntenic block with the follwoing columns:
      'chrA', 'startA', 'endA', 'chrB', 'startB', 'endB'
  """
  syntenicBlocksDf = syntenicBlocksDf.loc[
      allQtlPhysicalyMappedRow['Chromosome_Number'] == syntenicBlocksDf['chrA']]
  mask = ((allQtlPhysicalyMappedRow['Start_trust_interval'] >
           syntenicBlocksDf['startA']) &
          (allQtlPhysicalyMappedRow['Start_trust_interval'] <
           syntenicBlocksDf['endA']) |
          (allQtlPhysicalyMappedRow['End_trust_interval'] >
           syntenicBlocksDf['startA']) &
          (allQtlPhysicalyMappedRow['Start_trust_interval'] <
           syntenicBlocksDf['endA']))
  belongToSyntenicBlock = syntenicBlocksDf.loc[mask]
  if not belongToSyntenicBlock.empty:
    belongToSyntenicBlock = belongToSyntenicBlock.iloc[0]
    return belongToSyntenicBlock.loc[[
        'chrA', 'startA', 'endA', 'chrB', 'startB', 'endB'
    ]]


def group_by_syntenic_pair(qtlDf):
  """
  This function tries to group the QTL dataset according to the syntenic blocks.

  Parameters
  ----------
  qtlDf : pandas.core.frame.DataFrame
      QTL data frame with associated syntenic blocks

  Returns
  -------
  pandas.DataFrame.groupby
      Grouped dataframe following both syntenic chromosomes
  """
  qtlDfSyntenicBlocks = qtlDf.dropna(subset=['chrA', 'chrB'])
  return qtlDfSyntenicBlocks.groupby(by=['chrA', 'chrB'])


def get_ohnologous_syntenic_groups(groupedQtlDf):
  """
  This function aims to get all existing syntenic pairs

  Parameters
  ----------
  groupedQtlDf : pandas.DataFrame.groupby
      Grouped dataframe following both syntenic chromosomes

  Returns
  -------
  array
      Array of tuple of syntenic pairs
  """
  result = []
  # Get all grouped keys
  groupsNames = groupedQtlDf.groups.keys()
  # Iterate them
  for group1 in groupsNames:
    for group2 in groupsNames:
      # Check if current iteration gather a syntenic block
      if group1[0] == group2[1] and group1[1] == group2[0] and group1 != group2:
        # Store current tuple in an array
        array = [group1, group2]
        inversedArray = [group2, group1]
        # Check if this syntenic blocks is not already stored
        if inversedArray not in result:
          # If not add it in results
          result.append(array)
  return result


def compute_all_tests_syntenic_blocks(listOfSyntenicPairs,
                                      groupedAllQtlPhysicalyMappedDf,
                                      neededColumns, sortingColumn, threshold):
  """
  This function aims to compute all statistical tests of eacch pair of syntenic blocks

  Parameters
  ----------
  listOfSyntenicPairs : Array
      Array of syntenic pairs of chromosomes
  groupedAllQtlPhysicalyMappedDf : pandas.DataFrame.groupby
      [description]
  neededColumns : Array
      Name of needed columns
  sortingColumn : String
      Name of columns used to sort QTL DataFrame
  threshold : Float
      Threshold to filter major or minor QTLs

  Returns
  -------
  pandas.core.frame.DataFrame
      Result DataFrame storing all test results in following columns:
      ['chromosome', 'statistical_value', 'p_value', 'test_statistics', 'origin',
      'Sort_on', 'Threshold', 'Count_first_chromosome',
      'Count_second_chromosome', 'Total_count']
  """

  resultDataFrame = pd.DataFrame(columns=[
      'chromosome', 'statistical_value', 'p_value', 'test_statistics', 'origin',
      'Sort_on', 'Threshold', 'Count_first_chromosome',
      'Count_second_chromosome', 'Total_count', 'Orientation_of_imbalance'
  ])
  for syntenicPair in listOfSyntenicPairs:
    currentQtlDf = pd.concat([
        groupedAllQtlPhysicalyMappedDf.get_group(syntenicPair[0]),
        groupedAllQtlPhysicalyMappedDf.get_group(syntenicPair[1])
    ],
        axis=0)
    syntenicPair = [int(syntenicPair[0][0]), int(syntenicPair[0][1])]
    try:
      # Generate a table containing two columns gathering the labels (or another
      # column) of the QTL present on the chromosomes whose number is given in
      # argument
      dichotomicTable = generate_dichotomic_table(syntenicPair, neededColumns,
                                                  currentQtlDf)
    except ValueError:
      print(str(syntenicPair) + ' failed')
      dichotomicTable = pd.DataFrame()

    if sortingColumn != '':
      if isinstance(threshold, float):
        testedParameters = {
            'Sort_on': sortingColumn,
            'Threshold': round(threshold, 2)
        }
      else:
        testedParameters = {'Sort_on': sortingColumn, 'Threshold': threshold}
      dichotomicTable = sort_and_cut_dichotomic_table(dichotomicTable,
                                                      sortingColumn, threshold)
    else:
      testedParameters = {'Sort_on': 'No sort', 'Threshold': 'No filters applied'}
    if not dichotomicTable.empty:
      summaryOfNeededColumns = summary_of_needed_column(neededColumns,
                                                        dichotomicTable)
      numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
      blocksCoordinates = get_coordinates_of_compared_blocks(currentQtlDf)
      # Computation of statistical tests
      statisticalValue, pValue, table = execute_proportions_chisquare(
          dichotomicTable, syntenicPair)
      resultDataFrame = save_in_data_frame(syntenicPair, statisticalValue,
                                           pValue, 'proportions_chisquare',
                                           summaryOfNeededColumns['Origin'],
                                           resultDataFrame, testedParameters,
                                           numberOfSuccesses, totalSize,
                                           blocksCoordinates)
      statisticalValue, pValue = execute_proportions_ztest(
          dichotomicTable, syntenicPair)
      resultDataFrame = save_in_data_frame(syntenicPair, statisticalValue,
                                           pValue, 'proportions_ztest',
                                           summaryOfNeededColumns['Origin'],
                                           resultDataFrame, testedParameters,
                                           numberOfSuccesses, totalSize,
                                           blocksCoordinates)
      mcnemarResults = execute_mcnemar_test(dichotomicTable, syntenicPair)
      resultDataFrame = save_in_data_frame(
          syntenicPair, mcnemarResults.statistic, mcnemarResults.pvalue,
          'mcnemar_test', summaryOfNeededColumns['Origin'], resultDataFrame,
          testedParameters, numberOfSuccesses, totalSize, blocksCoordinates)
      pValue, numberOfSuccesses, totalSize = execute_binomial_test(
          dichotomicTable, syntenicPair)
      # Duplicate line because there is two ouput p-value
      for chromosome in syntenicPair:
        resultDataFrame = save_in_data_frame(
            syntenicPair, np.nan, pValue['chr_' + str(chromosome)],
            'binomial_test', summaryOfNeededColumns['Origin'], resultDataFrame,
            testedParameters, numberOfSuccesses, totalSize, blocksCoordinates)
    else:
      numberOfSuccesses = {
          'chromosome_' + str(syntenicPair[0]): 0,
          'chromosome_' + str(syntenicPair[1]): 0
      }
      totalSize = 0
      resultDataFrame = save_in_data_frame(syntenicPair, np.nan, np.nan, np.nan,
                                           np.nan, resultDataFrame,
                                           testedParameters, numberOfSuccesses,
                                           totalSize, 0)
  return resultDataFrame


def get_all_results(files):
  """
  [summary]

  [extended_summary]

  Parameters
  ----------
  files : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  allSignificativeResults = pd.DataFrame()

  for file in files:
    fileDf = pd.read_csv(file)
    allSignificativeResults = pd.concat([allSignificativeResults, fileDf])
  return (allSignificativeResults)


def size_of_syntenic_blocks(syntenicBlocksRow):
  return syntenicBlocksRow['endA'] - syntenicBlocksRow['startA']
