"""
This module makes it possible to manage the whole statistical aspect of the project.

The following statistical tests have been implemented:
  - Binomial test
  - Mc Nemar test
  - Proportions ztest
  - Proportions chisquare


"""
import numpy as np
from scipy import stats
from statsmodels.stats.contingency_tables import mcnemar
from statsmodels.stats.proportion import (proportions_chisquare,
                                          proportions_ztest)

from .dichotomic_table import (check_for_nan,
                               generate_sucesses_table)

###############################################################################
# Statistical tests
###############################################################################


def execute_binomial_test(dichotomicTable, chromosomeNumbers):
  """
  This function aims to carry out a binomial test.

  This function aims to carry out a binomial test between the number of QTLs present on the
  chromosome under consideration and an equivalent distribution of total QTLs between
  the two ohnologous chromosomes

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  dictionnary
    An array containing associated p-value from binomial test for current pair of chromosome
  """
  pValue = {}
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  if check_for_nan(numberOfSuccesses, totalSize, chromosomeNumbers):
    for chromosomeNumber in chromosomeNumbers:
      pValue['chr_' + str(chromosomeNumber)] = stats.binom_test(
          numberOfSuccesses['chromosome_' + str(chromosomeNumber)],
          totalSize,
          0.5,
          alternative='greater')
  return pValue, numberOfSuccesses, totalSize


def execute_mcnemar_test(dichotomicTable, chromosomeNumbers):
  """
  This function aims to carry out a mc Nemar test of homology.

  This function aims to carry out a mc Nemar test of homology on QTL
  repartition between two ohnologous chromosomes. This test is runned on chi-square distribution (exact=False) with a continuity correction (correction=True)
  For example for pair [1,7]:
  30 QTLs on chr1
  7 QTLs on chr7
  Following contingency table is constructed:
  |                   |                 | First Chromosome |                |
  |-------------------|-----------------|------------------|----------------|
  |                   |                 | Presence of QTL  | Absence of QTL |
  | Second Chromosome | Presence of QTL | 37               | 7              |
  |                   | Absence of QTL  | 30               | 0              |
  Mc Nemar test is carried out:
  Méthode : McNemar's Chi-squared test with continuity correction
  Statistique observée Qobs : 13.081081081081
  p-value : 0.00029829324714202

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  A bunch with attributes:
    - statistic: float or int, array
      The test statistic is the chisquare statistic if exact is false. If the exact binomial distribution is used, then this contains the min(n1, n2), where n1, n2 are cases that are zero in one sample but one in the other sample.
    - pvalue: float or array
      p-value of the null hypothesis of equal marginal distributions.
  """
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  firstRow = [
      totalSize, numberOfSuccesses['chromosome_' + str(chromosomeNumbers[1])]
  ]
  secondRow = [numberOfSuccesses['chromosome_' + str(chromosomeNumbers[0])], 0]
  # This test is runned on chi-square distribution (exact=False) with a continuity correction (correction=True)
  return mcnemar(np.array([firstRow, secondRow]), exact=False, correction=True)


def execute_proportions_ztest(dichotomicTable, chromosomeNumbers):
  """
  This function aims to carry out a proportions based on normal (z) test on QTL repartition between two ohnologous chromosomes

  [extended_summary]

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  stat : float
    Result value of test
  pval : float
    p-value associated to test
  """
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  count = np.array([
      numberOfSuccesses['chromosome_' + str(chromosomeNumbers[0])],
      numberOfSuccesses['chromosome_' + str(chromosomeNumbers[1])]
  ])
  nobs = np.array([totalSize, totalSize])
  stat, pval = proportions_ztest(count, nobs)
  return stat, pval


def execute_proportions_chisquare(dichotomicTable, chromosomeNumbers):
  """
  This function aims to carry out a proportions based on chisquare test.

  This function aims to carry out a proportions based on chisquare test on QTL repartition between two ohnologous chromosomes

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  stat : float
    Result value of test
  pval : float
    p-value associated to test
  (table, expected)
    table is a (k, 2) contingency table, expected is the corresponding table of counts that are expected under independence with given margins
  """
  numberOfSuccesses, totalSize = generate_sucesses_table(dichotomicTable)
  count = np.array([
      numberOfSuccesses['chromosome_' + str(chromosomeNumbers[0])],
      numberOfSuccesses['chromosome_' + str(chromosomeNumbers[1])]
  ])
  nobs = np.array([totalSize, totalSize])
  stat, pval, table = proportions_chisquare(count, nobs)
  return stat, pval, table


def significative_p_value(pValue, threshold):
  """
  Return True if p-value can be considered as singificative

  Parameters
  ----------
  pValue : float
      associated p-value
  threshold : float
      Threshold fixed for consider a test as singificative

  Returns
  -------
  Bool
      Return True if p-value can be considered as singificative
  """
  if pValue <= threshold:
    return True
