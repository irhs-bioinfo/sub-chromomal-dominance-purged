
def save_in_data_frame(syntenicPair, statisticalValue, pValue, testStatistics,
                       origin, resultDataFrame, testedParameters,
                       numberOfSuccesses, totalCount, blocksCoordinates):
  """
  Add a new row in result dataframe

  Parameters
  ----------
  syntenicPair : array
      Pair of chromosome studied
  statisticalValue : float
      statistic of test
  pValue : float
      Associated p-value of statistical test
  testStatistics : string
      Which test was applied
  origin : string
      A string from 'Origin' column from QTL dataset
      indicating from which origin each QTL comes from
  resultDataFrame : pandas.core.frame.DataFrame
      A data Frame storing all results and metadata associated to statistical tests
  testedParameters : dictionnary
    Sum up tested parameter
  numberOfSuccesses: dictionnary
    Dictionnary storing number of QTLs on each chromosome
  totalCount : Integer
    Total number of QTLs considered
  blocksCoordinates : dictionnary
        A dioctionnary storing size of compared blocks.
        {'chr1': 12,'chr2': 12,}
  Returns
  -------
  pandas.core.frame.DataFrame
      A data Frame storing all results and metadata associated to statistical tests
  """
  orientationOfImbalance = orientation_of_imbalance(
      numberOfSuccesses['chromosome_' + str(syntenicPair[0])],
      numberOfSuccesses['chromosome_' + str(syntenicPair[1])])
  return resultDataFrame.append(
      {
          'chromosome':
              syntenicPair,
          'statistical_value':
              statisticalValue,
          'p_value':
              pValue,
          'test_statistics':
              testStatistics,
          'origin':
              origin,
          'Sort_on':
              testedParameters['Sort_on'],
          'Threshold':
              testedParameters['Threshold'],
          'Count_first_chromosome':
              numberOfSuccesses['chromosome_' + str(syntenicPair[0])],
          'Count_second_chromosome':
              numberOfSuccesses['chromosome_' + str(syntenicPair[1])],
          'Total_count':
              totalCount,
          'Orientation_of_imbalance':
              orientationOfImbalance,
          'Blocks_coordinates':
              blocksCoordinates
      },
      ignore_index=True)


def summary_of_needed_column(neededColumns, dichotomicTable):
  """
  Generate dictionnary containning all origin in dataframe. Example: {'Origin': 'GWAS-Biparental'}

  Parameters
  ----------
  neededColumns : array
      Array of columns names used in analysis
  dichotomicTable : pandas.core.frame.DataFrame
      a data Frame containing different columns

  Returns
  -------
  dictionnary
      Dictionnary containning all origin in dataframe
  """
  summaryOfNeededColumns = {}
  for neededColumn in neededColumns:
    if neededColumn == 'Origin':
      arrayOfUniqueValues = dichotomicTable[neededColumn].unique()
      summaryOfNeededColumns[neededColumn] = '-'.join(arrayOfUniqueValues)
  return summaryOfNeededColumns


def orientation_of_imbalance(chrA, chrB):
  """
  Return orientation of imbalance as a bool value. 1 if first chromosome of pair has more QTLs than the second one. 0 in other case

  Parameters
  ----------
  chrA : integer
      Number of QTLs on chromosome
  chrB : integer
      Number of QTLs on chromosome

  Returns
  -------
  Bool
      Return orientation of imbalance as a bool value
  """
  if chrA > chrB:
    return 1
  return 0
