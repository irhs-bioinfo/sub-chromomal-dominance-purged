"""This module aims to generate project visualizations and in particular the QTLs with their positions, the category of line to which they are linked, the percentage of variance that they
"""
import numpy as np
import seaborn as sns
import altair as alt
import pandas as pd


def construct_legend(qtlDataset):
  """
  The aim of this function is to build the legend.

  The aim of this function is to build a table gathering the different labels necessary for the legend.
  The associated colors will be added by seaborn in the construction of the legend in the graphs.

  Parameters
  ----------
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  Returns
  -------
  np.array
    A table gathering the different labels needed for the legend. The associated colors will be added by seaborn in the construction of the legend in the graphs.
  """
  legendOrder = np.append(['Categorie'], qtlDataset['Categorie'].unique())
  return np.append(legendOrder, ['R2', 0.0, 25.0, 50.0, 75.0])


def generate_stripplot_with_limits(qtlDataset, colOrder, palette, outputPdf):
  """
  Generates a graph representing the QTLs present between each boundary along the chromosome. Their colour represents the type of trait they are linked to, their size the percentage of variance they explain

  Parameters
  ----------
  qtlDatasetset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  colOrder : array
    Stores the order in which the chromosomes should be represented.
  colorMap : seaborn.palettes._ColorPalette
    Store color Palette for visualizations
  """
  # Generate a legend object
  legendOrder = construct_legend(qtlDataset)
  # Generate a Facet Grid object to store all subplots into one. Each
  # subplot will represent a chromosome
  positionsOfQtlForEachChromosome = sns.FacetGrid(
      qtlDataset,
      col='Chromosome_Number',
      col_wrap=4,
      col_order=colOrder,
      aspect=1.8)
  # Generate for each chromosome a scatterplot. X axis will represent a
  # Label, allowing to avoid overlap. Y-axis
  positionsOfQtlForEachChromosome.map(
      sns.scatterplot,
      'Label',
      'Limit',
      'Categorie',
      marker='|',
      linewidth=2,
      size=qtlDataset['R2'] * 100,
      sizes=(50, 600),
      palette=palette)
  positionsOfQtlForEachChromosome.fig.subplots_adjust(wspace=.08, hspace=.1)

  # Remove x ticks labels because they doesn t represent anything
  positionsOfQtlForEachChromosome.set(xticks=[])
  # Remove x-axis title
  for axes in positionsOfQtlForEachChromosome.axes.flatten():
    axes.set_xlabel('')

  positionsOfQtlForEachChromosome.add_legend(
      frameon=True, label_order=legendOrder)
  positionsOfQtlForEachChromosome.savefig(outputPdf)


def generate_stripplot_with_positions(qtlDataset, colOrder, palette, outputPdf):
  """
  Generates a graph representing the positions of QTLs along the chromosome. Their colour represents the type of trait they are linked to, their size the percentage of variance they explain

  Parameters
  ----------
  qtlDataset : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing
    all the information of each QTL. Globally head of this Data Frame depend of from which
    CSV it comes but he gather at least those informations:
      -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                  'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  colOrder : array
    Stores the order in which the chromosomes should be represented.
  colorMap : seaborn.palettes._ColorPalette
    Store color Palette for visualizations
  """
  # Generate a legend object
  legendOrder = construct_legend(qtlDataset)
  # Generate a Facet Grid object to store all subplots into one. Each
  # subplot will represent a chromosome
  positionsOfQtlForEachChromosome = sns.FacetGrid(
      qtlDataset,
      col='Chromosome_Number',
      col_wrap=4,
      col_order=colOrder,
      aspect=1.8,
      sharey=False)
  positionsOfQtlForEachChromosome.map(
      sns.scatterplot,
      'Label',
      'End_trust_interval',
      'Categorie',
      marker='|',
      linewidth=3,
      size=qtlDataset['R2'] * 100,
      sizes=(50, 600),
      palette=palette)
  positionsOfQtlForEachChromosome.fig.subplots_adjust(wspace=.08, hspace=.1)

  # Remove x ticks labels because they doesn t represent anything
  positionsOfQtlForEachChromosome.set(xticks=[])
  # Remove x-axis title
  for axes in positionsOfQtlForEachChromosome.axes.flatten():
    axes.set_xlabel('')

  positionsOfQtlForEachChromosome.add_legend(
      frameon=True, label_order=legendOrder)
  positionsOfQtlForEachChromosome.savefig(outputPdf)


def generate_visualizations(allQtlPhysicalyMappedDf, colOrder,
                            outputPathStripplotWithPositions):
  """
  The purpose of this function is to configure seaborn and generate the three visualizations.

  Parameters
  ----------
  allQtlPhysicalyMappedDf :  pandas.core.frame.DataFrame
    Contains all QTLs informations for QTLs mapped on GDDH13 1.1 genome
  colOrder : array
      Order to sort graphs in faceted layer
  outputPathStripplotWithPositions : string
      Path to output file
  """
  # Seaborn aesthetic
  sns.set_style('darkgrid')
  # Get all unique categories from both datasets
  unique = allQtlPhysicalyMappedDf['Categorie'].unique()
  # Generate a dictionnary assigning each category to a color
  palette = dict(zip(unique, sns.color_palette('Set1', len(unique))))
  # Generate visualization representing QTL with their physical positions on
  # each chromosomes
  generate_stripplot_with_positions(allQtlPhysicalyMappedDf, colOrder, palette,
                                    outputPathStripplotWithPositions)


def generate_stripplot_with_altair(qtlDf, colOrder):
  """
  This function aims to generate a similar stripplot graph but implemented with the altair library.

  Parameters
  ----------
  qtlDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests
  colOrder : array
      Order to sort graphs in faceted layer

  Returns
  -------
  altair.vegalite.v4.api.LayerChart
      A layered chart composed by 17 charts representing QTLs on each chromosome
  """
  qtlDf['R2_percentage'] = qtlDf['R2'].apply(
      lambda x: [1 + 60 if x < 0.01 else x * 100 + 60])
  qtlDf['End_trust_interval'] = qtlDf['End_trust_interval'] + 1500000
  brush = alt.selection(type='interval')
  return generate_strip_plot_altair(qtlDf, colOrder, brush)


def generate_strip_plot_altair(qtlDf, colOrder, brush):
  """
  This function aims to generate a strip plot representing each QTL on chromosomes. Their height depends on real size. Their opacity depends on R2 and their color depend on category using Altair

  Parameters
  ----------
  qtlDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests
  colOrder : array
      Order to sort graphs in faceted layer
  brush : altair.vegalite.v4.api.Selection
      A handle on an interval selector on x axis
  Returns
  -------
  altair.vegalite.v4.api.Chart
      A strip plot
  """
  return alt.Chart(qtlDf).mark_bar(
      stroke='black', strokeWidth=1, size=2).encode(
          alt.X(
              'Label:N',
              axis=alt.Axis(labels=False),
              scale=alt.Scale(zero=False, clamp=True)),
          alt.Y('Start_trust_interval:Q', title='Position of QTL', axis=alt.Axis(format=".1e")),
          alt.Y2('End_trust_interval:Q'),
          alt.Opacity('R2_percentage:Q'),
          color=('Categorie:N'),
          tooltip=[
              'Label:N', 'R2:Q', 'Start_trust_interval:Q',
              'End_trust_interval:Q'
          ],
          facet=alt.Facet('Chromosome_Number:N', sort=colOrder, title='',
                          columns=4)).properties(
                              width=180,
                              height=180, background='transparent').resolve_scale(y='independent')


def generate_stacked_bar_chart(qtlDf, colOrder):
  """
  This function aims to generate a stacked bar chart representing number of QTL in each categories for each syntenic pairs using Altair

  Parameters
  ----------
  qtlDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests
  colOrder: array
      Order to sort chromosomes representations
  Returns
  -------
  altair.vegalite.v4.api.Chart
      A stacked bar chart
  """
  return alt.Chart(qtlDf).mark_bar(
      size=10, strokeWidth=2, color='black').encode(
          alt.Y('count(Label):Q', title='Count Of QTLs per category'),
          alt.X('Chromosome_Number:N', sort=colOrder),
          alt.Color(
              'Trait Category Level 2:N', scale=alt.Scale(scheme='category20')),
          alt.Order(
              # Sort the segments of the bars by this field
              'Trait Category Level 2:N',
              sort='ascending'),
          tooltip=['Categorie:N', 'count(Label):Q', 'Chromosome_Number:Q']).properties(background='transparent')


def summary_of_results(resultsDf):
  """
  This function is a wrapper aiming to generate a strip plot and a stacked bar chart using Altair

  Parameters
  ----------
  resultsDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests

  Returns
  -------
  altair.vegalite.v4.api.LayerChart
      A layered chart composed by two charts: One strip plot chart and a rule materializing 0.05 threshold on y axis
  """
  # Init a selector of interval on x axis
  brush = alt.selection(type='interval', encodings=['x'])
  # Generate graphs
  stripPlot = generate_strip_plot_result(resultsDf, brush)
  stackedBarChart = generate_stacked_bar_chart_result(resultsDf, brush)
  # Return them vertically layered
  return (stripPlot & stackedBarChart).properties(background='transparent')


def generate_strip_plot_result(resultsDf, brush):
  """
  This function aims to generate a strip plot representing all p-values of statistical tests in each categories using Altair

  Parameters
  ----------
  resultsDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests
  brush : altair.vegalite.v4.api.Selection
      A handle on an interval selector on x axis

  Returns
  -------
  altair.vegalite.v4.api.LayerChart
      A layered chart composed by two charts: One strip plot chart and a rule materializing 0.05 threshold on y axis
  """
  threshold = pd.DataFrame([{'threshold': [0.05]}])

  rule = alt.Chart(threshold).mark_rule(color='red').encode(y='threshold:Q')

  graph = alt.Chart(resultsDf).mark_circle().encode(
      alt.X('chromosome:N'),
      alt.Y('p_value:Q'),
      color=alt.condition(brush, 'Sort_on:N', alt.value('lightgray')),
      tooltip=[
          'chromosome:N', 'p_value:Q', 'Sort_on:N', 'Threshold:N',
          'Count_first_chromosome:Q', 'Count_second_chromosome:Q'
      ]).add_selection(brush)

  return graph + rule


def generate_stacked_bar_chart_result(resultsDf, brush):
  """
  This function aims to generate a stacked bar chart representing number of statistical tests in each categories using Altair

  Parameters
  ----------
  resultsDf : pandas.core.frame.DataFrame
      Contains all QTL mapped and filtered used in statistical tests
  brush : altair.vegalite.v4.api.Selection
      A handle on an interval selector on x axis

  Returns
  -------
  altair.vegalite.v4.api.LayerChart
      A layered chart composed by two charts: One stacked bar chart and associated text
  """
  # Generate a stacked bar chart representing number of statistical tests in each categories
  return alt.Chart(resultsDf).mark_bar().encode(
      alt.Y(
          'Threshold:N',
          sort=alt.EncodingSortField('Sort_on:N', order='ascending')),
      alt.Opacity('Orientation_of_imbalance:Q'),
      color='Sort_on:N',
      x=alt.X('count(p_value):Q', axis=alt.Axis(values=[0, 1, 2, 3], format='.0f')),
      row='chromosome:N',
      tooltip=[
          'count(p_value):Q', 'Sort_on:N', 'Count_first_chromosome:Q',
          'Count_second_chromosome:Q'
      ]).transform_filter(brush).add_selection(
          alt.selection_single()).resolve_scale(y='independent')


def heatmapOfResults(allResultsProportions):
  scale = alt.Scale(
    domain=[0.05],
    range=['#1B9E77', '#D95F02'],
    type='threshold'
  )
  return alt.Chart(allResultsProportions).mark_rect().encode(
    alt.X('chromosome:N', title="Chromosome pair"),
    alt.Y('Threshold:N'),
    # alt.Opacity('Orientation_of_imbalance:N'),
    alt.Color('p_value:Q', scale=scale, title="Associated p-value"),
    tooltip=[
        'p_value:Q', 'Sort_on:N', 'Count_first_chromosome:Q',
        'Count_second_chromosome:Q'
    ]
).properties(background='transparent')
