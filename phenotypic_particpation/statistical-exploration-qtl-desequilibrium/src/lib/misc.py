"""
This module implements different functions whose roles are diverse and varied
"""
import json
import os
import os.path

import pandas as pd


def delete_temporary_files(arrayOfPaths):
  """
  Remove file given in the array

  Parameters
  ----------
  arrayOfPaths : array
      Array of path where to check some files exist
  """
  for path in arrayOfPaths:
    if os.path.isfile(path):
      os.remove(path)


def description_of_datasets(qtlDataset):
  """
  This function aims to get following informations from givven Data frame:
    - Number of QTL associated to SSR
    - Number of QTL associated to SNP
    - Number of non mapped QTLs
    - Number of mapped QTLs

  Parameters
  ----------
  qtlDataset : pandas.core.frame.DataFrame
      A Data frame storing QTL

  Returns
  -------
  pandas.core.frame.DataFrame
      A data frame with following columuns:
      ["SSR", "SNP", "Non_Mapped", "Mapped", "Total"]
  """
  # Init a data frame with 18 columns to store information for the 17 chromosomes + chr00
  descriptionOfDatasets = pd.DataFrame(
      columns=['SSR', 'SNP', 'Non_Mapped', 'Mapped', 'Total'],
      index=list(range(1, 18)))
  descriptionOfDatasets.index.name = 'Chromosome_Number'
  # For each chromosome get following informations: ["SSR", "SNP", "Non_Mapped", "Mapped", "Total"]
  for chromosomeNumber in range(1, 18):
    descriptionOfDatasets.loc[chromosomeNumber, 'SSR'] = qtlDataset[
        (qtlDataset['Marker_Type'] == 'SSR')
        & (qtlDataset['Chromosome_Number'] == chromosomeNumber)].shape[0]
    descriptionOfDatasets.loc[chromosomeNumber, 'SNP'] = qtlDataset[
        (qtlDataset['Marker_Type'] == 'SNP')
        & (qtlDataset['Chromosome_Number'] == chromosomeNumber)].shape[0]
    descriptionOfDatasets.loc[chromosomeNumber, 'Mapped'] = qtlDataset[
        (~qtlDataset['Start_trust_interval'].isnull())
        & (qtlDataset['Chromosome_Number'] == chromosomeNumber)].shape[0]
    descriptionOfDatasets.loc[chromosomeNumber, 'Non_Mapped'] = qtlDataset[
        (qtlDataset['Start_trust_interval'].isnull())
        & (qtlDataset['Chromosome_Number'] == chromosomeNumber)].shape[0]
    descriptionOfDatasets.loc[chromosomeNumber, 'Total'] = qtlDataset[(
        qtlDataset['Chromosome_Number'] == chromosomeNumber)].shape[0]
  descriptionOfDatasets.loc['Total', 'SSR'] = qtlDataset[(
      qtlDataset['Marker_Type'] == 'SSR')].shape[0]
  descriptionOfDatasets.loc['Total', 'SNP'] = qtlDataset[(
      qtlDataset['Marker_Type'] == 'SNP')].shape[0]
  descriptionOfDatasets.loc['Total', 'Non_Mapped'] = qtlDataset[(
      qtlDataset['Start_trust_interval'].isnull())].shape[0]
  descriptionOfDatasets.loc['Total', 'Mapped'] = qtlDataset[(
      ~qtlDataset['Start_trust_interval'].isnull())].shape[0]
  descriptionOfDatasets.loc['Total', 'Total'] = qtlDataset.shape[0]
  return descriptionOfDatasets


def output_dataframe_in_csv(path, dataframe, verbose):
  """
  Save a data frame as a CSV file

  Parameters
  ----------
  path : string
      Path to save csv file
  qtlDataset : pandas.core.frame.DataFrame
      Panda Data Frame to save as a CSV file
  verbose: bool
    If true, print verbose for user
  """
  os.makedirs(os.path.dirname(path), exist_ok=True)
  if 'index' in dataframe.columns:
    # Dropping useless column
    dataframe = dataframe.drop('index', 1)
  # Saving data frame as a CSV
  dataframe.to_csv(path, sep=',', index=False, encoding='utf-8')
  if verbose:
    print('Saving data frame as CSV file stored in: ' + path)


def open_a_json_file(path):
  """
  This function simply load a json file

  Parameters
  ----------
  path : string
      Path to file to open

  Returns
  -------
  Dictionary
      Json content loaded as dictionnary
  """
  with open(path, 'r') as file:
    return json.load(file)


def reorder_chromosome_pair_for_visualisation(df, statisticalTest):
  df = df.loc[
    (df['p_value'] != 1)
    & (df['test_statistics'] == statisticalTest)]
  df.loc[:, 'chromosome'] = df['chromosome'].replace('[13, 16]', '[16, 13]', regex=False)
  df.loc[:, 'chromosome'] = df['chromosome'].replace('[4, 12]', '[12, 4]', regex=False)
  df.loc[:, 'chromosome'] = df['chromosome'].replace('[5, 10]', '[10, 5]', regex=False)
  df.loc[:, 'Threshold'] = df['Threshold'].replace(0, 'Minor QTL', regex=False)
  df.loc[:, 'Threshold'] = df['Threshold'].replace(0.15, 'Major QTL', regex=False)

  significativeDf = df.loc[
      df['p_value'] < 0.05
  ]
  return df, significativeDf
