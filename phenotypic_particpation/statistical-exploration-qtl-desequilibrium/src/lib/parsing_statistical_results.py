def interactive_results(resultDataFrame, test_statistics, Sort_on,
                        chromosomePair, significative):
  """
  A function aiming to construct an interactive widget to analyze results dataframe.

  Parameters
  ----------
  resultDataFrame : pandas.core.frame.DataFrame
      A dataframe storing summary of statistical tests in following columns:
      chromosome	statistical_value	p_value	test_statistics	origin	Sort_on	Threshold	Count_first_chromosome	Count_second_chromosome	Total_count
  test_statistics : widgets.Select
      A selector to select a particular statistical test
  Sort_on : widgets.Select
      A selector to select a particular sorting on QTL dataframe
  chromosomePair : widgets.Select
      A selector to select a syntenic pair
  significative : widgets.Checkbox
      Store if user asked to apply a threshold on p-value

  Returns
  -------
  ipywidgets.widgets.interaction.interactive
      An interactive widget
  """
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  if chromosomePair == r'(.*?)':
    return resultDataFrame[
        (resultDataFrame['Sort_on'].str.match(Sort_on))
        & resultDataFrame['test_statistics'].str.match(test_statistics) &
        (resultDataFrame['p_value'] <= significativeThreshold) &
        (resultDataFrame['chromosome'].str.match(chromosomePair))]
  else:
    return resultDataFrame[
        (resultDataFrame['Sort_on'].str.match(Sort_on))
        & resultDataFrame['test_statistics'].str.match(test_statistics) &
        (resultDataFrame['p_value'] <= significativeThreshold) &
        (resultDataFrame['chromosome'] == chromosomePair)]


def interactive_exploration(resultDataFrame, categorie, significative, column):
  """
  A function aiming to construct an interactive widget to analyze results dataframe.

  Parameters
  ----------
  resultDataFrame : pandas.core.frame.DataFrame
      A dataframe storing summary of statistical tests in following columns:
      chromosome	statistical_value	p_value	test_statistics	origin	Sort_on	Threshold	Count_first_chromosome	Count_second_chromosome	Total_count
  categorie : widgets.Select
      A selector to select a threshold
  significative : widgets.Checkbox
      Store if user asked to apply a threshold on p-value
  column : string
      Name of column where threshold is applied

  Returns
  -------
  ipywidgets.widgets.interaction.interactive
      An interactive widget
  """
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame[(resultDataFrame[column].str.match(categorie))
                         &
                         (resultDataFrame['p_value'] <= significativeThreshold)]


def interactive_exploration_R2(resultDataFrame, column, threshold,
                               significative):
  """
  A function aiming to construct an interactive widget to analyze results dataframe.

  Parameters
  ----------
  resultDataFrame : pandas.core.frame.DataFrame
      A dataframe storing summary of statistical tests in following columns:
      chromosome	statistical_value	p_value	test_statistics	origin	Sort_on	Threshold	Count_first_chromosome	Count_second_chromosome	Total_count
  column : string
      Name of column where threshold is applied. In this case Threshold column
  threshold : widgets.Select
      R2 threshold
  significative : widgets.Checkbox
      Store if user asked to apply a threshold on p-value

  Returns
  -------
  ipywidgets.widgets.interaction.interactive
      An interactive widget
  """
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame.loc[
      (resultDataFrame[column] >= threshold)
      & (resultDataFrame['p_value'] <= significativeThreshold)]


def interactive_exploration_vanilla_stats(resultDataFrame, significative):
  """
  A function aiming to construct an interactive widget to analyze results dataframe.

  Parameters
  ----------
  resultDataFrame : pandas.core.frame.DataFrame
      A dataframe storing summary of statistical tests in following columns:
      chromosome	statistical_value	p_value	test_statistics	origin	Sort_on	Threshold	Count_first_chromosome	Count_second_chromosome	Total_count
  significative : widgets.Checkbox
      Store if user asked to apply a threshold on p-value

  Returns
  -------
  ipywidgets.widgets.interaction.interactive
      An interactive widget
  """
  significativeThreshold = 1.0
  if significative:
    significativeThreshold = 0.05
  return resultDataFrame.loc[(resultDataFrame['p_value'] <=
                              significativeThreshold)]
