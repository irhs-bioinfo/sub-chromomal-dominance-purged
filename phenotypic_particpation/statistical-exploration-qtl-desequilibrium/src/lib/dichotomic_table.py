import pandas as pd


def get_coordinates_of_compared_blocks(qtlData):
  """
  This function aims to construct a dictionnary with coordinates of each syntenic blocks compared

  Parameters
  ----------
  qtlData : pandas.core.frame.DataFrame
      Dataframe with QTLs data

  Returns
  -------
  Dictionnary
      a dictionnary with coordinates of each syntenic blocks compared
  """
  blocksCoordinates = {}
  grouped = qtlData.groupby('Chromosome_Number')
  for name, group in grouped:
    blocksCoordinates[name] = [
        group['Start_trust_interval'].min(), group['End_trust_interval'].max()
    ]
  return blocksCoordinates


def generate_dichotomic_table(chromosomeNumbers, neededColumns, qtlData):
  """
  Generate a data Frame containing different columns following needed columns argument.


  This dataframe mainly gather:
  - Binarized QTL present on the chromosomes whose number is given in argument
  - A label column summing up two precedent column

  Example of table head:
  | Index | chromosome_9 | chromosome_17 | Label                       |
  |-------|--------------|---------------|-----------------------------|
  | 34    | 0            | 1             | qCLGA.RGxBr.2008-ch17(RG)   |
  | 35    | 0            | 1             | qCLGA.RGxBr.2008-ch17(RG).1 |
  | 36    | 0            | 1             | qCLGA.RGxBr.2010-ch17(RG)   |
  | 40    | 1            | 0             | qC3ARA.RGxBr.2008-ch9(RG)   |
  | 41    | 1            | 0             | qC3ARA.RGxBr.2008-ch9(BB)   |

  Parameters
  ----------
  chromosomeNumbers  : array
    Array storing all chromosome numbers needed in table
  neededColumns  : array
    Array containning column names needed
  qtlData  : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing all the information of each QTL.
    Globally head of this Data Frame depend of from which CSV it comes
    but he gather at least those informations:
    -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  Returns
  -------
  pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column

  """
  arrayOfDataFrameOfParticularChromosome = []
  # Create a Data Frame to store a Label column storing the inital name of label before binarization
  tempDataFrame = pd.DataFrame()
  # Subset qtlData keeping only needed chromosome and columns
  for chromosomeNumber in chromosomeNumbers:
    arrayOfDataFrameOfParticularChromosome.append(
        subset_qtl_data_data_of_one_chromosome(qtlData, chromosomeNumber,
                                               neededColumns))
  # Concatenate both datasets using row axis
  dichotomicTable = pd.concat([
      arrayOfDataFrameOfParticularChromosome[0],
      arrayOfDataFrameOfParticularChromosome[1]
  ],
      axis=1)
  # Concatenate both same named columns into one
  sump_up_columns(neededColumns, dichotomicTable, chromosomeNumbers,
                  tempDataFrame)
  # Keep only "sucess" column of each chromosome. Other columns are kept in
  # "tempDataFrame"
  dichotomicTable = dichotomicTable[[
      'chromosome_' + str(chromosomeNumbers[0]),
      'chromosome_' + str(chromosomeNumbers[1])
  ]]
  # Transform non NA as 1 and NA as 0
  dichotomicTable = dichotomicTable.notnull().astype('int')
  # Concatenate both data frame to produce final dichotomic table
  dichotomicTable = pd.concat([dichotomicTable, tempDataFrame], axis=1)
  return dichotomicTable


def generate_contingency_table(dichotomicTable, chromosomeNumbers):
  """
  Generate a contingency table following next example template:

  |              |   | chromosome_2 | chromosome_2 |
  |--------------|---|--------------|--------------|
  |              |   |      0       |      1       |
  | chromosome_1 | 0 |      X       |      X       |
  | chromosome_1 | 1 |      X       |      X       |

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  pandas.core.frame.DataFrame
    A contingency table

  """
  return pd.crosstab(
      index=dichotomicTable['chromosome_' + str(chromosomeNumbers[0])],
      columns=dichotomicTable['chromosome_' + str(chromosomeNumbers[1])],
      dropna=False,
      margins=True)


def generate_sucesses_table(dichotomicTable):
  """
  Calculate number of sucesses and number of observation

  Parameters
  ----------
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column

  Returns
  -------
  numberOfSuccesses : integer
    Number of QTL present on each chromosome
  totalSize : integer
    Number of QTL on both chromosomes
  """
  numberOfSuccesses = dichotomicTable.sum(axis=0, skipna=True)
  totalSize = len(dichotomicTable.index)
  return numberOfSuccesses, totalSize


def sort_and_cut_dichotomic_table(dichotomicTable, sortingColumn, threshold):
  """
  Sort following a column, calculate cumulated sum and keep only beginning of the table until a threshold fixed by argument

  Sorts the table in descending order based on a column passed as an argument. The cumulative frequency of this column is then calculated. We keep 80% of the cumulated sum for this column.  For example if we are interested in the impact of R2. The function will sort in descending order the table based on the R2 column. We will then calculate the cumulated sum.  A threshold corresponding to 80% of the cumulated value will be set. The lower part of the table will be deleted from the cumulative sum equal to this threshold.

  Parameters
  ----------
  dichotomicTable :  pandas.core.frame.DataFrame
      Dichotomic table
  sortingColumn : string
      Label of column used for sorting data frame
  threshold : float , string
    threshold used to slice Data Frame
  Returns
  -------
  pandas.core.frame.DataFrame
      Dichotomic table sorted and truncated following argument needings
  """
  # Sort in descending order dichtomic table following a sortingColumn
  dichotomicTable = dichotomicTable.sort_values(
      by=[sortingColumn], ascending=False)
  # Reset index
  dichotomicTable = dichotomicTable.reset_index().sort_index()

  if dichotomicTable[sortingColumn].dtypes == 'float64':
    dichotomicTable = dichotomicTable.loc[
        dichotomicTable[sortingColumn] >= threshold]
  else:
    # Return lowest indexes in each strings in the Series/Index where the substring is
    # fully contained between [start:end]. Return -1 on failure. Equivalent to
    # standard str.find().
    dichotomicTable = dichotomicTable[
        dichotomicTable[sortingColumn].str.contains(threshold)]
  return dichotomicTable


def check_for_nan(numberOfSuccesses, totalSize, chromosomeNumbers):
  """
  A function to check if there is no NA value in dichotomic table.

  NA value correspond to QTL with no label which will then not be counted as a 1 or a 0 in
  the dichotomic table.

  Parameters
  ----------
  numberOfSuccesses : pandas.core.frame.DataFrame
      Count for each column of dichotomic table
  totalSize : int
      Size of dichotomic table
  chromosomeNumbers : array
      Array storing chromosome numbers present in table

  Returns
  -------
  bool
    return True if total of sucesses is equal to size of dichotomic table
  """
  totalNumberOfSuccesses = 0
  for chromosomeNumber in chromosomeNumbers:
    totalNumberOfSuccesses = totalNumberOfSuccesses + \
        numberOfSuccesses['chromosome_' + str(chromosomeNumber)]
  return totalNumberOfSuccesses == totalSize


def subset_qtl_data_data_of_one_chromosome(qtlData, chromosomeNumber,
                                           neededColumns):
  """
  Subset qtl Data Frame with only data of one chromosome. Keep only some columns

  Parameters
  ----------
  neededColumns  : array
    Array containning column names needed
  qtlData  : pandas.core.frame.DataFrame
    Contains as a Panda Data Frame the CSV containing all the information of each QTL.
    Globally head of this Data Frame depend of from which CSV it comes
    but he gather at least those informations:
    -['Label', 'Start_trust_interval', 'End_trust_interval', 'Limit',
                'R2', 'Categorie', 'Markers', 'Chromosome_Number', 'Number_of_associated_markers', 'Origin']
  chromosomeNumbers  : array
    Array storing all chromosome numbers needed in table

  Returns
  -------
  pandas.core.frame.DataFrame
      Subset qtl Data Frame with only data of one chromosome. Only some columns were kept
  """
  # Initialize data Frame
  dataChromosome = pd.DataFrame(columns=neededColumns)
  # Filter data set with a given chromosome number
  dataChromosome = qtlData.loc[(qtlData.Chromosome_Number == chromosomeNumber)]
  # Keep only needed columns
  dataChromosome = dataChromosome[neededColumns]
  # Rename columns to keep information to which chromosome each column belong
  dataChromosome = dataChromosome.rename(
      columns={'Label': 'chromosome_' + str(chromosomeNumber)})
  return dataChromosome


def sump_up_columns(neededColumns, dichotomicTable, chromosomeNumbers,
                    tempDataFrame):
  """
  Concatenate both same named columns into one using pd.combine_first

  Concatenate both same named columns into one using combine_first. This
  function allows to combine two DataFrame objects by filling null values
  in one DataFrame with non-null values from other DataFrame. The row and
  column indexes of the resulting DataFrame will be the union of the two.

  Parameters
  ----------
  neededColumns  : array
    Array containning column names needed
  dichotomicTable : pandas.core.frame.DataFrame
    A data Frame containing different columns. Mainly:
      - Binarized QTL present on the chromosomes whose number is given in argument
      - A label column summing up two precedent column
  chromosomeNumbers  : array
    Array storing all chromosome numbers needed in table
  tempDataFrame : pandas.core.frame.DataFrame
      An empty Data Frame to store columns to sum up
  """
  # Loop on each columns that need a sum up
  for neededColumn in neededColumns:
    # A particular handle for Label column
    if neededColumn == 'Label':
      tempDataFrame[neededColumn] = dichotomicTable[
          'chromosome_' + str(chromosomeNumbers[0])].combine_first(
              dichotomicTable['chromosome_' + str(chromosomeNumbers[1])])
    else:
      # Store columns parsed
      neededColumnDataFrame = dichotomicTable[neededColumn]
      # Store it in a same named column. Combine second column of subseted Data Frame into first one
      tempDataFrame[
          neededColumn] = neededColumnDataFrame.iloc[:, [0]].combine_first(
              neededColumnDataFrame.iloc[:, [1]])
