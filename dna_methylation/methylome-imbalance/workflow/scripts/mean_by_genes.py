import argparse
import os
import pandas as pd
import dask.dataframe as dd
from scipy import stats
import warnings
from tqdm import tqdm

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      nargs="+",
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  for context in ["CHH", "CHG", "CG"]:
    cx_reportInContext = [string for string in args.cx_report if context in string]

    allCx = dd.read_csv(cx_reportInContext, sep="\t").compute()

    allCxMean = pd.DataFrame()
    for name, group in allCx.groupby("current"):
        tmpDict= {
        "couple":group["couple"].unique()[0],
        "gene": name,
        "ratio": group["ratio"].mean(),
        "context": context
        }
        allCxMean = allCxMean.append(tmpDict, ignore_index=True)
    if os.path.isfile(args.output):
        allCxMean.to_csv(args.output, index=False, mode="a", header=False)
    else:
        allCxMean.to_csv(args.output, index=False)
if __name__ == "__main__":
  main()
