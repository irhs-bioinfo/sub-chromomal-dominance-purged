import argparse
import os
# import modin.pandas as pd
import pandas as pd
from scipy import stats
import warnings
from tqdm import tqdm

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      nargs="+",
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--typeOfAnalysis",
      help="typeOfAnalysis",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def check_if_float(row):
  return isinstance(row['ratio'], float)


def compute_stat(couple: str, group: pd.DataFrame, context: str, sample: str, outPath: str):
  first = group[group["chromosome"]
                == couple.split("-")[0].zfill(2)]
  second = group[group["chromosome"]
                 == couple.split("-")[1].zfill(2)]
  # print("Computing stats for {}, run: {} and {} context for a population of {} and {}".format(
  #   couple, sample, context, first.shape[0], second.shape[0]))
  mannwhitneyuResults = stats.mannwhitneyu(first["ratio"],
                                           second["ratio"])
  ranksumsResults= stats.ranksums(first["ratio"],
                                  second["ratio"])
  dictRe = {"couple": couple,
            "statMannwhitneyu": mannwhitneyuResults[0],
            "pvalueMannwhitneyu": mannwhitneyuResults[1],
            "statranksums": ranksumsResults[0],
            "pvalueranksums": ranksumsResults[1],
            "context": context,
            "sample": sample,
            "sizeFirst": first.shape[0],
            "sizeSecond": second.shape[0],
            "medRatioFirst": first["ratio"].mean(),
            "medRatioSecond": second["ratio"].mean()}
  results = pd.DataFrame([dictRe])
  if os.path.isfile(outPath):
    results.to_csv(outPath,
                   sep="\t",
                   index=False,
                   mode="a",
                   header=False)
  else:
    results.to_csv(outPath,
                   sep="\t",
                   index=False)

  return None


def main():
  args = get_arguments()
  for cx_report in tqdm(args.cx_report):
    cxReportGeneCouple = pd.read_csv(cx_report,
                                     sep="\t",
                                     )
    if args.typeOfAnalysis == "gene_body" not in cx_report:
      context = cx_report.split("/")[-1].split(".")[7]
    elif args.typeOfAnalysis == "not_in_gene":
      context = cx_report.split("/")[-1].split(".")[8]
    else:
      context = cx_report.split("/")[-1].split(".")[9]

     
    sample = cx_report.split(
      "/")[-1].split(".")[0].replace("-TechRep_1-BioRep_1", "")
    cxReportGeneCouple["chromosome"] = cxReportGeneCouple["geneAssociated"].apply(
      lambda x: x[2:4])
    for name, group in cxReportGeneCouple.groupby("couple"):
      compute_stat(couple=name,
                   group=group,
                   context=context,
                   sample=sample,
                   outPath=args.output)


if __name__ == "__main__":
  main()
