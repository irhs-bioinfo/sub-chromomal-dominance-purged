import argparse

import gffutils


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--gff",
      help="Path to transposable elements gff model",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to SQL database",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  gffutils.create_db(args.gff,
                     dbfn=args.output,
                     force=True,
                     verbose=True,
                     merge_strategy='error',
                     keep_order=False,
                     sort_attribute_values=False)


if __name__ == "__main__":
  main()
