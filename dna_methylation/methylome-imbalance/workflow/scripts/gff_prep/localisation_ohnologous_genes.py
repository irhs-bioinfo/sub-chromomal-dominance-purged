# %%
import argparse

import gffutils
import pandas as pd
# import modin.pandas as pd
from tqdm import tqdm

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--database",
      help="Path to gene database",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to localised ohnologous genes",
      type=str,
      required=True)
  parser.add_argument(
      "-c",
      "--chromosomeSize",
      help="Path to a table storing chromosome size",
      type=str,
      required=True)
  parser.add_argument(
      "-i",
      "--ohnologousGenes",
      help="Path to i-adhore output",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def reorder_genes(row: pd.DataFrame) -> pd.DataFrame:
  """This function aims to reorder genes in order to have in gene x
  a gene with a chromosome number inferior to gene y one

  Parameters
  ----------
  row : pd.DataFrame
      Ohnologous genes dataframe row

  Returns
  -------
  pd.DataFrame
      Ohnologous genes dataframe row with reodered genes
  """
  if int(row["gene_x"][2:4]) > int(row["gene_y"][2:4]):
    row["gene_x"], row["gene_y"] = row["gene_y"], row["gene_x"]
  return row


def main():
  args = get_arguments()
  tqdm.pandas()
  chromosomes = pd.read_table(args.chromosomeSize,
                              sep="\s+",
                              names=["seqname", "size"],
                              index_col="seqname")
  db = gffutils.FeatureDB(args.database, keep_order=True)
  ohnologousGenes = pd.read_csv(args.ohnologousGenes,
                                sep=" ")

  columns_add = ['start_x',
                 'start_x_window',
                 'start_y',
                 'start_y_window'
                 'end_x',
                 'end_x_window',
                 'end_y',
                 'end_y_window']
  ohnologousGenes.assign(**dict([(_, None) for _ in columns_add]))

  ohnologousGenes = ohnologousGenes.apply(reorder_genes,
                                          axis=1)
  ohnologousGenes["couple"] = ohnologousGenes["gene_x"].str[2:4] + "-" \
      + ohnologousGenes["gene_y"].str[2:4]
  ohnoFlat = pd.DataFrame(columns=["gene",
                                   "chr",
                                   "start",
                                   "start_window",
                                   "end",
                                   "end_window"])
  notOhno = pd.DataFrame(columns=["gene",
                                  "chr",
                                  "start",
                                  "end"])

  for gene in tqdm(db.features_of_type('gene'),
                   desc='Computing density on each genes',
                   total=len(list(db.features_of_type('gene')))):
    geneID = gene.attributes.items()[1][1][0]
    if geneID in ohnologousGenes["gene_x"].to_list():
      mask = ohnologousGenes["gene_x"] == geneID
      ohnologousGenes.loc[mask, 'start_x'] = int(gene.start - 1)
      ohnologousGenes.loc[mask, 'start_x_window'] = int(
        max(0, gene.start - 2000))
      ohnologousGenes.loc[mask, 'end_x'] = int(gene.end + 1)
      ohnologousGenes.loc[mask, 'end_x_window'] = int(
        min(chromosomes.at[gene.seqid, "size"], gene.end + 2000))
      tmpDict = {
        "gene": geneID,
        "chr": geneID[2:4],
        "start": int(gene.start - 1),
        "start_window": int(
            max(0, gene.start - 2000)),
        "end": int(gene.end + 1),
        "end_window": int(min(chromosomes.at[gene.seqid, "size"], gene.end + 2000))
      }
      ohnoFlat = ohnoFlat.append(tmpDict, ignore_index=True)

    elif geneID in ohnologousGenes["gene_y"].to_list():
      mask = ohnologousGenes["gene_y"] == geneID
      ohnologousGenes.loc[mask, 'start_y'] = int(gene.start - 1)
      ohnologousGenes.loc[mask, 'start_y_window'] = int(
        max(0, gene.start - 2000))
      ohnologousGenes.loc[mask, 'end_y'] = int(gene.end + 1)
      ohnologousGenes.loc[mask, 'end_y_window'] = int(
        min(chromosomes.at[gene.seqid, "size"], gene.end + 2000))
      tmpDict = {
        "gene": geneID,
        "chr": geneID[2:4],
        "start": int(gene.start - 1),
        "start_window": int(
            max(0, gene.start - 2000)),
        "end": int(gene.end + 1),
        "end_window": int(min(chromosomes.at[gene.seqid, "size"], gene.end + 2000))
      }
      ohnoFlat = ohnoFlat.append(tmpDict, ignore_index=True)
    else:
      tmpDict = {
        "gene": geneID,
        "chr": geneID[2:4],
        "start": int(gene.start - 1),
        "end": int(gene.end + 1),
      }
      notOhno = notOhno.append(tmpDict, ignore_index=True)

  ohnologousGenes.to_csv(args.output, sep="\t", index=False)
  ohnoFlat.to_csv(args.output + ".flat", sep="\t", index=False)
  notOhno.to_csv(args.output + ".notOhno", sep="\t", index=False)


if __name__ == "__main__":
  main()
# %%
