# %%
import argparse

import gffutils
import pandas as pd
from tqdm import tqdm
import gffutils.biopython_integration as bp
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--database",
      help="Path to gene database",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to localised ohnologous genes",
      type=str,
      required=True)
  args = parser.parse_args()
  return args



def main():
  args = get_arguments()
  db = gffutils.FeatureDB(args.database, keep_order=True)

  for intron in db.features_of_type('intron'):
      tmpDict = {"chr": intron.seqid,
                 "start": intron.start,
                 "end": intron.end,}
      test = pd.DataFrame([tmpDict])
      test.to_csv(args.output, sep="\t", header=False, index=False, mode="a")


if __name__ == "__main__":
    main()
