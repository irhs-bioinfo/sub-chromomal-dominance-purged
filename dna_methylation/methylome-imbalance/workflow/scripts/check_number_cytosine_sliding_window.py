import numpy as np
from scipy import stats
import pandas as pd
import argparse
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

pd.options.mode.chained_assignment = None  # default='warn'


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  parser.add_argument(
      "-n",
      "--nonSwapping",
      help="nonSwapping",
      type=str,
      required=True)
  parser.add_argument(
    "-a",
    "--anaDiffCounts",
    help="anaDiffCounts",
    type=str,
    required=True)
  parser.add_argument(
      "-d",
      "--diffMin",
      help="diffMin",
      type=float,
      required=True)
  args = parser.parse_args()
  return args


def melting_non_swapping_genes(dfNS: pd.DataFrame) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  dfNS : pd.DataFrame
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  nonX = dfNS[["couple",
               "gene_x",
               "nonSwappingGenesGeneX"]]
  nonY = dfNS[["couple",
               "gene_y",
               "nonSwappingGenesGeneY"]]
  nonX = nonX.rename({"gene_x": "gene",
                      "nonSwappingGenesGeneX": "nonSwappingGenesGene"},
                     axis=1)
  nonY = nonY.rename({"gene_y": "gene",
                      "nonSwappingGenesGeneY": "nonSwappingGenesGene"},
                     axis=1)
  return pd.concat([nonX, nonY])


def compute_percent_for_all_genes(anaDiffCounts: pd.DataFrame, fullyNonSwapping: pd.DataFrame, couples: list) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  anaDiffCounts : pd.DataFrame
      _description_
  fullyNonSwapping : pd.DataFrame
      _description_
  couples : list
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  percentDf = pd.DataFrame()
  for couple in couples:
    currentCouple = anaDiffCounts[anaDiffCounts["couple"] == couple]

    tmpDict = {"nonSwapping": fullyNonSwapping[fullyNonSwapping["couple"] == couple].shape[0],
               "normal": currentCouple[~currentCouple["gene_couple"].isin(fullyNonSwapping["gene_couple"])].shape[0],
               "couple": couple}
    percentDf = percentDf.append(tmpDict, ignore_index=True)
  percentDf["total"] = percentDf["nonSwapping"] + percentDf["normal"]
  percentDf["percent"] = percentDf["nonSwapping"] / percentDf["total"]
  return percentDf


def compute_percent_for_most_diff_genes(nlargestdNS: pd.DataFrame) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  nlargestdNS : pd.DataFrame
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  resultDiff = pd.DataFrame()
  for name, group in nlargestdNS.groupby("couple"):

    tmpDict = {"couple": name,
               "nonSwapping": group[group["NonSwapping"] == True].shape[0],
               "swapping": group[group["NonSwapping"] == False].shape[0],
               "tot": group.shape[0]}
    resultDiff = resultDiff.append(tmpDict, ignore_index=True)

  resultDiff["percent"] = resultDiff["nonSwapping"] / resultDiff["tot"]
  return resultDiff


def filter_position_following_range(cx_report: pd.DataFrame, size: int) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  cx_report : pd.DataFrame
      _description_
  size : int
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  cx_report_current = pd.DataFrame()
  for name, group in cx_report.groupby("geneAssociated"):
    currentGroup = group[group["position"].between(group["position"].max() - size,
                                                   group["position"].max()+ (size-50))]

    cx_report_current = pd.concat([cx_report_current, currentGroup])
  return cx_report_current


def compute_diff_percent(row):
  return np.abs(row["gene_x"] - row["gene_y"])/ max(row["gene_x"], row["gene_y"])


def compute_diff_CHH_between_ohno(cx_report_current: pd.DataFrame) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  cx_report_current : pd.DataFrame
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  results = pd.DataFrame()
  for name, group in cx_report_current.groupby(["gene_couple"]):
    tmpDict = {"gene_couple": name,
               "couple": group.iloc[0]["couple"]
               }
    tmpDict = tmpDict | group["variable"].value_counts().to_dict()

    results = results.append(tmpDict, ignore_index=True)
  results["diff"] = results["gene_x"] - results["gene_y"]
  results["diff_percent"] = results.apply(compute_diff_percent, axis=1)


  return results


def compute_enrichment_test(resultDiff: pd.DataFrame, percentDf: pd.DataFrame, enrichmentResult: pd.DataFrame, size: int, couples: list) -> pd.DataFrame:
  """_summary_

  Parameters
  ----------
  resultDiff : pd.DataFrame
      Dataframe storing the difference between the nonSwapping genes for 50 more different genes
  percentDf : pd.DataFrame
      Dataframe storing the percentage of nonSwapping genes for all ohnologous genes
  enrichmentResult : pd.DataFrame
      Dataframe storing final results
  size : int
      Upstream size analysed
  couples : list
      List of all couples analysed in the analysis

  Returns
  -------
  pd.DataFrame
      Dataframe storing final results
  """
  for couple in couples:
    table = np.array([[resultDiff[resultDiff["couple"] == couple].iloc[0]["nonSwapping"],
                       percentDf[percentDf["couple"] == couple].iloc[0]["nonSwapping"]],
                      [resultDiff[resultDiff["couple"] == couple].iloc[0]["tot"],
                       percentDf[percentDf["couple"] == couple].iloc[0]["total"]]])
    odd, p = stats.fisher_exact(table, alternative='two-sided')
    tmpDict = {"pval": p,
               "couple": couple,
               "percentDiff": resultDiff[resultDiff["couple"] == couple].iloc[0]["percent"],
               "percentNorm": percentDf[percentDf["couple"] == couple].iloc[0]["percent"],
               "size": size}
    enrichmentResult = enrichmentResult.append(tmpDict, ignore_index=True)
  return enrichmentResult


def main():
  enrichmentResult = pd.DataFrame()

  args = get_arguments()
  sample = args.cx_report.split(
    "/")[-1].split(".")[0].replace("-TechRep_1-BioRep_1", "")
  cx_report = pd.read_csv(args.cx_report, sep="\t")

  anaDiffCounts = pd.read_csv(args.anaDiffCounts)
  fullyNonSwapping = pd.read_csv(args.nonSwapping)

  cx_report["chr"] = cx_report["geneAssociated"].str[2:4]

  fullyNonSwappingMelt = melting_non_swapping_genes(fullyNonSwapping)

  percentDf = compute_percent_for_all_genes(
    anaDiffCounts, fullyNonSwapping, cx_report["couple"].unique())

  for size in range(50, 2050, 50):
    nlargestd = pd.DataFrame()
    # Get all position that fit between TSS and max size
    cx_report_current = filter_position_following_range(cx_report, size)
    # Compute number of CHH position and compute diff between ohno
    results = compute_diff_CHH_between_ohno(cx_report_current)
    if args.diffMin > 0:
      for name, group in results.groupby("couple"):
        nlargestd = group[group["diff_percent"]>0.10]
    else:
      for name, group in results.groupby("couple"):
        nlargestd = nlargestd.append(group.nlargest(n=25,
                                     columns=['diff']),
                                    ignore_index=True)
    
    nlargestd["gene_x"] = nlargestd["gene_couple"].apply(
      lambda x: x.split("-")[0])
    nlargestd["gene_y"] = nlargestd["gene_couple"].apply(
      lambda x: x.split("-")[1])

    nlargestdMelt = nlargestd.melt(id_vars=["gene_couple",
                                            "couple",
                                            "diff"],
                                   value_vars=["gene_x",
                                               "gene_y"])

    nlargestdNS = nlargestdMelt.merge(fullyNonSwappingMelt,
                                      left_on="value",
                                      right_on="gene",
                                      how="left")
    areNS = nlargestdNS[~nlargestdNS["nonSwappingGenesGene"].isna()]
    areNS["NonSwapping"] = True
    notNS = nlargestdNS[nlargestdNS["nonSwappingGenesGene"].isna()]
    notNS["NonSwapping"] = False
    nlargestdNS = pd.concat([areNS, notNS])
    nlargestdNS = nlargestdNS.drop(["couple_y", "gene"], axis=1)
    nlargestdNS = nlargestdNS.rename({"couple_x": "couple"},
                                     axis=1)
    resultDiff = compute_percent_for_most_diff_genes(nlargestdNS)

    enrichmentResult = compute_enrichment_test(
      resultDiff, percentDf, enrichmentResult, size, nlargestd["couple"].unique())

  enrichmentResult["sample"] = sample

  enrichmentResult.to_csv(args.output, index=False)  # type: ignore


if __name__ == "__main__":
  main()
