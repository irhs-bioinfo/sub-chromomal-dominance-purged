import argparse

import pandas as pd


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      help="ohnologous file",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def add_gene_couple(row: pd.DataFrame):
  """Construct a string with both gene ID like MD01GXXXX-MD07GXXXX

  Parameters
  ----------
  row : pd.DataFrame
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  if type(row["gene_x"]) != float and type(row["gene_y"]) != float:
    if int(row["gene_x"][2:4]) < int(row["gene_y"][2:4]):
      return row["gene_x"] + "-" + row["gene_y"]
    else:
      return row["gene_y"] + "-" + row["gene_x"]


def main():
  args = get_arguments()

  ohnoAll = pd.read_csv(
      args.input, sep="\t")
  ohnoAll["gene_couple"] = ohnoAll.apply(
    add_gene_couple, axis=1)
  ohnoAllmelted = ohnoAll.melt(
    id_vars="gene_couple", value_vars=["gene_x", "gene_y"])
  ohnoAllmelted.to_csv(args.output, index=False)


if __name__ == "__main__":
  main()
