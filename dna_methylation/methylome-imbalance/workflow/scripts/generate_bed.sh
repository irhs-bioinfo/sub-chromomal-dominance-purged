# $1 ohno
# $2 ohnoFlat
# $3 ohno2kb
# $4 ohno500bp
# $5 ohno100bp

awk 'BEGIN{OFS="\t"} {print "Chr"$2, $3+1, $5-1, $1}' $1 | tail -n+2 | sort -k 1,1 -k 2n,2 > $2
awk 'BEGIN{OFS="\t"} {print $1, $2-2000, $2+2000, $4}' $2 | sort -k 1,1 -k 2n,2 > $3
awk 'BEGIN{OFS="\t"} {print $1, $2-500, $2, $4}' $2 | sort -k 1,1 -k 2n,2 > $4
awk 'BEGIN{OFS="\t"} {print $1, $2-100, $2, $4}' $2 | sort -k 1,1 -k 2n,2 > $5
awk 'BEGIN{OFS="\t"} {print $1, $2-2000, $2, $4}' $2 | sort -k 1,1 -k 2n,2 > $6

awk '$2<0 {$2=0} 1' $3 > tmp_file && sed -i 's/ /\t/g' tmp_file && mv tmp_file $3
awk '$2<0 {$2=0} 1' $4 > tmp_file && sed -i 's/ /\t/g' tmp_file && mv tmp_file $4
awk '$2<0 {$2=0} 1' $5 > tmp_file && sed -i 's/ /\t/g' tmp_file && mv tmp_file $5
awk '$2<0 {$2=0} 1' $6 > tmp_file && sed -i 's/ /\t/g' tmp_file && mv tmp_file $6

