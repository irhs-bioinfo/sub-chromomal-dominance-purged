# %%
import argparse
# import modin.pandas as pd
import swifter
import pandas as pd
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-g",
      "--ohno",
      help="ohnologous genes localised",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def add_gene_couple(row: pd.DataFrame):
  """Construct a string with both gene ID like MD01GXXXX-MD07GXXXX

  Parameters
  ----------
  row : pd.DataFrame
      _description_

  Returns
  -------
  pd.DataFrame
      _description_
  """
  if type(row["gene_x"]) != float and type(row["gene_y"]) != float:
      if int(row["gene_x"][2:4]) > int(row["gene_y"][2:4]):
          return row["gene_x"] + "-" + row["gene_y"]
      else:
          return row["gene_y"] + "-" + row["gene_x"]


def get_median_ratio(row, CX_report: pd.DataFrame, accesStr: str):
    if accesStr == "in_gene":
        cxReportGeneCouple_x = CX_report[CX_report["in_gene"] == row["gene_x"]]
        cxReportGeneCouple_y = CX_report[CX_report["in_gene"] == row["gene_y"]]
    else:
        cxReportGeneCouple_x = CX_report[CX_report["before"] == row["gene_x"] | CX_report["after"] == row["gene_x"]]
        cxReportGeneCouple_y = CX_report[CX_report["before"] == row["gene_y"] | CX_report["after"] == row["gene_y"]]
    return [cxReportGeneCouple_x["ratio"].median(), cxReportGeneCouple_y["ratio"].median()]


def main():
  args = get_arguments()

  CX_report = pd.read_csv(args.cx_report, sep="\t")
  ohnologousGenes = pd.read_csv(
      args.ohno, sep="\t")

  ohnologousGenesFiltered = pd.DataFrame()
  for name, group in ohnologousGenes.groupby("couple"):
    if group.shape[0] > 400:
      ohnologousGenesFiltered = ohnologousGenesFiltered.append(group)
  ohnologousGenesFiltered["gene_couple"] = CX_report.swifter.apply(add_gene_couple, axis=1)

  for name, group in ohnologousGenesFiltered.groupby("gene_couple"):
    if group.shape[0] > 400:
      current = CX_report[CX_report["gene_couple"] == name]
  if CX_report[~CX_report["before"].isna()].shape[0] > 10000:
    accesStr = "before"
  else:
    accesStr = "in_gene"
  ohnologousGenesFiltered[["median_x", "median_y"]] = ohnologousGenesFiltered.swifter.apply(get_median_ratio,
                                                                                    args=(CX_report,accesStr),
                                                                                    axis=1,
                                                                                    result_type="expand")
  ohnologousGenesFiltered.to_csv(args.output,
                                 sep="\t",
                                 index=False)

if __name__ == "__main__":
  main()
# %%
