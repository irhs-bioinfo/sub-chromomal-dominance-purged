import argparse
import os
import pandas as pd
import swifter

# pd.options.mode.chained_assignment = None  # default='warn'


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-i",
      "--introns",
      help="Intronic regions localised",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args

def is_intronic(row : pd.DataFrame, intron : pd.DataFrame):
  value=0
  try:
    value = intron.iloc[intron.index.get_loc(row)]["booleanVal"]
  except:
    pass
  if not value != 1:
    return True
  else:
    return False

def main():
  args = get_arguments()
  intron = pd.read_csv(args.introns, sep="\t", names=["chr", "start", "end"])
  intron.index = pd.IntervalIndex.from_arrays(intron['start'],intron['end'],closed='both')
  intron["booleanVal"] = 1

  with pd.read_csv(args.cx_report,
                   sep="\t",
                   header=None,
                   names=["chromosome",
                          "position",
                          "strand",
                          "count methylated",
                          "count unmethylated",
                          "C-context",
                          "trinucleotide context"],
                   chunksize=10000000,
                   iterator=True) as cx_reportHandler:  # type: ignore
    for cx_report in cx_reportHandler:
      for name, group in cx_report.groupby(["chromosome"]):
        if name != "Chr00":
          intronChr = intron[intron["chr"] == group.iloc[0]["chromosome"]]
          group['intron'] = group["position"].swifter.apply(is_intronic, args=(intronChr,),axis=1)
          group = group[group["intron"] == False]
          group = group.drop(columns=["intron"])
          if os.path.isfile(args.output):
              header=False
          else:
              header=True
          group.to_csv(args.output,
                          index=False,
                          header=header,
                          sep="\t",
                          mode="a")
if __name__ == "__main__":
  main()
