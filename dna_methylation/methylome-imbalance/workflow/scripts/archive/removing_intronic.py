# rule filter_cx_report_intron:
#     """
#     This rule aims to filter the cx report, removing Chr00 and all intronic position.
#     """
#     input:
#         intron="results/introns.csv",
#         cx_report = "resources/biseps_output/{SRR}-TechRep_1-BioRep_1/methylation_extraction_bismark/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt",
#         intronTemplate = "results/templates/template.CX_report.txt.intron.noIntron.intronic"
#     output:
#         cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron",
#         cleaned_cx_report_intron = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron.intronic",
#     threads:
#         workflow.cores
#     conda:
#         "../envs/generate_te_stats.yaml"
#     shell:
#         """
#         python3 {scripts}/removing_intronic.py \
#         --introns {input.intron} \
#         --intronTemplate {input.intronTemplate} \
#         --cx_report {input.cx_report} \
#         --output {output.cleaned_cx_report} \
#         """


import argparse
import os
from collections import namedtuple

import modin.pandas as pd
import pandas as pandas
import swifter
from modin.config import ProgressBar
from tqdm import tqdm

ProgressBar.enable()
range_tuple = namedtuple('range_tuple', 'low high')
pd.options.mode.chained_assignment = None  # default='warn'


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
    "-i",
    "--introns",
    help="Intronic regions localised",
    type=str,
    required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--intronTemplate",
      help="intronTemplate",
      type=str,
      required=False)

  args = parser.parse_args()
  return args


def intronic(cxReportRow: pd.DataFrame, introns):
  """
  This function checks if the position is in an intron

  Parameters
  ----------
  cxReportRow: pd.DataFrame
      CX report row
  introns: pd.DataFrame
      introns dataframe
    Returns
  -------
  bool
      True if in intron, False if not
  """
  if any(intron.low <= cxReportRow <= intron.high for intron in introns):
    return True
  return False


def intron_range(intron: pd.DataFrame):
  """
  This function returns the range of introns

  Parameters
  ----------
  intron : pd.DataFrame
      introns

  Returns
  -------
  range_tuple
      range of introns
  """
  return range_tuple(intron['start'], intron['end'])


def main():
  args = get_arguments()

  intron = pandas.read_csv(args.introns,
                           sep="\t",
                           names=["chr", "start", "end"])
  intronRanges = {}
  for name, group in intron.groupby(["chr"]):
    intronRanges[name] = group.apply(intron_range, axis=1)
  intronTemplate = pandas.DataFrame()
  if args.intronTemplate:
    intronTemplate = pandas.read_csv(args.intronTemplate, sep="\t",)

  with pd.read_csv(args.cx_report,
                   sep="\t",
                   header=None,
                   names=["chromosome",
                          "position",
                          "strand",
                          "count methylated",
                          "count unmethylated",
                          "C-context",
                          "trinucleotide context"],
                   chunksize=60000000,
                   iterator=True) as cx_reportHandler:  # type: ignore
    for cx_report in cx_reportHandler:
      for name, group in tqdm(cx_report.groupby(["chromosome"])):
        if name != "Chr00":
          if not intronTemplate.empty:
            intronTemplateCurrent = intronTemplate[intronTemplate["chromosome"]==name]
            group = group[~group.position.isin(intronTemplateCurrent.position)]
          if os.path.isfile(args.output):
            header = False
          else:
            header = True
          # https://stackoverflow.com/questions/6053974/python-efficiently-check-if-integer-is-within-many-ranges
          # https://stackoverflow.com/questions/23639361/fast-checking-of-ranges-in-python?noredirect=1&lq=1
          print(f"computing {name}")
          group["intronic"] = group["position"].swifter.apply(intronic, args=(intronRanges[name],))
          group[group["intronic"] == True].to_csv(args.output + ".intronic",
                                                  index=False,
                                                  header=header,
                                                  sep="\t",
                                                  mode="a")

          group[group["intronic"] == False].to_csv(args.output,
                                                   index=False,
                                                   header=header,
                                                   sep="\t",
                                                   mode="a")


if __name__ == "__main__":
  main()
