# rule filter_cx_report_main_pairs_not_in_gene_500:
#     """
#     This rule aims to filter the gene gff file, removing Chr00 mainly
#     """
#     input:
#         "results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.{context}.csv"
#     output:
#         "results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.mainPairs.{context}.csv"
#     threads:
#         workflow.cores/2
#     conda:
#         "../envs/generate_te_stats.yaml"
#     shell:
#         """
#         python3 {scripts}/filter_on_main_pairs.py \
#         --cx_report {input} \
#         --output {output}
#         """


import argparse
import modin.pandas as pd


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  mainPairs = ["08-15",
               "13-16",
               "01-07",
               "09-17",
               "03-11",
               "05-10",
               "02-15",
               "06-14", ]
  CX_report = pd.read_csv(args.cx_report, sep="\t")
  CX_report = CX_report[CX_report["couple"].isin(mainPairs)]
  CX_report.to_csv(args.output, sep="\t", index=False)


if __name__ == "__main__":
  main()
