# rule filter_cx_report:
#     """
#     This rule aims to filter the gene gff file, removing Chr00 mainly. Localisation ohnologous genes, and upstream regions
#     """
#     input:
#         ohno="resources/genome/ohnologousGenes_localised.csv.flat",
#         cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
#     output:
#         cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered",
#         cleaned_cx_report_not_in_gene = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene",
#         cleaned_cx_report_not_in_gene_100bp = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp",
#         cleaned_cx_report_not_in_gene_500bp = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp"
#     threads:
#         workflow.cores
#     conda:
#         "../envs/generate_te_stats.yaml"
#     shell:
#         """
#         python3 {scripts}/annotate_cx_report.py \
#         --ohno {input.ohno} \
#         --cx_report {input.cx_report} \
#         --output {output.cleaned_cx_report} \
#         """


import argparse
import os
from collections import namedtuple

import modin.pandas as pd
import numpy as np
import pandas
import swifter
from modin.config import ProgressBar

ProgressBar.enable()
range_tuple = namedtuple('range_tuple', 'low high')
pd.options.mode.chained_assignment = None  # default='warn'


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-g",
      "--ohno",
      help="ohnologous genes localised",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def in_gene(cxReportRow: pd.DataFrame, ohno):
  """
  This function checks if the position is in a gene

  Parameters
  ----------
  cxReportRow: pd.DataFrame
      CX report row
  ohno: pd.DataFrame
      ohno dataframe

  Returns
  -------
  bool
      True if in gene, False if not
  """
  # check if position column is between start and end of at least one gene
  for ohnoKeys, ohnoItem in ohno.items():
    if ohnoItem.low <= cxReportRow <= ohnoItem.high:
      return ohnoKeys
    elif ohnoItem.high > cxReportRow:
      return np.nan
  return np.nan


def before_gene(cxReportRow: pd.DataFrame, ohno, window: int):
  """
    This function checks if the position is before a gene

  Parameters

  ----------
  cxReportRow : pd.DataFrame
      row of cx report
  ohno : pd.DataFrame
      ohno genes
  window : int
      window size
  Returns
  -------
  str
      Associated gene name
  """

  for ohnoKeys, ohnoItem in ohno.items():
    if ohnoItem.low - window < cxReportRow < ohnoItem.low:
      return ohnoKeys
    elif ohnoItem.low > cxReportRow:
      return np.nan
  return np.nan


def after_gene(cxReportRow: pd.DataFrame, ohno, window: int):
  """
  This function checks if the position is after a gene

  Parameters
  ----------
  cxReportRow : pd.DataFrame
      row of cx report
  ohno : pd.DataFrame
      ohno genes
  window : int
      window size
  Returns
  -------
  str
      Associated gene name
  """
  for ohnoKeys, ohnoItem in ohno.items():
    if ohnoItem.high < cxReportRow < ohnoItem.high + window:
      return ohnoKeys
    elif ohnoItem.high + window > cxReportRow:
      return np.nan
  return np.nan


def ohno_range(ohno: pd.DataFrame):
  """
  This function returns the range of ohno genes

  Parameters
  ----------
  ohno : pd.DataFrame
      ohno genes

  Returns
  -------
  range_tuple
      range of ohno genes
  """
  return {ohno["gene"]: range_tuple(ohno['start'], ohno['end'])}


def main():
  args = get_arguments()

  ohno = pandas.read_csv(args.ohno, sep="\t")

  ohnoRangesDict = {}
  for name, group in ohno.groupby(["chr"]):
    ohnoRangesDict["Chr" + str(name).zfill(2)] = group.apply(ohno_range, axis=1)
  with pd.read_csv(args.cx_report,
                  sep="\t",
                  header=None,
                  names=["chromosome",
                          "position",
                          "strand",
                          "count methylated",
                          "count unmethylated",
                          "C-context",
                          "trinucleotide context"],
                  chunksize=10000000,
                  iterator=True) as cx_reportHandler:  # type: ignore
    for cx_report in cx_reportHandler:
      for name, group in cx_report.groupby(["chromosome"]):
        if os.path.isfile(args.output):
          header = False
        else:
          header = True

        ohnoRanges = {k: v for d in ohnoRangesDict[name].to_list()
                      for k, v in d.items()}

        # check if position is in gene
        group["in_gene"] = group["position"].swifter.apply(
            in_gene, args=(ohnoRanges,))
        group["before"] = group["position"].swifter.apply(
            before_gene, args=(ohnoRanges, 2000))
        group["after"] = group["position"].swifter.apply(
            after_gene, args=(ohnoRanges, 2000))

        group[group["in_gene"].notnull()].to_csv(args.output,
                                                index=False,
                                                header=header,
                                                sep="\t",
                                                mode="a")
        group[group["before"].notnull() | group["after"].notnull()].to_csv(args.output + ".not_in_gene",
                                                                          index=False,
                                                                          header=header,
                                                                          sep="\t",
                                                                          mode="a")
        for windowSize in [100, 500]:
          group["before_" + str(windowSize) + "bp"] = group["position"].swifter.apply(
              before_gene, args=(ohnoRanges, windowSize))
          group[group["before_" + str(windowSize) + "bp"].notnull()].to_csv(args.output + ".not_in_gene." + str(windowSize) + "bp",
                                                                            index=False,
                                                                            header=header,
                                                                            sep="\t",
                                                                            mode="a")


if __name__ == "__main__":
  main()
