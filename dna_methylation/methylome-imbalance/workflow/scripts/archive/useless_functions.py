def get_data(path: str) -> pd.DataFrame:
  """This function returns dataframe from csv file

  Parameters
  ----------
  path : str
      path to csv file

  Returns
  -------
  pd.DataFrame
      dataframe from csv file
  """
  li = []
  all_files = glob.glob(path)
  for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0, sep="\t")
    li.append(df)

  all_test = pd.concat(li, axis=0, ignore_index=True)
  return all_test
