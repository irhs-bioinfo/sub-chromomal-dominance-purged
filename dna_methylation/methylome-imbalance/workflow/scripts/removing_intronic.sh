#!/bin/bash

# $1 cx_report
# $2 tmpFile
# $3 cleaned_cx_report_intron.
# $4 cleaned_cx_report
# $5 intron

# Open CX report, get all needed column and intersect it with intron file, constructing following file, asssociating each posiiton with an intronic region
# Chr09   2562    2563    -       0       16      CHH     CAT     Chr09   2557    3817
# Chr09   2566    2567    +       0       13      CHH     CAT     Chr09   2557    3817
# Chr09   2569    2570    -       0       17      CHH     CAT     Chr09   2557    3817
# Chr09   2574    2575    +       0       14      CHH     CAA     Chr09   2557    3817

awk 'BEGIN{OFS="\t"} {print $1, $2-1, $2, $3, $4, $5, $6, $7}' $1 \
| intersectBed -a - -b $5 -wa -wb -nonamecheck > $2
# Keep only needed column and output it in a file
awk 'BEGIN{OFS="\t"} {print $1, $3, $4, $5, $6, $7, $8, $9, $10, $11}' $2 > $3
# Get all non intronic regions
awk 'BEGIN{OFS="\t"} {print $1, $2-1, $2, $3, $4, $5, $6, $7}' $1 \
| intersectBed -a - -b $5 -wa -v -wb -nonamecheck > $2
# Keep only needed column and output it in a file
awk 'BEGIN{OFS="\t"} {print $1, $3, $4, $5, $6, $7, $8, $9, $10, $11}' $2 > $4
