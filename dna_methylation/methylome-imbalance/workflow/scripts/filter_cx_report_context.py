# %%
import argparse
import numpy as np

import pandas as pd
# import modin.pandas as pd
import warnings
import swifter
warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-g",
      "--ohno",
      help="ohnologous genes localised",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def compute_ratio_window(cxReportGeneCouple: pd.DataFrame):
  if cxReportGeneCouple["count unmethylated"].sum() == 0:
    ratio = cxReportGeneCouple["count methylated"].sum()
  else:
    ratio = round(cxReportGeneCouple["count methylated"].sum() /
                  cxReportGeneCouple["count unmethylated"].sum(), 4)

  return [ratio,
          cxReportGeneCouple["count unmethylated"].sum(),
          cxReportGeneCouple["count methylated"].sum(),
          cxReportGeneCouple["position"].min(),
          cxReportGeneCouple["position"].max(), ]


def compute_ratio(row):
  if pd.isna(row["count unmethylated"]):
    return 0
  try:
    if int(row["count unmethylated"]) == 0:
      return int(row["count methylated"])
    else:
      return int(row["count methylated"]) / int(row["count unmethylated"])
  except:
    print(row)


def add_gene_couple(gene_x, gene_y):
  """_summary_

  Parameters
  ----------
  row : pd.DataFrame
      _description_

  Returns
  -------
  _type_
      _description_
  """

  if int(gene_x[2:4]) > int(gene_y[2:4]):
    return gene_x + "-" + gene_y
  else:
    return gene_y + "-" + gene_x

def add_chr_couple(gene_couple):
  """_summary_

  Parameters
  ----------
  row : pd.DataFrame
      _description_

  Returns
  -------
  _type_
      _description_
  """

  return gene_couple.split("-")[0][2:4] + "-" + gene_couple.split("-")[1][2:4]


def main():
  args = get_arguments()

  CX_report = pd.read_csv(args.cx_report,
                          sep="\t",
                          names=["chromosome",
                                 "position",
                                 "strand",
                                 "count methylated",
                                 "count unmethylated",
                                 "C-context",
                                 "trinucleotide context",
                                 "startMatch",
                                 "endMatch",
                                 "geneAssociated"],
                          header=None,
                          index_col=None
                          )
  ohnologousGenes = pd.read_csv(args.ohno,
                                sep=",")
  CX_report = CX_report.reset_index(drop=True)
  if not CX_report.empty:
    CX_report["ratio"] = np.nan
    CX_report["ratio"] = CX_report.swifter.apply(compute_ratio, axis=1)
    CX_report["ratio"] = CX_report["ratio"].fillna(0)

    CX_report = CX_report.merge(ohnologousGenes,
                                left_on=["geneAssociated"],
                                right_on=["value"],
                                how="left")

    CX_report.drop(["value"], axis=1, inplace=True)
    CX_report["chromosome"] = CX_report["geneAssociated"][2:4]
    CX_report["couple"] = CX_report.swifter.apply(
      lambda x: add_chr_couple(x.gene_couple), axis=1)
    CX_report = CX_report.drop_duplicates(subset=["chromosome",
                                                  "position",
                                                  "strand",
                                                  "C-context",
                                                  "trinucleotide context"])
    # CX_report["gene_couple"] = CX_report.swifter.apply(
    #   lambda x: add_gene_couple(x.gene_x, x.gene_y), axis=1)

    for name, CX_report in CX_report.groupby("C-context"):
      CX_report.to_csv(args.output + "{}.csv".format(name),
                       sep="\t",
                       index=False,
                       header=True,
                       mode="a")


if __name__ == "__main__":
  main()
# %%
