import argparse
import os

# import modin.pandas as pd
import swifter
import pandas as pd
import warnings
from tqdm import tqdm
import numpy as np

warnings.simplefilter(action='ignore', category=FutureWarning)


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--cx_report",
      help="CX report file",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  annotatedRatio = pd.DataFrame()

  cxReportGeneCouple = pd.read_csv(args.cx_report,
                                   sep="\t",
                                   dtype={
                                     "chromosome": str,
                                     "position": int,
                                     "strand": str,
                                     "count methylated": int,
                                     "count unmethylated": int,
                                     "C-context": str,
                                     "trinucleotide context": str,
                                     "startMatch":str,
                                     "endMatch":str,
                                     "geneAssociated":str,
                                     "ratio":float,
                                     "gene_couple":str,
                                     "variable":str,
                                     "couple":str,
                                   })
  context = args.cx_report.split("/")[-1].split(".")[7]
  sample = args.cx_report.split(
    "/")[-1].split(".")[0].replace("-TechRep_1-BioRep_1", "")
  
  for name, group in tqdm(cxReportGeneCouple.groupby("geneAssociated")):
    
      tmpDict = {"couple": group.couple.values[0],
                 "current": name,
                 "ratio": group.ratio.mean(),
                 "chromosome": group.chromosome.values[0],
                 "context": context,
                 "sample": sample,
                 "gene_couple":group.gene_couple.values[0],
                 "variable": group.variable.values[0]
                 }
      annotatedRatio = annotatedRatio.append(tmpDict, ignore_index=True)

  annotatedRatio.to_csv(args.output,
                 sep="\t",
                 index=False)


if __name__ == "__main__":
  main()
