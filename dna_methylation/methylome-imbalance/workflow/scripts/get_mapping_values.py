import argparse
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)
import pandas as pd
import os

def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-c",
      "--mappingFile",
      help="bismarck mapping file",
      type=str,
      nargs="+",
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="output file basename",
      type=str,
      required=True)

  args = parser.parse_args()
  return args



def main():
  args = get_arguments()
  for mappingFile in args.mappingFile:
    print(args.output)
    if os.path.exists(args.output):
        mapping = pd.read_csv(mappingFile, sep='\t')
        mapping.to_csv(args.output, sep='\t', index=False, header=False, mode="a")
    else:
        mapping = pd.read_csv(mappingFile, sep='\t')
        mapping.to_csv(args.output, sep='\t', index=False, header=True, mode="a")

if __name__ == "__main__":
  main()
