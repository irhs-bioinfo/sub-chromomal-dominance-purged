rule remove_chr00:
    """
    This rule aims to filter the cx report, removing Chr00
    """
    input:
        cx_report = "resources/biseps_output/{SRR}-TechRep_1-BioRep_1/methylation_extraction_bismark/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt",
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt"
    conda:
        "../envs/bedtools.yaml"
    threads:
        workflow.cores / 4
    shell:
        """
        #  grep -e 'Chr07' -e 'Chr01' |
        # Remove all Chr00 from query file
        grep -v "Chr00" {input.cx_report} | sort -k 1,1 -k 2n,2 > {output.cleaned_cx_report}
        """


rule filter_cx_report_intron:
    """
    This rule aims to filter the cx report all intronic position.
    """
    input:
        intron="results/introns.csv",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt",
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron",
        cleaned_cx_report_intron = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron.intronic",
        tmpFile = temp("results/tables/cxReportPurged/{SRR}/{SRR}_output.txt")
    threads:
        workflow.cores/4
    benchmark:
        "benchmarks/{SRR}.intronic.benchmark.txt"
    log:
        "logs/filter_cx_report_intron/{SRR}.log"
    conda:
        "../envs/bedtools.yaml"
    shell:
        """
        {scripts}/removing_intronic.sh {input.cx_report} {output.tmpFile} {output.cleaned_cx_report_intron} {output.cleaned_cx_report} {input.intron}
        wc -l {input.cx_report} {output.cleaned_cx_report_intron} {output.cleaned_cx_report}
        """


rule filter_cx_report:
    """
    This rule aims to filter the gene gff file, removing Chr00 mainly. Localisation ohnologous genes, and upstream regions
    """
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat.bed",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered",
    threads:
        workflow.cores/10
    benchmark:
        "benchmarks/{SRR}.genes.benchmark.txt"
    log:
        "logs/filter_cx_report/{SRR}.log"
    conda:
        "../envs/bedtools.yaml"
    shell:
        """
        awk 'BEGIN{{OFS="\t"}} {{print $1, $2-1, $2, $3, $4, $5, $6, $7}}' {input.cx_report} | intersectBed -a - -b {input.ohno} -wa -wb -sorted -nonamecheck -sorted > {output.cleaned_cx_report}
        awk 'BEGIN{{OFS="\t"}} {{print $1, $2, $3, $4, $5, $6, $7, $8, $10, $11, $12}}' {output.cleaned_cx_report} > {output.cleaned_cx_report}.tmpfile && mv {output.cleaned_cx_report}.tmpfile {output.cleaned_cx_report}

        wc -l {input.cx_report} {output.cleaned_cx_report}
        """


use rule filter_cx_report as filter_cx_report_not_in_gene with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat.bed.2kb",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene",
    benchmark:
        "benchmarks/{SRR}.genes.not_in_gene.benchmark.txt"
    log:
        "logs/filter_cx_report/{SRR}.not_in_gene.log"


use rule filter_cx_report as filter_cx_report_2000_up with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat.bed.up.2kb",
        cx_report = "results/tables/cxReportPurged/SRR4252210/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/SRR4252210/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.up.2kb",
    benchmark:
        "benchmarks/SRR4252210.genes.not_in_gene.benchmark.txt"
    log:
        "logs/filter_cx_report/SRR4252210.not_in_gene.up.2kb.log"



use rule filter_cx_report as filter_cx_report_not_in_gene_100 with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat.bed.100bp",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp",
    benchmark:
        "benchmarks/{SRR}.genes.not_in_gene.100bp.benchmark.txt"
    log:
        "logs/filter_cx_report/{SRR}.not_in_gene.100bp.log"


use rule filter_cx_report as filter_cx_report_not_in_gene_500 with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat.bed.500bp",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.noIntron"
    output:
        cleaned_cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp",
    benchmark:
        "benchmarks/{SRR}.genes.not_in_gene.500bp.benchmark.txt"
    log:
        "logs/filter_cx_report/{SRR}.not_in_gene.500bp.log"



rule annotate_ohnologous:
    """
    This rule aims to compute the statistics of desequilibrated.
    """
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv",
        cx_report = expand("results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        ohnoAnnotation="results/tables/ohnoAnnotation/{SRR}/{SRR}.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python3 {scripts}/annotate_ohnologous.py \
        --ohno {input.ohno} \
        --cx_report {input.cx_report} \
        --output {output.ohnoAnnotation}
        """


use rule annotate_ohnologous as annotate_ohnologous_not_in_gene with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv",
        cx_report = expand("results/tables/cxReportPurged/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"])
    output:
        ohnoAnnotation="results/tables/ohnoAnnotation/{SRR}/{SRR}.not_in_gene.csv"
