
rule compute_stat:
    """
    This rule aims to compute the statistics of desequilibrated.
    """
    input:
        cx_report=expand("results/tables/cxReportPurged/{SRR}/gene_body/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.mainPairs.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        stat="results/tables/stat/stat.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    params:
        typeOfAnalysis="gene_body"
    threads:
        workflow.cores/4
    shell:
        """
        python3 {scripts}/compute_stat.py \
        --cx_report {input.cx_report} \
        --output {output.stat} \
        --typeOfAnalysis {params.typeOfAnalysis}
        """


use rule compute_stat as compute_stat_not_in_gene with:
    input:
        cx_report = expand("results/tables/cxReportPurged/{SRR}/2kb_up_down/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.mainPairs.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"])
    output:
        stat="results/tables/stat/stat.not_in_gene.csv"
    params:
        typeOfAnalysis="not_in_gene"


use rule compute_stat as compute_stat_not_in_gene_500bp with:
    input:
        cx_report = expand("results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.mainPairs.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"])
    output:
        stat="results/tables/stat/stat.not_in_gene_500bp.csv"
    params:
        typeOfAnalysis="500bp"


use rule compute_stat as compute_stat_not_in_gene_100bp with:
    input:
        cx_report = expand("results/tables/cxReportPurged/{SRR}/100bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.mainPairs.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"])
    output:
        stat="results/tables/stat/stat.not_in_gene_100bp.csv"
    params:
        typeOfAnalysis="100bp"



# rule ratio_per_gene:
#     """
#     This rule aims to compute the statistics of desequilibrated.
#     """
#     input:
#         cx_report = expand("results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
#     output:
#         ratio_per_gene="results/tables/stat/ratio_per_gene.csv",
#     conda:
#         "../envs/generate_te_stats.yaml"
#     threads:
#         workflow.cores
#     shell:
#         """
#         python3 {scripts}/ratio_per_genes.py \
#         --cx_report {input.cx_report} \
#         --output {output.ratio_per_gene}
#         """

# use rule ratio_per_gene as ratio_per_gene_not_in_gene with:
#     input:
#         cx_report = expand("results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.{context}.csv", SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
#     output:
#         ratio_per_gene="results/tables/stat/ratio_per_gene.not_in_gene.csv"
#     conda:
#         "../envs/generate_te_stats.yaml"
