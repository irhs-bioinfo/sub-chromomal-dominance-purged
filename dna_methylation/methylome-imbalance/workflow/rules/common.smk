from snakemake.utils import validate
import pandas as pd

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

configfile: "config/config.yaml"
# validate(config, schema="../schemas/config.schema.yaml")

samples = pd.read_csv("config/samples.tsv", sep="\t", comment="#")
SAMPLES = samples["sample"]

# Scripts dir to avoid long commands
scripts = "workflow/scripts"
baseNameCx = "TechRep_1-BioRep_1.deduplicated.CX_report.txt"
cxReportBase = "results/tables/cxReportPurged/"
