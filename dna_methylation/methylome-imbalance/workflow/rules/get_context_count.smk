rule get_context_count_2kbUP:
    """
    This rule aims to compute the number of context position
    """
    input:
        cx_report = "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.mainPairs.CHH.csv",
        nonSwapping = "resources/genome/nonSwapping/fullyNonSwapping.refactored.csv",
        anaDiffCounts = "resources/genome/nonSwapping/ohnlogousGenesNonSwapping.refactored.csv",
    output:
        stat="results/tables/context_positions_2kbUp/CHH/SRR4252210.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    params:
        diffMin = 0
    shell:
        """
        python3 {scripts}/check_number_cytosine.py \
        --cx_report {input.cx_report} \
        --nonSwapping {input.nonSwapping} \
        --anaDiffCounts {input.anaDiffCounts} \
        --output {output.stat} \
        --diffMin {params.diffMin}
        """


rule get_context_count_10:
    """
    This rule aims to compute the number of context position
    """
    input:
        cx_report =  "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.mainPairs.CHH.csv",
        nonSwapping = "resources/genome/nonSwapping/fullyNonSwapping.refactored.csv",
        anaDiffCounts = "resources/genome/nonSwapping/ohnlogousGenesNonSwapping.refactored.csv",
    output:
        stat="results/tables/context_positions_10/CHH/SRR4252210.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    params:
        diffMin = 0.1
    shell:
        """
        python3 {scripts}/check_number_cytosine.py \
        --cx_report {input.cx_report} \
        --nonSwapping {input.nonSwapping} \
        --anaDiffCounts {input.anaDiffCounts} \
        --output {output.stat} \
        --diffMin {params.diffMin}
        """


rule get_context_count_2kbUP_sliding_window:
    """
    This rule aims to compute the number of context position
    """
    input:
        cx_report = "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.mainPairs.CHH.csv",
        nonSwapping = "resources/genome/nonSwapping/fullyNonSwapping.refactored.csv",
        anaDiffCounts = "resources/genome/nonSwapping/ohnlogousGenesNonSwapping.refactored.csv",
    output:
        stat="results/tables/context_positions_2kbUp_sliding_window/CHH/SRR4252210.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    params:
        diffMin = 0
    shell:
        """
        python3 {scripts}/check_number_cytosine_sliding_window.py \
        --cx_report {input.cx_report} \
        --nonSwapping {input.nonSwapping} \
        --anaDiffCounts {input.anaDiffCounts} \
        --output {output.stat} \
        --diffMin {params.diffMin}
        """


rule get_mapping_values:
    """
    This rule aims to compute the number of context position
    """
    input:
        multiqc_bismark_alignment = expand("resources/biseps_output/{SRR}-TechRep_1-BioRep_1/{SRR}-TechRep_1-BioRep_1-multiqc_report_data/multiqc_bismark_alignment.txt", SRR=SAMPLES),
    output:
        outputFile="results/tables/mapping_value.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python3 {scripts}/get_mapping_values.py \
        --mappingFile {input.multiqc_bismark_alignment} \
        --output {output.outputFile}
        """
