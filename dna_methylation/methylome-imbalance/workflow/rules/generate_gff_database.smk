
rule filter_gene_gff:
    """
    This rule aims to filter the gene gff file, removing Chr00 mainly
    """
    input:
        gff_file=config["geneGff"],
    output:
        cleaned_gff=temp(config["geneGff"].replace(".gff3", ".filtered.gff")),
        stats=report(
            "workflow/report/removed_genes.txt",
            caption="../report/removed_genes.txt",
            category="Cleaning",
        ),
    conda:
        "../envs/gffutils.yaml"
    shell:
        """
        python3 {scripts}/gff_prep/filter_gff.py \
        --gff {input.gff_file} \
        --output {output.cleaned_gff} \
        --type genes \
        --stats {output.stats}
        """


rule generate_gff_database:
    """
    This rule aims to generate a MySLQ database using GFF file storing gene annotations
    """
    input:
        gff=rules.filter_gene_gff.output.cleaned_gff,
    output:
        db="results/db.sql",
    conda:
        "../envs/gffutils.yaml"
    shell:
        """python {scripts}/gff_prep/generate_gff_database.py \
        --gff {input.gff} \
        --output {output.db}"""


rule generate_intron_database:
    """
    This rule aims to generate a MySLQ database using GFF file storing gene annotations
    """
    input:
        dbGff="results/db.sql",
    output:
        dbIntrons="results/db.intron.sql",
    conda:
        "../envs/gffutils.yaml"
    shell:
        """python {scripts}/gff_prep/generate_intron_database.py \
        --database {input.dbGff} \
        --output {output.dbIntrons}"""


rule generate_intron_file:
    """
    This rule aims to generate a a file storing all intron posiitons
    """
    input:
        dbIntrons="results/db.intron.sql",
    output:
        "results/introns.csv"
    conda:
        "../envs/gffutils.yaml"
    shell:
        """python {scripts}/gff_prep/output_introns_as_csv.py \
        --database {input.dbIntrons} \
        --output {output}"""



rule localise_ohnolgous_genes:
    """
    This rule aims to generate to locate all ohnologoues genes using gff database

    | gene_x       | gene_y       | multiplicon | couple | start_y   | start_y_window | end_y     | end_y_window | start_x   | start_x_window | end_x     | end_x_window |
    |--------------|--------------|-------------|--------|-----------|----------------|-----------|--------------|-----------|----------------|-----------|--------------|
    | MD13G1031100 | MD16G1033300 | 1           | 13-16  | 2413174.0 | 2411175.0      | 2418713.0 | 2420712.0    | 2233716.0 | 2231717.0      | 2239987.0 | 2241986.0    |
    | MD13G1031200 | MD16G1033400 | 1           | 13-16  | 2421767.0 | 2419768.0      | 2422383.0 | 2424382.0    | 2240890.0 | 2238891.0      | 2243174.0 | 2245173.0    |

    """
    input:
        dbIntrons="results/db.intron.sql",
        chromosomeSize="resources/genome/chromosome_size.tsv",
        ohnologousGenes="resources/genome/ohnologousGenes.csv",
    output:
        ohnoLoc="resources/genome/ohnologousGenes_localised.csv",
        ohnoLocFlat="resources/genome/ohnologousGenes_localised.csv.flat",
        notOhno="resources/genome/ohnologousGenes_localised.csv.notOhno",
    conda:
        "../envs/gffutils.yaml"
    shell:
        """python {scripts}/gff_prep/localisation_ohnologous_genes.py \
        --database {input.dbIntrons} \
        --chromosomeSize {input.chromosomeSize} \
        --ohnologousGenes {input.ohnologousGenes} \
        --output {output.ohnoLoc}"""
