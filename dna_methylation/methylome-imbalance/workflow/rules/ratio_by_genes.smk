rule ratio_by_genes:
    """
    This rule aims to compute the statistics of desequilibrated.
    """
    input:
        cx_report = "results/tables/cxReportPurged/{SRR}/gene_body/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.mainPairs.{context}.csv"
    output:
        cxReportPurgedGenes= "results/tables/cxReportPurgedGenes/{SRR}/gene_body/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv"
    threads:
        workflow.cores/10
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python3 {scripts}/ratio_per_genes.py \
        --cx_report {input.cx_report} \
        --output {output.cxReportPurgedGenes}
        """


use rule ratio_by_genes as ratio_by_genes_100 with:
    input:
        cx_report = "results/tables/cxReportPurged/{SRR}/100bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.mainPairs.{context}.csv"
    output:
        cxReportPurgedGenes = "results/tables/cxReportPurgedGenes/{SRR}/100bp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",


use rule ratio_by_genes as ratio_by_genes_2kb with:
    input:
        cx_report = "results/tables/cxReportPurged/{SRR}/2kb_up_down/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.mainPairs.{context}.csv"
    output:
        cxReportPurgedGenes = "results/tables/cxReportPurgedGenes/{SRR}/2kbUp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",


use rule ratio_by_genes as ratio_by_genes_500bp with:
    input:
        cx_report = "results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.mainPairs.{context}.csv"
    output:
        cxReportPurgedGenes = "results/tables/cxReportPurgedGenes/{SRR}/500bp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",


rule mean_by_genes:
    """
    This rule aims to compute the statistics of desequilibrated.
    """
    input:
        cx_report = expand("results/tables/cxReportPurgedGenes/{SRR}/gene_body/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        cxReportPurgedMeanGenes= "results/tables/gene_body.csv"
    threads:
        workflow.cores/10
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python3 {scripts}/mean_by_genes.py \
        --cx_report {input.cx_report} \
        --output {output.cxReportPurgedMeanGenes}
        """


use rule mean_by_genes as mean_by_genes_100 with:
    input:
        cx_report = expand("results/tables/cxReportPurgedGenes/{SRR}/100bp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        cxReportPurgedMeanGenes = "results/tables/100bp.csv"


use rule mean_by_genes as mean_by_genes_2kb with:
    input:
        cx_report = expand("results/tables/cxReportPurgedGenes/{SRR}/2kbUp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        cxReportPurgedMeanGenes = "results/tables/2kb.csv"


use rule mean_by_genes as mean_by_genes_500bp with:
    input:
        cx_report = expand("results/tables/cxReportPurgedGenes/{SRR}/500bp/{SRR}.CX_report.txt.filtered.geneCouple.mainPairs.geneMean.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    output:
        cxReportPurgedMeanGenes = "results/tables/500bp.csv"
