rule construct_bed_ohno_window:
    """
    This rule aims to filter the cx report, removing Chr00 and all intronic position.
    Making a templates of intronic position improving performance of next filters.
    """
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.flat",
    output:
        ohnoFlat="resources/genome/ohnologousGenes_localised.csv.flat.bed",
        ohno2kb="resources/genome/ohnologousGenes_localised.csv.flat.bed.2kb",
        ohno2kbUp="resources/genome/ohnologousGenes_localised.csv.flat.bed.up.2kb",
        ohno500bp="resources/genome/ohnologousGenes_localised.csv.flat.bed.500bp",
        ohno100bp="resources/genome/ohnologousGenes_localised.csv.flat.bed.100bp",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        {scripts}/generate_bed.sh \
        {input.ohno} \
        {output.ohnoFlat} \
        {output.ohno2kb} \
        {output.ohno500bp} \
        {output.ohno100bp} \
        {output.ohno2kbUp} \
        """

rule construct_melted_ohno_for_merging:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv",
    output:
        ohnoMelted = "resources/genome/ohnologousGenes_localised.csv.melted",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python {scripts}/melt_ohno.py --input {input.ohno} --output {output.ohnoMelted}
        """