rule filter_context_annotate_ohnologous_genes:
    """
    This rule aims to reconstruct gene couples and compute ratio
    """
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.melted",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered",
    output:
        expand("results/tables/cxReportPurged/{{SRR}}/gene_body/{{SRR}}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    params:
        cx_report="results/tables/cxReportPurged/{SRR}/gene_body/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.",
    conda:
        "../envs/generate_te_stats.yaml"
    log:
        "logs/filter_context_annotate_ohnologous_genes/{SRR}.log"
    threads:
        workflow.cores
    shell:
        """
        python3 {scripts}/filter_cx_report_context.py \
        --ohno {input.ohno} \
        --cx_report {input.cx_report} \
        --output {params.cx_report}

        wc -l {input.cx_report} {output} > {log}
        """


rule filter_cx_report_main_pairs:
    """
    This rule aims to filter on reconstructed pairs keeping only main
    """
    input:
        "results/tables/cxReportPurged/{SRR}/gene_body/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.{context}.csv"
    output:
        "results/tables/cxReportPurged/{SRR}/gene_body/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.geneCouple.mainPairs.{context}.csv"
    conda:
        "../envs/bedtools.yaml"
    log:
        "logs/filter_cx_report_main_pairs/{SRR}_{context}.log"
    threads:
        workflow.cores/4
    shell:
        """
        grep 'chromosome\|08-15\|13-16\|01-07\|09-17\|03-11\|05-10\|02-15\|06-14' {input} > {output}
        wc -l {input} {output} > {log}
        """


############## Parsing file 2kb up and downstream ##############

use rule filter_context_annotate_ohnologous_genes as filter_context_annotate_ohnologous_genes_not_in_gene with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.melted",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene"
    output:
        expand("results/tables/cxReportPurged/{{SRR}}/2kb_up_down/{{SRR}}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    params:
        cx_report="results/tables/cxReportPurged/{SRR}/2kb_up_down/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.",
    log:
        "logs/filter_context_annotate_ohnologous_genes_not_in_gene/{SRR}.log"


use rule filter_cx_report_main_pairs as filter_cx_report_main_pairs_not_in_gene with:
    input:
        "results/tables/cxReportPurged/{SRR}/2kb_up_down/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.{context}.csv"
    output:
        "results/tables/cxReportPurged/{SRR}/2kb_up_down/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.geneCouple.mainPairs.{context}.csv"
    log:
        "logs/filter_cx_report_main_pairs_not_in_gene/{SRR}_{context}.log"


############## Parsing file 100bp upstream ##############

use rule filter_context_annotate_ohnologous_genes as filter_context_annotate_ohnologous_genes_not_in_gene_100 with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.melted",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp"
    output:
        expand("results/tables/cxReportPurged/{{SRR}}/100bp/{{SRR}}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    params:
        cx_report="results/tables/cxReportPurged/{SRR}/100bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.",
    log:
        "logs/filter_context_annotate_ohnologous_genes_not_in_gene_100/{SRR}.log"


use rule filter_cx_report_main_pairs as filter_cx_report_main_pairs_not_in_gene_100 with:
    input:
        "results/tables/cxReportPurged/{SRR}/100bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.{context}.csv"
    output:
        "results/tables/cxReportPurged/{SRR}/100bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.100bp.geneCouple.mainPairs.{context}.csv"
    log:
        "logs/filter_cx_report_main_pairs_not_in_gene_100/{SRR}_{context}.log"

############## Parsing file 500bp upstream ##############


use rule filter_context_annotate_ohnologous_genes as filter_context_annotate_ohnologous_genes_not_in_gene_500 with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.melted",
        cx_report = "results/tables/cxReportPurged/{SRR}/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp"
    output:
        expand("results/tables/cxReportPurged/{{SRR}}/500bp/{{SRR}}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.{context}.csv",SRR=SAMPLES, context=["CHH", "CHG", "CG"]),
    params:
        cx_report="results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.",
    log:
        "logs/filter_context_annotate_ohnologous_genes_not_in_gene_500/{SRR}.log"


use rule filter_cx_report_main_pairs as filter_cx_report_main_pairs_not_in_gene_500 with:
    input:
        "results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.{context}.csv"
    output:
        "results/tables/cxReportPurged/{SRR}/500bp/{SRR}-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.500bp.geneCouple.mainPairs.{context}.csv"
    log:
        "logs/filter_cx_report_main_pairs_not_in_gene_500/{SRR}_{context}.log"


############## Parsing file 2kbUp upstream ##############


use rule filter_context_annotate_ohnologous_genes as filter_context_annotate_ohnologous_genes_not_in_gene_2kbUp with:
    input:
        ohno="resources/genome/ohnologousGenes_localised.csv.melted",
        cx_report = "results/tables/cxReportPurged/SRR4252210/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.up.2kb"
    output:
        "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.CHH.csv",
    params:
        cx_report="results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.",
    log:
        "logs/filter_context_annotate_ohnologous_genes_not_in_gene_2kbUp/SRR4252210.log"


use rule filter_cx_report_main_pairs as filter_cx_report_main_pairs_not_in_gene_2kbUp with:
    input:
        "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.CHH.csv"
    output:
        "results/tables/cxReportPurged/SRR4252210/2kbUp/SRR4252210-TechRep_1-BioRep_1.deduplicated.CX_report.txt.filtered.not_in_gene.2kbUp.geneCouple.mainPairs.CHH.csv"
    log:
        "logs/filter_cx_report_main_pairs_not_in_gene_2kbUp/SRR4252210_CHH.log"

