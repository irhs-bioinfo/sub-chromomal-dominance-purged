"""
Utility functions for snakemake pipeline
"""
from typing import List
import pandas as pd


def samples2metadata(db_sra, samples: List[str], config: dict) -> dict:
  """
  Get the required info from a list of samples following to case:
  - If a sample already exists locally, we only want to know if it is paired-end or single-end.
  - If a sample does not exist locally
    - find its corresponding SRX number and all runs that belong to it,
    - check if they all have the same layout, if not, crash
    - see if we can download the runs from ena

  output:
    {
      "GSM1234": {"layout": "PAIRED",
             "runs": ["SRR1234", "SRR4321"],
             "ena_fastq_ftp": {...},
      "SRR5678": {"layout": "SINGLE",
            "runs": ["SRR5678"],
            ena_fastq_ftp: None,
    }
  """
  # Start with a dictionary filled with all samples
  sampledict = {sample: dict() for sample in samples}
  # Initialise a result dictionnary
  sampleToFill = {}

  # Now add the already SRA compliant names with a reference to itself
  sampleToFill.update(
      {sample: sample for sample in samples})
  # Check samples on sra
  try:
    # Need detailled flag to allow getting download URL
    df_sra = db_sra.sra_metadata(list(sampleToFill.values()), detailed=True)
  except:
    df_sra = pd.DataFrame()

  # Iterate on dictionnary of results
  for sample, clean in sampleToFill.items():
    # table indices
    idxs = df_sra.index[df_sra["run_accession"] == sample].tolist()
    # get all runs that belong to the sample
    runs = df_sra.loc[idxs]["run_accession"].tolist()

    sampledict[sample]["runs"] = runs
    # get the layout
    layout = df_sra.loc[idxs]["library_layout"].values[0]
    sampledict[sample]["layout"] = layout
    # get the ena url
    sampledict[sample]["ena_fastq_ftp"] = dict()
    for run in runs:
      if layout == "SINGLE":
        sampledict[sample]["ena_fastq_ftp"][run] = df_sra[df_sra["run_accession"] ==
                                                          run]["ena_fastq_ftp"].tolist()
      elif layout == "PAIRED":
        sampledict[sample]["ena_fastq_ftp"][run] = df_sra[df_sra["run_accession"] == run]["ena_fastq_ftp_1"].tolist() + df_sra[
            df_sra["run_accession"] == run]["ena_fastq_ftp_2"].tolist()
      else:
        sampledict[sample]["ena_fastq_ftp"] = None

  return {**sampledict}
