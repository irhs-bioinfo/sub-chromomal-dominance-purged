import yaml
import argparse


def config_parser(configPath):
  """
  A function to open a configuration file and load it in a dictionnary

  Parameters
  ----------
  configPath : string
      Path of config file

  Returns
  -------
  dictionnary
      dictionnary storing configuration
  """
  with open(configPath) as configFile:
    configObject = yaml.safe_load(configFile)
  return configObject
