# %% [markdown]
"""
## Few numbers:

"""
# %%
import time

import numpy as np
import pandas as pd
from lib.config_parser import config_parser
from lib.requesting_on_web import get_metadata, get_read_length, get_size_of_file
from pysradb import SRAweb
from tqdm import tqdm

# %%
# Load configuration
tqdm.pandas()


conf = config_parser("../../config/get_sra_config.yml")
# conf = override_config_with_args(conf, args)

# Init empty Dataframes
allMetadataRunAccession = pd.DataFrame()
allMetadataStudyAccession = pd.DataFrame()

# %%
# Init SRA db in web fashion not SQLite DB
db = SRAweb()
# Search for in SRA with Malus
df = db.search_sra(search_str=conf["organismName"])

dfRNASeq = df[(df["library_strategy"] == conf["library_strategy"]) &
              (df["total_size"].astype(int) > conf["total_size"])]
dfMalus = dfRNASeq[(dfRNASeq["organism_name"] == conf["organismName"])]
library_layout = dfMalus[dfMalus["library_layout"] == conf["library_layout"]]
library_layout = library_layout.drop_duplicates()

# Save all run ID (SRR ID) in a list
allRunAccessionList = library_layout["run_accession"].to_list()
# Save all study ID (SRP ID) in a list
allStudyAccessionList = library_layout["study_accession"].unique()
# %%
################################ Get Metadata ################################
for i in tqdm(range(0, len(allRunAccessionList), 99)):
  allMetadataRunAccession = get_metadata(IDs=allRunAccessionList[i:i + 99],
                                         resultsDf=allMetadataRunAccession,
                                         saveInAseparateFile=False,
                                         conf=conf)

# %%
######## Get Metadata of each study in a separate file ########
for srp in tqdm(allStudyAccessionList):
  allMetadataStudyAccession = get_metadata(IDs=srp,
                                           resultsDf=allMetadataStudyAccession,
                                           saveInAseparateFile=True,
                                           conf=conf)

# %%
allSRR = pd.concat(
    [allMetadataRunAccession, allMetadataStudyAccession], ignore_index=True)
allSRR = allSRR.drop_duplicates(
  subset=["run_accession", "study_accession", "experiment_accession"])
allSRR = allSRR.replace('N/A', np.NaN)
allSRR = allSRR.replace('', np.NaN)
allSRR = allSRR.loc[allSRR["library_layout"] == "PAIRED"]
allSRR = allSRR.loc[allSRR["library_strategy"] == conf["library_strategy"]]
if conf["debug"]:
  allSRR = allSRR.head(100)


# %%
######## Get read length of each run, drop all under 125 bp ########
allSRR["read_length"] = np.nan
allSRR["read_length"] = allSRR['run_accession'].progress_apply(
    get_read_length)
allSRRNotFilteredOnReadLength = allSRR.copy()
allSRR = allSRR[allSRR["read_length"].astype(int) > 100]
allSRR = allSRR.dropna(axis=1, how='all')


# %%
######## Get size of all files ########
# If file size is asked
if conf["fileSize"]:
  # Iterate on columns
  for number in ["1", "2"]:
    # If columns not exist, create it and init to nan
    if "size_file" + number not in allSRR:
      allSRR["size_file" + number] = np.nan
    # For all nan values, get file size
    allSRR["size_file" + number] = allSRR[(allSRR['ena_fastq_http_' + number].notna()) & (allSRR["size_file" + number].isna())]['ena_fastq_http_' + number].progress_apply(
        get_size_of_file)

# %%
if conf["debug"]:
  print("Number of runs found with current filter (specie={}): {}".format(
    conf["organismName"], str(df.shape[0])))
  print("Number of runs found with current filter (library_strategy={} and total_size> {}): {}".format(
    conf["library_strategy"], conf["total_size"], str(dfRNASeq.shape[0])))
  print("Number of runs found with current filter (library_layout={}: ".format(
    conf["library_layout"], str(library_layout.shape[0])))
  print("Number of unique study: {}".format(
    str(allMetadataRunAccession["study_accession"].unique().shape[0])))
  print("Number of unique experiment: " +
        str(allMetadataRunAccession["experiment_accession"].unique().shape[0]))
  print("By filtering on read length, final results are:")

print("Number of SRR IDs: " +
      str(allSRR["run_accession"].unique().shape[0]))
print("Number of SRP IDs: " +
      str(allSRR["study_accession"].unique().shape[0]))
print("Number of SRX IDs: " +
      str(allSRR["experiment_accession"].unique().shape[0]))
if conf["fileSize"]:
  print("Total size planned: " +
        str(allSRR.loc[:, ['size_file1', 'size_file2']].sum(axis=1).sum()) +
        " GB")
# %%

allSRR = allSRR.sort_values(
    ["run_accession", "study_accession", "experiment_accession"])

allSRR["replicate"] = allSRR["experiment_batch"].fillna('')
allSRR["replicate"] = "biological replicate " + \
    allSRR["replicate"].loc[allSRR["replicate"].str.isdigit()]

allSRR["studied_tissue"] =  allSRR["tissue"].fillna('')

######## Output all dataframes in CSV format########

allSRRPurged = allSRR.drop(columns=[
    # URL columns
    'sra_url_alt',
    'sra_url',
    'ena_fastq_http',
    'ena_fastq_http_1',
    'ena_fastq_http_2',
    'ena_fastq_ftp',
    'ena_fastq_ftp_1',
    'ena_fastq_ftp_2',
    'sra_url_alt1',
    'sra_url_alt2',
    'md5',
    # Institute columns
    'INSDC center alias',
    'INSDC center name',
    'INSDC first public',
    'INSDC last update',
    'INSDC status',
    # 'run_alias',
    'experiment_alias',
    # Concatenated columns
    "experiment",
    "experiment_batch",
    "specimen with known storage state",
    "scab resistance",
    "genotype",
    "infect",
    "title",
    "dev_stage",
    "ecotype",
    # "growth habit",
    "phenotype",
    "Replicate",
    "organism",
    "organism part",
    "tissues",
    "tissue",
    "tissue_type",
    "source_name",
    # "variety",
    # "apple variety",
    "temp",
    # Useless columns
    # "population",
    "ENA checklist",
    "collected_by",
    "sample_type",
    "extract protocol",
    "geo_loc_name",
    "alias",
    "Alias",
    "geographic location (country and/or sea)",
    # "read count",
    # "base count",
    # "strain",
    "molecule",
    # "sample_title",
    # "note",
    "broker name",
    "cell_type",
    "biomaterial_provider",
    "BioSampleModel",
    # Duplicated informations
    "organism",
    "SRA accession",
    "Sample Name"
], axis=1,
    errors='ignore')

allSRR.reset_index(drop=True, inplace=True)
allSRR.to_csv(conf["outputPath"] + 'allSRR.csv', sep=',')
allSRRPurged.to_csv(conf["outputPath"] + 'allSRR.purged.csv', sep=',')


# %%

if conf["OutputToDowloadPipeline"]:
  allSRRPurged = pd.read_csv(
    conf["outputPath"] + 'allSRR.purged.csv', sep=',', index_col=0, header=0)

  allSRRToDl = allSRRPurged[["run_accession",
                            "study_accession", "experiment_accession"]]
  allSRRToDl = allSRRToDl.rename(columns={"run_accession": "sample"})

  allSRRToDl.to_csv(
      "../../config/samples.tsv", sep='\t', index=False)

  allSRRToDl = allSRRPurged[["run_accession",
                             "study_accession",
                             "experiment_accession",
                             "replicate",]]
  allSRRToDl = allSRRToDl.rename(columns={"run_accession": "sample"})

  allSRRToDl.to_csv(
      "../../config/units.tsv", sep='\t', index=False)
# %%
