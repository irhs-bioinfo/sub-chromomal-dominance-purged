import yaml
import numpy as np
import pandas as pd
from snakemake.utils import validate, min_version
from scripts.util import samples2metadata
from time import sleep
import pysradb

# make sure the snakemake version corresponds to version in environment
min_version("5.18")

############################ Handle with some configuration ###################
configfile: "config/config.yaml"

validate(config, schema="../schemas/config.schema.yaml")

# Load samples.tsv
samples = pd.read_csv(config["samples"], sep='\t', dtype='str', comment='#')
validate(samples, schema="../schemas/samples.schema.yaml")

# check if paired-end filename suffixes are lexicographically ordered
config['fqext'] = [config['fqext1'], config['fqext2']]

############################ Handle with input file ###################

# Add column
samples.columns = samples.columns.str.strip()
# Make smaple column as index
samples = samples.set_index('sample')
samples.index = samples.index.map(str)


############################ Get metadatas ###################

# Get SRA DB initialised
db_sra = pysradb.SRAweb()

# Init an empty dictionnary
sampleDict = {}
# make a collection of all samples
all_samples = [sample for sample in samples.index]
# Check for sample not in local needing to get metadata online
missing_samples = [sample for sample in all_samples if sample not in sampleDict.keys()]

stepSize = min(len(missing_samples), 99)
for i in range(0, len(missing_samples), stepSize):
  sampleDict.update(samples2metadata(db_sra, missing_samples[i:i+stepSize], config))
  sleep(10)

# Only keep samples for this run
sampleDict = {sample: values for sample, values in sampleDict.items() if sample in all_samples}
# Now check where to download which sample
# Get download link per run
run2download = dict()
for sample, values in sampleDict.items():
  for run in values.get("runs", []):
    if values["ena_fastq_ftp"] and values["ena_fastq_ftp"][run]:
      if config.get("ascp_path") and config.get("ascp_key"):
        run2download[run] = [url.replace("uk/", "uk:").replace("ftp", "era-fasp@fasp") for url in values["ena_fastq_ftp"][run]]
      else:
        run2download[run] = [url.replace("era-fasp@fasp", "ftp") for url in values["ena_fastq_ftp"][run]]
