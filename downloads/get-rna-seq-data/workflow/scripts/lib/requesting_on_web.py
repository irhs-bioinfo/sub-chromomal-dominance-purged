import re
import time
import pandas as pd
import requests
from pysradb import SRAweb


def get_metadata(IDs, resultsDf, conf, saveInAseparateFile=False):
  """[summary]

  Parameters
  ----------
  IDs : [type]
      [description]
  resultsDf : [type]
      [description]
  conf : [type]
      [description]
  saveInAseparateFile : bool, optional
      [description], by default False

  Returns
  -------
  [type]
      [description]
  """
  # Try to get metadatas on EBI
  db = SRAweb()
  try:
    df = db.sra_metadata(IDs,
                         assay=False,
                         sample_attribute=True,
                         detailed=True,
                         expand_sample_attributes=False,
                         output_read_lengths=True,
                         acc_is_searchstr=False)
    # Filter on organism name
    df = df.loc[df['organism_name'] == conf["organismName"]]
  except:
    print("no result found for" + str(IDs))
    df = pd.DataFrame()
  # Sleep to avoid ban
  time.sleep(1)
  # If asked save metadata in a CSV file
  if saveInAseparateFile:
    df.to_csv(conf["outputPath"] + IDs + '.csv', sep=',')
  # Concatennate results
  resultsDf = pd.concat([resultsDf, df], ignore_index=True)
  # Drop possible duplciated row
  resultsDf = resultsDf.drop_duplicates()
  return resultsDf


def get_read_length(SRRID):
  """This function aims to request to an URL on NCBI,
  get metadat on SRR and return read_length

  Parameters
  ----------
  URL : string
      SSR ID

  Returns
  -------
  int
      Read Length
  """
  # Init result var
  readLength = 0
  if SRRID != "":
    # Get JSON file associated to current SRR ID from SRA
    req = requests.get(
        "http://trace.ncbi.nlm.nih.gov/Traces/sra/?run_spot=" + SRRID)
    # If request is OK
    if req.status_code == 200:
      time.sleep(1)
      # Decode response with right encoding, split on \n
      my_dict = req.content.decode(req.encoding).split('\n')
      # Iterate on it
      for line in my_dict:
        # If line contains read_len
        if re.match('read_len:', line):
          # Get first group of digit
          readLength = re.search(r'\d+', line).group()
          # And return it
          return readLength


def get_size_of_file(URL):
  """This function aims to request to an URL,
  get Content lenght in HTML header and output size of file in GigaBytes

  Parameters
  ----------
  URL : string
      URL string SSR file

  Returns
  -------
  float
      Size of SRR file in GigaBytes
  """
  size = 0
  attempt = 0
  while attempt < 4:
    if pd.notna(URL) and URL != '':
      req = requests.head(URL)
      if req.status_code == 200:
        size = int(req.headers.get('content-length'))
        if URL.startswith('https://sra-download.ncbi.nlm.nih.gov/'):
          size = size / 2
        break
      else:
        attempt += 1
        time.sleep(1)
    else:
      break
  return size / (10 ** 9)
