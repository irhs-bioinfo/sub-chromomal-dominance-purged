def get_arguments():
  """[summary]

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i", "--input",
      help="Input Project ID to add to script")
  parser.add_argument(
      "-o", "--output",
      help="Output base directory for sra metadata files")
  parser.add_argument(
      "-s", "--sample",
      help="Output path for sample.tsv file, inout file for snakemake file")
  parser.add_argument(
      "-c", "--config",
      help="Path to config file",
      default="../../config/get_sra_config.yml")

  args = parser.parse_args()
  return args


def override_config_with_args(conf, args):
  """[summary]

  Parameters
  ----------
  conf : [type]
      [description]
  args : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """

  if args.output:
    conf["outputPath"] = args.output
  if args.input:
    conf["inputFilePath"] = args.input
  return conf
