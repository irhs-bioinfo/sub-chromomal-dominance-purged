"""
This set of rules aims to download single ended or paired end runs from NCBI servers using sra-toolkit.
"""

rule prefetchDownload:
  """
  Download the SRA of a sample by its unique identifier.

  Tries first downloading with the faster ascp protocol, if that fails it
  falls back on the slower http protocol.
  """
  output:
    temp(directory(expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}/srafile", **config))),
  log:
    expand("{log_dir}/run2sra/{{study_acession}}/{{experiment_acession}}/{{run}}.log", **config),
  benchmark:
    expand("{benchmark_dir}/run2sra/{{study_acession}}/{{experiment_acession}}/{{run}}.benchmark.txt", **config)[0]
  message: 
    "Public samples were downloaded from the Sequence Read Archive"
  conda:
    "../envs/get_fastq.yml"
  params:
    log_level="debug"
  shell:
    """
    # three attempts
    for i in {{1..3}}
    do
      # dump, if prefetch is OK, break loop
      prefetch \
      --output-directory {output} \
      --log-level {params.log_level} \
      --progress {wildcards.run} >> {log} 2>&1 && break
      sleep 10
    done
    """


def define_output_path(config):
  if config["fqext"] == "":
    return expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}.{fqsuffix}.gz", **config)
  else:
    return expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}_{fqext}.{fqsuffix}.gz", **config)


rule sra_download:
  """
  Downloaded (raw) SRAs are converted to single-end fastq files.
  """
  input:
    rules.prefetchDownload.output,
  output:
    fastq=expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}_{fqext}.{fqsuffix}.gz", **config),
    tmpdir=temp(directory(expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/tmp/{{run}}", **config))),
  log:
    expand("{log_dir}/sra2fastq_PE/{{study_acession}}/{{experiment_acession}}/{{run}}.log", **config),
  benchmark:
    expand("{benchmark_dir}/sra2fastq_PE/{{study_acession}}/{{experiment_acession}}/{{run}}.benchmark.txt", **config)[0]
  threads: 8
  conda:
    "../envs/get_fastq.yml"
  shell:
    """
    # dump to tmp dir
    parallel-fastq-dump -s {input}/* -O {output.tmpdir} \
    --threads {threads} \
    --split-e \
    --skip-technical \
    --dumpbase \
    --readids \
    --clip \
    --read-filter pass \
    --defline-seq '@$ac.$si.$sg/$ri' \
    --defline-qual '+' --gzip  >> {log} 2>&1 
    

    # rename file and move to output dir
    mv {output.tmpdir}/*_1* {output.fastq[0]}
    mv {output.tmpdir}/*_2* {output.fastq[1]}
    """

# rule move_files:
#   """
#   """
#   input:
#     rules.sra_download.output
#   output:
#     define_output_path(config),
#   run:
#     # rename file and move to output dir
#     if config["fqext"] == "":
#       shell("mv {input}/*.fastq.gz {output}")
#     else:
#       shell("mv {input}/*_1* {output}")
#       shell("mv {input}/*_2* {output}")
