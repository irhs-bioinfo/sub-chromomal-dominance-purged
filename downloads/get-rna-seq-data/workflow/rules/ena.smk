"""
This set of rules aims to download single ended or paired end runs from ENA ebi server.
"""

def define_output_path(config):
  if config["fqext"] == "":
    return expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}.{fqsuffix}.gz", **config)
  else:
    return expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{run}}_{fqext}.{fqsuffix}.gz", **config)


rule ena_download:
  """
  Download paired-end fastq files directly from the ENA. This download can be done using to fashions depending on accessible tools.
  If aspera is installed, this protocol, more fast will be use. If not, wget is used.
  """
  output:
    define_output_path(config)
  log:
    expand("{log_dir}/enaDownloads/{{study_acession}}/{{experiment_acession}}/{{run}}.log", **config)
  benchmark:
    report(expand("{benchmark_dir}/enaDownloads/{{study_acession}}/{{experiment_acession}}/{{run}}.benchmark.txt", **config)[0])
  threads: 
    workflow.cores * 0.33
  run:
    url = run2download[wildcards["run"]]
    for i in range(0, len(url)):
      urTmp = url[i]
      outputTmp = output[i]
      if config.get('ascp_path') and config.get('ascp_key'):
        # Aspera parameters
        # -Q Enables the fair transfer policy, which ensures that the available bandwidth is shared amongst other traffic and transfers at a fair rate
        # -T Disables encryption for maximum throughput
        # -L Creates an error log
        # -DDD debug level max!!! Fill log dir very quickly..
        # -k2 Enables fatal transfer restarts
        # -l Maximum bandwidth
        shell("{config[ascp_path]} -QT -k 1 -P33001 -i {config[ascp_key]} {urTmp} {outputTmp} >> {log} 2>&1")
      else:
        shell("mkdir -p {config[fastq_dir]}/tmp/")
        shell("wget {urTmp} -O {outputTmp} --waitretry 20 >> {log} 2>&1")
