def get_runs_from_sample(wildcards):
  run_fastqs = []
  for run in sampledict[wildcards.sample.split('_')[0]]["runs"]:
    run_fastqs.append(f"{config['fastq_dir']}/{wildcards.study_acession}/{wildcards.experiment_acession}/{run}{wildcards.suffix}")
  return run_fastqs


rule runs2sample:
  """
  Concatenate a single run or multiple runs together into a fastq
  """
  input:
    get_runs_from_sample
  output:
    expand("{fastq_dir}/{{study_acession}}/{{experiment_acession}}/{{sample}}{{suffix}}", **config),
  log:
    expand("{log_dir}/run2sample/{{study_acession}}/{{experiment_acession}}/{{sample}}{{suffix}}.log", **config),
  benchmark:
    expand("{benchmark_dir}/run2sample/{{study_acession}}/{{experiment_acession}}/{{sample}}{{suffix}}.benchmark.txt", **config)[0]
  run:
    shell("cp {input[0]} {output}")
    # now append all the later ones
    for i in range(1, len(input)):
      inputfile = input[i]
      shell("cat {inputfile} >> {output}")
