if config["trimmer"] == "trimmomatic" and config["params"]["trimming"] == True:

    ruleorder: trimmomatic_pe > trimmomatic_se

    rule trimmomatic_pe:
        input:
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz",
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz",
        output:
            r1=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz"
            ),
            r1_unpaired=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_1Unpaired.fastq.gz"
            ),
            r2=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz"
            ),
            r2_unpaired=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_2Unpaired.fastq.gz"
            ),
        conda:
            "../envs/trimmomatic.yaml"
        log:
            "logs/trimmomatic/{studyAcession}/{experimentationAcession}/{runAcession}.log",
        params:
            trimmer=config["params"]["trimmomatic-pe"]["trimmer"],
            trimmeropts=config["params"]["trimmomatic-pe"]["trimmer-options"],
            adapters=config["resources"]["adapters"],
            # optional parameters
            extra="",
        threads: 2
        shell:
            """
            trimmomatic PE -phred33 \
            -threads {threads} \
            -trimlog {log} \
            {input} \
            {output} \
            {params.trimmer}:{params.adapters}:{params.trimmeropts} {params.extra} \
            2> {log}
            """

    rule trimmomatic_se:
        input:
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}.fastq.gz",
        output:
            r1=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}.fastq.gz"
            ),
            r1_unpaired=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}Unpaired.fastq.gz"
            ),
        conda:
            "../envs/trimmomatic.yaml"
        log:
            "logs/trimmomatic/{studyAcession}/{experimentationAcession}/{runAcession}.log",
        params:
            trimmer=config["params"]["trimmomatic-pe"]["trimmer"],
            trimmeropts=config["params"]["trimmomatic-pe"]["trimmer-options"],
            adapters=config["resources"]["adapters"],
            # optional parameters
            extra="",
        threads: 2
        shell:
            """
            trimmomatic SE -phred33 \
            -threads {threads} \
            -trimlog {log} \
            {input} \
            {output} \
            {params.trimmer}:{params.adapters}:{params.trimmeropts} {params.extra} \
            2> {log}
            """


if config["trimmer"] == "cutadapt" and config["params"]["trimming"] == True:

    ruleorder: cutadapt_pe > cutadapt_se

    rule cutadapt_pe:
        input:
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz",
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz",
        output:
            fastq1=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz"
            ),
            fastq2=temp(
                "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz"
            ),
            qc="results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}.qc.txt",
        params:
            # https://cutadapt.readthedocs.io/en/stable/guide.html#adapter-types
            adapters="-a AGAGCACACGTCTGAACTCCAGTCAC -g AGATCGGAAGAGCACACGT -A AGAGCACACGTCTGAACTCCAGTCAC -G AGATCGGAAGAGCACACGT",
            # https://cutadapt.readthedocs.io/en/stable/guide.html#
            others="--minimum-length 1 -q 20",
        log:
            "logs/cutadapt/{studyAcession}/{experimentationAcession}/{runAcession}.log",
        wrapper:
            "0.67.0/bio/cutadapt/pe"

    rule cutadapt_se:
        input:
            "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}.fastq.gz",
        output:
            fastq=temp(
                "resources/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}.fastq.gz"
            ),
            qc="results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}.qc.txt",
        params:
            "-a {} {}".format(
                config["resources"]["adapters"], config["params"]["cutadapt-se"]
            ),
        log:
            "logs/cutadapt/{studyAcession}/{experimentationAcession}/{runAcession}.log",
        wrapper:
            "0.67.0/bio/cutadapt/se"
