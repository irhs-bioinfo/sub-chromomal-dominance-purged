from snakemake.utils import validate
import pandas as pd

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

configfile: "config/config.yaml"

validate(config, schema="../schemas/config.schema.yaml")

# Load samples.tsv
samples = pd.read_csv(config["samples"], sep='\t', dtype='str', comment='#')
# Add column
samples.columns = samples.columns.str.strip()
# Scripts dir to avoid long commands
scripts = "workflow/scripts"


def get_reads(wildcards):
  """
    Function that returns the reads for any aligner.
  """
  if config["params"]["trimming"] == True:
    return {"r2": "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz",
            "r1": "results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz"}
  else:
    return {"r2": "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz",
            "r1": "resources/fastq/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz"}