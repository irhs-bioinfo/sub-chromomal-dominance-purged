"""
This modile aims to make alignement. This can be done using two different tool:
Salmon, making pseudo mapping
STAR, making a real mapping.
Results seems to be quite similar but STAR is more computing consummer and needs also a trimming step
"""
if config["aligner"] == "salmon":

    rule salmon_index:
        """
        Aims to build an index on our transcriptome.
        The index is a structure that salmon uses to quasi-map RNA-seq reads during quantification.
        This steps need transcriptome and will produce a directory containning index
        """
        input:
            "resources/assembly/GDDH13_1-1_mrna.fasta",
        output:
            directory("resources/salmon/transcriptome_index"),
        log:
            "logs/salmon/transcriptome_index.log",
        threads: 20
        params:
            # optional parameters
            extra="",
        conda:
            "../envs/salmon.yaml"
        shell:
            """
            salmon index \
            -t {input} \
            -i {output} \
            --threads {threads} {params.extra} >> {log} 2>&1
            """

    rule salmon_quant_reads:
        """
        Aims to quantify samples, this step needs runs and salmon's index.
        Salmon is configured with following flags:
          - -i to specify path to the index
          - -l A allows to determine automatically library type of reads
          - -1 and -2 specify where right and left reads are
          - -p for the number of threads
          - -o argument specifies the directory where salmon’s quantification results sould be written.
        """
        input:
            unpack(get_reads),
            index="resources/salmon/transcriptome_index",
        output:
            quant="results/salmon/{studyAcession}/{experimentationAcession}/{runAcession}/quant.sf",
            libFormat="results/salmon/{studyAcession}/{experimentationAcession}/{runAcession}/lib_format_counts.json",
            outputDir=directory(
                "results/salmon/{studyAcession}/{experimentationAcession}/{runAcession}"
            ),
        log:
            "logs/salmon/{studyAcession}/{experimentationAcession}/{runAcession}/salmon_quant_reads.log",
        params:
            # optional parameters
            libtype="A",
            extra="",
        threads: 20
        conda:
            "../envs/salmon.yaml"
        shell:
            """
            salmon quant -i {input.index} \
            -l {params.libtype} \
            -1 {input.r1} \
            -2 {input.r2} \
            -o {output.outputDir} \
            -p {threads} \
            {params.extra} \
            >> {log} 2>&1 
            """


if config["aligner"] == "star":

    rule star_index:
        input:
            fasta="resources/assembly/GDDH13_1-1_mrna.fasta",
        output:
            directory("resources/star/transcriptome_index"),
        message:
            "Generating STAR index"
        threads: 1
        params:
            extra="",
        log:
            "logs/star/transcriptome_index.log",
        wrapper:
            "0.67.0/bio/star/index"

    rule align_star:
        input:
            r2="results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_2.fastq.gz",
            r1="results/trimmed/{studyAcession}/{experimentationAcession}/{runAcession}_1.fastq.gz",
            index="resources/star/transcriptome_index",
        output:
            # see STAR manual for additional output files
            "results/star/{studyAcession}/{experimentationAcession}/{runAcession}/Aligned.sortedByCoord.out.bam",
            "results/star/{studyAcession}/{experimentationAcession}/{runAcession}/ReadsPerGene.out.tab",
        log:
            "logs/star/{studyAcession}/{experimentationAcession}/{runAcession}.log",
        params:
            # path to STAR reference genome index
            index="resources/star/transcriptome_index",
            # optional parameters
            extra="--outSAMtype BAM SortedByCoordinate --quantMode GeneCounts --sjdbGTFfile {} {}".format(
                config["resources"]["ref"]["annotation"], config["params"]["star"]
            ),
        threads: 24
        wrapper:
            "0.67.0/bio/star/align"
