"""
This rule collects metrics on raw fastq files.
More information at:
https://snakemake-wrappers.readthedocs.io/en/stable/wrappers/fastqc.html
"""


rule fastqc:
  """
  This rule collects metrics on raw fastq files.
  """
  input:
    unpack(get_reads)
  output:
    html = report(
      "results/qc/fastqc/{studyAcession}/{experimentationAcession}/{runAcession}_fastqc.html",
      category="Quality Controls"
    ),
    zip = "results/qc/fastqc/{studyAcession}/{experimentationAcession}/{runAcession}_fastqc.zip",
  params:
    ""
  threads:
    1
  log:
    "logs/fastqc/{studyAcession}/{experimentationAcession}/{runAcession}.log"
  message:
    "Controling quality of {input} fastq file with FastQC"
  wrapper:
    "0.67.0/bio/fastqc"
