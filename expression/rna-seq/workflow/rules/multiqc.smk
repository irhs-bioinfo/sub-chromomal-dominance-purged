"""
This rule runs MultiQC in order to collect metrics on most of our tools and
raw files: Fastq + Salmon. We need to include the fasta reference for
the report option only.
"""

def get_trimmed_files(config):
  if config["trimmer"] == "cutadapt" and config["params"]["trimming"] ==True:
    return expand("results/trimmed/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}.qc.txt", \
           samples=get_samples_ID())
  elif config["trimmer"] == "trimmomatic" and config["params"]["trimming"] ==True:
      return expand("logs/trimmomatic/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}.log", \
           samples=get_samples_ID())
  else:
      return []


def get_aligned_files(config):
  if config["aligner"] == "star":
    return expand("results/star/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}/Aligned.sortedByCoord.out.bam", \
           samples=get_samples_ID())
  elif config["aligner"] == "salmon":
      return expand("results/salmon/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}", \
           samples=get_samples_ID())


rule multiqc:
  input:
    expand("results/qc/fastqc/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}_fastqc.zip", \
           samples=get_samples_ID()),
    get_trimmed_files(config),
    get_aligned_files(config)
  output:
    report(
      "results/qc/multiqc_report/multiqc_report.html",
      # caption="../report/multiqc.rst",
      category="Quality Controls"
    ),
    dirBaseName = directory("results/qc/multiqc_report")
  threads: 1
  conda:
    "../envs/multiqc.yaml"
  params:
    name= "multiqc_report"

  log:
    "logs/multiqc.log"
  message:
    "Gathering quality reports with MultiQC"
  shell:
    """
    multiqc \
    --force \
    -o {output.dirBaseName} \
    -n {params.name} \
    {input} \
    >> {log} 2>&1
    """
