
rule aggregate_salmon_gene_counts:
    """
    This rule performs quantification aggregations: from multiple quant.sf to
    a single large quantification table for all sample
    """
    input:
        quant=expand(
            "results/salmon/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}/quant.sf",
            samples=get_samples_ID(),
        ),
        tx_to_gene="results/aggregation/transcript_to_gene.tsv",
    output:
        txi="results/downstream_analysis/tpm.tsv",
    message:
        "Aggregating all targets {input.quant} abundancies"
    threads: 1
    params:
        extra="type='salmon', txOut=TRUE",
    log:
        "logs/aggregation/aggregate.log",
    conda:
        "../envs/tximport.yaml"
    script:
        "../scripts/tximport.R"


rule tr2gene:
    """
    This rule creates the transcript to gene table from a GTF file. It's a bed-like
    TSV-formatted text file.
    """
    input:
        annotations=config["resources"]["ref"]["annotation"],
    output:
        tsv="results/aggregation/transcript_to_gene.tsv",
    message:
        "Creating a transcript to gene table based on GTF"
    threads: 1
    params:
        tx2gene=True,
    log:
        "logs/transcript_to_gene.log",
    conda:
        "../envs/annotation_to_tr.yaml"
    script:
        "../scripts/tr2gene.R"


rule gene_counts_analysis:
    """
    This rule creates the transcript to gene table from a GTF file. It's a bed-like
    TSV-formatted text file.
    """
    input:
        quant=expand(
            "results/salmon/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}/quant.sf",
            samples=get_samples_ID(),
        ),
        tx_to_gene="results/aggregation/transcript_to_gene.tsv",
        annotations=config["resources"]["ref"]["annotation"],
        indexDir=directory("resources/salmon/transcriptome_index"),
    output:
        counts="results/downstream_analysis/counts.csv",
        tpm="results/downstream_analysis/tpm.csv",
        fpm="results/downstream_analysis/fpm.csv",
        fpkm="results/downstream_analysis/fpkm.csv",
        medianOfRatios="results/downstream_analysis/medianOfRatios.csv"
    message:
        "Aggregating all targets {input.quant} abundancies"
    threads: 1
    log:
        "logs/downstream_analysis/downstream_analysis.log",
    conda:
        "../envs/deseq2.yaml"
    script:
        "../scripts/DGE_count_normalization.R"


# rule modelling_raw_count_for_each_genes:
#   """
#   This rule creates the transcript to gene table from a GTF file. It's a bed-like
#   TSV-formatted text file.
#   """
#   input:
#     quant=expand("results/salmon/{samples.study_accession}/{samples.experiment_accession}/{samples.sample}/quant.sf", samples=get_samples_ID()),
#     tx_to_gene="results/aggregation/transcript_to_gene.tsv",
#     annotations = config["resources"]["ref"]["annotation"],
#     indexDir=directory("resources/salmon/transcriptome_index")
#   output:
#     medianOfRatios=report(
#       "results/downstream_analysis/medianOfRatios.csv",
#       category="Counts"
#     ),
#   message:
#     "Aggregating all targets {input.quant} abundancies"
#   threads:
#     1
#   log:
#     "logs/downstream_analysis/downstream_analysis.log"
#   conda:
#     "../envs/deseq2.yaml"
#   script:
#     "../scripts/modelling_raw_count_for_each_genes.R"
