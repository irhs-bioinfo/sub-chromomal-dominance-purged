rule construct_studies:
  """
  This rule construct summary files and split ohnologous gene couples into two files
  """
  input:
    allSRR="resources/allSRR/allSRR.purged.purged.csv",
    ohnologousGenes="resources/ohnologousGenes/ohnologousGenes_unique_names.csv"
  output:
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/summary_{studyAcession}.txt",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep1.count",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep1.count",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep2.count",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep2.count",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep3.count",
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep3.count",
  message:
    "Construct studies"
  threads:
    1
  params:
    studyAccession = "{studyAcession}"
  log:
    "results/logs/construct_studies/construct_studies_{studyAcession}_{SplittingColumn}.log"
  conda:
    "../envs/construct_studies.yaml"
  shell:
    """
    python workflow/scripts/construct_studies.py \
    --studyAcession {params.studyAccession} \
    --allSRR {input.allSRR} \
    --ohnologousGenes {input.ohnologousGenes} \
    >> {log} 2>&1
    """



rule construct_ambigous_reads:
  """
  This rule construct a CSV file storing ambiguous reads
  """
  input:
    allSRR="resources/allSRR/allSRR.purged.purged.csv",
  output:
    "results/downstream_analysis/ambigInfo.tsv"
  message:
    "Construct ambigous reads file"
  threads:
    1
  conda:
    "../envs/construct_studies.yaml"
  shell:
    """
    python workflow/scripts/get_ambig_info.py \
    --allSRR {input.allSRR}
    """


rule construct_mapped_percent:
  """
  This rule construct summary files and split ohnologous gene couples into two files
  """
  input:
    allSRR="resources/allSRR/allSRR.purged.purged.csv",
  output:
    "results/downstream_analysis/mappingPercent.tsv"
  message:
    "Construct mapped ratio file"
  threads:
    1
  conda:
    "../envs/construct_studies.yaml"
  shell:
    """
    python workflow/scripts/get_percent_mapped.py \
    --allSRR {input.allSRR} \
    --output {output}
    """
