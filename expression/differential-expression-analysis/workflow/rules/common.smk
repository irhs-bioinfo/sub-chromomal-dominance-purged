from snakemake.utils import validate
import pandas as pd

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"

##### Load config and sample sheets #####

configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

# Load samples.tsv
samples = pd.read_csv(config["samples"], sep=',', dtype='str', comment='#', index_col=0)
# Add column
samples.columns = samples.columns.str.strip()

samples = samples[~samples["SplittingColumn"].isna()]
validate(samples, schema="../schemas/samples.schema.yaml")
