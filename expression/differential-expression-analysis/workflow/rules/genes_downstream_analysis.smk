
def get_all_non_swapping_genes_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_non_swapping_genes.csv"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))


def get_all_Deseq2_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_DESeq2_genesCouples.txt"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))


rule reconstruct_gene_names:
  """
  This rule reconstruct files from anaDiff script
  """
  input:
    DESeq2 = rules.move_anadiff_results.output.DESeq2,
    edgeR = rules.move_anadiff_results.output.edgeR
  output:
    DESeq2 = "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_DESeq2_genesCouples.txt",
    edgeR = "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_edgeR_genesCouples.txt",
  message:
    "Reconstruct gene names"
  params:
    uniqueNames = "resources/ohnologousGenes/ohnologousGenes_unique_names.csv"
  threads:
    1
  conda:
    "../envs/reconstruction_of_gene_names.yaml"
  shell:
    """
    python workflow/scripts/reconstruction_of_gene_names.py \
    --pathDeseq2 {input.DESeq2} \
    --pathEdgeR {input.edgeR} \
    --uniqueNames {params.uniqueNames}
    """


rule get_non_swapping_genes:
  """
  This rule get all genes that are oriented in the same way between all expremients
  """
  input:
    DESeq2 = rules.reconstruct_gene_names.output.DESeq2,
    templateFile = "resources/ohnologousGenes/template_ohnologous_genes.csv"
  output:
    "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_non_swapping_genes.csv",
  message:
    "All genes non swapping between experiments for {wildcards.studyAcession}"
  threads:
    1
  conda:
    "../envs/genes_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/get_non_swapping_genes.py \
    --input "{input.DESeq2}" \
    --template "{input.templateFile}" \
    --output {output}
    """


rule aggregate_non_swapping_genes:
  """
  This rule get all genes that are oriented in the same way between all expremients
  """
  input:
    inputFiles = get_all_non_swapping_genes_files
  output:
    "results/tables/non_swapping_genes.csv",
  message:
    "All genes non swapping between experiments"
  conda:
    "../envs/genes_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_non_swapping_genes.py \
    --input "{input.inputFiles}" \
    --output {output}
    """


rule aggregate_deseq2_files:
  """
  This rule get logFC of all gene couples
  """
  input:
    inputFiles = get_all_Deseq2_files
  output:
    "results/tables/logfc_aggregated.csv",
  message:
    "All genes logFC for all experiments"
  conda:
    "../envs/genes_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_logFC.py \
    --input "{input.inputFiles}" \
    --output {output}
    """


rule aggregate_read_counts:
  """
  This rule get all read counts from DESEQ2 results
  """
  input:
    inputFiles = get_all_Deseq2_files
  output:
    "results/tables/read_counts_aggregated.csv",
  message:
    "All genes logFC for all experiments"
  conda:
    "../envs/genes_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_read_counts.py \
    --input "{input.inputFiles}" \
    --output {output}
    """