def get_all_binom_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_binom_test.csv"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))


def get_all_binom_on_genes_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_DESeq2_genesCouples.txt"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))

def get_all_chi2_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_chi2_test.csv"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))


def get_all_wilcoxon_files(wildcards):
  return expand(["results/anaDiff/{samples.study_accession}/{samples.SplittingColumn}/summary_{samples.study_accession}/{samples.study_accession}_wilcoxon_test.csv"],
        samples=set(samples[["study_accession","experiment_accession", "run_accession", "SplittingColumn"]].itertuples()))


rule generate_visualisations:
  """
  This rule reconstruct files from anaDiff script.
  In input only first rep is given.
  It is normal these files are used only for reconstruct lenght of transcript
  """
  input:
    DESeq2 = rules.reconstruct_gene_names.output.DESeq2,
    firstPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep1.count",
    secondPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep1.count",
  output:
    "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_length_variance_distribution.png",
  message:
    "Run some statistical tests analysis"
  params:
    uniqueNames = "resources/ohnologousGenes/ohnologousGenes_unique_names.csv",
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/statistical_tests/visualisations_distributions.py \
    --pathDeseq2 {input.DESeq2} \
    --uniqueNames {params.uniqueNames} \
    --firstPart {input.firstPart} \
    --secondPart {input.secondPart}
    """


rule compute_binom_tests:
  """
  This rule reconstruct files from anaDiff script.
  In input only first rep is given.
  It is normal these files are used only for reconstruct lenght of transcript
  """
  input:
    DESeq2 = rules.reconstruct_gene_names.output.DESeq2,
    firstPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep1.count",
    secondPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep1.count",
  output:
    "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_binom_test.csv",
  message:
    "Run some statistical tests analysis"
  params:
    uniqueNames = "resources/ohnologousGenes/ohnologousGenes_unique_names.csv",
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/statistical_tests/binomial_test.py \
    --pathDeseq2 {input.DESeq2} \
    --uniqueNames {params.uniqueNames} \
    --firstPart {input.firstPart} \
    --secondPart {input.secondPart}
    """


rule compute_wilcoxon_tests:
  """
  This rule reconstruct files from anaDiff script.
  In input only first rep is given.
  It is normal these files are used only for reconstruct lenght of transcript
  """
  input:
    DESeq2 = rules.reconstruct_gene_names.output.DESeq2,
    firstPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep1.count",
    secondPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep1.count",
  output:
    "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_wilcoxon_test.csv",
  message:
    "Run some statistical tests analysis"
  params:
    uniqueNames = "resources/ohnologousGenes/ohnologousGenes_unique_names.csv",
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/statistical_tests/whitney.py \
    --pathDeseq2 {input.DESeq2} \
    --uniqueNames {params.uniqueNames} \
    --firstPart {input.firstPart} \
    --secondPart {input.secondPart}
    """


rule compute_chi2_tests:
  """
  This rule reconstruct files from anaDiff script.
  In input only first rep is given.
  It is normal these files are used only for reconstruct lenght of transcript
  """
  input:
    DESeq2 = rules.reconstruct_gene_names.output.DESeq2,
    firstPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/firstPart_rep1.count",
    secondPart = "results/parsedSalmon/{studyAcession}/{SplittingColumn}/secondPart_rep1.count",
  output:
    "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_chi2_test.csv"
  message:
    "Run some statistical tests analysis"
  params:
    uniqueNames = "resources/ohnologousGenes/ohnologousGenes_unique_names.csv",
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/statistical_tests/chi2_test.py \
    --pathDeseq2 {input.DESeq2} \
    --uniqueNames {params.uniqueNames} \
    --firstPart {input.firstPart} \
    --secondPart {input.secondPart}
    """

rule aggregate_downstream_analysis_binom:
  """
  This rule reconstruct files from anaDiff script
  """
  input:
    get_all_binom_files
  output:
    "results/tables/aggregated_binomial.csv",
  message:
    "Aggregate binomial tests"
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_tests.py \
    --input "{input}" \
    --output {output}
    """


rule aggregate_deseq2_on_genes:
  """
  This rule reconstruct files from anaDiff script
  """
  input:
    get_all_binom_on_genes_files
  output:
    "results/tables/aggregated_binomial_on_genes.csv",
  message:
    "Aggregate binomial tests"
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  log:
    "results/logs/aggregation/aggregate_deseq2_on_genes.log"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_genes.py \
    --input "{input}" \
    --output {output} >> {log} 2>&1
    """


rule aggregate_downstream_analysis_chi2:
  """
  This rule reconstruct files from anaDiff script
  """
  input:
    get_all_chi2_files
  output:
    "results/tables/aggregated_chi2.csv",
  message:
    "Aggregate chi2 tests"
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_tests.py \
    --input "{input}" \
    --output {output}
    """


rule aggregate_downstream_analysis_wilcoxon:
  """
  This rule reconstruct files from anaDiff script
  """
  input:
    get_all_wilcoxon_files
  output:
    "results/tables/aggregated_wilcoxon.csv",
  message:
    "Aggregate wilcoxon tests"
  threads:
    1
  conda:
    "../envs/stats_downstream_analysis.yaml"
  shell:
    """
    python workflow/scripts/aggregation_scripts/aggregate_tests.py \
    --input "{input}" \
    --output {output}
    """
