rule run_anaDiff:
  """
  This rule performs differential analysis using AnaDiff script
  """
  input:
    "results/parsedSalmon/{studyAcession}/{SplittingColumn}/summary_{studyAcession}.txt"
  output:
    mainDir = temp(directory("results/parsedSalmon/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/"))
  message:
    "Run anaDiff"
  threads:
    1
  log:
    "results/logs/anaDiff/anaDiff_{studyAcession}_{SplittingColumn}.log"
  conda:
    "../envs/anaDiff.yaml"
  shell:
    """
    Rscript workflow/scripts/run_anaDiff_wrapper.R {input} >> {log} 2>&1
    """


rule move_anadiff_results:
  """
  This rule just move result files from anaDiff script
  """
  input:
    rules.run_anaDiff.output
  output:
    DESeq2 = "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_DESeq2.txt",
    edgeR = "results/anaDiff/{studyAcession}/{SplittingColumn}/summary_{studyAcession}/{studyAcession}_edgeR.txt",
    mainDir = directory("results/anaDiff/{studyAcession}/{SplittingColumn}/"),
  message:
    "Move anaDiff result files"
  threads:
    1
  log:
    "results/logs/mv_command/anaDiff_{studyAcession}_{SplittingColumn}.log"
  conda:
    "../envs/anaDiff.yaml"
  shell:
    """
    mv -v {input} {output.mainDir} >> {log} 2>&1
    """
