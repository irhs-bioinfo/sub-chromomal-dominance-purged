# %%
import argparse
import os
import os.path
from typing import Dict, List, Optional, Set, Tuple

import numpy as np
import pandas as pd
from tqdm import tqdm


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-s",
                      "--studyAcession",
                      help="Study acession ID",
                      type=str)
  parser.add_argument("-a",
                      "--allSRR",
                      help="all SRR IDs",
                      type=str)
  parser.add_argument("-o",
                      "--ohnologousGenes",
                      help="ohnologous Genes with unique names",
                      type=str)
  args = parser.parse_args()
  return args


def generate_paths(nameReplicate: str,
                   groupReplicate: pd.DataFrame) -> Tuple[str, str, str]:
  """This function aims to generate path to access to salmon file and to output files

  Parameters
  ----------
  nameReplicate : string
      This string store name of replicate such as "Biological replicate 1"
  groupReplicate : pd.DataFrame
      This dataframe stored grouped dataframe for each repicate

  Returns
  -------
  List
      A list of paths
  """
  pathToSalmonQuant = "resources/salmon/" + \
                      groupReplicate["study_accession"].values[0] + \
                      "/" + groupReplicate["experiment_accession"].values[0] + \
                      "/" + groupReplicate["run_accession"].values[0] + \
                      "/quant.sf"
  fileNameFirstPart = "firstPart_rep" + nameReplicate[-1] + ".count"
  fileNameSecondPart = "secondPart_rep" + nameReplicate[-1] + ".count"
  return pathToSalmonQuant, fileNameFirstPart, fileNameSecondPart


def generate_summary_row(nameStudy: str,
                         nameTreatment: str,
                         number: str,
                         treatment: str,
                         filePath: str) -> Dict:
  """Generate summary file using dictionnary

  Parameters
  ----------
  nameStudy : string
      Accession ID of study
  nameTreatment : string
      nameTreatment
  number : string
      A number to create groupsnameStudy
  -------
  Dict
      A dictionnary gathering all information for a summary file row
  """
  firstPartDic = {"Comparison": nameStudy,
                  "File": filePath,
                  "Name": nameTreatment + number,
                  "Group": treatment}
  return firstPartDic


def size_normalisation(common: pd.DataFrame,
                       allPart: List[pd.DataFrame]) -> List[pd.DataFrame]:
  """Normalisation by size of the longest gene of ohnologous couple

  Parameters
  ----------
  common : pd.DataFrame
      Genes in couple
  allPart : pd.DataFrame
      Splitted couple in both dataframe, one for each file (first and second part)

  Returns
  -------
  List[pd.DataFrame]
      return array of allPart storing splitted genes couples in two dataframes
  """
  for index, row in common.iterrows():
    ohnologousGeneName = row["Name"]
    geneInfo = pd.concat(
        [allPart[0].loc[allPart[0]["Name"] == ohnologousGeneName],
         allPart[1].loc[allPart[1]["Name"] == ohnologousGeneName]])
    geneInfo = geneInfo.sort_values(by=["Length"])
    maxLength = geneInfo.iloc[0, geneInfo.columns.get_loc("Length")]
    RPK = (geneInfo.iloc[0]["NumReads"] *
           geneInfo.iloc[1]["Length"]) / geneInfo.iloc[0]["Length"]
    geneInfo.iloc[0, geneInfo.columns.get_loc("NumReads")] = RPK
    if allPart[0].loc[(allPart[0]["Name"] == ohnologousGeneName) & (allPart[0]["Length"] == geneInfo.iloc[0]["Length"])].shape[0] == 1:
      allPart[0].loc[(allPart[0]["Name"] == ohnologousGeneName) & (
        allPart[0]["Length"] == maxLength), "NumReads"] = RPK
    else:
      allPart[1].loc[(allPart[1]["Name"] == ohnologousGeneName) & (
        allPart[1]["Length"] == maxLength), "NumReads"] = RPK
  return allPart


def generatePathSummaryFile(nameStudy: str, nameTreatment: str) -> str:
  """[summary]

  Parameters
  ----------
  nameStudy : str
      [description]
  nameTreatment : str
      [description]

  Returns
  -------
  str
      [description]
  """
  # Generate a path to save summary file
  pathSummary = "results/parsedSalmon/" + \
      nameStudy + "/" + nameTreatment + "/"
  # Check if directories are existing
  os.makedirs(pathSummary, exist_ok=True)
  return pathSummary


# %%
args = generate_arguments()

allSRR = pd.read_csv(args.allSRR, index_col=0)
ohnologousGenes = pd.read_csv(args.ohnologousGenes, index_col=0)

allSRR = allSRR[allSRR["study_accession"] == args.studyAcession]
# %%
# Group by studies
groupByStudies = allSRR.groupby(by=["study_accession"])
# Iterate on it
for nameStudy, groupStudy in tqdm(groupByStudies, desc="Computed studies"):
  # Group by splitting columun
  groupByTreatment = groupStudy.groupby(by=["SplittingColumn"])
  # Iterate on splitted studies
  for nameTreatment, groupTreatment in tqdm(groupByTreatment, desc="Computed conditions"):
    # Init a summary dataframe
    summary = pd.DataFrame(columns=["Comparison", "File", "Name", "Group"])
    # Generate a path to save summary file
    pathSummary = generatePathSummaryFile(nameStudy, nameTreatment)
    # Group by replicates
    for nameReplicate, groupReplicate in groupTreatment.groupby(by=["replicate"]):
      # Generate all needed files
      pathToSalmonQuant, fileNameFirstPart, fileNameSecondPart = generate_paths(
        nameReplicate, groupReplicate)
      # Init a list to store part of gene couples
      allParts = []
      # Read raw file from salmon
      quantDf = pd.read_csv(pathToSalmonQuant, sep="\t")
      # Iterate on couple of ohnologous genes
      for genePart in ["gene_x", "gene_y"]:
        # Get all salmon result for current set of genes
        dfPart = pd.merge(quantDf,
                          ohnologousGenes,
                          how='right',
                          left_on=['Name'],
                          right_on=[genePart])
        # Drop na genes
        dfPart = dfPart.dropna(how="any")
        # Save it in results list
        allParts.append(dfPart)
      # Change name of genes with unique
      for i in range(0, len(allParts)):
        allParts[i]["Name"] = allParts[i]["uniqueName"]
        allParts[i] = allParts[i].drop(columns=["gene_x",
                                                "gene_y",
                                                "multiplicon",
                                                "uniqueName"])
      # Get genes that can be gathered as couples
      common = allParts[0][["Name"]].merge(
        allParts[1][["Name"]], on=['Name', 'Name'])
      allParts = size_normalisation(common, allParts)
      # For each part of results get genes that can be coupled
      for i, filePath in zip(range(0, len(allParts)), [fileNameFirstPart, fileNameSecondPart]):
        allParts[i] = allParts[i][allParts[i]["Name"].isin(common["Name"])]
        # Save it as tsv
        allParts[i].to_csv(pathSummary + filePath,
                           sep="\t",
                           index=False)
      # Generate associated summary row
      for number, treatment, filePath in zip(["1", "2"], ["Ttmt", "Control"], [fileNameFirstPart, fileNameSecondPart]):
        dicSummaryRow = generate_summary_row(
          nameStudy, nameTreatment, number, treatment, filePath)
        summary = summary.append(dicSummaryRow, ignore_index=True)
    # Save summary files
    summary.to_csv(pathSummary + "summary_" + nameStudy +
                   ".txt", sep="\t", header=True, index=False)
