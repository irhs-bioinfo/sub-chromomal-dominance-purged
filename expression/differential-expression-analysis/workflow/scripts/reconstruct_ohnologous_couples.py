# %%

import pandas as pd


def sort_ohnologous_genes(dfRow):
  if dfRow["gene_x"][2:4] != dfRow["gene_y"][2:4]:
    if dfRow["gene_x"][2:4] > dfRow["gene_y"][2:4]:
      dfRow["gene_x"], dfRow["gene_y"] = dfRow["gene_y"], dfRow["gene_x"]
    return dfRow


ohnologousGenes = pd.read_csv(
  "../../resources/ohnologousGenes/ohnologousGenes.csv", sep="\s")

ohnologousGenes = ohnologousGenes.apply(sort_ohnologous_genes, axis=1)
ohnologousGenes = ohnologousGenes.dropna()
ohnologousGenes.to_csv(
  "../../resources/ohnologousGenes/ohnologousGenesParsed.csv", sep=" ")
# %%
