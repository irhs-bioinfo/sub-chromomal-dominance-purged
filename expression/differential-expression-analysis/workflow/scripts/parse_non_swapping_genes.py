# %%
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd
import seaborn as sns
from scipy import stats

# %%


def compute_binom_tests(df: pd.DataFrame,
                        binomTestsResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  binomTestsResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
        number_genes_first_chr
        number_of_genes
        ratio
        pval
        couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    firstChr = name.split("-")[0]
    if group.shape[0] > 100:
      numberGenesFirstChr = group[group["genes"].str.startswith(
        "MD" + firstChr)].shape[0]
      numberGenes = group.shape[0]
      pvalBinom = stats.binom_test(
        numberGenesFirstChr,
        n=numberGenes,
        p=0.5,
        alternative='two-sided')

      dic = {"number_genes_first_chr": numberGenesFirstChr,
             "number_of_genes": numberGenes,
             "ratio": numberGenesFirstChr / numberGenes,
             "pval": pvalBinom,
             "couple": name}
      binomTestsResults = binomTestsResults.append(
        dic, ignore_index=True)
  return binomTestsResults


def compute_ks_2samp_tests(df: pd.DataFrame,
                           ksResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  ksResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    firstChr = name.split("-")[0]
    secondChr = name.split("-")[1]
    if group.shape[0] > 300:
      genesFirstChr = group[group["genes"].str.startswith(
        "MD" + firstChr)]
      genesSecondChr = group[group["genes"].str.startswith(
        "MD" + secondChr)]
      stat, pval = stats.ks_2samp(
        data1=genesFirstChr["ratio"],
        data2=genesSecondChr["ratio"])

      dic = {"pval": pval,
             "couple": name}
      ksResults = ksResults.append(
        dic, ignore_index=True)
  return ksResults


def get_couple(group: pd.DataFrame) -> str:
  """Reconstruct couple names using genes names

  Parameters
  ----------
  group : pd.DataFrame
      A ohnolgous gene pair dataframe

  Returns
  -------
  str
      A couple string like: "13-16"
  """
  return group.iloc[0]["genes"][2:4] + "-" + \
      group.iloc[1]["genes"][2:4]


# %%
nonSwappingGenes = pd.read_csv(
    "../../results/tables/non_swapping_genes.csv", index_col=0)
# Get total of watched runs
totalExperiments = nonSwappingGenes.loc[nonSwappingGenes["genes"]
                                        == "Total", "InSomeDf"].values[0]
# Drop row with total
nonSwappingGenes = nonSwappingGenes[~nonSwappingGenes["genes"].str.contains(
  "Total", na=False)]
# Compute ratio of non swapping genes, meaning:
# Number of times where genes is upregulated in run divided by number of runs watched
nonSwappingGenes["ratio"] = nonSwappingGenes["InSomeDf"] / totalExperiments
# Get chromosome number as a new column
nonSwappingGenes["chrom"] = nonSwappingGenes["genes"].str[2:4].astype(int)
# Save couple as a new column
nonSwappingGenes["couple"] = nonSwappingGenes.groupby(
  nonSwappingGenes.index).apply(get_couple)
# Keep genes with a ratio superior to 90%
nonSwapping = nonSwappingGenes[nonSwappingGenes["ratio"] > 0.90]
# Plot it
sns.histplot(data=nonSwapping, x="chrom", bins=17)

# %%
# Init a df to store resutls of binomial tests
binomResults = pd.DataFrame(columns=["number_genes_first_chr",
                                     "number_of_genes",
                                     "ratio",
                                     "pval",
                                     "couple"])
# Compute binomial test for all possible couples
binomResults = compute_binom_tests(
  nonSwapping, binomResults)

binomResults.sort_values(by=["pval"], ascending=True)
# %%
nonSwappingGenes["chrom"] = pd.to_numeric(nonSwappingGenes["chrom"])
sns.displot(data=nonSwappingGenes, col="chrom", col_wrap=3, x="ratio")

# %%

# Init a df to store resutls of binomial tests
ksResults = pd.DataFrame(columns=[
    "pval",
    "couple"])
# Compute binomial test for all possible couples
ksResults = compute_ks_2samp_tests(
  nonSwappingGenes, ksResults)

ksResults.sort_values(by=["pval"], ascending=True)

# %%
