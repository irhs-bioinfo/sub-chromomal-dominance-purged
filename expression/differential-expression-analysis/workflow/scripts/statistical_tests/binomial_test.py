# %%
import argparse
from typing import Dict, List

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  parser.add_argument("-f",
                      "--firstPart",
                      help="Path to a first part file from count files",
                      type=str)
  parser.add_argument("-g",
                      "--secondPart",
                      help="Path to a second part file from count files",
                      type=str)
  args = parser.parse_args()
  return args


def compute_binom_tests(df: pd.DataFrame,
                        dfResults: pd.DataFrame,
                        ohnologousGenesUniqueNames: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      Dataframe storing DESEQ2 results
  dfResults : pd.DataFrame
      Dataframe where is appended results
  ohnologousGenesUniqueNames: pd.DataFrame
      Dataframe storing all ohnologous genes couples


  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  for name, group in df.groupby(by="couple"):
    if group.shape[0] > 30:
      allOhnologousCouples = ohnologousGenesUniqueNames.loc[
        ohnologousGenesUniqueNames["couple"] == name]
      pval_allOhnologousCouples, allOhnologousCouples_genes, sizeOfPopulationallOhnologousCouples = binom_test_all_ohnologous_couples(
        group, allOhnologousCouples)
      pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE = binom_test_two_fold_rule(
        group)
      pval_horse_race, horse_race_genes, sizeOfPopulationDE = binom_test_horse_race_rule(
        group)

      dic = {"pval_two_fold_rule": pval_two_fold_rule,
             "number_upregulated_genes_two_fold": two_fold_genes,
             "pval_horse_race": pval_horse_race,
             "number_upregulated_genes_horserace": horse_race_genes,
             "number_of_horserace_genes": sizeOfPopulationDE,
             "number_of_two_fold_genes": sizeOfPopulationHighlyDE,
             "pval_allOhnologousCouples": pval_allOhnologousCouples,
             "allOhnologousCouples_genes": allOhnologousCouples_genes,
             "number_of_all_ohnologous": sizeOfPopulationallOhnologousCouples,
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


def binom_test_all_ohnologous_couples(group: pd.DataFrame,
                                      allOhnologousCouples: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  horse_race_genes = group["logFC"][group["logFC"] > 0].shape[0]
  sizeOfPopulationDE = allOhnologousCouples.shape[0]

  pval_horse_race = stats.binom_test(
      horse_race_genes,
      n=sizeOfPopulationDE,
      p=0.5,
      alternative='two-sided')
  return pval_horse_race, horse_race_genes, sizeOfPopulationDE


def binom_test_horse_race_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  horse_race_genes = group["logFC"][group["logFC"] > 0].shape[0]
  sizeOfPopulationDE = group["logFC"][group["logFC"] >
                                      0].shape[0] + group["logFC"][group["logFC"] < 0].shape[0]
  pval_horse_race = stats.binom_test(
      horse_race_genes,
      n=sizeOfPopulationDE,
      p=0.5,
      alternative='two-sided')
  return pval_horse_race, horse_race_genes, sizeOfPopulationDE


def binom_test_two_fold_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  two_fold_genes = group["logFC"][group["logFC"] > 2].shape[0]
  sizeOfPopulationHighlyDE = group["logFC"][group["logFC"] >
                                            2].shape[0] + group["logFC"][group["logFC"] < -2].shape[0]
  pval_two_fold_rule = stats.binom_test(
    two_fold_genes,
    n=sizeOfPopulationHighlyDE,
    p=0.5,
    alternative='two-sided')
  return pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE


# Load arguments
args = generate_arguments()

outputPath = args.pathDeseq2.replace("_DESeq2_genesCouples.txt", "")

# Load files as dataframes
ohnologousGenesUniqueNames = pd.read_csv(args.uniqueNames,
                                         sep=",",
                                         index_col=0)

firstPart = pd.read_csv(args.firstPart, sep="\t")
secondPart = pd.read_csv(args.secondPart, sep="\t")

ohnologousGenesUniqueNames["couple"] = ohnologousGenesUniqueNames["gene_x"].str[2:4] + \
   "-" + ohnologousGenesUniqueNames["gene_y"].str[2:4]

# Open DESeq2 file
DESeq2 = pd.read_csv(
    args.pathDeseq2, sep=",", index_col=0)

# Filter on FDR pval, to check
DESeq2 = DESeq2[DESeq2["pval_DESeq2"] < 0.05]
DESeq2.dropna(subset=['logFC', 'pval_DESeq2'], inplace=True)

# Compute binomial tests
binomTestsResults = pd.DataFrame(columns=["number_upregulated_genes_two_fold",
                                          "number_of_two_fold_genes",
                                          "pval_two_fold_rule",
                                          "number_upregulated_genes_horserace",
                                          "number_of_horserace_genes",
                                          "pval_horse_race",
                                          "couple"])
binomTestsResults = compute_binom_tests(
  DESeq2, binomTestsResults, ohnologousGenesUniqueNames)
binomTestsResults.sort_values(by=["number_of_horserace_genes", "pval_horse_race"],
                              inplace=True,
                              ascending=False)
binomTestsResults.to_csv(outputPath + "_binom_test.csv")
