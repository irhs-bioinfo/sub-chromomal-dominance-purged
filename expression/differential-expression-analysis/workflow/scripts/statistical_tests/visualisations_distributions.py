# %%
import argparse
from typing import Dict, List

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  parser.add_argument("-f",
                      "--firstPart",
                      help="Path to a first part file from count files",
                      type=str)
  parser.add_argument("-g",
                      "--secondPart",
                      help="Path to a second part file from count files",
                      type=str)
  args = parser.parse_args()
  return args


def reconstruct_right_couples(dfRow: pd.DataFrame,
                              firstPart: pd.DataFrame,
                              secondPart: pd.DataFrame) -> Dict:
  """This function aims to generate a dictionnary using ohnologous couples informations

  Parameters
  ----------
  dfRow : pd.DataFrame
      ohnologous couples informations
  firstPart : pd.DataFrame
      First part of a anaDiff input file
  secondPart : pd.DataFrame
      Second part of a anaDiff input file

  Returns
  -------
  Dict
      A dict with following keys:
        gene_x
        gene_y
        uniqueName
        Length_x
        Length_y
  """
  dic = {}
  row1 = firstPart[dfRow["uniqueName"] == firstPart["Name"]]
  row2 = secondPart[dfRow["uniqueName"] == secondPart["Name"]]
  if row1.shape[0] != 0 and row2.shape[0] != 0:
    dic = {"gene_x": dfRow["gene_x"],
           "gene_y": dfRow["gene_y"],
           "uniqueName": dfRow["uniqueName"],
           "Length_x": row1["Length"].values[0],
           "Length_y": row2["Length"].values[0]}
  return dic


def compute_length_variance_between_ohnologous(dfRow: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute variance between mature transcript

  Parameters
  ----------
  dfRow : pd.DataFrame
      Dataframe storing following columns:
        gene_x
        gene_y
        uniqueName
        Length_x
        Length_y

  Returns
  -------
  pd.DataFrame
      Dataframe storing one column more storing lengthVar
  """
  arrayOfLength = np.array([dfRow["Length_x"], dfRow["Length_y"]])
  dfRow["lengthVar"] = np.std(arrayOfLength.astype(np.int64))
  return dfRow


# Load arguments
args = generate_arguments()

# Load files as dataframes
ohnologousGenesUniqueNames = pd.read_csv(
  args.uniqueNames, sep=",", index_col=0)

firstPart = pd.read_csv(args.firstPart, sep="\t")
secondPart = pd.read_csv(args.secondPart, sep="\t")

ohnologousGenesUniqueNames["couple"] = ohnologousGenesUniqueNames["gene_x"][2:4] + \
  ohnologousGenesUniqueNames["gene_y"][2:4]

# Generate a basename path for output files
outputPath = args.pathDeseq2.replace("_DESeq2_genesCouples.txt", "")
# Construct a list of dict with genes of couples and associated length of transcript from Salmon output files
length = ohnologousGenesUniqueNames.apply(reconstruct_right_couples,
                                          args=(firstPart, secondPart),
                                          axis=1)
# Save it as a dataframe
lengthDf = pd.DataFrame(length.tolist())
# Compute variance between length of transcript
lengthDf = lengthDf.apply(compute_length_variance_between_ohnologous, axis=1)
# Plot it
displot = sns.displot(data=lengthDf,
                      x="lengthVar")
displot.savefig(outputPath + "_length_variance_distribution.png")


# Open DESeq2 file
DESeq2 = pd.read_csv(
    args.pathDeseq2, sep=",", index_col=0)

# Filter on FDR pval, to check
DESeq2 = DESeq2[DESeq2["pval_DESeq2"] < 0.05]

# Filter groups to get couple with at least 500 couples
DESeq2Filtered = pd.DataFrame()
# Group by files
for name, group in DESeq2.groupby(by="couple"):
  if group.shape[0] > 500:
    DESeq2Filtered = DESeq2Filtered.append(group, ignore_index=True)
# Get all result columns from DESEQ2's output file
logColumnName = [col for col in DESeq2Filtered.columns if 'log' in col]

# Plot them
for columnName in logColumnName:
  displot = sns.displot(data=DESeq2Filtered,
                        x=columnName,
                        col="couple",
                        col_wrap=3)
  displot.savefig(outputPath + "_" + columnName + ".svg")
  displot.savefig(outputPath + "_" + columnName + ".png")
