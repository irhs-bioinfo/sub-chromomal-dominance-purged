from scipy import stats
import seaborn as sns
import pandas as pd
import numpy as np
from typing import Dict, List
import argparse


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  parser.add_argument("-f",
                      "--firstPart",
                      help="Path to a first part file from count files",
                      type=str)
  parser.add_argument("-g",
                      "--secondPart",
                      help="Path to a second part file from count files",
                      type=str)
  args = parser.parse_args()
  return args


def compute_chi2_tests(df: pd.DataFrame,
                       dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    biased1 = group["logFC"][group["logFC"] > 3.5].shape[0]
    biased2 = group["logFC"][group["logFC"] < -3.5].shape[0]
    if biased1 > 5 and biased2 > 5:
      expected = (biased1 + biased2) / 2
      f_obs = [biased1, biased2]
      f_exp = [expected, expected]
      statChi2, pvalChi2 = stats.chisquare(f_obs,
                                           f_exp)
      dic = {"pval_chi2": pvalChi2,
             "observed_biased1": biased1,
             "observed_biased2": biased2,
             "expected": expected,
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


# Load arguments
args = generate_arguments()
outputPath = args.pathDeseq2.replace("_DESeq2_genesCouples.txt", "")

# Load files as dataframes
ohnologousGenesUniqueNames = pd.read_csv(
  args.uniqueNames, sep=",", index_col=0)

firstPart = pd.read_csv(args.firstPart, sep="\t")
secondPart = pd.read_csv(args.secondPart, sep="\t")

ohnologousGenesUniqueNames["couple"] = ohnologousGenesUniqueNames["gene_x"][2:4] + \
  ohnologousGenesUniqueNames["gene_y"][2:4]
# Open DESeq2 file
DESeq2 = pd.read_csv(
    args.pathDeseq2, sep=",", index_col=0)

# Filter on FDR pval, to check
DESeq2 = DESeq2[DESeq2["pval_DESeq2"] < 0.05]
DESeq2.dropna(subset=['logFC', 'pval_DESeq2'], inplace=True)


chi2TestResults = pd.DataFrame(columns=["pval_chi2",
                                        "couple"])
# Compute chi2 tests
chi2TestResults = compute_chi2_tests(DESeq2, chi2TestResults)
chi2TestResults.sort_values(by=["pval_chi2"],
                            inplace=True,
                            ascending=False)
chi2TestResults.to_csv(outputPath + "_chi2_test.csv")
