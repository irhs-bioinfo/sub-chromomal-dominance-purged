# %%
import argparse
from typing import Dict, List

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  parser.add_argument("-f",
                      "--firstPart",
                      help="Path to a first part file from count files",
                      type=str)
  parser.add_argument("-g",
                      "--secondPart",
                      help="Path to a second part file from count files",
                      type=str)
  args = parser.parse_args()
  return args


def compute_whitney_tests(df: pd.DataFrame,
                          dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    if group.shape[0] > 30:
      logColumnName = [
        col for col in group.columns if 'logCPM' in col]
      statWilcoxon, pvalWilcoxon = stats.wilcoxon(group[logColumnName[0]],
                                                  group[logColumnName[1]])
      statTtest_rel, pvalTtest_rel = stats.ttest_rel(group[logColumnName[0]],
                                                     group[logColumnName[1]])
      dic = {"pvalWilcoxon": pvalWilcoxon,
             "pvalTtest_rel": pvalTtest_rel,
             "group1": group[logColumnName[0]],
             "group2": group[logColumnName[1]],
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


# Load arguments
args = generate_arguments()

outputPath = args.pathDeseq2.replace("_DESeq2_genesCouples.txt", "")

# Load files as dataframes
ohnologousGenesUniqueNames = pd.read_csv(
  args.uniqueNames, sep=",", index_col=0)

firstPart = pd.read_csv(args.firstPart, sep="\t")
secondPart = pd.read_csv(args.secondPart, sep="\t")

ohnologousGenesUniqueNames["couple"] = ohnologousGenesUniqueNames["gene_x"][2:4] + \
  ohnologousGenesUniqueNames["gene_y"][2:4]
# Open DESeq2 file
DESeq2 = pd.read_csv(
    args.pathDeseq2, sep=",", index_col=0)

# Filter on FDR pval, to check
DESeq2 = DESeq2[DESeq2["pval_DESeq2"] < 0.05]
DESeq2.dropna(subset=['logFC', 'pval_DESeq2'], inplace=True)

whitneyTestsResults = pd.DataFrame(columns=["pvalWilcoxon",
                                            "pvalTtest_rel",
                                            "group1",
                                            "group2",
                                            "couple"])
# Compute Wilcoxon tests
whitneyTestsResults = compute_whitney_tests(DESeq2, whitneyTestsResults)
whitneyTestsResults.sort_values(by=["pvalWilcoxon"],
                                inplace=True,
                                ascending=False)
whitneyTestsResults.to_csv(outputPath + "_wilcoxon_test.csv")
