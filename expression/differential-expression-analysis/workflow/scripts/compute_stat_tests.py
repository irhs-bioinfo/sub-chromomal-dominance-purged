
# %%
import argparse
from typing import Dict, List

import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats

# %%


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  parser.add_argument("-f",
                      "--firstPart",
                      help="Path to a first part file from count files",
                      type=str)
  parser.add_argument("-g",
                      "--secondPart",
                      help="Path to a second part file from count files",
                      type=str)
  args = parser.parse_args()
  return args


def reconstruct_right_couples(dfRow: pd.DataFrame,
                              firstPart: pd.DataFrame,
                              secondPart: pd.DataFrame) -> Dict:
  """This function aims to generate a dictionnary using ohnologous couples informations

  Parameters
  ----------
  dfRow : pd.DataFrame
      ohnologous couples informations
  firstPart : pd.DataFrame
      First part of a anaDiff input file
  secondPart : pd.DataFrame
      Second part of a anaDiff input file

  Returns
  -------
  Dict
      A dict with following keys:
        gene_x
        gene_y
        uniqueName
        Length_x
        Length_y
  """
  dic = {}
  row1 = firstPart[dfRow["uniqueName"] == firstPart["Name"]]
  row2 = secondPart[dfRow["uniqueName"] == secondPart["Name"]]
  if row1.shape[0] != 0 and row2.shape[0] != 0:
    dic = {"gene_x": dfRow["gene_x"],
           "gene_y": dfRow["gene_y"],
           "uniqueName": dfRow["uniqueName"],
           "Length_x": row1["Length"].values[0],
           "Length_y": row2["Length"].values[0]}
  return dic


def compute_length_variance_between_ohnologous(dfRow: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute variance between mature transcript

  Parameters
  ----------
  dfRow : pd.DataFrame
      Dataframe storing following columns:
        gene_x
        gene_y
        uniqueName
        Length_x
        Length_y

  Returns
  -------
  pd.DataFrame
      Dataframe storing one column more storing lengthVar
  """
  arrayOfLength = np.array([dfRow["Length_x"], dfRow["Length_y"]])
  dfRow["lengthVar"] = np.std(arrayOfLength.astype(np.int64))
  return dfRow


def compute_binom_tests(df: pd.DataFrame,
                        dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    if group.shape[0] > 30:
      pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE = binom_test_two_fold_rule(
        group)
      pval_horse_race, horse_race_genes, sizeOfPopulationDE = binom_test_horse_race_rule(
        group)

      dic = {"pval_two_fold_rule": pval_two_fold_rule,
             "number_upregulated_genes_two_fold": two_fold_genes,
             "pval_horse_race": pval_horse_race,
             "number_upregulated_genes_horserace": horse_race_genes,
             "number_of_horserace_genes": sizeOfPopulationDE,
             "number_of_two_fold_genes": sizeOfPopulationHighlyDE,
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


def binom_test_horse_race_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  horse_race_genes = group["logFC"][group["logFC"] > 0].shape[0]
  sizeOfPopulationDE = group["logFC"][group["logFC"] >
                                      0].shape[0] + group["logFC"][group["logFC"] < 0].shape[0]
  pval_horse_race = stats.binom_test(
      horse_race_genes,
      n=sizeOfPopulationDE,
      p=0.5,
      alternative='two-sided')
  return pval_horse_race, horse_race_genes, sizeOfPopulationDE


def binom_test_two_fold_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  two_fold_genes = group["logFC"][group["logFC"] > 2].shape[0]
  sizeOfPopulationHighlyDE = group["logFC"][group["logFC"] >
                                            2].shape[0] + group["logFC"][group["logFC"] < -2].shape[0]
  pval_two_fold_rule = stats.binom_test(
    two_fold_genes,
    n=sizeOfPopulationHighlyDE,
    p=0.5,
    alternative='two-sided')
  return pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE


def compute_whitney_tests(df: pd.DataFrame,
                          dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    if group.shape[0] > 30:
      logColumnName = [
        col for col in DESeq2Filtered.columns if 'logCPM' in col]
      statWilcoxon, pvalWilcoxon = stats.wilcoxon(group[logColumnName[0]],
                                                  group[logColumnName[1]])
      statTtest_rel, pvalTtest_rel = stats.ttest_rel(group[logColumnName[0]],
                                                     group[logColumnName[1]])
      dic = {"pvalWilcoxon": pvalWilcoxon,
             "pvalTtest_rel": pvalTtest_rel,
             "group1": group[logColumnName[0]],
             "group2": group[logColumnName[1]],
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


def compute_chi2_tests(df: pd.DataFrame,
                       dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by="couple")
  for name, group in dfGrouped:
    biased1 = group["logFC"][group["logFC"] > 3.5].shape[0]
    biased2 = group["logFC"][group["logFC"] < -3.5].shape[0]
    if biased1 > 5 and biased2 > 5:
      expected = (biased1 + biased2) / 2
      f_obs = [biased1, biased2]
      f_exp = [expected, expected]
      statChi2, pvalChi2 = stats.chisquare(f_obs,
                                           f_exp)
      dic = {"pval_chi2": pvalChi2,
             "observed_biased1": biased1,
             "observed_biased2": biased2,
             "expected": expected,
             "couple": name}
      dfResults = dfResults.append(
        dic, ignore_index=True)
  return dfResults


# %%
# Load arguments
args = generate_arguments()

# Generate a basename path for output files
pathToFig = args.pathDeseq2.replace("_DESeq2_genesCouples.txt", "")

# Load files as dataframes
ohnologousGenesUniqueNames = pd.read_csv(
  args.uniqueNames, sep=",", index_col=0)

firstPart = pd.read_csv(args.firstPart, sep="\t")
secondPart = pd.read_csv(args.secondPart, sep="\t")

# Construct a list of dict with genes of couples and associated length of transcript from Salmon output files
length = ohnologousGenesUniqueNames.apply(reconstruct_right_couples,
                                          args=(firstPart, secondPart),
                                          axis=1)
# Save it as a dataframe
lengthDf = pd.DataFrame(length.tolist())
# Compute variance between length of transcript
lengthDf = lengthDf.apply(compute_length_variance_between_ohnologous, axis=1)
# Plot it
displot = sns.displot(data=lengthDf,
                      x="lengthVar")
displot.savefig(pathToFig + "_length_variance_distribution.png")
# %%
# Open DESeq2 file
DESeq2 = pd.read_csv(
    args.pathDeseq2, sep=",", index_col=0)

# Filter on FDR pval, to check
DESeq2 = DESeq2[DESeq2["pval_DESeq2"] < 0.05]

# Filter groups to get couple with at least 500 couples
DESeq2Filtered = pd.DataFrame()
# Group by files
for name, group in DESeq2.groupby(by="couple"):
  if group.shape[0] > 500:
    DESeq2Filtered = DESeq2Filtered.append(group, ignore_index=True)
# Get all result columns from DESEQ2's output file
logColumnName = [col for col in DESeq2Filtered.columns if 'log' in col]

# Plot them
for columnName in logColumnName:
  displot = sns.displot(data=DESeq2Filtered,
                        x=columnName,
                        col="couple",
                        col_wrap=3)
  displot.savefig(pathToFig + "_" + columnName + ".svg")
  displot.savefig(pathToFig + "_" + columnName + ".png")

DESeq2.dropna(subset=['logFC', 'pval_DESeq2'], inplace=True)


# %%
# Compute binomial tests
binomTestsResults = pd.DataFrame(columns=["number_upregulated_genes_two_fold",
                                          "number_of_two_fold_genes",
                                          "pval_two_fold_rule",
                                          "number_upregulated_genes_horserace",
                                          "number_of_horserace_genes",
                                          "pval_horse_race",
                                          "couple"])
binomTestsResults = compute_binom_tests(
  DESeq2, binomTestsResults)
binomTestsResults.sort_values(by=["number_of_horserace_genes", "pval_horse_race"],
                              inplace=True,
                              ascending=False)
binomTestsResults.to_csv(pathToFig + "_binom_test.csv")

# %%
whitneyTestsResults = pd.DataFrame(columns=["pvalWilcoxon",
                                            "pvalTtest_rel",
                                            "group1",
                                            "group2",
                                            "couple"])
# Compute Wilcoxon tests
whitneyTestsResults = compute_whitney_tests(DESeq2, whitneyTestsResults)
whitneyTestsResults.sort_values(by=["pvalWilcoxon"],
                                inplace=True,
                                ascending=False)
whitneyTestsResults.to_csv(pathToFig + "_wilcoxon_test.csv")
# %%
chi2TestResults = pd.DataFrame(columns=["pval_chi2",
                                        "couple"])
# Compute chi2 tests
chi2TestResults = compute_chi2_tests(DESeq2, chi2TestResults)
chi2TestResults.sort_values(by=["pval_chi2"],
                            inplace=True,
                            ascending=False)
chi2TestResults.to_csv(pathToFig + "_chi2_test.csv")
