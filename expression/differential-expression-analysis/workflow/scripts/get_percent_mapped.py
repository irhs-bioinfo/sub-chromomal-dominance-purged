# %%
import argparse
import json
import os
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd
from tqdm import tqdm


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-a",
                      "--allSRR",
                      help="all SRR IDs",
                      type=str)
  parser.add_argument("-o",
                      "--output",
                      help="Percent mapped df output",
                      type=str)
  args = parser.parse_args()
  return args


def get_percent_mapped(dfRow: pd.DataFrame) -> pd.DataFrame:
  """ This function aims to open salmon output metadatas and get percent_mapped field

  Parameters
  ----------
  dfRow : pd.DataFrame
      A row with informations of a run

  Returns
  -------
  pd.DataFrame
      A row with informations of a run and percent of mapped reads
  """
  pathToSalmonAuxInfo = "../../resources/salmon/" + \
                        dfRow["study_accession"] + \
                        "/" + dfRow["experiment_accession"] + \
                        "/" + dfRow["run_accession"] + \
                        "/aux_info/meta_info.json"

  if os.path.isfile(pathToSalmonAuxInfo):
    with open(pathToSalmonAuxInfo) as json_file:
      auxInfo = json.load(json_file)
      dfRow["percent_mapped"] = auxInfo["percent_mapped"]
  return dfRow


def main():

  args = generate_arguments()

  allSRR = pd.read_csv(args.allSRR, index_col=0)
  allSRR = allSRR.apply(get_percent_mapped, axis=1)
  allSRR[["study_accession",
          "experiment_accession",
          "run_accession",
          "percent_mapped"]].to_csv(args.output, sep="\t")


if __name__ == "__main__":
  main()
