# %%
import pandas as pd
import argparse


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-i",
                      "--input",
                      help="Path to statistical results output files",
                      type=str)
  parser.add_argument("-t",
                      "--template",
                      help="Path to a template file storing all possible ohnologous genes",
                      type=str)
  parser.add_argument("-o",
                      "--output",
                      help="Path to aggregated statistical test output files",
                      type=str)
  args = parser.parse_args()
  return args


def get_non_swapping_genes(group: pd.DataFrame,
                           df: pd.DataFrame,
                           templateGenesNames: pd.DataFrame) -> pd.DataFrame:
  """[summary]

  Parameters
  ----------
  group : pd.DataFrame
      [description]
  df : pd.DataFrame
      [description]
  templateGenesNames : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      [description]
  """
  # If it is not last line of template file storing tool
  if group.iloc[0]["genes"] != "Total":
    # Search couple's result from anaDiff
    logResult = df[(df["gene_x"] == group.iloc[0]["genes"])
                   & (df["gene_y"] == group.iloc[1]["genes"])]
    # If a result if find which is not is not mandatory because of insignificant couples
    if logResult.shape[0] != 0 and logResult["pval_DESeq2"].values[0] < 0.05:
      # If LogFC is superior to 1 meaning first gene of couple is upregulated
      if logResult["logFC"].values[0] > 0.5:
        # Find first gene of couple and add +1
        templateGenesNames.loc[(templateGenesNames["genes"] == group.iloc[0]["genes"]) &
                               (templateGenesNames.index ==
                                group.iloc[0].name),
                               "InSomeDf"] += 1
      elif logResult["logFC"].values[0] < -0.5:
        # Find second gene of couple and add +1
        templateGenesNames.loc[(templateGenesNames["genes"] == group.iloc[1]["genes"]) &
                               (templateGenesNames.index ==
                                group.iloc[1].name),
                               "InSomeDf"] += 1
  else:
    # Total count of studied experiments
    templateGenesNames.loc[templateGenesNames["genes"]
                           == "Total", "InSomeDf"] += 1


args = generate_arguments()
# Here we open a template file with all possible ohnologous genes
templateGenesNames = pd.read_csv(
  args.template, index_col=0, header=0, comment="#")
templateGenesNames["InSomeDf"] = 0
templateGenesNames.loc[len(templateGenesNames.index)] = ['Total', 0]

# Load anaDiff files
df = pd.read_csv(args.input, index_col=0, header=0)

# Group by index (which means group by ohnologous couple genes)
templateGenesNames.groupby(
  by=[templateGenesNames.index]).apply(
    get_non_swapping_genes,
    df=df,
    templateGenesNames=templateGenesNames)

# Save final result to a file
templateGenesNames.to_csv(args.output)
