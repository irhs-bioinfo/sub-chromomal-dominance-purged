import pandas as pd
import argparse


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-i",
                      "--input",
                      help="Path to statistical results output files",
                      type=str)
  parser.add_argument("-o",
                      "--output",
                      help="Path to aggregated statistical test output files",
                      type=str)
  args = parser.parse_args()
  return args


args = generate_arguments()
list = []

for filename in args.input.split():
  splittedFilename = filename.split("/")
  df = pd.read_csv(filename, index_col=0, header=0)
  df["experience"] = splittedFilename[2] + "-" + splittedFilename[3]
  list.append(df)

finalDf = pd.concat(list, axis=0, ignore_index=True)
finalDf = finalDf.drop_duplicates()
finalDf.to_csv(args.output)
