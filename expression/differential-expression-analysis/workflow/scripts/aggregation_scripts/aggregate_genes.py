# %%
import pandas as pd
import argparse
from scipy import stats
from typing import Dict, List
import os


def compute_binom_tests(df: pd.DataFrame,
                        dfResults: pd.DataFrame) -> pd.DataFrame:
  """This function aims to compute a binomial test to check if number of down regulated genes is unbalanced

  Parameters
  ----------
  df : pd.DataFrame
      [description]
  dfResults : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      A dataframes storing binomial tests results into following columns
      pvalue
      number_down_regulated_genes
      total_number_of_genes
      couple
  """
  dfGrouped = df.groupby(by=["gene_x", "gene_y"])
  for name, group in dfGrouped:

    pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE = binom_test_two_fold_rule(
      group)
    pval_horse_race, horse_race_genes, sizeOfPopulationDE = binom_test_horse_race_rule(
      group)

    dic = {"pval_two_fold_rule": pval_two_fold_rule,
           "number_upregulated_genes_two_fold": two_fold_genes,
           "pval_horse_race": pval_horse_race,
           "number_upregulated_genes_horserace": horse_race_genes,
           "number_of_horserace_genes": sizeOfPopulationDE,
           "number_of_two_fold_genes": sizeOfPopulationHighlyDE,
           "couple": name[0] + "-" + name[1]}
    dfResults = dfResults.append(
      dic, ignore_index=True)
  return dfResults


def binom_test_horse_race_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  horse_race_genes = group["logFC"][group["logFC"] > 0].shape[0]
  sizeOfPopulationDE = group["logFC"][group["logFC"] >
                                      0].shape[0] + group["logFC"][group["logFC"] < 0].shape[0]
  pval_horse_race = stats.binom_test(
      horse_race_genes,
      n=sizeOfPopulationDE,
      p=0.5,
      alternative='two-sided')
  return pval_horse_race, horse_race_genes, sizeOfPopulationDE


def binom_test_two_fold_rule(group: pd.DataFrame) -> List[float]:
  """This function aims to compute binomial test
  to search for biased expression between ohnologous genes based on
  horserace rule -> FC > 0 Is an upregulated gene

  Parameters
  ----------
  group : pd.Dataframe
      [description]

  Returns
  -------
  List[float]
      [description]
  """
  two_fold_genes = group["logFC"][group["logFC"] > 2].shape[0]
  sizeOfPopulationHighlyDE = group["logFC"][group["logFC"] >
                                            2].shape[0] + group["logFC"][group["logFC"] < -2].shape[0]
  pval_two_fold_rule = stats.binom_test(
    two_fold_genes,
    n=sizeOfPopulationHighlyDE,
    p=0.5,
    alternative='two-sided')
  return pval_two_fold_rule, two_fold_genes, sizeOfPopulationHighlyDE


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-i",
                      "--input",
                      help="Path to statistical results output files",
                      type=str)
  parser.add_argument("-o",
                      "--output",
                      help="Path to aggregated statistical test output files",
                      type=str)
  args = parser.parse_args()
  return args


args = generate_arguments()
list = []
inputFilenames = args.input.split()
for i, filename in zip(range(0, len(inputFilenames)), inputFilenames):
  splittedFilename = filename.split("/")
  df = pd.read_csv(filename, index_col=0, header=0)
  df["experience"] = splittedFilename[2] + "-" + splittedFilename[3]
  df = df.loc[:, ~df.columns.str.startswith('logCPM_')]
  # if i == 0:
  #   df.to_csv('file.csv', index=False)
  # else:
  #   df.to_csv('file.csv', mode='a', header=False, index=False)
  list.append(df)

# finalDf = pd.read_csv('file.csv', index_col=0, header=0)
finalDf = pd.concat(list, axis=0, ignore_index=True)
finalDf = finalDf.drop_duplicates()
binomTestsResults = pd.DataFrame(columns=["number_upregulated_genes_two_fold",
                                          "number_of_two_fold_genes",
                                          "pval_two_fold_rule",
                                          "number_upregulated_genes_horserace",
                                          "number_of_horserace_genes",
                                          "pval_horse_race",
                                          "couple"])


binomTestsResults = compute_binom_tests(finalDf, binomTestsResults)

binomTestsResults.to_csv(args.output)


if os.path.exists("file.csv"):
  os.remove("file.csv")
# %%
