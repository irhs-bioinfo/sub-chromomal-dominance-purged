# %%
import pandas as pd
import argparse


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-i",
                      "--input",
                      help="Path to statistical results output files",
                      type=str)
  parser.add_argument("-o",
                      "--output",
                      help="Path to aggregated statistical test output files",
                      type=str)
  args = parser.parse_args()
  return args


args = generate_arguments()

list = []

# Iterate on args.input (list of anaDiff files with gene name reconstructed)
# Please note the use of set to avoid duplicated path, avoiding to parse more than one times each files
for filename in set(args.input.split()):
  # Load anaDiff files
  df = pd.read_csv(filename, index_col=0, header=0)
  # Save opened anaDiff file in a list
  list.append(df)


finalDf = list[0].copy(deep=True)
for i in range(1, len(list)):
  finalDf = finalDf.reset_index().merge(
    list[i], on=["genes", finalDf.index]).set_index('index')
  finalDf = finalDf.loc[:, ~finalDf.columns.str.contains('^key', case=False)]


finalDf['InSomeDf'] = finalDf.loc[:, finalDf.columns.str.contains(
  '^InSomeDf_', case=False)].sum(axis=1)


finalDf = finalDf.loc[:, ~finalDf.columns.str.contains(
  '^InSomeDf_', case=False)]

# Save final result to a file
finalDf.to_csv(args.output)
