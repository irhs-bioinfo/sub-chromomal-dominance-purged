# %%
import argparse
import sys
from typing import Dict, List, Optional, Set, Tuple

import numpy as np
import pandas as pd
from tqdm import tqdm


def ratio_ambig(row):
  if row["UniqueCount"] != 0:
    return row["AmbigCount"] / row["UniqueCount"]
  else:
    return 0


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-a",
                      "--allSRR",
                      help="all SRR IDs",
                      type=str)
  args = parser.parse_args()
  return args


def generate_paths(groupReplicate: pd.DataFrame) -> Tuple[str, str, str]:
  """This function aims to generate path to access to salmon file and to output files

  Parameters
  ----------
  nameReplicate : string
      This string store name of replicate such as "Biological replicate 1"
  groupReplicate : pd.DataFrame
      This dataframe stored grouped dataframe for each repicate

  Returns
  -------
  List
      A list of paths
  """
  pathToSalmonQuant = "resources/salmon/" + \
                      groupReplicate["study_accession"].values[0] + \
                      "/" + groupReplicate["experiment_accession"].values[0] + \
                      "/" + groupReplicate["run_accession"].values[0] + \
                      "/quant.sf"
  pathToSalmonAmbigInfo = "resources/salmon/" + \
      groupReplicate["study_accession"].values[0] + \
      "/" + groupReplicate["experiment_accession"].values[0] + \
      "/" + groupReplicate["run_accession"].values[0] + \
      "/aux_info/ambig_info.tsv"
  return pathToSalmonQuant, pathToSalmonAmbigInfo


def main():
  args = generate_arguments()
  allSRR = pd.read_csv(args.allSRR, index_col=0)
  for nameRuns, groupRuns in tqdm(allSRR.groupby(by=["run_accession"]), desc="Computed runs"):
    # Generate all needed files
    pathToSalmonQuant, pathToSalmonAmbigInfo = generate_paths(groupRuns)
    # Read raw file from salmon
    quantDf = pd.read_csv(pathToSalmonQuant, sep="\t")
    # Read raw file from salmon
    ambigInfo = pd.read_csv(pathToSalmonAmbigInfo, sep="\t")
    numberOfAmbig = pd.concat([quantDf, ambigInfo], axis=1)
    numberOfAmbig = numberOfAmbig[["Name", "UniqueCount", "AmbigCount"]]
    numberOfAmbig["RatioAmbig"] = numberOfAmbig.apply(ratio_ambig, axis=1)
    numberOfAmbig["experience"] = nameRuns
    numberOfAmbig.to_csv("results/downstream_analysis/ambigInfo.tsv",
                         sep="\t",
                         mode="a",
                         index=False,
                         header=True)


if __name__ == "__main__":
  main()
