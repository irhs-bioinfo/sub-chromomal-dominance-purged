# %%
import os
import os.path
from typing import List, Set, Dict, Tuple, Optional
import pandas as pd
import seaborn as sns
import argparse


def reconstruct_right_couples(dfRow: pd.DataFrame,
                              ohnologousGenesUniqueNames: pd.DataFrame) -> pd.DataFrame:
  """Iterate on each row,
  get ID and gather in ohnologues genes, initial ID of gene

  Parameters
  ----------
  dfRow : pd.DataFrame
      Result from anaDiff script, with unique ID to encode ohnologus genes
  ohnologousGenesUniqueNames : pd.DataFrame
      Dataframe storing inital genes names

  Returns
  -------
  pd.DataFrame
      Result from anaDiff script with intial genes names
  """
  row = ohnologousGenesUniqueNames[ohnologousGenesUniqueNames["uniqueName"] == dfRow["id"]]
  dfRow["couple"] = row["gene_x"].values[0][2:4] + \
      "-" + row["gene_y"].values[0][2:4]
  dfRow["gene_x"] = row["gene_x"].values[0]
  dfRow["gene_y"] = row["gene_y"].values[0]
  return dfRow


def generate_arguments():
  """This function generate arguments parser

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument("-d",
                      "--pathDeseq2",
                      help="Path to Deseq2 output file",
                      type=str)
  parser.add_argument("-e",
                      "--pathEdgeR",
                      help="Path to EdgeR output file",
                      type=str)
  parser.add_argument("-u",
                      "--uniqueNames",
                      help="Path to table of conversion of gene names",
                      type=str)
  args = parser.parse_args()
  return args


args = generate_arguments()
pathDeseq2 = args.pathDeseq2
pathEdgeR = args.pathEdgeR
uniqueNames = args.uniqueNames


# %%
# Open files
deseq2 = pd.read_csv(pathDeseq2, sep="\t")
edgeR = pd.read_csv(pathEdgeR, sep="\t")
ohnologousGenesUniqueNames = pd.read_csv(uniqueNames, sep=",", index_col=0)

# Iterate on each row, get ID and gather in ohnologues genes, initial ID of genes
deseq2 = deseq2.apply(reconstruct_right_couples, axis=1,
                      args=(ohnologousGenesUniqueNames,))
edgeR = edgeR.apply(reconstruct_right_couples, axis=1,
                    args=(ohnologousGenesUniqueNames,))

pathDeseq2 = pathDeseq2.replace(".txt", "_genesCouples.txt")
pathEdgeR = pathEdgeR.replace(".txt", "_genesCouples.txt")

deseq2.to_csv(pathDeseq2)
edgeR.to_csv(pathEdgeR)
# %%
