# %% [markdown]
"""
## Aims of this script

This script aims to parse allSRR.purged.csv, a file containning all IDs, and metadatas of RNA-Seq studies.
"""

# %%
import json
import os

import pandas as pd
import seaborn as sns

# %%


def get_percent_mapped(dfRow: pd.DataFrame) -> pd.DataFrame:
  """ This function aims to open salmon output metadatas and get percent_mapped field

  Parameters
  ----------
  dfRow : pd.DataFrame
      A row with informations of a run

  Returns
  -------
  pd.DataFrame
      A row with informations of a run and percent of mapped reads
  """
  pathToSalmonAuxInfo = "../../resources/salmon/" + \
                        dfRow["study_accession"] + \
                        "/" + dfRow["experiment_accession"] + \
                        "/" + dfRow["run_accession"] + \
                        "/aux_info/meta_info.json"

  if os.path.isfile(pathToSalmonAuxInfo):
    with open(pathToSalmonAuxInfo) as json_file:
      auxInfo = json.load(json_file)
      dfRow["percent_mapped"] = auxInfo["percent_mapped"]
  return dfRow


def drop_na_and_not_enough_replicates(allSRR: pd.DataFrame,
                                      numberOfReplicates: int) -> pd.DataFrame:
  """ This function aims to drop non splitingColumns
  and studies with not enought replicates

  Parameters
  ----------
  allSRR : pd.DataFrame
      Dataframe with all runs informations
  numberOfReplicates : int
      Number of needed replicates

  Returns
  -------
  pd.DataFrame
      Dataframe with all only filtered runs
  """
  allSRR = allSRR.dropna(how="all")
  allSRR = allSRR[allSRR['SplittingColumn'].notna()]
  allSRRGrouped = allSRR.groupby(by=["SplittingColumn"])
  allSRR = pd.DataFrame()
  for name, group in allSRRGrouped:
    if group.shape[0] >= numberOfReplicates:
      allSRR = allSRR.append(group, ignore_index=True)
  return allSRR


# %%
# Load ohnlogous genes
ohnologousGenes = pd.read_csv(
  "../../resources/ohnologousGenes/ohnologousGenesParsed.csv", sep="\s")
# Assign a unique ID for each Couple based on index
ohnologousGenes["uniqueName"] = ohnologousGenes.index
# Save this new ID
ohnologousGenes.to_csv(
  "../../resources/ohnologousGenes/ohnologousGenes_unique_names.csv")

# Get SRR metadatas
allSRR = pd.read_csv("../../resources/allSRR/allSRR.purged.csv", index_col=0)

# Check if old annotation needs to be merged into new metadatas
if os.path.isfile("../../resources/allSRR_589_bak/allSRR.purged.csv"):
  # Open annotated SRR
  allSRROldAnnotation = pd.read_csv(
    "../../resources/allSRR_589_bak/allSRR.purged.csv", index_col=0)
  # Keep only needed columns
  allSRROldAnnotation = allSRROldAnnotation[["run_accession",
                                             "study_accession",
                                             "SplittingColumn",
                                             "studied_tissue",
                                             "studied_condition",
                                             "experiment_accession",
                                             "replicate"]]

  tmp = allSRR.merge(allSRROldAnnotation,
                     on=["run_accession",
                         "study_accession",
                         "SplittingColumn",
                         "studied_tissue",
                         # "studied_condition",
                         "experiment_accession",
                         "replicate"],
                     how="left")

  tmp.to_csv("../../resources/allSRR.to_annotate.purged.csv")
# %%

if os.path.isfile("../../resources/allSRR.to_annotate.purged.csv"):
  allSRR = pd.read_csv("../../resources/allSRR.to_annotate.purged.csv",
                       index_col=0)


# Get percent of mapped reads using salmon metadata file
allSRR = allSRR.apply(get_percent_mapped, axis=1)
# Plot distribution of mapped reads
sns.displot(data=allSRR, x="percent_mapped")
# Copy it to compute a diff
allSRRDropped = allSRR.copy(deep=True)
# Keep only SRR with at least 70% of mapped reads
allSRR = allSRR[allSRR["percent_mapped"] > 70.0]
# Keep only batch with at least 3 replicates
allSRR = drop_na_and_not_enough_replicates(allSRR, 3)
# Save dropped rows for historic
allSRRDropped = allSRRDropped[~allSRRDropped["run_accession"].isin(
  allSRR["run_accession"])]
# Save dropped rows
allSRRDropped.to_csv("../../resources/allSRR/allSRR.purged.dropped.csv")
# Save kept SRR to snakemake pipeline
allSRR[["run_accession", "study_accession", "SplittingColumn", "experiment_accession"]].to_csv(
  "../../config/samples.csv")
# Drop empty columns
allSRR.dropna(how='all', axis=1, inplace=True)

allSRR.to_csv("../../resources/allSRR/allSRR.purged.purged.csv")

# %%
