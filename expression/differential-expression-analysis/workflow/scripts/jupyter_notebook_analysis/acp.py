# %%


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.express as px
import prince
from sklearn.preprocessing import OneHotEncoder
import seaborn as sns
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
# %%


def pca_visualisation(pca, components, dimensions):
  explainedVariance = pca.explained_variance_ratio_.sum()
  # Visualise it
  labels = {
      str(i): f"PC {i+1} ({explainedVariance:.1f}%)"
      for i, explainedVariance in enumerate(pca.explained_variance_ratio_ * 100)
  }

  fig = px.scatter_matrix(
      components,
      labels=labels,
      dimensions=range(dimensions),
      title=f'Total Explained Variance: {explainedVariance:.1f}',
      # color=X["couple"]
  )
  fig.update_traces(diagonal_visible=False)
  return fig


# %%
LogFcDf = pd.read_csv(
    "../../../results/anaDiff/SRP050139/seedling-shoot-apex/summary_SRP050139/SRP050139_DESeq2_genesCouples.txt", index_col=0, header=0)
# %%
X = LogFcDf[["logFC", "pval_DESeq2"]]
X = X.fillna(0)


# %%
# Step 1
# Load and Standardize Data

# make sure that we standardize the data by transforming it onto a unit scale (mean=0 and variance=1)
sc = StandardScaler()
# Standardize LogFcDf
X_normalized = sc.fit_transform(X)

# %%
# Step 2: Covariance Matrix and Eigendecomposition
# a covariance matrix is created based on the standardized data. The covariance matrix is a representation of the covariance between each feature in the original dataset.
mean_vec = np.mean(X_normalized, axis=0)
cov_mat = (X_normalized - mean_vec).T.dot((X_normalized - mean_vec)
                                          ) / (X_normalized.shape[0] - 1)
print('Covariance matrix \n%s' % cov_mat)

# After the covariance matrix is generated, eigendecomposition is performed on the covariance matrix. Eigenvectors and eigenvalues are found as a result of the eigendceomposition. Each eigenvector has a corresponding eigenvalue, and the sum of the eigenvalues represents all of the variance within the entire dataset.
cov_mat = np.cov(X_normalized.T)
eig_vals, eig_vecs = np.linalg.eig(cov_mat)
print('Eigenvectors \n%s' % eig_vecs)
print('\nEigenvalues \n%s' % eig_vals)


# Visually confirm that the list is correctly sorted by decreasing eigenvalues
eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:, i])
             for i in range(len(eig_vals))]
print('Eigenvalues in descending order:')
for i in eig_pairs:
  print(i[0])

# %%
pca = PCA().fit(X_normalized)
exp_var_cumul = np.cumsum(pca.explained_variance_ratio_)
components = pca.fit_transform(X_normalized)

# Construct a Screeplot to test for more components
fig = px.area(
    x=range(1, exp_var_cumul.shape[0] + 1),
    y=exp_var_cumul,
    labels={"x": "# Components", "y": "Explained Variance"}
)
fig.show()


fig1 = pca_visualisation(pca, components, 2)
fig1.show()


# %%

LogFcDf = LogFcDf.groupby(
  'couple').filter(lambda x: x.shape[0] >= 400)
X = LogFcDf[["logFC", "pval_DESeq2", "couple"]]
X = X.fillna(0)


model = prince.FAMD(
    n_components=X.shape[1],
    copy=True,
    check_input=True,
    engine='auto',
    random_state=1
).fit(X)

model.row_coordinates(X)

ax = model.plot_row_coordinates(
    X,
    ax=None,
    figsize=(6, 6),
    # x_component=0,
    # y_component=1,
    color_labels=['couple {}'.format(t) for t in X['couple']],
    # ellipse_outline=False,
    # ellipse_fill=True,
    show_points=True
)

model.explained_inertia_
for name, fa in sorted(model.partial_factor_analysis_.items()):
  print('{} eigenvalues: {}'.format(name, fa.eigenvalues_))
# %%

# %%
