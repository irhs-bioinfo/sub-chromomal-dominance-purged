# KaKS - Orthofinder - iADHoRe

## Installation

## Dependencies

### Tools

Following table gather all tools used in project:

| Installed tool | Version     |
|----------------|-------------|
| ncbi Blast     | 2.10.1      |
| Circos         | 0.69-9      |
| MCScanX        | 2           |
| Orthofinder    | 2.4.0       |
| Mafft          | 7.407       |
| FastTree       | 2.1.11 SSE3 |
| FastME         | 2.1.6.2     |
| DIAMOND        | 0.9.29      |
| Pal2Nal.pl     | 14          |
| PAML           | 4.9j        |
| bedops         | 2.4.39      |
| python         | 3.*         |
| python deps    | requirements|
| clustal omega  | 1.2.4       |
| PAML           | 4.9j        |
| Pal2Nal.pl     | 14          |

### Python libraries

Exact dependencies version of all used libraries are stored in [environment file](./environment.yml).
Principal dependecies are gathered in following table:

| Library name | Version |
|--------------|---------|
| python       | 3.7.7   |
| Pandas       | 1.1.1   |
| Seaborn      | 0.10.1  |
| Numpy        | 1.19.1  |
| jupyter      | 1.0.0   |
| matplotlib   | 3.3.1   |
| notebook     | 6.1.1   |
| scipy        | 1.5.2   |
| statsmodels  | 0.11.1  |
| tqdm         | 4.48.2  |

## Deployement

### Python environment

#### Using a virtual environment

To deploy an environment that gathers all needed dependencies please follow these steps. If you want to use Pipenv:

    # Install it
    sudo apt install pipenv
    # Go at root of the repository
    cd kaks-orthofinder
    pipenv install

To use pip environnements

    # Activate your virtual environnement and:
    pip install -r requirements.txt

To use conda environnements

    # Install dependencies
    apt-get install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6
    # Download it
    wget https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh
    # Install it
    bash Anaconda3-2020.07-Linux-x86_64.sh
    # Update it
    conda update conda
    # Create env with all dependencies
    conda create --name kaks --file environment.yml
    conda activate kaks

#### Using Docker

An image (called kaks-orthofinder:master in forgeMIA) will contain the code as well as the tools required for analysis via PAML. In the case of [orthofinder](orthofinder.sh), [make_blast_and_mcscanx](make_blast_and_mcscanx.sh), [from_orthofinder_to_iadhore](from_orthofinder_to_iadhore.sh) bash scripts, it is necessary to build the images of the necessary tools or pulls from the forgeMIA.

Here is steps to follow to get image storing PAML pipeline. To get other tool please use one of those tag:

| Tag          | Installed tool | Version     |
|--------------|----------------|-------------|
| blast        | ncbi Blast     | 2.10.1      |
| circos       | Circos         | 0.69-9      |
| clustalOmega | clustal omega  | 1.2.4       |
| mcscanx      | MCScanX        | 2           |
| orthofinder  | Orthofinder    | 2.4.0       |
|              | Mafft          | 7.407       |
|              | FastTree       | 2.1.11 SSE3 |
|              | FastME         | 2.1.6.2     |
|              | DIAMOND        | 0.9.29      |
| pal2nal      | Pal2Nal.pl     | 14          |
| paml         | PAML           | 4.9j        |
| bedops       | bedops         | 2.4.39      |
| master       | python         | 3.*         |
|              | python deps    | requirements|
|              | clustal omega  | 1.2.4       |
|              | PAML           | 4.9j        |
|              | Pal2Nal.pl     | 14          |



First login to container registry using

    docker login registry.forgemia.inra.fr/tanguy.lallemand/kaks-orthofinder -u login -p access_token

Continue by pulling image

    docker pull registry.forgemia.inra.fr/tanguy.lallemand/kaks-orthofinder:master

You can rename image with a more readable one

    docker image tag registry.forgemia.inra.fr/tanguy.lallemand/kaks-orthofinder:master

Start a container

    docker run --rm -it -p 8885:8888 ka-ks-analysis:latest

finally go to http://localhost:8895 and start to use notebook from src directory


## Pipeline to construct ka_ks datas

### Loading genomic data

For this part different genetic data are loaded. First of all, the protein sequences of the organisms studied are gathered from repositories. The sequences are downloaded from GDR. Malus genome is downloaded as GDDH13 version 1.1 and prunus persica is downloaded in v2.0.a1.
The URLs used are provided below:

    ftp://ftp.bioinfo.wsu.edu/species/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/GDDH13_1-1_prot.fasta.gz
    ftp://ftp.bioinfo.wsu.edu/species/Malus_x_domestica/Malus_x_domestica-genome_GDDH13_v1.1/genes/gene_models_20170612.gff3.gz
    ftp://ftp.bioinfo.wsu.edu/species/Prunus_persica/Prunus_persica-genome.v2.0.a1/genes/Prunus_persica_v2.0.a1.primaryTrs.pep.fa.gz
    ftp://ftp.bioinfo.wsu.edu/species/Prunus_persica/Prunus_persica-genome.v2.0.a1/genes/Prunus_persica_v2.0.a1.gene.gff3.gz

### Parsing of Orthogroups

Orthogroups can come from different tools. They can come from Orthofinde run tool allowing to build gene families, through an approach by Reciprocal Best Blast hit or Best Blast hit with syntenic informations. This two approaches allows to construct triplet of high quality.

#### OrthoFinder

Orthgroups are coming from another module of this project and was generated using Orthofinder 2.4.0 containerized using following dockerfile: [Orthofinder](dockerfiles/orthofinder.dockerfile).
In details, following call were used:

    orthofinder -f /input -S blast -M msa -a 1 -t 4

Following tools are used:
Blast +
MAFFT is used for the multiple alignment
FastTree for the tree inference

#### Reciprocal Best Blast hit

The principle of this approach is to make four blast all against all:

- a forward malus vs malus and its associated reverse equivalent
- a prunus malus and its associated reverse side

By merging all results it is possible tro construct triplet with two malus ohnologous genes and one associated prunus gene

#### Best Blast hit with syntenic informations

This approach gather syntenic informations to get ohnologous genes in Malus genome and blast this couples on Prunus genome to get a mutual Best Blast hit. Each triplet is gatherred and will be input for current project.


### Construct orthogroup's fasta files

For each orthgroup, sequence ID are retreived and used to construct two fasta files:

- One storing proteine sequences and stored in ./output/temp/protein_fasta_files
- Another storing nucleic sequences and stored in ./output/temp/nucleic_fasta_files

Please be advised that order of sequences and header informations are important to allow PAL2NAL.pl to run.
Here is example for orthogroup with ID: OG0018388 that gather two genes:
MD17G1070500 and Prupe.3G243700.1

    ./output/temp_orthofinder/nucleic_fasta_files/OG0008091.fasta

    >MD05G1113000
    TTTGTTCAAAGGTGAGGTACTGACAGATGAACTACGTACACAAGCAACTCAGCAAGAGCT
    ATATATAAAAGCTTGAAGCTGCAGGTCAAGGTCTTGTGCTCTCTAAGGGAGAGAGAATCA
    CTTGGGAGAAGAACAAGCTTTGGTCATTAATTGTTCGATCGAGTGACCCTTCCGTGAGCT
    ...
    >MD10G1115500
    CACAAGAACTTAGCAAGAGCTATAAAAGCTTGAGGCTACGGGTCAAGGTCTTGTACGATC
    TCTAAGGGAGAGAGAGTCACTTGCAAGAAGAATAAGCTTTGGTCATTAATTGTTCGAGAA
    TAAGCTTTCTTACTTCTAATACTATTCACTAAATATTTGTCGTATGATTCAGCTTCTTCT
    ...
    >Prupe.8G157500.1 polypeptide=Prupe.8G157500.1.p locus=Prupe.8G157500 ID=Prupe.8G157500.1_v2.0.a1 annot-version=v2.1
    ATGGAGATTTCGTCGATAGGAGGGATGTCTGAACTGGGAATGGAAGATCCCTACTTCATC
    AACCAGTGGCACATGAACTCTCTGGATGAAGTCAGCATGCTGCCACTAGCAGCTCCATTT
    GGAGAGAACTTTAACCAGTCTCACTTTCACCCAAATTTCAATCTTAAAACTTCTATGGAT
    ...

    ./output/temp_orthofinder/protein_fasta_files/OG0008091.fasta

    >MD05G1113000
    MEISSRGGLSELGMEDPFFINQWQYMNSLDELGMLPLAAAFGENLQHSQFHPIFNLKTPI
    DNSHTAIDRPTKQLKTDGWTSCKTDHLVSNPQVASLPPNILSFVNSHHANQMPVLKPKGE
    GAAVCSKSTNTLPSDILLSQGSFGNRNYSSNGISTTTRLSQTQDHIIAERKRREKLSQRF
    IALSAIVPGLKKMDKASVLGDSIKYIKQLQDKVKTLEEQTRNKNMESVVFVKKTQLFANS
    DSSCSDENSSTGPFDTTLPEIEARFCDNNVLIRIHCEKRKGIVEKTIAEVEKLQLKFINS
    SVMTFGSCALDVTIIAQMEAEFSLSVKDLVKNLRAAFELFM
    >MD10G1115500
    MEISSRGGLSELGMEDPFFINQWQYMNSLDELGMLPLAAAIGDNLQHSHFHPTFNLKTPI
    DNSHTGIDRPAKQLKTDGWTSCKTDHLVLNPQVASSPNILSFVNSNHGNQMVVLKPKEEA
    AAVCSKSTNTLPSDILLSQGSFGNQNCLFKASQGTNRNSTNPRLSATQDHIIAERKRREK
    LSQRFVALSALIPGLKKMDKASVLGDSIKYIKQLQDKVKTLEEHTRNKNMESVVFVKKTQ
    LFANGDKTSSNENNSTGPFDATLPEIEARFCDNNVLIRIHCEKRKGVVEKTISEVEKLQL
    KFINSSVMTFGSCALDVTIIAQMEAEFSLSVQDLVKNLRTAFNLFM
    >Prupe.8G157500.1 polypeptide=Prupe.8G157500.1.p locus=Prupe.8G157500 ID=Prupe.8G157500.1_v2.0.a1 annot-version=v2.1
    MEISSIGGMSELGMEDPYFINQWHMNSLDEVSMLPLAAPFGENFNQSHFHPNFNLKTSMD
    SCHSGIDRPMKQLKTDGWSSCKTDQHGSNPQVASSPNILSFVNSNSTNQMGVLKPKEEAA
    VCSKSNNSLPSDILLSQSSFGNQSYLFKASQGTKRVNTNTRLSTTQDHIIAERKRREKLS
    QRFIALSAMVPGLKKMDKASVLGDAIKYIKQLQDKVKTLEEQTRKKNMESVVFVKKTQLF
    ANDDNSSSEENNSSGPFEETLPEIEARFCDNNVMIRIHCEKRKGVVEKTIAEVEKLQLKF
    INSSVLTFGGCALDVTIIAQMEVEFSLSVKELVKNLRSAFDMFM*


### Clustal Omega

The next step is to align the different protein sequences of each of the orthogroups. Multiple alignement need three or more sequences. Thus, orthgroups with two sequences are not used. To do this Clustal Omega in version 1.2.4 is used with following parameters:

    --outfmt fa to output in fasta format, a format used as input in next step
    --auto to detect autmomaticcaly if sequences are nucleic or proteic
    --threads 12
    -v to allows verbosity
    --force to allow overwrite of existing file without interaction
    --percentid is set to True to store distance as percentage of identity
    --distmat_full is set to True to output distance matrix with percentage of identity,
    --distmat_out get a path to output it in a file with .mat extension

This step is also implemented with a multithreading fashion. In this case threads is set to 1 and multiple alignement are launched in parallel.

Resulting alignements are stored in ./output/temp/alignements with .aln extension
Here is an example stored at:

    ./output/temp/alignements/OG0008091.fasta.aln

    >MD05G1113000
    MEISSRGGLSELGMEDPFFINQWQYMNSLDELGMLPLAAAFGENLQHSQFHPIFNLKTPI
    DNSHTAIDRPTKQLKTDGWTSCKTDHLVSNPQVASLPPNILSFVNSHHANQMPVLKPKGE
    GAAVCSKSTNTLPSDILLSQGSFGNRNYSSN------GISTTTRLSQTQDHIIAERKRRE
    KLSQRFIALSAIVPGLKKMDKASVLGDSIKYIKQLQDKVKTLEEQTRNKNMESVVFVKKT
    QLFANSDSSCSDENSSTGPFDTTLPEIEARFCDNNVLIRIHCEKRKGIVEKTIAEVEKLQ
    LKFINSSVMTFGSCALDVTIIAQMEAEFSLSVKDLVKNLRAAFELFM-
    >MD10G1115500
    MEISSRGGLSELGMEDPFFINQWQYMNSLDELGMLPLAAAIGDNLQHSHFHPTFNLKTPI
    DNSHTGIDRPAKQLKTDGWTSCKTDHLVLNPQVAS-SPNILSFVNSNHGNQMVVLKPKEE
    AAAVCSKSTNTLPSDILLSQGSFGNQNCLFKASQGTNRNSTNPRLSATQDHIIAERKRRE
    KLSQRFVALSALIPGLKKMDKASVLGDSIKYIKQLQDKVKTLEEHTRNKNMESVVFVKKT
    QLFANGDKTSSNENNSTGPFDATLPEIEARFCDNNVLIRIHCEKRKGVVEKTISEVEKLQ
    LKFINSSVMTFGSCALDVTIIAQMEAEFSLSVQDLVKNLRTAFNLFM-
    >Prupe.8G157500.1 polypeptide=Prupe.8G157500.1.p locus=Prupe.8G157500 ID=Prupe.8G157500.1_v2.0.a1 annot-version=v2.1
    MEISSIGGMSELGMEDPYFINQWH-MNSLDEVSMLPLAAPFGENFNQSHFHPNFNLKTSM
    DSCHSGIDRPMKQLKTDGWSSCKTDQHGSNPQVAS-SPNILSFVNSNSTNQMGVLKPKEE
    -AAVCSKSNNSLPSDILLSQSSFGNQSYLFKASQGTKRVNTNTRLSTTQDHIIAERKRRE
    KLSQRFIALSAMVPGLKKMDKASVLGDAIKYIKQLQDKVKTLEEQTRKKNMESVVFVKKT
    QLFANDDNSSSEENNSSGPFEETLPEIEARFCDNNVMIRIHCEKRKGVVEKTIAEVEKLQ
    LKFINSSVLTFGGCALDVTIIAQMEVEFSLSVKELVKNLRSAFDMFM*

### PAL2NAL

PAL2NAL is used to converts multiple sequence alignment of proteins and the corresponding DNA (or mRNA) sequences into a codon alignment. The program automatically assigns the corresponding codon sequence even if the input DNA sequence has mismatches with the input protein sequence, or contains UTRs, polyA tails. It can also deal with frame shifts in the input alignment, which is suitable for the analysis of pseudogenes. This codon alignement can be outputed as PAML input format.

This tool is used in version 14 with following parameters:
-nogap
-output paml
This file needs a strict input: One file with protein sequences in fasta format and one file with nucleic sequences in fasta format too. Order of sequences between the input files must be conserved
Output files are stored in PAML format (a strict Phylip format) in ./output/temp/pal2nal/ with .pal2nal extension

Here is an example stored at:

    ./output/temp/pal2nal/OG0008091.pal2nal

    3   1014
    MD05G1113000
    ATGGAGATTTCATCAAGAGGAGGATTATCTGAGCTGGGAATGGAGGATCCCTTCTTCATC
    AACCAGTGGCAGATGAACTCTTTGGATGAGCTCGGTATGCTCCCATTAGCAGCTGCATTT
    GGAGAGAATTTACAACACTCTCAGTTCCACCCAATTTTCAACCTCAAAACTCCAATCGAT
    ...
    MD10G1115500
    ATGGAGATTTCATCAAGAGGAGGATTATCTGAGCTGGGAATGGAAGATCCCTTCTTCATC
    AACCAGTGGCAGATGAACTCTTTGGATGAGCTCGGTATGCTCCCACTAGCAGCTGCAATT
    GGAGATAATTTACAACACTCTCACTTCCACCCAACTTTCAACCTCAAAACTCCTATCGAT
    ...
    Prupe.8G157500.1
    ATGGAGATTTCGTCGATAGGAGGGATGTCTGAACTGGGAATGGAAGATCCCTACTTCATC
    AACCAGTGGCACATGAACTCTCTGGATGAAGTCAGCATGCTGCCACTAGCAGCTCCATTT
    GGAGAGAACTTTAACCAGTCTCACTTTCACCCAAATTTCAATCTTAAAACTTCTATGGAT
    ...
### PAML

#### Computing ka/ks

Finally, we try to calculate the values of the synonymous and non-synonymous substitution rate as well as the ratio of these two values (omega or ka/ks).
This is allowed using the PAML package of Biopython. This package requires the installation of the PAML tool, which has been added to the development container. This tool gathers different tool wrapper and in particular the one allowing to call the yn00 tool. This function allows to compute the ka/ks between pairs of aligned genes (at DNA level). Thus for each of the files generated by pal2nal, we have DNA alignments which are then used to calculate the omega ratio. The results are then stored in the paml folder with the extension .yn00.

Different methods are implemented. This method use different counting methods detailled there:

- NG86 (Nei and Gojobori 1986). NG86 assumes no transition-transversion rate difference and no codon usage bias
- LWL85 (Li,Wu, and Luo 1985)
- LPB (Li 1993; Pamilo and Bianchi 1993)
- LWL85m, a modified version of LWL85 (Yang 2006, p. 55) LWL85, LPB, and LWL85m all account for the transition-transversion rate difference to some extent but assume no codon usage bias.
- YN00 (Yang and Nielsen (2000)) accommodates both the transition-transversion rate difference and unequal codon frequencies.

Here is an example stored at:

    ./output/temp/yn00/OG0008091.yn00

          3   1014

    MD05G1113000                    ATG GAG ATT TCA TCA AGA GGA GGA TTA TCT GAG CTG GGA ATG GAG GAT CCC TTC TTC ATC AAC CAG TGG CAG ATG AAC TCT TTG GAT GAG CTC GGT 
    ...
    MD10G1115500                    ATG GAG ATT TCA TCA AGA GGA GGA TTA TCT GAG CTG GGA ATG GAA GAT CCC TTC TTC ATC AAC CAG TGG CAG ATG AAC TCT TTG GAT GAG CTC GGT 
    ...
    Prupe.8G157500.1                ATG GAG ATT TCG TCG ATA GGA GGG ATG TCT GAA CTG GGA ATG GAA GAT CCC TAC TTC ATC AAC CAG TGG CAC ATG AAC TCT CTG GAT GAA GTC AGC ATG CTG CCA CTA GCA GCT CCA TTT GGA GAG AAC TTT AAC CAG TCT CAC TTT CAC CCA AAT 
    ...



    Printing out site pattern counts


            3        543  P

    MD05G1113000                    AAA AAA AAA AAC AAC AAC AAC AAG AAG AAG AAT AAT AAT AAT ACA ACA ACA ACA ACC ACC ACC ACC ACC ACG ACG ACG ACG ACT ACT ACT ACT ACT AGA AGA AGA AGC AGC AGC AGC AGC AGC AGG AGT AGT AGT ATA ATA ATC ATC ATC ATC ATC ATG ATG ATT ATT ATT ATT ATT CAA CAA CAA CAC CAC CAC CAG CAG CAG CAG CAG CAT CAT CAT CAT CCA CCA CCA CCA CCC CCG CCG CCT CCT CGA CGC CGC CGG CGG CTA CTC CTC CTC CTC CTG CTG CTG CTG CTT CTT CTT GAA GAA GAA GAC GAC GAC GAC GAG GAG GAG GAG GAG GAT GAT GAT GCA GCA GCA GCA GCA GCC GCC GCC GCG GCT GGA GGA GGA GGC GGG GGG GGG GGT GGT GGT GTA GTA GTC GTG GTG GTG GTG GTT GTT GTT TAC TAT TCA TCA TCA TCA TCA TCA TCC TCG TCG TCT TCT TCT TCT TCT TGC TGC TGG TGT TTA TTA TTA TTA TTC TTC TTC TTC TTG TTG TTG TTG TTT TTT TTT TTT 
    MD10G1115500                    ... ... ..G ... ... ... ..T ... ... C.. ..G ... ... ... ... ... ... ..G .A. ... ... ..T ..T ..A ... G.A G.. ..C ..C ... ... C.. ... ... ..G .A. ... ... ... ..T G.. ... .AA ... ... ... G.. .A. ... ... ... C.. ... ... .C. ..C ..C ... G.. ... ... ..G ... ... ..T ..C ... ... ..T GCA A.. ..C ... ... ... ... ... ..T ... ... GT. ... T.A .A. ... ... ... ..T ... ... ... ... ... ..A ... ... ... ... ... ... ... ..G ..G A.. ... ... ..T A.T ..A ... ... ..T ... ... ... A.. ... ... .G. T.G ..A ... .GT ... ... ... ... ... ... A.. .A. ... ..C ... ... ... ... ... ..C ... ... ... A.. ... ... ... .G. ... ... ... .T. .TG .TT ... ... ... A.. ..A ..A ... ... .C. ... ... ... C.. ... ... ... ... ... ... ..T ..A ... ... ... A.. ..C ... ... 
    Prupe.8G157500.1                ... ..G ... ... ..G ..T ... ..A ... ... ..G ..C ... .G. ... ..G ..T .GT .A. .A. ... ..T .GT ..A ..C GA. .T. ..C ... ... .G. ... ... .T. ... .AT .A. ... G.. ... GAT ... .AC ..C ... ... ... G.. ... ..G ..T ..G ... T.. .A. ..C ... ... G.. A.C ... ... ... ..G ... ..C ..C ... ... ACA A.. AGC ..A ... ... ..C ..G T.T ... ..C GG. ... T.. .A. A.G ... ... A.. ... ... ..G ..T G.. ... A.. ... ..T .AC ..C ... ... ... ..G ..A ... ..G ..T ..C ..A ..A ... ... ..A ..C ... T.T C.. ... A.. ... ..A ... .G. .T. ... ... ..C ..G ... A.. .A. ... ..C A.C A.. .G. ... ... ..C ..C ... ..T ... ..A ... ..T ... G.T ... ..G .TG ..G .TT ... ..A ..C ..A ..A ... ... .G. .C. ... ... ... C.. A.G ... ..T .A. ... ..T ..T C.T A.. C.. ... ... ..C ..C ... 

        9    1    1    7    1    3    1    2   12    1    1    3    3    2    3
        1    1    1    1    1    1    1    1    1    1    1    1    1    1    1
        2    1    4    1    1    1    1    2    1    1    1    2    1    1    1
        3    1    1    3    1    2    1    9    1    1    1    1    6    1    1
        6    1    2    1    1    1    1    5    1    1    1    1    1    1    3
        1    1    1    1    1    1    4    1    1    1    1    1    1    2    6
        1    2    1    1    1    2    1    1    1    5    7    1    2    1    4
        1    1    1    1    2    6    1    1    1    8    1    1    5    1    1
        1    1    1    1    7    5    2    1    1    1    1    1    1    1    1
        1    1    4    1    1    4    1    1    1    3    1    1    1    2    3
        1    1    1    2    1    2    1    1    1    8    1    1    1    2    4
        1    1    1    1    1    7    2    1    1    1    1    1    1    1    1
        3

    YN00 ../pal2nal/OG0008091.pal2nal

    ns =   3        ls = 338

    Codon position x base (3x4) table for each sequence.

    MD05G1113000        
    position  1:    T:0.18047    C:0.20118    A:0.34911    G:0.26923
    position  2:    T:0.29290    C:0.23964    A:0.33136    G:0.13609
    position  3:    T:0.27219    C:0.23669    A:0.25444    G:0.23669

    MD10G1115500        
    position  1:    T:0.17751    C:0.20118    A:0.34911    G:0.27219
    position  2:    T:0.29882    C:0.22781    A:0.34320    G:0.13018
    position  3:    T:0.27811    C:0.23669    A:0.25444    G:0.23077

    Prupe.8G157500.1    
    position  1:    T:0.17456    C:0.18047    A:0.36982    G:0.27515
    position  2:    T:0.29586    C:0.20414    A:0.35503    G:0.14497
    position  3:    T:0.28698    C:0.24556    A:0.23077    G:0.23669

    Average
    position  1:    T:0.17751    C:0.19428    A:0.35602    G:0.27219
    position  2:    T:0.29586    C:0.22387    A:0.34320    G:0.13708
    position  3:    T:0.27909    C:0.23964    A:0.24655    G:0.23471

    Codon usage for each species
    --------------------------------------------------------------------------------------
    Phe TTT   6   6   9 | Ser TCT  12   9  12 | Tyr TAT   1   0   2 | Cys TGT   4   5   5
        TTC  11  11   9 |     TCC   2   3   5 |     TAC   1   1   1 |     TGC   2   1   1
    Leu TTA   4   5   1 |     TCA   9   9   5 | *** TAA   0   0   0 | *** TGA   0   0   0
        TTG   4   4   3 |     TCG   3   4   4 |     TAG   0   0   0 | Trp TGG   2   2   2
    --------------------------------------------------------------------------------------
    Leu CTT   7   7   9 | Pro CCT   5   6   4 | His CAT   4   4   1 | Arg CGT   0   1   0
        CTC  10  11   7 |     CCC   1   1   3 |     CAC   4   5   6 |     CGC   2   2   1
        CTA   2   4   3 |     CCA   6   5   4 | Gln CAA   8   8   9 |     CGA   1   0   0
        CTG   5   4   5 |     CCG   2   1   1 |     CAG   9   8   7 |     CGG   2   1   1
    --------------------------------------------------------------------------------------
    Ile ATT  10   8   9 | Thr ACT   6   7   5 | Asn AAT   9  11   9 | Ser AGT   3   3   8
        ATC   8   8   4 |     ACC   5   4   3 |     AAC  12  15  16 |     AGC   7   4   6
        ATA   4   3   5 |     ACA   6   7   6 | Lys AAA  11  11  12 | Arg AGA   6   5   5
    Met ATG  10  10  15 |     ACG   4   2   1 |     AAG  15  16  16 |     AGG   2   4   5
    --------------------------------------------------------------------------------------
    Val GTT   5   5   6 | Ala GCT   7   7   8 | Asp GAT  10  12  10 | Gly GGT   3   3   0
        GTC   4   5   8 |     GCC   3   1   1 |     GAC   7   5   6 |     GGC   1   3   6
        GTA   2   3   2 |     GCA   9   9   7 | Glu GAA  10   8  13 |     GGA   8   9   6
        GTG   7   7   5 |     GCG   1   2   0 |     GAG  11  12  12 |     GGG   3   1   3
    --------------------------------------------------------------------------------------


    Sums
    ------------------------------------------------------
    Phe TTT  21 | Ser TCT  33 | Tyr TAT   3 | Cys TGT  14
        TTC  31 |     TCC  10 |     TAC   3 |     TGC   4
    Leu TTA  10 |     TCA  23 | *** TAA   0 | *** TGA   0
        TTG  11 |     TCG  11 |     TAG   0 | Trp TGG   6
    ------------------------------------------------------
    Leu CTT  23 | Pro CCT  15 | His CAT   9 | Arg CGT   1
        CTC  28 |     CCC   5 |     CAC  15 |     CGC   5
        CTA   9 |     CCA  15 | Gln CAA  25 |     CGA   1
        CTG  14 |     CCG   4 |     CAG  24 |     CGG   4
    ------------------------------------------------------
    Ile ATT  27 | Thr ACT  18 | Asn AAT  29 | Ser AGT  14
        ATC  20 |     ACC  12 |     AAC  43 |     AGC  17
        ATA  12 |     ACA  19 | Lys AAA  34 | Arg AGA  16
    Met ATG  35 |     ACG   7 |     AAG  47 |     AGG  11
    ------------------------------------------------------
    Val GTT  16 | Ala GCT  22 | Asp GAT  32 | Gly GGT   6
        GTC  17 |     GCC   5 |     GAC  18 |     GGC  10
        GTA   7 |     GCA  25 | Glu GAA  31 |     GGA  23
        GTG  19 |     GCG   3 |     GAG  35 |     GGG   7
    ------------------------------------------------------



    (A) Nei-Gojobori (1986) method



    Nei & Gojobori 1986. dN/dS (dN, dS)
    (Note: This matrix is not used in later ML. analysis.
    Use runmode = -2 for ML pairwise comparison.)

    MD05G1113000        
    MD10G1115500         0.2954 (0.0562 0.1902)
    Prupe.8G157500.1     0.2098 (0.0963 0.4590) 0.2225 (0.0934 0.4197)


    (B) Yang & Nielsen (2000) method

    Yang Z, Nielsen R (2000) Estimating synonymous and nonsynonymous substitution rates under realistic evolutionary models. Mol. Biol. Evol. 17:32-43

    (equal weighting of pathways)

    seq. seq.     S       N        t   kappa   omega     dN +- SE    dS +- SE

    2    1   256.9   757.1   0.2623  2.0753  0.3380 0.0584 +- 0.0091  0.1729 +- 0.0299
    3    1   248.4   765.6   0.5358  2.0753  0.2369 0.0998 +- 0.0120  0.4214 +- 0.0580
    3    2   248.6   765.4   0.5014  2.0753  0.2525 0.0969 +- 0.0118  0.3836 +- 0.0535


    (C) LWL85, LPB93 & LWLm methods

    Li W.-H., C.-I. Wu, Luo (1985) A new method for estimating synonymous and nonsynonymous rates of nucleotide substitutions considering the relative likelihood of nucleotide and codon changes. Mol. Biol. Evol. 2: 150-174.
    Li W-H (1993) Unbiased estimation of the rates of synonymous and nonsynonymous substitution. J. Mol. Evol. 36:96-99
    Pamilo P, Bianchi NO (1993) Evolution of the Zfx and Zfy genes - rates and interdependence between the genes. Mol. Biol. Evol. 10:271-281
    Yang Z (2006) Computational Molecular Evolution. Oxford University Press, Oxford. Eqs. 2.12 & 2.13

    2 (MD10G1115500) vs. 1 (MD05G1113000)

    L(i):      661.5     209.5     143.0  sum=   1014.0
    Ns(i):   20.5000   19.5000   12.0000  sum=  52.0000
    Nv(i):   15.0000    6.5000    7.5000  sum=  29.0000
    A(i):     0.0326    0.1064    0.0967
    B(i):     0.0232    0.0320    0.0554
    LWL85:  dS =  0.2069 dN =  0.0545 w = 0.2632 S =  212.8 N =  801.2
    LWL85m: dS =  0.1595 dN =  0.0592 w = 0.3709 S =  276.2 N =  737.8 (rho = 0.636)
    LPB93:  dS =  0.1579 dN =  0.0580 w = 0.3671

    3 (Prupe.8G157500.1) vs. 1 (MD05G1113000)

    L(i):      666.0     211.0     137.0  sum=   1014.0
    Ns(i):   24.5000   36.0000   17.5000  sum=  78.0000
    Nv(i):   29.5000   18.5000   22.0000  sum=  70.0000
    A(i):     0.0395    0.2319    0.1721
    B(i):     0.0464    0.0964    0.1937
    LWL85:  dS =  0.4777 dN =  0.0961 w = 0.2012 S =  207.3 N =  806.7
    LWL85m: dS =  0.4192 dN =  0.0997 w = 0.2379 S =  236.3 N =  777.7 (rho = 0.471)
    LPB93:  dS =  0.4021 dN =  0.0979 w = 0.2436

    3 (Prupe.8G157500.1) vs. 2 (MD10G1115500)

    L(i):      665.5     211.5     137.0  sum=   1014.0
    Ns(i):   22.0000   34.0000   16.0000  sum=  72.0000
    Nv(i):   31.0000   17.5000   20.5000  sum=  69.0000
    A(i):     0.0353    0.2137    0.1527
    B(i):     0.0489    0.0905    0.1778
    LWL85:  dS =  0.4361 dN =  0.0932 w = 0.2138 S =  207.5 N =  806.5
    LWL85m: dS =  0.3855 dN =  0.0965 w = 0.2503 S =  234.7 N =  779.3 (rho = 0.462)
    LPB93:  dS =  0.3676 dN =  0.0943 w = 0.2564


### Parsing results

Each output file are read using a function from biopython. Results are used to construct dataframes for each genetic model and outputed as CSV files.
To finish some downstreams analysis are runned:

- Percent identity of each alignements are gathered from distance matrix files stored at ./output/temp/distance_matrix and outputed in a CSV file
- Alignement lenght of each alignements are gathered from phylip files stored at ./output/temp/pal2nal and outputed in a CSV file

## Pipeline to analyse Ka Ks results

