FROM continuumio/miniconda3:4.8.2

ENV envName = 'kaks-comparisons'
RUN mkdir app


WORKDIR /app
# Create the environment:
COPY ./environment.yml .
# Update conda
RUN conda update -n base -c defaults conda
# Use conda env create instead of conda create
RUN conda env create --name ${envName} --file environment.yml

################## Allow rootless user ######################

RUN chown -R 1005:1005 /app && \
  chmod 755 /app && \
  chown -R 1005:1005 /app

SHELL ["conda", "run", "-n", "myenv", "/bin/bash", "-c"]
ENTRYPOINT ["conda", "run", "-n", "myenv", "python", "run.py"]