from statsmodels.stats.proportion import proportions_ztest
from scipy import stats
import pandas as pd
import numpy as np


def run_binom(df: pd.DataFrame, value: str) -> pd.DataFrame:
  """This function runs the proportion test to compare distributions of ohnologous chromosome.

  Parameters
  ----------
  df : pd.DataFrame
      KaKs dataframe.
  value : str
      Which value to use for the binom and Z proportion test. dS, dN or omega

  Returns
  -------
  pd.DataFrame
      Results of the binom and Z proportion test.
  """  
  results = pd.DataFrame()
  for name, group in df.groupby("couple"):
    if group.shape[0] > 800:
      group = group[group["Sequence2"].str.startswith("Prupe")]
      onFirst = group[group["geneChr"] == name.split("-")[0]]
      onSecond = group[group["geneChr"] == name.split("-")[1]]
      onFirst = onFirst[onFirst["geneCouple"].isin(onSecond["geneCouple"])]
      onSecond = onSecond[onSecond["geneCouple"].isin(onFirst["geneCouple"])]
      total = onFirst.merge(onSecond, on="geneCouple", how="inner")
      total["substract"] = total[value+"_x"] - total[value+"_y"]
      total["abs"] = np.abs(total["substract"])
      total = total[total["abs"] > 0.005]
      resultsBinom = stats.binom_test(x=total[total["substract"]>0].shape[0],n=total.shape[0],p=0.5)
      stat, pval = proportions_ztest(total[total["substract"]>0].shape[0], total.shape[0], 0.5)
      tmpDict = {"couple": name,
                 "pValue": resultsBinom,
                 "pvalueZ": pval,
                 "n": total.shape[0],
                 "gene_x_winning":total[total["substract"]>0].shape[0],
                 "gene_y_winning":total[total["substract"]<0].shape[0]}
      results = results.append(tmpDict, ignore_index=True)
  return results
