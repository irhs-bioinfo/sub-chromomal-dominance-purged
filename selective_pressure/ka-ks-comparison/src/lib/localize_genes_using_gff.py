def localize_gene_on_genome(dfRow, gffHandleDb):
  geneAnnotation = gffHandleDb['gene:' + dfRow['Sequence1']]
  dfRow['Sequence1-start'] = geneAnnotation.start
  dfRow['Sequence1-stop'] = geneAnnotation.stop
  return dfRow
