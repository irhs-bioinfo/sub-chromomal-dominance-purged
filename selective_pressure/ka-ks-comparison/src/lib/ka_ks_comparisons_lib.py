import pandas as pd
from scipy.stats import mannwhitneyu, ks_2samp
import numpy as np


def find_triplet(orthologueMalus, group):
  """[summary]

  Parameters
  ----------
  orthologueMalus : [type]
      [description]
  group : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  group = group.loc[~group['Sequence2'].str.contains('MD')]
  group = group.loc[~group['Sequence1'].str.contains('Prupe')]
  for name, group1 in group.groupby('Sequence2'):
    for index, row in orthologueMalus.iterrows():
      if not group1.loc[group1['Sequence1'] == row['Sequence1']].empty and not group1.loc[group1['Sequence1'] == row['Sequence2']].empty:
        return pd.concat([group1.loc[group1['Sequence1'] == row['Sequence1']], group1.loc[group1['Sequence1'] == row['Sequence2']]], axis=0)


def compute_statistical_test(groupedByChr, group1, group2, resultDf, test):
  """[summary]

  Parameters
  ----------
  groupedByChr : [type]
      [description]
  group1 : [type]
      [description]
  group2 : [type]
      [description]
  resultDf : [type]
      [description]
  test : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  test2 = groupedByChr.get_group(group1)
  test3 = groupedByChr.get_group(group2)
  if test == 'mannwhitneyu':
    stat, pvalue = mannwhitneyu(test2['omega'], test3['omega'])
  else:
    stat, pvalue = ks_2samp(test2['omega'], test3['omega'])
  newRow = pd.Series(data={'tested_group1': group1,
                           'tested_group2': group2,
                           'stat': stat,
                           'pvalue': pvalue,
                           'size1': test2.shape[0],
                           'size2': test3.shape[0],
                           'median1': test2['omega'].median(),
                           'median2': test3['omega'].median()})
  resultDf = resultDf.append(newRow, ignore_index=True)
  return resultDf


def compute_all_stat_tests(tripletDf, resultDf, testName):
  """[summary]

  Parameters
  ----------
  tripletDf : [type]
      [description]
  resultDf : [type]
      [description]
  testName : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  groupedByChr = tripletDf.groupby(['malus_chr', 'prunus_chr'])
  for name, group in groupedByChr:
    for name1, group1 in groupedByChr:
      if 'Chr00' not in name and 'Chr00' not in name1 and 'Pp00' not in name and 'Pp00' not in name1:
        if name[0] != name1[0] and name[1] == name1[1]:
          resultDf = compute_statistical_test(groupedByChr, name,
                                              name1,
                                              resultDf, testName)
  resultDfKS = resultDf.loc[(resultDf['size1'] > 100)
                            & (resultDf['size2'] > 100)]
  resultDfKS.reset_index(drop=True, inplace=True)

  resultDfKSpivoted = resultDfKS.pivot(
      "tested_group1", "tested_group2", "pvalue")
  return resultDf, resultDfKSpivoted

def select_only_coherent_couples(resultDfRow):
  """[summary]

  Parameters
  ----------
  resultDfRow : [type]
      [description]
  """
  if resultDfRow['size1'] > 10 and resultDfRow['size2'] > 10:
    resultDfRow['median2'] = np.nan


def add_results_in_dataframe(results, testName, couple, studiedVar, populationSize, stat, pvalue, meanDiffOmega):
  dictTemp = {}
  dictTemp["test"] = testName
  dictTemp["couple"] = couple
  dictTemp["studiedVar"] = studiedVar
  dictTemp["populationSize"] = populationSize
  dictTemp["stat"] = stat
  dictTemp["pvalue"] = pvalue
  dictTemp["meanDiffOmega"] = meanDiffOmega
  results = results.append(dictTemp, ignore_index=True)
  return results
