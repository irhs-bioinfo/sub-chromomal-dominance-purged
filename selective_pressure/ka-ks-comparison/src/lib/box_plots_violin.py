import pandas as pd
import plotly.graph_objects as go


def color(text: str, sizeOfGroup: str, color1: str, color2: str) -> str:
  """This function returns colored axis following splitting. Add also size of group

  Parameters
  ----------
  text : str
      couple name separated by a "-". Like: 01-07

  Returns
  -------
  str
      html formated text with added colors
  """
  splited = text.split("-")
  return "<span style='color: {}'> {} </span>".format(color1, splited[0]) + \
         "<span style='color: {}'> {} </span>".format("black", "-") + \
         "<span style='color: {}'> {} </span>".format(color2, splited[1]) + \
      "<br>" + \
         "<span style='color: {}'> (size: {} )</span>".format("black",
                                                              str(sizeOfGroup))


def boxplot(df: pd.DataFrame, statDf: pd.DataFrame, value: str, publi: bool):
  ticktext = []
  if publi:
    showlegend = False
    title = ""
  else:
    showlegend = True
    title = 'Distribution of {} for choromosome couple'.format(value)
  cat_order = df["couple"].unique()
  for name, group in df.groupby("couple"):
    ticktext.append(color(name, sizeOfGroup=int(
        group.shape[0] / 2), color1="#1B9E77", color2="#D95F02"))
  if value == "dN":
    range = [0, 0.4]
  else:
    range = [0, 1]

  first = df[df["first"] == "first"]
  second = df[df["first"] == "second"]
  fig = go.Figure()
  fig.add_trace(go.Box(
      x=first["couple"],
      y=first[value],
      name='On first chromosome of pair',
      marker_color='#1B9E77'
  ))
  fig.add_trace(go.Box(
      x=first["couple"],
      y=second[value],
      name='On second chromosome of pair',
      marker_color='#D95F02'
  ))
  fig.update_layout(
      xaxis_type="category",
      yaxis_title='{}'.format(value),
      yaxis=dict(range=range),
      xaxis=dict(tickmode='array',
                 ticktext=ticktext,
                 tickvals=first["couple"].unique()),
      # group together boxes of the different traces for each value of x
      boxmode='group',
      showlegend=showlegend,
      title=title,
      #   width=2000,
  )
  for couple in df["couple"].unique():
    if not statDf[statDf["couple"] == couple].empty:
      aggregated_pval = statDf[statDf["couple"] ==
                               couple].iloc[0]["pValue"]
      add_pvalue_annotation_box_plots(fig,
                                      cat_order,
                                      couple,
                                      [1.01, 1.05],
                                      pvalue_th=0.05,
                                      aggregated_pvalue=aggregated_pval)
  return fig


def add_pvalue_annotation_violin_plots(fig, cat_order, couple, y_range, symbol='*', pvalue_th=0.05, aggregated_pvalue=0.05):
  """This function add annotation for plotly violin plot

  Parameters
  ----------
  couple : str
      couple name
  y_range : list
      list of y_range
  symbol : str
      symbol to add
  pvalue_th : float
      pvalue threshold
  aggregated_pvalue : float
      aggregated pvalue threshold
  """
  if aggregated_pvalue < pvalue_th:
    # for bars, there is a direct mapping from the bar number to 0, 1, 2...
    bar_xcoord_map = {x: idx for idx, x in enumerate(cat_order)}
    x_coordinate = bar_xcoord_map[couple]
    x_start, x_end = x_coordinate - 0.2, x_coordinate + 0.2
    # add text at the correct x, y coordinates
    fig.add_annotation(dict(font=dict(color="black", size=14),
                            x=x_coordinate,
                            y=y_range[1] * 1.08,
                            showarrow=False,
                            text=symbol,
                            textangle=0,
                            xref="x",
                            yref="paper",
                            hovertext="p-value: {}".format(
        aggregated_pvalue),
    ))


def add_pvalue_annotation_box_plots(fig, cat_order, couple, y_range, symbol='*', pvalue_th=0.05, aggregated_pvalue=0.05):
  """This function add annotation for plotly boxplot

  Parameters
  ----------
  couple : str
      couple name
  y_range : list
      list of y_range
  symbol : str
      symbol to add
  pvalue_th : float
      pvalue threshold
  aggregated_pvalue : float
      aggregated pvalue threshold
  """
  if aggregated_pvalue < pvalue_th:
    # for bars, there is a direct mapping from the bar number to 0, 1, 2...
    bar_xcoord_map = {x: idx for idx, x in enumerate(cat_order)}
    x_coordinate = bar_xcoord_map[couple]
    x_start, x_end = x_coordinate - 0.2, x_coordinate + 0.2
    symbol = '*'
    fig.add_shape(type="line",
                  xref="x",
                  yref="paper",
                  x0=x_start,
                  y0=y_range[0],
                  x1=x_start,
                  y1=y_range[1],
                  line=dict(
                      color="black",
                      width=2,
                  )
                  )
    fig.add_shape(
        type="line",
        xref="x",
        yref="paper",
        x0=x_start,
        y0=y_range[1],
        x1=x_end,
        y1=y_range[1],
        line=dict(
            color="black",
            width=2,
        )
    )
    fig.add_shape(
        type="line",
        xref="x",
        yref="paper",
        x0=x_end,
        y0=y_range[1],
        x1=x_end,
        y1=y_range[0],
        line=dict(
            color="black",
            width=2,
        )
    )
    # add text at the correct x, y coordinates
    fig.add_annotation(dict(font=dict(color="black", size=14),
                            x=x_coordinate,
                            y=y_range[1] * 1.08,
                            showarrow=False,
                            text=symbol,
                            textangle=0,
                            xref="x",
                            yref="paper",
                            hovertext="p-value: {}".format(aggregated_pvalue),
                            ))
