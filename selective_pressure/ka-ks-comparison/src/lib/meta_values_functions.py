import pandas as pd
def extract_some_meta_values(metaValuesDict, orthogroupsDf, percentIdentityDataFrame, kaKsFinalDataframe):
  """[summary]

  Parameters
  ----------
  metaValuesDict : [type]
      [description]
  orthogroupsDf : [type]
      [description]
  percentIdentityDataFrame : [type]
      [description]
  kaKsFinalDataframe : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  metaValuesDict['medianPercentageOfIdentItyAlignement'] = percentIdentityDataFrame['percentIdentity'].median()
  allImplicatedGenes = pd.concat(
      [percentIdentityDataFrame['gene1'], percentIdentityDataFrame['gene2']]).unique()
  allImplicatedGenes = pd.Series(allImplicatedGenes)
  geneMalus = allImplicatedGenes.loc[allImplicatedGenes.str.startswith('MD')]
  genePrunus = allImplicatedGenes.loc[allImplicatedGenes.str.startswith(
      'Prupe')]
  metaValuesDict['numberGeneMalus'] = geneMalus.shape[0]
  metaValuesDict['numberGenePrunus'] = genePrunus.shape[0]
  orthogroupsGrouped = kaKsFinalDataframe.groupby('Orthogroup')
  numberOfOrthogroups = orthogroupsGrouped.ngroups
  sizeOfOrtogroups = orthogroupsGrouped.size()
  metaValuesDict['medianSizeOfOrthogroupsComputed'] = sizeOfOrtogroups.median()
  percentIdentityDataFrame.groupby('Orthogroup').ngroups
  orthogroupsDf['number_of_commas_MD'] = 1 + \
      orthogroupsDf['GDDH13_1-1_prot'].str.count(',').fillna(0)
  orthogroupsDf['number_of_commas_Pp'] = 1 + \
      orthogroupsDf['Prunus_persica_v2.0.a1.primaryTrs.pep'].str.count(',').fillna(
      0)
  orthogroupsDf['sizeOfOrthogroup'] = orthogroupsDf['number_of_commas_MD'] + \
      orthogroupsDf['number_of_commas_Pp']
  orthogroupsDf.drop(columns=['number_of_commas_MD',
                              'number_of_commas_Pp'],
                     inplace=True)
  metaValuesDict['medianSizeOfAllOrthogroups'] = orthogroupsDf['sizeOfOrthogroup'].median()
  return metaValuesDict
