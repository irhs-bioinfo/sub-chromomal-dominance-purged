import os
import yaml


def config_parser(configPath):
  """
  A function to open a configuration file and load it in a dictionnary

  Parameters
  ----------
  configPath : string
      Path of config file

  Returns
  -------
  dictionnary
      dictionnary storing configuration
  """
  with open(configPath) as configFile:
    configObject = yaml.safe_load(configFile)
  return configObject


def create_all_directories_from_config_file(nestedDict):
  """
  A recursive function aiming to recreate all needed directories needed by script

  Parameters
  ----------
  nestedDict : Dictionnary
      config dictionnaries storing path as values
  """
  # Iterate on a level of dictionnary
  for key, value in nestedDict.items():
    # If a child level exist, relaunch function
    if type(value) is dict:
        create_all_directories_from_config_file(value)
    # Else get value as a path and check if directory exist. If not construct it
    else:
        os.makedirs(os.path.dirname(value), exist_ok=True)
