import pandas as pd
import numpy as np
from Bio import SeqIO
from tqdm import tqdm


def find_multiplicon_of_couple(ohnologousGenes, gene1, gene2):
  """
  This function aims to get multiplicon number of a gene couple

  Parameters
  ----------
  ohnologousGenes : pandas.DataFrame
      A dataframe with three column: gene1, gene2 and associated multiplicon
  gene1 : string
      Gene name
  gene2 : string
      Gene name

  Returns
  -------
  integer
      Multiplicon ID
  """
  row = ohnologousGenes[(ohnologousGenes["gene1"] == gene1)
                        & (ohnologousGenes["gene2"] == gene2)]
  if row.empty:
    row = ohnologousGenes[(ohnologousGenes["gene1"] == gene2)
                          & (ohnologousGenes["gene2"] == gene1)]
  if not row.empty:
    return row["multiplicon"].values[0]


def loadSequencesFile(files):
  """
  This function aims to load a fasta file

  Parameters
  ----------
  files : array
      Array of path to fasta file

  Returns
  -------
  array
      Array of SeqRecord iterator
  """
  sequences = []
  # Iterate on path array
  for file in files:
    # Load fasta file and add SeqRecord iterator in result array
    sequences.append(SeqIO.parse(
        file,
        "fasta"))
  return sequences


def get_gene_ids_of_a_rbbh(tripletRow, recordDict):
  """
  This function aims to get SeqRecord from and orthogroup family ID

  Parameters
  ----------
  tripletRow : pandas.DataFrame
      A row of triplet Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  rbdbhDict = {}
  ids = []
  for column in ['query_x_malus', 'query_y', 'query_x_prunus']:
    ids.append(tripletRow[column])
  name = '_'.join([str(elem) for elem in ids])
  rbdbhDict[name]=find_sequence(
      ids, recordDict)
  return rbdbhDict



def get_gene_ids_of_a_family(orthogroupsRow, recordDict, column):
  """
  This function aims to get SeqRecord from and orthogroup family ID

  Parameters
  ----------
  orthogroupsRow : pandas.DataFrame
      A row of orthogroup Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  column : string
      Column Name storing gene ID

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  orthoFamily = {}
  if not pd.isnull(orthogroupsRow[column]):
    ids = orthogroupsRow[column].split(',')
    if ids != np.nan:
      orthoFamily[orthogroupsRow['Orthogroup']] = find_sequence(ids, recordDict)
  return orthoFamily


def get_gene_ids_of_a_ohnologous(ohnologousRow, recordDict, columns):
  """
  This function aims to get SeqRecord from an orthogroup family ID

  Parameters
  ----------
  ohnologousRow : pandas.DataFrame
      A row of orthogroup Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  columns : string
      Column Name storing gene ID

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  ohnoFamily = {}
  ids = []
  for column in columns:
    if not pd.isnull(ohnologousRow[column]):
      ids.append(ohnologousRow[column])
      if len(ids) == 2 or column == 'gene3':
        ohnoFamily[ohnologousRow['index']] = find_sequence(ids, recordDict)
  return ohnoFamily


def find_sequence(ids, records):
  """
  This function aims to get SeqRecord from sequence ID

  Parameters
  ----------
  ids : array
      Array of ID of sequence
  records : dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key

  Returns
  -------
  dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  rec = {}
  for ID in ids:
    ID = ID.replace(" ", "")
    if ID.startswith('Pr') and not ID.endswith('.1'):
      ID = ID + '.1'
    if records.get(ID).id not in rec:
      rec[records.get(ID).id] = records.get(ID)
  return rec


def construct_fasta_of_orthogroup(orthoGroups, conf, type):
  """
  This function aims to construct a fasta file with all sequence of an orthogroup

  Parameters
  ----------
  orthoGroups : dictionary
    Dictionnary of SeqRecord iterator as value and Orthogroup's ID as key
  conf : dictionnary
      A dictionnary storing configuration variable
  type : string
      Indicate type of computed sequence, it is used to access key in conf dictionnary
  """
  for specie in tqdm(orthoGroups, desc="Constructing Fasta Files"):
    for i in specie.keys():
      for orthoFamily in specie[i].keys():
        with open(conf['computePath'][type] + str(orthoFamily) + ".fasta", "a") as output_handle:
          for j in specie[i][orthoFamily].values():
              SeqIO.write(j, output_handle, "fasta")
