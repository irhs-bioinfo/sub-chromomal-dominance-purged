import matplotlib.pyplot as plt
from matplotlib.pyplot import legend
import pandas as pd
import seaborn as sns


def kn_ks_and_ratio_in_subplots(kaKsDf, keys, title=''):
  """
  This function aims to create concatenated graph representing, kn, ks and kn/ks.

  Parameters
  ----------
  kaKsDf : pandas.core.frame.DataFrame
      This dataframe store all syntenics informations aswell as log of mutations rates
  keys : array
      String name of column names used in plot
  title : string, optional
      A global title for plots

  Returns
  -------
  matplotlib Axes
      Returns the Axes object with the plot for further tweaking. This plot gather the subplots representing all mutations rates (kn/ks)
  """
  # Create a subplot of desired lenght
  fig, axes = plt.subplots(
      len(keys), 1, sharex=True, figsize=(
          len(keys) * 3, len(keys) * 2))
  # Flatten axes. flatten() is method of the numpy array. Instead of an
  # iterator, it returns a flattened version of the array:
  axes = axes.flatten()
  for i in range(len(keys)):
    # Construct a distribution plot
    # bins represent number of bar representing distribution
    # kde is the curve above it
    # label is used to build legend
    graph = sns.distplot(
        kaKsDf[keys[i]],
        bins=400,
        kde=False,
        ax=axes[i],
        label=keys[i])
    # graph.set_xscale("log")
    graph.set_xlim(-3,3)

  # Adjust space between subplots
  fig.subplots_adjust(hspace=0.4)
  # Sharex make tick labels disapearring. reconstruct them here
  for ax in axes:
    ax.tick_params(axis='x', labelbottom=True)
  plt.suptitle(title)
  return graph


def construct_distribution_plot(dfName, title, conf):
  """
  This function aims to construct histograms of dn, ds and omega, loading a csv file, and plotting it
  Parameters
  ----------
  dfName : string
      name of key in config file storing path to dataframe
  title : string
      Title for plot
  conf : dictionnary
      storing all configuration variables
  Returns
  -------
  array
      Return two plots (one with log value and another wihout)
  """
  kaKsWholeResults = pd.read_csv(
      conf['input']['kaKsDataFrames'][dfName],
      compression='infer',
      sep=',')
  plotkaKsWholeResults = kn_ks_and_ratio_in_subplots(
      kaKsWholeResults, [
          'dN', 'dS', 'omega'], title)
  plotkaKsWholeResultsLog = kn_ks_and_ratio_in_subplots(
      kaKsWholeResults, [
          'log_dN', 'log_dS', 'log_omega'], title)
  return [plotkaKsWholeResults, plotkaKsWholeResultsLog]


def construct_histogram_plot(dataDf, outputPath, title, variable):
  """
  Construct an histogramm plot of asked variable, output it in a pdf file
  Parameters
  ----------
  dataDf : pandas.DataFrame
    A pandas dataframe
  outputPath : string
    Path to output file
  title : string
    A title for plot
  variable : string
    Name of column to plot
  """
  fig, axes = plt.subplots(1,1)
  fig.tight_layout()
  # Construct histogram plot
  histPlot = sns.histplot(data=dataDf,
                          x=variable,
                          bins=100,
                          ax=axes).set_title(title)
  # Show it
  plt.show()
  # Save it
  for ext in ['.pdf', '.jpg']:
    fig.savefig(outputPath + ext)


def construct_binarized_difference(dataFrame,QtlDataframe, title, titleQtl, column, outputPath):
  """[summary]

  Parameters
  ----------
  dataFrame : [type]
      [description]
  title : [type]
      [description]
  column : [type]
      [description]
  outputPath : [type]
      [description]
  """

  fig, axes = plt.subplots(2,1)
  fig.tight_layout()
  dataFrame = dataFrame.sort_values(
      by='gene1', ascending=True)
  dataFrame["multiplicon_odd"] = dataFrame["multiplicon"]%2
  sns.barplot(data=dataFrame,
              x='gene1-start',
              y=column + "Bin",
              ax=axes[0],
              hue="multiplicon_odd").legend([], [], frameon=False)

  axes[0].set_xticks([])

  sns.despine(left=True, bottom=True, ax=axes[0])
  sns.stripplot(x="Start_trust_interval",
                hue="Marker_Type",
                palette="muted",
                data=QtlDataframe,
                dodge=True,
                ax=axes[1])

  axes[0].set_title(title)
  axes[1].set_title(titleQtl)
  
  # Show it
  plt.show()
  # Save it
  for ext in ['.pdf', '.jpg']:
    fig.savefig(outputPath + ext)

