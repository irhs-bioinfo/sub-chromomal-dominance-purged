#%%
import itertools
import os

import gffutils
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
from scipy.stats import binom_test, ttest_rel, wilcoxon
from tqdm import tqdm

from lib.config_parser import (config_parser,
                               create_all_directories_from_config_file)
from lib.dn_ds_visualisation import (construct_binarized_difference,
                                     construct_distribution_plot,
                                     construct_histogram_plot,
                                     kn_ks_and_ratio_in_subplots)
from lib.ka_ks_comparisons_lib import (add_results_in_dataframe,
                                       compute_all_stat_tests, find_triplet)
from lib.meta_values_functions import extract_some_meta_values
from lib.misc import get_list_of_file_with_extension, output_dataframe
from lib.sequences_parsing import find_multiplicon_of_couple
from lib.localize_genes_using_gff import localize_gene_on_genome
#%%
tqdm.pandas(desc="Pandas Apply")

# Load config
conf = config_parser('./conf/ka_ks_comparisons.yaml')
# Check if all directories exists
create_all_directories_from_config_file(conf)


metaValuesDict = {}

# Load files
ohnologousGenes = pd.read_csv(conf['input']['ohnologousGenes'], sep=',')

percentIdentityDataFrame = pd.read_csv(
    conf['input']['percentIdentityDataFrames'],
    compression='infer',
    sep=',')
kaKsFinalDataframe = pd.read_csv(
    conf['input']['kaKsDataFrames']['kaKsFinalDataframe'] + '.YN00',
    compression='infer',
    sep=',')

alignementSizeDf = pd.read_csv(
    conf['input']['alignementSize'],
    compression='infer',
    sep=',')

qtlDf = pd.read_csv(
    conf['input']['qtlDf'],
    compression='infer',
    sep=',')

#%%
db = gffutils.create_db(conf['input']['annotations']['malusGff'], dbfn=conf['output']['gffDb'], force=True, keep_order=True,
     merge_strategy='merge', sort_attribute_values=True)
db = gffutils.FeatureDB(conf['output']['gffDb'], keep_order=True)

kaKsFinalDataframe = kaKsFinalDataframe.progress_apply(
    localize_gene_on_genome, args=(db,), axis=1)

#%% [markdown]
## Some Meta values

#%%

plot = construct_histogram_plot(percentIdentityDataFrame,
                         conf['output']['plots']['distributions']['percentIdentity'],
                         "Distribution of percentage of identity of aligned proteins of orthogroups",
                         "percentIdentity")

plot = construct_histogram_plot(alignementSizeDf,
                         conf['output']['plots']['distributions']['alignementSize'],
                         "Distribution of size of multiple alignements",
                         "size")


#%%

for specie in ['MD', 'interSpecific', 'Pp']:
  if specie == 'MD':
    baseName = os.path.splitext(
        conf['output']['plots']['distributions']['malus'])[0]
    extension = os.path.splitext(
        conf['output']['plots']['distributions']['malus'])[1]
    titleOfPlot = 'Distribution of Dn, Ds and Dn/Ds of malus genome'
    arrayOfPlots = construct_distribution_plot('kaKsMalusDataframe',
                                               titleOfPlot,
                                               conf)
    plotkaKsWholeResults = arrayOfPlots[0]
    plotkaKsWholeResultsLog = arrayOfPlots[1]
    plt.show()
    plotkaKsWholeResults.figure.savefig(
        baseName + extension + ".jpg",
        bbox_inches='tight')
    
    plotkaKsWholeResultsLog.figure.savefig(
        baseName + '.log' + extension + ".jpg",
        bbox_inches='tight')

  else:
    baseName = os.path.splitext(
        conf['output']['plots']['distributions']['malusVsPrunus'])[0]
    extension = os.path.splitext(
        conf['output']['plots']['distributions']['malusVsPrunus'])[1]
    titleOfPlot = 'Distribution of Dn, Ds and Dn/Ds between malus and prunus genome'
    arrayOfPlots = construct_distribution_plot('kaKsInterSpecificDataframe',
                                               titleOfPlot,
                                               conf)
    plotkaKsWholeResults = arrayOfPlots[0]
    plotkaKsWholeResultsLog = arrayOfPlots[1]
    plt.show()
    plotkaKsWholeResults.figure.savefig(
        baseName + extension + '.jpg',
        bbox_inches='tight')
    plotkaKsWholeResultsLog.figure.savefig(
        baseName + '.log' + extension + '.jpg',
        bbox_inches='tight')


#%%
tripletDf = pd.DataFrame(
    columns=[
        'S',
        'N',
        't',
        'kappa',
        'omega',
        'dN',
        'dN SE',
        'dS',
        'dS SE',
        'Sequence1',
        'Sequence2',
        'Orthogroup'])

for name, group in kaKsFinalDataframe.groupby('Orthogroup'):
  # If it is a triplet
  if group['Sequence2'].str.contains('MD').any(
  ) and group.shape[0] == 3:
    if group['Sequence2'].iloc[1] == group['Sequence2'].iloc[2]:
      if group['Sequence1'].iloc[1][2:4] != group['Sequence1'].iloc[2][2:4]:
        tripletDf = pd.concat([tripletDf, group], axis=0)
  # If it is not a triplet
  if group['Sequence2'].str.contains('MD').any(
  ) and group.shape[0] > 3:
    orthologueMalusDf = group.loc[group['Sequence2'].str.contains('MD')]
    orthologueMalus = pd.merge(orthologueMalusDf[[
                      'Sequence1', 'Orthogroup']], orthologueMalusDf[['Sequence2', 'Orthogroup']])
    orthologueMalus.drop_duplicates(inplace=True)
    orthologueMalus.drop(['Orthogroup'], axis=1, inplace=True)
    rbbhDf = find_triplet(orthologueMalus, group)
    tripletDf = pd.concat([tripletDf, rbbhDf], axis=0)

tripletDf.reset_index(inplace=True)
tripletDf['malus_chr'] = 'Chr' + tripletDf['Sequence1'].str[2:4]
tripletDf['prunus_chr'] = 'Pp' + tripletDf['Sequence2'].str[6:7].str.zfill(2)

plt.subplots(figsize=(30, 25))

resultDf = pd.DataFrame(
    columns=[
        'tested_group1',
        'tested_group2',
        'stat',
        'pvalue',
        'size1',
        'size2',
        'median1',
        'median2'])

# Compute Mann whitney tests for each couples
resultDfManWhitney, resultDfManWhitneypivoted = compute_all_stat_tests(
    tripletDf, resultDf, 'mannwhitneyu')
resultDfManWhitney = resultDfManWhitney.loc[(resultDfManWhitney["size1"]>200) & (resultDfManWhitney["size2"]>200)]
print(resultDfManWhitney.loc[(resultDfManWhitney["pvalue"] <= 0.05)])
heatmapStore = sns.heatmap(resultDfManWhitneypivoted, vmin=0, vmax=0.05)
heatmapStore.figure.savefig(conf["output"]["plots"]["heatmap"]["manwhitney"]+".jpg")
output_dataframe(path=conf['output']['statistics']['dataframes']
                 ['manwhitney'],
                 dataframe=resultDfManWhitney,
                 separator=',',
                 verbose=True)

# Compute Kolmogorov Smirnov tests for each couples
resultDf, resultDfKSpivoted = compute_all_stat_tests(
    tripletDf, resultDf, 'ks_2samp')
resultDf = resultDf.loc[(
    resultDf["size1"] > 200) & (resultDf["size2"] > 200)]
print(resultDf.loc[(resultDf["pvalue"] <= 0.05)])

heatmapStore = sns.heatmap(resultDfKSpivoted, vmin=0, vmax=0.05)
heatmapStore.figure.savefig(conf["output"]["plots"]["heatmap"]["ks2samp"]+".jpg")
output_dataframe(path=conf['output']['statistics']['dataframes']
                 ['ks2samp'],
                 dataframe=resultDfKSpivoted,
                 separator=',',
                 verbose=True)
#%%
# init a result dataframe
kaksResults = pd.DataFrame(
    columns=["gene1",
             "gene2",
             "Orthogroup",
             "multiplicon",
             "Omega1",
             "Omega2",
             "dN1",
             "dN2",
             "dS1",
             "dS2",
             "diffdN",
             "diffdS",
             "diffOmega",
             "diffdNBin",
             "diffdSBin",
             "diffOmegaBin",
             "malusOrthologues",
             "gene1-start",
             "gene2-start",
             "gene1-stop",
             "gene2-stop"])

# Group by triplet group names
kaKsFinalDataframeGrouped = kaKsFinalDataframe.groupby("Orthogroup")
# Compute Ka ks final dataframe to get diff dataframe
for name, group in tqdm(kaKsFinalDataframeGrouped):
  if group.shape[0] == 3:
    dictTemp = {}
    dictTemp["malusOrthologues"] = [group.iloc[0]["Sequence1"],  # Add first gene Malus
                                    # Add second gene Malus
                                    group.iloc[0]["Sequence2"],
                                    group.iloc[1]["Sequence2"]]  # Add Prunus gene
    dictTemp["Orthogroup"] = group.iloc[0]["Orthogroup"]
    # Add first gene Malus
    dictTemp["gene1"] = group.iloc[1]["Sequence1"]
    # Add second gene Malus
    dictTemp["gene2"] = group.iloc[2]["Sequence1"]
    dictTemp["gene1-start"] = group.iloc[1]["Sequence1-start"]
    dictTemp["gene2-start"] = group.iloc[2]["Sequence1-start"]
    dictTemp["gene1-stop"] = group.iloc[1]["Sequence1-stop"]
    dictTemp["gene2-stop"] = group.iloc[2]["Sequence1-stop"]
    dictTemp["multiplicon"] = find_multiplicon_of_couple(
        ohnologousGenes, dictTemp["gene1"], dictTemp["gene2"])
    # Compute Diff
    dictTemp["Omega1"] = group.iloc[1]["omega"]
    dictTemp["Omega2"] = group.iloc[2]["omega"]
    dictTemp["dN1"] = group.iloc[1]["dN"]
    dictTemp["dN2"] = group.iloc[2]["dN"]
    dictTemp["dS1"] = group.iloc[1]["dS"]
    dictTemp["dS2"] = group.iloc[2]["dS"]
    # Compute Diff
    dictTemp["diffOmega"] = group.iloc[1]["omega"] - \
        group.iloc[2]["omega"]
    dictTemp["diffdN"] = group.iloc[1]["dN"] - group.iloc[2]["dN"]
    dictTemp["diffdS"] = group.iloc[1]["dS"] - group.iloc[2]["dS"]
    # Append result in a dataframe
    kaksResults = kaksResults.append(
        dictTemp, ignore_index=True)


# Binarize difference for each diff column
for column in ["diffOmega", "diffdN", "diffdS"]:
  kaksResults[column + "Bin"].loc[kaksResults[column] > 0] = 1
  kaksResults[column + "Bin"].loc[kaksResults[column] < 0] = -1
#%%
# Construct distribution of diff and binarized diff for some choosed couples
for couple in [("17", "09"), ("16", "13"), ("11", "03"), ("10", "05"), ("07", "01"), ("14", "06"), ("15", "08")]:
  for studiedVar in ["diffOmega", "diffdN", "diffdS"]:
    # Construct variable access name
    nameOfVar = studiedVar.replace("diff", "")
    # Subset to get only dataframe of couple
    kaKsFinalDataframeDiffSubset = kaksResults.loc[kaksResults["gene1"].str.startswith(
        "MD"+ couple[0]) & kaksResults["gene2"].str.startswith("MD"+couple[1])]
    # qtlDfSubset = qtlDf.loc[(qtlDf["Chromosome_Number"] == int(couple[0]))
    #                         | (qtlDf["Chromosome_Number"] == int(couple[1]))]
    qtlDfSubset = qtlDf.loc[(qtlDf["Chromosome_Number"] == int(couple[0]))]
    # Construct a title
    title = "Distribution of difference of {} \n between {} and {} along chromosome {}".format(
        nameOfVar, couple[0], couple[1], couple[0])
    # Construct an output path
    outputPath = conf['output']['plots']['distributions']["templateDistributionDiff"] \
        + "MD" + couple[0] + "MD" + couple[1] + studiedVar
    # Construct distribution of diff size
    construct_histogram_plot(dataDf=kaKsFinalDataframeDiffSubset,
                            outputPath=outputPath,
                            title=title,
                            variable=studiedVar)
    # Construct an output path
    outputPath = conf['output']['plots']['distributions']["templateDistributionDiffBin"] \
        + "MD" + couple[0] + "MD" + couple[1] + studiedVar + "Bin"
    # Construct a title
    title = "Distribution of binarized difference of {} \n between {} and {} along chromosome {}".format(
        nameOfVar, couple[0], couple[1], couple[0])
    titleQtl = "Localisation of QTLs along chromosome {}".format(couple[0])
    # Construct an histogramm plot for repartition of binarized differenceb between a pair of ohnologous chromome along one chromosome
    construct_binarized_difference(dataFrame=kaKsFinalDataframeDiffSubset,
                                   QtlDataframe=qtlDfSubset,
                                   title=title,
                                   titleQtl=titleQtl,
                                   column=studiedVar,
                                   outputPath=outputPath)

os.makedirs(conf['output']['plots']['distributions']["templateDistributionDiffBin"] + "/jpg/", exist_ok=True)
returnCode = os.system("mv " + conf['output']['plots']['distributions']["templateDistributionDiffBin"]
     + "*.jpg" + " " + conf['output']['plots']['distributions']["templateDistributionDiffBin"] + "jpg/")
os.makedirs(conf['output']['plots']['distributions']
            ["templateDistributionDiff"] + "/jpg/", exist_ok=True)
returnCode = os.system("mv " + conf['output']['plots']['distributions']["templateDistributionDiff"] + \
     "*.jpg" + " " + conf['output']['plots']['distributions']["templateDistributionDiff"] + "jpg/")

# %% [markdown]
"""
### Choice of statistical test:
In this analysis, both Wilcoxon signed rank test and paired Student’s t-test led to the rejection of the null hypothesis. In general, however, which test is more appropriate? The answer is, it depends on several criteria:

  -*Hypothesis*: Student’s t-test is a test comparing means, while Wilcoxon’s tests the ordering of the data. For example, if you are analyzing data with many outliers such as individual wealth (where few billionaires can greatly influence the result), Wilcoxon’s test may be more appropriate.
  -*Interpretation*: Although confidence intervals can also be computed for Wilcoxon’s test, it may seem more natural to argue about the confidence interval of the mean in the t-test than the pseudomedian for Wilcoxon’s test.
  -*Fulfillment of assumptions*: The assumptions of Student’s t-test may not be met for small sample sizes. In this case, it is often safer to select a non-parametric test. However, if the assumptions of the t-test are met, it has greater statistical power than Wilcoxon’s test.
"""
#%%
# Store all combinations to generate all possible couples
couplesCombinations = itertools.permutations(range(18), 2)
# Init a result dataframe
results = pd.DataFrame(columns=["test", "couple", "studiedVar", "stat", "pvalue"])

# Iterate on all possible couples
for couple in couplesCombinations:
  # Iterate on variables
  for studiedVar in ["Omega", "dN", "dS"]:
    # Subset to get only dataframe of couple
    kaksDataFrameSubset = kaksResults.loc[kaksResults["gene1"].str.startswith(
        "MD" + str(couple[0]).zfill(2)) & kaksResults["gene2"].str.startswith("MD" + str(couple[1]).zfill(2))]
    kaksDataFrameSubset = kaksDataFrameSubset.loc[~kaksDataFrameSubset["diffOmega"].between(-0.02, 0.02)]
    # Pass loop iteration if dataframe is empty or too small
    if kaksDataFrameSubset.empty or kaksDataFrameSubset.shape[0]<20:
      continue
    numberOfSuccess = kaksDataFrameSubset.loc[kaksDataFrameSubset['diff'
                                                                  + studiedVar + 'Bin'] == 1].shape[0]
    pvalue = binom_test(numberOfSuccess, kaksDataFrameSubset.shape[0], p=0.5, alternative='two-sided'
        )
    results = add_results_in_dataframe(
        results, "binom_test", couple, studiedVar, int(kaksDataFrameSubset.shape[0]), numberOfSuccess, pvalue, kaksDataFrameSubset['diffOmega'].mean())
    # Compute paired t-test on both variable columns
    stat, pvalue = ttest_rel(
        kaksDataFrameSubset[studiedVar + "1"], kaksDataFrameSubset[studiedVar + "2"], axis=0)
    # Store stat results in final dataframes
    results = add_results_in_dataframe(
        results, "paired T-test", couple, studiedVar, int(kaksDataFrameSubset.shape[0]), stat, pvalue, kaksDataFrameSubset['diffOmega'].mean())
    # Compte wilcoxon test, non parametric version paired t Test 
    stat, pvalue= wilcoxon(
        kaksDataFrameSubset[studiedVar + "1"],
        kaksDataFrameSubset[studiedVar + "2"],
        zero_method='wilcox',
        correction=False)
    # Store stat results in final dataframes
    results = add_results_in_dataframe(
        results, "wilcoxon", couple, studiedVar, int(kaksDataFrameSubset.shape[0]), stat, pvalue, kaksDataFrameSubset['diffOmega'].mean())
    stat, pvalue= stats.ranksums(
        kaksDataFrameSubset[studiedVar + "1"],
        kaksDataFrameSubset[studiedVar + "2"])
    # Store stat results in final dataframes
    results = add_results_in_dataframe(
        results, "ranksums", couple, studiedVar, int(kaksDataFrameSubset.shape[0]), stat, pvalue, kaksDataFrameSubset['diffOmega'].mean())
    

# Output it in CSV file
output_dataframe(path=conf['output']['statistics']['dataframes']
                 ['appariedTest'],
                 dataframe=results,
                 separator=',',
                 verbose=True)
# print results
display(results.loc[(results["test"] == "wilcoxon")
                    & (results["studiedVar"] == "Omega")].sort_values(by=['pvalue']))
display(results.loc[(results["test"] == "binom_test")
                    & (results["studiedVar"] == "Omega")].sort_values(by=['pvalue']))
display(results.loc[(results["test"] == "paired T-test")
                    & (results["studiedVar"] == "Omega")].sort_values(by=['pvalue']))
display(results.loc[(results["test"] == "ranksums")
                    & (results["studiedVar"] == "Omega")].sort_values(by=['pvalue']))


# %%
