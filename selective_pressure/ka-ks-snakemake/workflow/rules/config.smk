from snakemake.utils import validate
import pandas as pd

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

triplets = pd.read_csv(config["familiesDataset"],
                       sep=',',
                       header=0,
                       index_col=0,
                       comment='#')
indexTriplet = list(set(triplets.index))
