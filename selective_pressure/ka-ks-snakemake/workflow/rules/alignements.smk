if config["aligner"] == "muscle":


  rule align_proteins:
    input:
      "results/temp/protein_fasta_files/{proteicFastaFile}.fasta"
    output:
      alignementFile = temp("results/temp/alignements/{proteicFastaFile}.aln")
    message:
      "Align proteic sequences of triplet {wildcards.proteicFastaFile}"
    conda:
      "../envs/pipeline_env.yaml"
    log:
      "results/logs/muscle/{proteicFastaFile}.log"
    shell:
      """
      muscle \
      -in {input} \
      -out {output.alignementFile} \
      # –seqtype auto \
      –clw \
      -quiet \
      # –verbose \
      ­–log {log}
      """


  rule matrix_alignement:
    input:
        "results/temp/alignements/{proteicFastaFile}.aln"
    output:
      matrixFile = temp("results/temp/matrix/{proteicFastaFile}.mat")
    params:
      outfmt = 'fasta',
      extra = '-v'
    message:
      "Reconstruct alignement matrix distances for {wildcards.proteicFastaFile}"
    conda:
      "../envs/pipeline_env.yaml"
    log:
      "results/logs/mat_from_muscle/{proteicFastaFile}.log"
    shell:
      """
      clustalo -i {input} \
      --percent-id \
      --distmat-out={output.matrixFile} \
      --full \
      --force >{log} 2>&1
        """


elif config["aligner"] == "clustalo":


  rule align_proteins:
    input:
      "results/temp/protein_fasta_files/{proteicFastaFile}.fasta"
    output:
      alignementFile = temp("results/temp/alignements/{proteicFastaFile}.aln"),
      matrixFile = temp("results/temp/matrix/{proteicFastaFile}.mat")
    params:
      outfmt = 'fasta',
      extra = '-v'
    message:
      "Align proteic sequences of triplet {wildcards.proteicFastaFile}"
    conda:
      "../envs/pipeline_env.yaml"
    log:
      "results/logs/clustal_omega/{proteicFastaFile}.log"
    shell:
      """
      clustalo \
      -i {input} \
      -o {output.alignementFile} \
      {params.extra} \
      --auto \
      --outfmt {params.outfmt} \
      --force \
      --full \
      --distmat-out {output.matrixFile} \
      --percent-id \
      --threads {threads} >{log} 2>&1
      """


rule construct_nucleic_alignement:
  input:
    alignementFile = "results/temp/alignements/{alignementFile}.aln",
    nucleicFastaFile = "results/temp/nucleic_fasta_files/{alignementFile}.fasta"
  output:
    temp("results/temp/pal2nal/{alignementFile}.pal2nal")
  message:
    "Construct a nucleic alignement from multiple alignement with proteic sequences using pal2nal on: {wildcards.alignementFile}"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    """
    pal2nal.pl \
    {input.alignementFile} \
    {input.nucleicFastaFile} \
    -nogap \
    -output paml > {output}
    """

