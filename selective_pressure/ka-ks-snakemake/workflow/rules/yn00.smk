rule run_yn00:
  input:
    "results/temp/pal2nal/{phylipFile}.pal2nal"
  output:
    yn00File = temp("results/temp/yn00/{phylipFile}.yn00"),
    
  params:
    extra = "--verbose",
    workingDir = temp(directory("results/temp/workingDir/{phylipFile}/"))
  message:
    "Run YN00 from PAML package to compute pairwise Ka/Ks between all sequences of {wildcards.phylipFile} in pairwise fashion "
  conda:
    "../envs/pipeline_env.yaml"
  log:
    "results/logs/yn00/{phylipFile}.log"
  shell:
    """
    python workflow/scripts/compute_yn00.py \
    --alignementFile {input} \
    --output {output.yn00File} \
    --workingDir {params.workingDir} \
    {params.extra} >{log} 2>&1
    """


rule parse_ka_ks:
  input:
    yn00 = expand("results/temp/yn00/{yn00File}.yn00", yn00File=indexTriplet)
  output:
    LPB93 = "results/tables/kaKsFinalDataframe.csv.LPB93",
    LWL85 = "results/tables/kaKsFinalDataframe.csv.LWL85",
    LWL85m = "results/tables/kaKsFinalDataframe.csv.LWL85m",
    NG86 = "results/tables/kaKsFinalDataframe.csv.NG86",
    YN00 = "results/tables/kaKsFinalDataframe.csv.YN00"
  params:
    yn00Directory = lambda w, input: os.path.dirname(input.yn00[0]) + "/",
    outputDirectory = "results/tables/kaKsFinalDataframe.csv"
  message:
    "Parse YN00 results"
  conda:
    "../envs/kaks_parsing_env.yaml"
  log:
    "results/logs/parse_ka_ks.log"
  shell:
    """
    python workflow/scripts/parse_ka_ks.py \
    --yn00Directory {params.yn00Directory} \
    --output {params.outputDirectory} >{log} 2>&1
    """


rule parse_alignements:
  input:
    pal2nal = expand("results/temp/pal2nal/{pal2nal}.pal2nal", pal2nal=indexTriplet)
  output:
    "results/tables/alignementSize.csv"
  params:
    pal2nalDirectory = lambda w, input: os.path.dirname(input.pal2nal[0]) +"/",
  message:
    "Parse alignements results"
  conda:
    "../envs/kaks_parsing_env.yaml"
  log:
    "results/logs/pal2nal.log"
  shell:
    """
    python workflow/scripts/parse_alignements.py \
    --pal2nalDirectory {params.pal2nalDirectory} \
    --output {output} >{log} 2>&1
    """


rule parse_percent_identity:
  input:
    mat = expand("results/temp/matrix/{matFile}.mat", matFile=indexTriplet)
  output:
    "results/tables/percentIdentityAlignements.csv"
  params:
    matrixDirectory = lambda w, input: os.path.dirname(input.mat[0]) +"/"
  message:
    "Parse percent identity results"
  conda:
    "../envs/kaks_parsing_env.yaml"
  log:
    "results/logs/parse_percent_identity.log"
  shell:
    """
    python workflow/scripts/parse_percent_identity.py \
    --matrixDirectory {params.matrixDirectory} \
    --output {output} >{log} 2>&1
    """
