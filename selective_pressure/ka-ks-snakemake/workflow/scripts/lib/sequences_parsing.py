import re
from typing import Dict, List, Optional, Set, Tuple

import numpy as np
import pandas as pd
from Bio import SeqIO


def get_gene_ids_of_a_family(orthogroupsRow: pd.DataFrame, recordDict, columns: str):
  """
  This function aims to get SeqRecord from and orthogroup family ID

  Parameters
  ----------
  orthogroupsRow : pandas.DataFrame
      A row of orthogroup Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  column : string
      Column Name storing gene ID

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  orthoFamily = {}
  for column in columns:
    if not pd.isnull(orthogroupsRow[column]):
      ids = orthogroupsRow[column].split(',')
      if ids != np.nan:
        if orthogroupsRow['Orthogroup'] not in orthoFamily:
          orthoFamily[orthogroupsRow['Orthogroup']
                      ] = find_sequence(ids, recordDict)
        else:
          orthoFamily[orthogroupsRow['Orthogroup']
                      ].update(find_sequence(ids, recordDict))
  return orthoFamily


def construct_fasta_of_orthogroup(sequencesList, outputPath: Dict, typeList: List[str]):
  """
  This function aims to construct a fasta file with all sequence of an orthogroup

  Parameters
  ----------
  orthoGroups : dictionary
    Dictionnary of SeqRecord iterator as value and Orthogroup's ID as key
  outputPath : dictionnary
      A dictionnary storing configuration variable
  type : string
      Indicate type of computed sequence, it is used to access key in conf dictionnary
  """

  for sequenceType, type in zip(sequencesList, typeList):
    for seq in sequenceType:
      for i in seq.keys():
        for orthoFamily in seq[i].keys():
          with open(outputPath[type] + str(orthoFamily) + ".fasta", "a") as output_handle:
            for j in seq[i][orthoFamily].values():
              SeqIO.write(j, output_handle, "fasta")


def find_multiplicon_of_couple(ohnologousGenes: pd.DataFrame, gene1: str, gene2: str) -> pd.DataFrame:
  """
  This function aims to get multiplicon number of a gene couple

  Parameters
  ----------
  ohnologousGenes : pandas.DataFrame
      A dataframe with three column: gene1, gene2 and associated multiplicon
  gene1 : string
      Gene name
  gene2 : string
      Gene name

  Returns
  -------
  integer
      Multiplicon ID
  """
  row = ohnologousGenes[(ohnologousGenes["gene1"] == gene1)
                        & (ohnologousGenes["gene2"] == gene2)]
  if row.empty:
    row = ohnologousGenes[(ohnologousGenes["gene1"] == gene2)
                          & (ohnologousGenes["gene2"] == gene1)]

  return row["multiplicon"].values[0]


def loadSequencesFile(files: List[str]):
  """
  This function aims to load a fasta file

  Parameters
  ----------
  files : array
      Array of path to fasta file

  Returns
  -------
  array
      Array of SeqRecord iterator
  """
  sequences = []
  # Iterate on path array
  for file in files:
    # Load fasta file and add SeqRecord iterator in result array
    sequences.append(SeqIO.parse(
        file,
        "fasta"))
  return sequences


def get_gene_ids_of_a_rbbh(tripletRow: pd.DataFrame, recordDict: Dict) -> Dict:
  """
  This function aims to get SeqRecord from and orthogroup family ID

  Parameters
  ----------
  tripletRow : pandas.DataFrame
      A row of triplet Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  rbdbhDict = {}
  ids = []
  for column in ['query_x_malus', 'query_y', 'query_x_prunus']:
    ids.append(tripletRow[column])
  name = '_'.join([str(elem) for elem in ids])
  rbdbhDict[name] = find_sequence(
      ids, recordDict)
  return rbdbhDict


def get_gene_ids_of_a_ohnologous(ohnologousRow: pd.DataFrame, recordDict: Dict, columns: str) -> Dict:
  """
  This function aims to get SeqRecord from an orthogroup family ID

  Parameters
  ----------
  ohnologousRow : pandas.DataFrame
      A row of orthogroup Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  columns : string
      Column Name storing gene ID

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  ohnoFamily = {}
  ids = []
  for column in columns:
    if not pd.isnull(ohnologousRow[column]):
      ids.append(ohnologousRow[column])
      if len(ids) == 2 or column == 'gene3':
        ohnoFamily[ohnologousRow['index']] = find_sequence(ids, recordDict)
  return ohnoFamily


def find_sequence(ids: List[str], records: Dict) -> Dict:
  """
  This function aims to get SeqRecord from sequence ID

  Parameters
  ----------
  ids : array
      Array of ID of sequence
  records : dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence ID as key

  Returns
  -------
  dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  rec = {}
  for ID in ids:
    # Remove whitespace in sequence ID if exist
    ID = "".join(ID.split())
    if records.get(ID).id not in rec:
      rec[records.get(ID).id] = records.get(ID)
  return rec
