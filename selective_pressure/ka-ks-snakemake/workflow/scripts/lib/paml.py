import re

import numpy as np
import pandas as pd
from Bio.Phylo.PAML import yn00


def get_all_methods(file):
  """
  [summary]

  [extended_summary]

  Parameters
  ----------
  file : [type]
      [description]
  pathToDir : string


  Returns
  -------
  [type]
      [description]
  """
  uniqueMethods = []
  result = yn00.read(file)
  for sequence1 in result.keys():
    for sequence2 in result[sequence1].keys():
      for methodName in result[sequence1][sequence2].keys():
        if methodName not in uniqueMethods:
          uniqueMethods.append(methodName)
  return uniqueMethods


def compute_log_of_column(df, columnNames):
  """[summary]

  Parameters
  ----------
  df : [type]
      [description]
  columnNames : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """

  for columnName in columnNames:
    if df.columns.isin([columnName]).any():
      if columnName == 'omega':
        df['log_omega'] = -np.log10(df[columnName].astype(float)).replace(
            [np.inf, -np.inf], np.nan)
      else:
        df['log_' + columnName] = np.log10(df[columnName].astype(float)).replace(
          [np.inf, -np.inf], np.nan)
  return df


# Not used since correction on build_dictionnary_of_yn00_results
# but kept in case
def drop_duplicated_rows_by_check_string(df, columns):
  """[summary]

  Parameters
  ----------
  df : [type]
      [description]
  columns : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  df['check_string'] = df.apply(
      lambda row: ''.join(sorted([row[columns[0]], row[columns[1]]])), axis=1)
  # Drop duplciated check string
  df.drop_duplicates(
      'check_string', inplace=True)
  df.drop('check_string', 1, inplace=True)
  return df


def build_dictionnary_of_yn00_results(yn00File, storeDf, uniqueMethods, result):
  """[summary]

  Parameters
  ----------
  yn00File : [type]
      [description]
  storeDf : [type]
      [description]
  uniqueMethods : array
  result : [type]
      [description]
  """
  # Get orthogroup ID, or a unique ID
  splittedYn00File = yn00File.split('.')
  if len(splittedYn00File) > 2:
    groupID = yn00File.replace('.yn00', '').replace('.fasta', '')
  else:
    groupID = yn00File.split('.')[0]
  # Init a dictionnary to store current orthogroup
  storeDf[groupID] = {}
  pairs = [tuple(sorted([seq1, seq2]))
           for seq1 in result.keys()
           for seq2 in result[seq1].keys()]
  pairs = list(set(pairs))
  # Iterate on each method
  for method in uniqueMethods:
    # Transform complex structure into a dict that can be trabsformed into pandas Data frame
    currentmethodInDict = [dict(result[pair[0]][pair[1]][method],
                                **{'Sequence1': pair[0], 'Sequence2': pair[1]})
                           for pair in pairs]
    # Store it in a pandas data frame
    aDF = pd.DataFrame(currentmethodInDict)

    # Save orthgroup Name
    aDF['Orthogroup'] = re.search(r"\/(\w+)$", groupID).group(1)
    # Save it in dictionnary
    storeDf[groupID][method] = aDF
