"""
  A module aiming to store functions allowing to manage various operations, in particular on the file system, the recording in the form of files
"""

import glob
import os
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd


def create_dir(directories: List, verbose: bool = False):
  """
  Create directories from a list of names passed as an argument

  Parameters
  ----------
  directories : array
      List of directories names
  verbose : int, optional
      Ask function to be verbose, by default False
  """
  for directory in directories:
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise
    if verbose:
      print('Directory ' + directory + 'created')


def get_list_of_file_with_extension(directory: str, extension: str) -> List:
  """
  Builds a list of the names of the files in a given directory that have the requested extension.

  Parameters
  ----------
  directory : string
      Directory to check
  extension : string
      Extension searched

  Returns
  -------
  array
      List of filename in asked directory with given extension
  """

  return glob.glob(directory + extension)


def output_dataframe(path: str = './',
                     dataframe: pd.DataFrame = pd.DataFrame(),
                     separator: str = ',',
                     verbose: bool = False) -> None:
  """
  Save a data frame as a CSV file

  Parameters
  ----------
  path : string
      Path to save csv file
  qtlDataset : pandas.core.frame.DataFrame
      Panda Data Frame to save as a file
  separator : string
      Char used as separator
  verbose: bool, optional
    If true, print verbose for user, by default 0
  """
  os.makedirs(os.path.dirname(path), exist_ok=True)
  if 'index' in dataframe.columns:
    # Dropping useless column
    dataframe = dataframe.drop('index', 1)
  # Saving data frame as a CSV
  dataframe.to_csv(path,
                   sep=separator,
                   index=False,
                   encoding='utf-8',
                   compression='infer')
  if verbose:
    print('Saving data frame as CSV file stored in: ' + path)
