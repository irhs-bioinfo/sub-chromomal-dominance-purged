import argparse
import re
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd
from lib.misc import get_list_of_file_with_extension, output_dataframe
from tqdm import tqdm


def get_arguments():
  """This function aims to define argument used for this script

  Returns
  -------
  ArgumentParser object
      This object store all possible arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-p",
      "--pal2nalDirectory",
      help="Path to alignement files",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to output file",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def construct_alignement_length(alignementLengthDf: pd.DataFrame, alignementFile: str) -> pd.DataFrame:
  """[summary]

  Parameters
  ----------
  alignementLengthDf : [type]
      [description]
  alignementFile : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  # Open file
  with open(alignementFile, 'r') as pal2nalFile:
    # Init some storing variable
    tempDict = {}
    arrayOfGenes = []
    numberOfAlignedAA = 0

    # Read lines of file
    lines = pal2nalFile.readlines()
    # Iterate on lines
    for line in lines:
      # If line begin one or more space and one digit, it is header of file
      # (like    3     2011)
      if re.match(r"^\s+\d", line):
        # Get in array all numbers
        arrayOfNumber = get_numbers(line)
        # Get size of alignement
        numberOfAlignedAA = arrayOfNumber[1]
      # Get line that begin with 3 charcaters different (ATCG) which is a gene name
      elif re.search(r"[^ATCG]{3}", line):
        # delete \n
        line = line.replace('\n', '')
        # Save gene name in an array
        arrayOfGenes.append(line)
    # Get orthogroup name using filename withoput extension
    tempDict['Orthogroup'] = alignementFile.replace('fasta.pal2nal', '')
    # Save array of gene names
    tempDict['genesList'] = arrayOfGenes
    # Save size of alignement
    tempDict['size'] = numberOfAlignedAA
    # Append dictionnary in a new row of pandas result dataframe
    alignementLengthDf = alignementLengthDf.append(tempDict, ignore_index=True)
  return alignementLengthDf


def get_numbers(str: str) -> List:
  """[summary]

  Parameters
  ----------
  str : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  array = re.findall(r'[0-9]+', str)
  return array


def main() -> bool:
  # tqdm initialisation for pandas apply
  tqdm.pandas(desc="Pandas Apply")
  # Get argument parser
  args = get_arguments()

  # Get all filenames of pal2nal files
  listOfPhylipFiles = get_list_of_file_with_extension(
      args.pal2nalDirectory, "*.pal2nal")
  # Init a panda dataframe to store results
  alignementLengthDf = pd.DataFrame(
    columns=['Orthogroup', 'genesList', 'size'])
  # Iterate on all pal2nal file
  for alignementFile in tqdm(listOfPhylipFiles):
    # Get alignement length, gene names and orthogroup name and add it in result panda dataframe
    alignementLengthDf = construct_alignement_length(
        alignementLengthDf, alignementFile)

  # Save it in csv format
  output_dataframe(
      path=args.output,
      dataframe=alignementLengthDf,
      separator=',',
      verbose=True)
  return True


main()
