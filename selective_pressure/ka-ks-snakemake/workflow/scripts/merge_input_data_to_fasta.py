# %%
import argparse
from typing import Dict

import pandas as pd
from Bio import SeqIO
from lib.sequences_parsing import (construct_fasta_of_orthogroup,
                                   get_gene_ids_of_a_family,
                                   get_gene_ids_of_a_ohnologous,
                                   get_gene_ids_of_a_rbbh, loadSequencesFile)

# %%


def get_arguments():
  """This function aims to define argument used for this script

  Returns
  -------
  ArgumentParser object
      This object store all possible arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      type=str,
      help="Input families",
      required=True)
  parser.add_argument(
      "-t",
      "--type",
      type=str,
      help="Type of Input",
      choices=['rbbh', 'bdbh', 'orthofinder'],
      required=True)
  parser.add_argument(
      "-u",
      "--units",
      type=str,
      help="Path to units file",
      required=True)

  args = parser.parse_args()
  return args


# %%
# Get argument parser
args = get_arguments()

outputPath = {"proteinFasta": "results/temp/protein_fasta_files/",
              "nucleicFasta": "results/temp/nucleic_fasta_files/"}
# %%
# Load sequences as an array of SeqIO handle
pathToFasta = pd.read_csv(args.units,
                          sep='\t',
                          index_col=0,
                          comment="#")

familiesData = pd.read_csv(args.input,
                           sep="\t",
                           compression='infer',
                           comment="#")

familiesData["Orthogroup"] = familiesData["Orthogroup"].str.replace(' ', '')

proteinSequences = loadSequencesFile(pathToFasta["proteinSequences"].tolist())
cdsSequences = loadSequencesFile(pathToFasta["cdsSequences"].tolist())

# Merge SeqIO objects to dictionanries of records
recordsProteins, recordsNucleic = {}, {}
for i in range(0, len(proteinSequences)):
  recordsProteins.update(SeqIO.to_dict(proteinSequences[i]))
  recordsNucleic.update(SeqIO.to_dict(cdsSequences[i]))

if args.type == "rbbh":
  # Load files
  rbbh_malus_malus = pd.read_csv(
      args.input[0],
      compression='infer',
      sep=',')
  rbbh_malus_prunus = pd.read_csv(
      args.input[0],
      compression='infer',
      sep=',')

  rbbhTriplet = pd.merge(rbbh_malus_malus[['query_x', 'query_y']],
                         rbbh_malus_prunus[['query_x', 'subject_x']],
                         left_on='query_x', right_on='subject_x',
                         how='inner',
                         suffixes=['_malus', '_prunus'])
  # Get Seq.record of each genes associated to each orthogroups
  orthoProteins = rbbhTriplet.apply(
      get_gene_ids_of_a_rbbh, axis=1, args=(
          recordsProteins, ))
  orthoNucleic = rbbhTriplet.apply(
      get_gene_ids_of_a_rbbh, axis=1, args=(
          recordsNucleic, ))
  # Creates a set of fasta files named after the orthogroup identifier,
  # and the associated protein or nucleic sequences therein
  construct_fasta_of_orthogroup(
      [[orthoProteins],
       [orthoNucleic]],
      outputPath,
      ['proteinFasta', 'nucleicFasta'])

elif args.type == "bdbh":

  familiesData = familiesData.reset_index()
  # Get Seq.record of each genes associated to each orthogroups
  proteinsRecords = familiesData.apply(
      get_gene_ids_of_a_ohnologous, axis=1, args=(
          recordsProteins,
          ['gene1', 'gene2', 'gene3']))
  nucleicRecords = familiesData.apply(
      get_gene_ids_of_a_ohnologous, axis=1, args=(
          recordsNucleic,
          ['gene1', 'gene2', 'gene3']))

  # Creates a set of fasta files named after the orthogroup identifier, and the associated protein sequences therein
  construct_fasta_of_orthogroup(
      [[proteinsRecords],
       [nucleicRecords]],
      outputPath,
      ['proteinFasta', 'nucleicFasta'])

elif args.type == "orthofinder":

  columnsOfSpecies = [
    x for x in familiesData.columns if x != "Orthogroup"]

  proteinsRecords = familiesData.apply(
    get_gene_ids_of_a_family, axis=1, args=(
        recordsProteins,
        columnsOfSpecies))
  nucleicRecords = familiesData.apply(
      get_gene_ids_of_a_family, axis=1, args=(
          recordsNucleic,
          columnsOfSpecies))
  # Creates a set of fasta files named after the orthogroup identifier,
  # and the associated protein sequences therein
  construct_fasta_of_orthogroup(
      [[proteinsRecords],
       [nucleicRecords]],
      outputPath,
      ['proteinFasta', 'nucleicFasta'])
