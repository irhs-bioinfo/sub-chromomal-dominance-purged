import argparse
import re
from typing import Dict, List, Optional, Set, Tuple

import numpy as np
import pandas as pd
from lib.misc import get_list_of_file_with_extension, output_dataframe
from tqdm import tqdm


def get_arguments():
  """This function aims to define argument used for this script

  Returns
  -------
  ArgumentParser object
      This object store all possible arguments
  """
  parser = argparse.ArgumentParser()

  parser.add_argument(
      "-m", "--matrixDirectory",
      help="Path to alignement matrix of percentage of identity files",
      type=str,
      required=True)
  parser.add_argument(
      "-o", "--output",
      help="Path to output file",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def construct_percent_identity_df(file: str, dfFinalResult: pd.DataFrame) -> pd.DataFrame:
  """ 
  Open file in a dataframe. Load it with one or more space as delimiter
  skip first row storing number of implicated sequence, get no header and
  set first column as an index column
  Loaded file is a square matrix with index in first column and no header as provided below:

    3
    MD06G1023100     83.783782 100.000000 73.648649
    MD00G1000200     100.000000 83.783782 76.470590
    Prupe.5G024300.1 76.470590 73.648649 100.000000


  Parameters
  ----------
  file : str
      [description]
  dfFinalResult : pd.DataFrame
      [description]

  Returns
  -------
  pd.DataFrame
      [description]
  """

  distanceMatrix = pd.read_csv(
    file, delimiter='\s+', skiprows=1, header=None, index_col=0)
  # Set index column as header array too
  distanceMatrix.columns = distanceMatrix.index
  # Give a name to index
  distanceMatrix.index.names = ['GeneNames']
  # Get only superior triangular matrix using numpy triu (triangular upper)
  stackedDf = square_matrix_to_dataframe(distanceMatrix)
  # rename column name
  stackedDf.columns = ['gene2', 'percentIdentity']
  # Reset last index allowing ta save it in final file
  stackedDf = stackedDf.reset_index()
  # Rename it
  stackedDf.columns = ['gene1', 'gene2', 'percentIdentity']
  # Add a new column with rbbh group or orthogroup ID
  stackedDf['Orthogroup'] = file.replace('.fasta.mat', '.')
  # Delete alignement with himself by deleting duplicated value between gene1 and gene2 and with 100 % of identity
  stackedDf = stackedDf.apply(drop_alignement_with_himself, axis=1)
  stackedDf.dropna(subset=['percentIdentity'], inplace=True)
  # Append in gloabl data frames
  dfFinalResult = dfFinalResult.append(stackedDf)
  return dfFinalResult


def square_matrix_to_dataframe(distanceMatrix: pd.DataFrame) -> pd.DataFrame:
  """[summary]

  Parameters
  ----------
  distanceMatrix : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  # Get only superior triangular matrix using numpy triu (triangular upper)
  distanceTriangularMatrix = distanceMatrix.where(
      np.triu(np.ones(distanceMatrix.shape)).astype(np.bool))
  # Reste first index, cannot reset both index at the same time beacause they have the same name
  stackedDf = distanceTriangularMatrix.stack().reset_index(level=1)
  return stackedDf


def drop_alignement_with_himself(stackedDfRow: pd.DataFrame) -> pd.DataFrame:
  """[summary]

  Parameters
  ----------
  stackedDfRow : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  if stackedDfRow['gene1'] == stackedDfRow['gene2'] and stackedDfRow['percentIdentity'] == 100:
    stackedDfRow['percentIdentity'] = np.nan
  return stackedDfRow


def main() -> True:

  # tqdm initialisation for pandas apply
  tqdm.pandas(desc="Pandas Apply")
  # Get argument parser
  args = get_arguments()
  # Get name of matrix files
  listOfDistanceMatrixFiles = get_list_of_file_with_extension(
      args.matrixDirectory, "*.mat")
  # Initialize a data frame to store results
  dfFinalResult = pd.DataFrame()
  # Iterate on all matrix files
  for file in tqdm(listOfDistanceMatrixFiles):
    dfFinalResult = construct_percent_identity_df(file, dfFinalResult)

  # Save it in csv format
  output_dataframe(
      args.output,
      dfFinalResult,
      ',',
      True)
  return True


# %%
main()
