import argparse
import re
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd
from Bio.Phylo.PAML import yn00
from lib.misc import get_list_of_file_with_extension, output_dataframe
from lib.paml import (build_dictionnary_of_yn00_results, compute_log_of_column,
                      drop_duplicated_rows_by_check_string, get_all_methods)
from tqdm import tqdm


def get_arguments():
  """This function aims to define argument used for this script

  Returns
  -------
  ArgumentParser object
      This object store all possible arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-y",
      "--yn00Directory",
      help="Yn00 output file directory",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to config file",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def remove_miss_aligned_groups(listOfyn00Files: List[str], removedFiles: List[str]):
  for yn00File in tqdm(listOfyn00Files):
    with open(yn00File, "r") as file:
      firstLine = file. readline()
      if re.search(r"^\s+\d+\s+0$", firstLine) != None:
        removedGroup = re.search(r"\/(\w+)\.", yn00File).group(1)
        removedFiles.append(removedGroup)
        listOfyn00Files.remove(yn00File)
  return listOfyn00Files


# %%
# tqdm initialisation for pandas apply
tqdm.pandas(desc="Pandas Apply")
# Get argument parser
args = get_arguments()

# Get list of files results
listOfyn00Files = get_list_of_file_with_extension(
    args.yn00Directory, "*.yn00")

# Get list of methods computed
uniqueMethods = get_all_methods(listOfyn00Files[0])
"""
# Details about regex

## "^\s+\d+\s+0$"

^ asserts position at start of a line
+ Quantifier — Matches between one and unlimited times
\s+ matches any whitespace character
\d+ matches a digit (equal to [0-9])
\s+ matches any whitespace character
0 matches the character 0 literally
$ asserts position at the end of a line

## "\/(\w+)\."

\/ matches the character / literally
1st Capturing Group (\w+)
\w+ matches any word character (equal to [a-zA-Z0-9_])
+ Quantifier — Matches between one and unlimited times
\. matches the character . literally
"""

removedFiles = []

listOfyn00Files = remove_miss_aligned_groups(listOfyn00Files, removedFiles)

print("Removed groups:" + str(removedFiles))
# Init dictionnary to store results
storeDf = {}
for yn00File in tqdm(listOfyn00Files):
  try:
    # Read result for current orthogroup
    result = yn00.read(yn00File)
    # Append results from yn00 file into nested dictionnary of dataframes
    build_dictionnary_of_yn00_results(yn00File, storeDf, uniqueMethods, result)
  except OSError as Yn00Error:
    print(Yn00Error)
    continue


# %%
##################### Output results in CSV for each method #####################

studiedMethods = ['NG86', 'YN00', 'LWL85', 'LWL85m', 'LPB93']
firstOrthogroup = list(storeDf.keys())[0]
kaKsWholeResults = {}
# Init dataframe for each method with first orthogroup
for method in studiedMethods:
  kaKsWholeResults[method] = pd.DataFrame(storeDf[firstOrthogroup][method])
# Init dictionary to store dataframes
tempList = {'NG86': [],
            'YN00': [],
            'LWL85': [],
            'LWL85m': [],
            'LPB93': []}
for orthogroup in tqdm(storeDf.keys()):
  for method in studiedMethods:
    if not storeDf[orthogroup][method].empty:
      tempList[method].append(storeDf[orthogroup][method])

for method in studiedMethods:
  for tempDf in tempList[method]:
    kaKsWholeResults[method] = pd.concat(
        [kaKsWholeResults[method], tempDf], ignore_index=True)

# Compute log values, purge duplicated sequences and output dataframes in CSV files
for method in studiedMethods:
  # Drop duplicated rows by constructing a check string
  #kaKsWholeResults[method] = drop_duplicated_rows_by_check_string(kaKsWholeResults[method],
  #                                                                ['Sequence1', 'Sequence2', 'Orthogroup'])
  # If it is possible, compute log values
  kaKsWholeResults[method] = compute_log_of_column(kaKsWholeResults[method],
                                                   ['omega', 'dS', 'dN'])
  # Output dataframe as a csv file
  output_dataframe(
      args.output + "." + method,
      kaKsWholeResults[method],
      ',',
      True)
