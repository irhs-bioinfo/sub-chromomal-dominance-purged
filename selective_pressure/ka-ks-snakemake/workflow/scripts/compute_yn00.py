import argparse
import re
from typing import Dict, List, Optional, Set, Tuple

from Bio.Phylo.PAML import yn00


def get_arguments():
  """This function aims to define argument used for this script

  Returns
  -------
  ArgumentParser object
      This object store all possible arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-a",
      "--alignementFile",
      help="Input alignement file from PAL2NAL script",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Output file for yn00",
      type=str,
      required=True)
  parser.add_argument(
      "-w",
      "--workingDir",
      help="Path to config file",
      type=str,
      required=True)
  parser.add_argument(
      "-v",
      "--verbose",
      help="Ask for some verbose",
      action='store_true')
  args = parser.parse_args()
  return args


def construct_ctl_file(alignementFile: str,
                       outputFile: str,
                       workingDir: str,
                       verbose: bool) -> bool:
  """[summary]

  Parameters
  ----------
  alignementFile : str
      Path to alignement file
  outputFile : str
      Path to output file
  workingDir : str
      Path to working directory
  verbose : bool
      A boolean to ask verbose or not

  Returns
  -------
  bool
      A boolean to ask verbose or not
  """
  with open(alignementFile, "r") as file:
    header = re.findall(r'\d+', file.readline())
    # Check if file is not empty
    if header:
      # # Check in header size of aligned proteins. If it is superior to twelve try tu run YN00
      # if int(header[1]) > 20:
      yn = yn00.Yn00(alignment=alignementFile,
                     out_file=outputFile,
                     working_dir=workingDir)
      yn.set_options(verbose=verbose,
                     icode=0,
                     weighting=0,
                     commonf3x4=0)
      yn.run(ctl_file=None,
             command="yn00",
             parse=False)
  return True


def main() -> bool:
  args = get_arguments()
  result = construct_ctl_file(alignementFile=args.alignementFile,
                              outputFile=args.output,
                              workingDir=args.workingDir,
                              verbose=args.verbose)
  return True


main()
