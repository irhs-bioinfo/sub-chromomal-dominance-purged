# %% [markdown]
"""
###  Parse YN00's CSV file
Transform dataframe for each intra and inter specific and output it in CSV files
"""
# %%


def keep_interspecific_relationships(dfRow):
  """
  [summary]

  Parameters
  ----------
  dfRow : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  if dfRow['Sequence1'][:2] == dfRow['Sequence2'][:2]:
    dfRow['log_omega'] = np.nan
  return dfRow


def keep_intraspecific_relationships(dfRow, pattern):
  """
  [summary]

  Parameters
  ----------
  dfRow : [type]
      [description]
  pattern : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  if not dfRow['Sequence1'][:2] == pattern or not dfRow['Sequence1'][:2] == dfRow['Sequence2'][:2]:
    dfRow['log_omega'] = np.nan
  return dfRow


# Load whole results from CSV file
kaKsWholeResults = pd.read_csv(
    conf['output']['dataFrames']['kaKsFinalDataframe'] + '.YN00',
    compression='infer',
    sep=',')

# TODO: hardcoded
for specie in ['MD', 'Pr']:
  # Keep only relationships between genes of same specie
  kaKsSpecieResults = kaKsWholeResults.apply(
      keep_intraspecific_relationships, axis=1, args=(specie,))
  kaKsSpecieResults.dropna(subset=['log_omega'], inplace=True)
  # Check where save dataframe as a CSV file
  if specie == 'MD':
    output_dataframe(
        conf['output']['dataFrames']['kaKsMalusDataframe'],
        kaKsSpecieResults,
        ',',
        1)
  else:
    output_dataframe(
        conf['output']['dataFrames']['kaKsPrunusDataframe'],
        kaKsSpecieResults,
        ',',
        1)

# Get all relationships between two species
kaKsInterSpecificDataframe = kaKsWholeResults.apply(
    keep_interspecific_relationships, axis=1)
kaKsInterSpecificDataframe.dropna(subset=['log_omega'], inplace=True)

# Save it in csv format
output_dataframe(
    conf['output']['dataFrames']['kaKsInterSpecificDataframe'],
    kaKsInterSpecificDataframe,
    ',',
    1)
