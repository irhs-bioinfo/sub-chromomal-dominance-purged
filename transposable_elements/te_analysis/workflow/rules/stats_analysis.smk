rule te_clustering:
    input:
        te_stats_data="results/te_stats_data.csv",
    output:
        kmedoids="results/tables/genes_clusters_by_TE.kmedoids.csv",
        mini="results/tables/genes_clusters_by_TE.mini.csv",
        DBScan="results/tables/genes_clusters_by_TE.DBScan.csv",
    conda:
        "../envs/jupyter_downstream.yaml"
    shell:
        """python {scripts}/stats/te_clustering.py \
        --input {input.te_stats_data} \
        --output {output.kmedoids}"""


rule reconstruct_couple:
    input:
        kmedoids="results/tables/genes_clusters_by_TE.kmedoids.csv",
        ohnoGenes=config["duplicatedGenes"],
    output:
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
    conda:
        "../envs/parse_te_clustering.yaml"
    shell:
        """python {scripts}/misc/reconstruct_clustered_couples.py \
        --input {input.kmedoids} \
        --ohnoGenes {input.ohnoGenes} \
        --output {output.reconstructedCouples}"""
