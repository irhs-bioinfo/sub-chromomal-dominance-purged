from snakemake.utils import validate
import pandas as pd
import pathlib


# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
singularity: "docker://continuumio/miniconda3"


##### load config and sample sheets #####


configfile: "config/config.yaml"


validate(config, schema="../schemas/config.schema.yaml")

# Scripts dir to avoid long commands
scripts = "workflow/scripts"

# Ensure a 'results' directory is present
pathlib.Path("results").mkdir(parents=True, exist_ok=True)
