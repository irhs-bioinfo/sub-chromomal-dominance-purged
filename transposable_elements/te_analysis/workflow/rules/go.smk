rule get_go_annotation:
    input:
        annotations="resources/annotations/GDDH13_1-1_prot.fasta.tsv.gz",
    output:
        go_annotations="results/GO/go_annotations.tsv",
    conda:
        "../envs/GO.yaml"
    shell:
        """python {scripts}/GO/construct_go_annotations.py \
        --input {input.annotations} \
        --output {output.go_annotations}"""


rule download_go_schemas:
    output:
        go="resources/GO/go-basic.obo",
        goslim_plant="resources/GO/goslim_plant.obo",
    conda:
        "../envs/GO.yaml"
    shell:
        """
        wget http://purl.obolibrary.org/obo/go/go-basic.obo \
        --no-clobber \
        --output-document={output.go}
        wget http://current.geneontology.org/ontology/subsets/goslim_plant.obo \
        --no-clobber \
        --output-document={output.goslim_plant}"""


rule slim_go_annotation:
    input:
        go_annotations="results/GO/go_annotations.tsv",
        go="resources/GO/go-basic.obo",
        goslim_plant="resources/GO/goslim_plant.obo",
    output:
        go_slim_annotations="results/GO/go_slim_annotations.tsv",
    conda:
        "../envs/GO.yaml"
    shell:
        """
        python {scripts}/GO/map2slim/map_to_slim.py \
        --association_file {input.go_annotations} \
        {input.go} \
        {input.goslim_plant} > {output.go_slim_annotations}.notcleaned
        # Remove few lines of comments made by script but not handled by goatools
        grep -e '^MD' {output.go_slim_annotations}.notcleaned > {output.go_slim_annotations}
        rm -f  {output.go_slim_annotations}.notcleaned"""


rule compute_enrichment:
    """
    We compared the functions of duplicated genes and singleton genes for each TE density category
    and flanking region size. Genes of the same gene family are likely to share the same or similar
    function so we built random gene sets For each set of duplicated genes in TE-poor, TE-medium, and TE-rich categories, one gene was randomly chosen from a gene family.
    The functions of duplicated genes from these random sets and singleton genes were compared as well as the complete sets.

    The statistical tests used were Fisher’s exact tests with false discovery rate (FDR) correction according to the Benjamini–Hochberg procedure.
    """
    input:
        go_annotations="results/GO/go_annotations.tsv",
        go_slim_annotations="results/GO/go_slim_annotations.tsv",
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
        TEMetadata="results/tables/genes_clusters_by_TE.kmedoids.csv",
    output:
        go_enrichment="results/GO/all_experiences.csv.gz",
        go_slim_enrichment="results/GO/all_experiences_slim.csv.gz",
    conda:
        "../envs/GO.yaml"
    shell:
        """
        python {scripts}/GO/GO_enrichment.py \
        --goSlim {input.go_slim_annotations} \
        --go {input.go_annotations} \
        --teMetadata {input.TEMetadata} \
        --reconstructedCouples {input.reconstructedCouples} \
        --outputGoSlim {output.go_slim_enrichment} \
        --outputGo {output.go_enrichment}

        rm -f {scripts}/GO/*.obo
        """


rule compute_enrichment_between_cluster:
    """
        We then compared the functions of sets of genes according to their TE environment (TE-poor vs. TE-rich, TE-poor vs. TE-medium, TE-medium vs. TE-rich)
    for the two sizes of flanking regions.
    The statistical tests used were Fisher’s exact tests with false discovery rate (FDR) correction according to the Benjamini–Hochberg procedure.
    """
    input:
        go_annotations="results/GO/go_annotations.tsv",
        go_slim_annotations="results/GO/go_slim_annotations.tsv",
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
        TEMetadata="results/tables/genes_clusters_by_TE.kmedoids.csv",
    output:
        go_enrichment="results/GO/all_experiences_against_cluster.csv.gz",
        go_slim_enrichment="results/GO/all_experiences_slim_against_cluster.csv.gz",
    conda:
        "../envs/GO.yaml"
    shell:
        """
        python {scripts}/GO/GO_enrichment_between_cluster.py \
        --goSlim {input.go_slim_annotations} \
        --go {input.go_annotations} \
        --teMetadata {input.TEMetadata} \
        --reconstructedCouples {input.reconstructedCouples} \
        --outputGoSlim {output.go_slim_enrichment} \
        --outputGo {output.go_enrichment}

        rm -f {scripts}/GO/*.obo
        """


rule compute_enrichment_between_couples:
    """
        We then compared the functions of sets of genes according to their TE environment (TE-poor vs. TE-rich, TE-poor vs. TE-medium, TE-medium vs. TE-rich)
    for the two sizes of flanking regions.
    The statistical tests used were Fisher’s exact tests with false discovery rate (FDR) correction according to the Benjamini–Hochberg procedure.
    """
    input:
        go_annotations="results/GO/go_annotations.tsv",
        go_slim_annotations="results/GO/go_slim_annotations.tsv",
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
        TEMetadata="results/tables/genes_clusters_by_TE.kmedoids.csv",
    output:
        go_enrichment="results/GO/all_experiences_against_couples.csv.gz",
        go_slim_enrichment="results/GO/all_experiences_slim_against_couples.csv.gz",
    conda:
        "../envs/GO.yaml"
    shell:
        """
        python {scripts}/GO/GO_enrichment_between_couples.py \
        --goSlim {input.go_slim_annotations} \
        --go {input.go_annotations} \
        --teMetadata {input.TEMetadata} \
        --reconstructedCouples {input.reconstructedCouples} \
        --outputGoSlim {output.go_slim_enrichment} \
        --outputGo {output.go_enrichment}

        rm -f {scripts}/GO/*.obo
        """
