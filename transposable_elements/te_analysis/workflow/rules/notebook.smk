rule cluster_analysis:
    input:
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
    conda:
        "../envs/jupyter_downstream.yaml"
    log:
        # optional path to the processed notebook
        notebook="../notebook/cluster_analysis.ipynb.test",
    notebook:
        "../notebook/cluster_analysis.ipynb"


rule descriptive_stats:
    input:
        reconstructedCouples="results/tables/genes_clusters_by_TE.kmedoids.reconstructed_couples.csv",
    conda:
        "../envs/jupyter_downstream.yaml"
    log:
        # optional path to the processed notebook
        notebook="../notebook/descriptive_stats.ipynb.test",
    notebook:
        "../notebook/descriptive_stats.ipynb"
