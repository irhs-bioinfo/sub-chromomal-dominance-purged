rule generate_te_database:
    """
    This rule aims to generate a MySLQ database using GFF file storing Transposable Elements
    """
    input:
        gff=rules.filter_te_gff.output.cleaned_gff,
    output:
        tedb="results/tedb.sql",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_te_database.py \
        --gff {input.gff} \
        --output {output.tedb}"""


rule generate_gff_database:
    """
    This rule aims to generate a MySLQ database using GFF file storing gene annotations
    """
    input:
        gff=rules.filter_gene_gff.output.cleaned_gff,
    output:
        db="results/db.sql",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_te_database.py \
        --gff {input.gff} \
        --output {output.db}"""


rule generate_intron_database:
    """
    This rule aims to generate a MySLQ database using GFF file storing gene annotations
    """
    input:
        dbGff="results/db.sql",
    output:
        dbIntrons="results/db.intron.sql",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_intron_database.py \
        --database {input.dbGff} \
        --output {output.dbIntrons}"""


rule generate_te_dataframe:
    """
    This rule aims to generate csv file based on:
    informations gathered from Transposable elements database and gene annotation database
    """
    input:
        tedb="results/tedb.sql",
        db="results/db.intron.sql",
    output:
        te_density="results/te_density.csv",
        coverage="results/chromosomes_coverage.csv",
        exonic="results/exonic_te.csv",
    params:
        chrSize=config["chromosomeSize"],
        frameSize=config["frameSize"],
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_te_dataframe.py \
        --genes {input.db} \
        --tes {input.tedb} \
        --chromosomes {params.chrSize} \
        --frameSize {params.frameSize} \
        --output {output.te_density} \
        --coverage {output.coverage} \
        --exonic {output.exonic}"""


rule generate_te_stats:
    """
    """
    input:
        df="results/te_density.csv",
    params:
        tes=config["teGff"],
        genes=config["geneGff"],
        duplicates=config["duplicatedGenes"],
        densityMethod=config["densityMethod"],
    output:
        stats="results/te_stats_data.csv",
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_te_stats.py \
        --input {input.df} \
        --duplicates {params.duplicates} \
        --density {params.densityMethod} \
        --output {output.stats}"""
