rule filter_gene_gff:
    """
    This rule aims to filter the gene gff file, removing Chr00 mainly
    """
    input:
        gff_file=config["geneGff"],
    output:
        cleaned_gff=config["geneGff"].replace(".gff3", ".filtered.gff"),
        stats=report("workflow/report/removed_genes.txt", caption="../report/removed_genes.txt", category="Cleaning"),
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """
        python3 {scripts}/filter_gff.py \
        --gff {input.gff_file} \
        --output {output.cleaned_gff} \
        --type genes \
        --stats {output.stats}
        """


rule filter_te_gff:
    """
    This rule aims to filter the TE gff file, removing match part lines
    """
    input:
        gff_file=config["teGff"],
    output:
        cleaned_gff=config["teGff"].replace(".gff3", ".filtered.gff"),
        stats=report("workflow/report/removed_tes.txt", caption="../report/removed_genes.txt", category="Cleaning"),
    params:
        size=80,
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python3 {scripts}/filter_gff.py \
        --gff {input.gff_file} \
        --output {output.cleaned_gff} \
        --type TE \
        --size {params.size} \
        --stats {output.stats}
        """
