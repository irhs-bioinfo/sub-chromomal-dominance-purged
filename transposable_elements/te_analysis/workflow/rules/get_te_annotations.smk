# TODO: remove this function and base all on one file per chr


def get_number_of_waited_files(geneGff):
    """
    This function aims to get the number of genes in the gene annotation file
    """
    import pandas as pd

    dfGenes = pd.read_csv(geneGff, sep="\t", header=None, comment="#")
    dfGenes.columns = [
        "chr",
        "source",
        "type",
        "start",
        "end",
        "score",
        "strand",
        "phase",
        "attributes",
    ]
    dfGenes = dfGenes[dfGenes["type"] == "gene"]
    return range(0, len(dfGenes) // 1000, 1)


def get_number_of_chr(geneGff):
    """
    This function aims to get the number of genes in the gene annotation file
    """
    import pandas as pd

    dfGenes = pd.read_csv(geneGff, sep="\t", header=None, comment="#")
    dfGenes.columns = [
        "chr",
        "source",
        "type",
        "start",
        "end",
        "score",
        "strand",
        "phase",
        "attributes",
    ]
    chrList = dfGenes["chr"].unique()
    return range(0, len(chrList), 1)


rule generate_te_summary:
    """
    This rule aims to generate csv file based on:
    informations gathered from Transposable elements database and gene annotation database
    """
    input:
        rules.filter_gene_gff.output.cleaned_gff,
        tedb="results/tedb.sql",
        db="results/db.intron.sql",
    output:
        te_summary=expand(
            "results/tmp/{number}.csv",
            number=get_number_of_waited_files(
                rules.filter_gene_gff.output.cleaned_gff
            ),
        ),
    params:
        chrSize=config["chromosomeSize"],
        frameSize=config["frameSize"],
        basedir="results/tmp/",
        annotationType="TE_BLRtx",  #or TE_BLRx
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_te_summary.py \
        --genes {input.db} \
        --tes {input.tedb} \
        --chromosomes {params.chrSize} \
        --frameSize {params.frameSize} \
        --basedir {params.basedir} \
        --annotationType {params.annotationType}"""


rule concatenate_te_summary:
    """
    This rule aims to generate csv file based on:
    informations gathered from Transposable elements database and gene annotation database
    """
    input:
        rules.filter_gene_gff.output.cleaned_gff,
        te_summary=expand(
            "results/tmp/{number}.csv",
            number=get_number_of_waited_files(
                rules.filter_gene_gff.output.cleaned_gff
            ),
        ),
    output:
        te_summary_concatenated=expand(
            "results/te_metadatas/{number}_te_summary.csv.gz",
            number=get_number_of_chr(rules.filter_gene_gff.output.cleaned_gff),
        ),
        all_te_concatenated="results/te_metadatas/all_te_summary.csv.gz",
    params:
        basedir="results/tmp/",
        output_basedir="results/te_metadatas/number_te_summary.csv.gz",
        number_chr=len(get_number_of_chr(rules.filter_gene_gff.output.cleaned_gff)),
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/concatenate_te_summary_files.py \
        --basedir {params.basedir} \
        --output {params.output_basedir} \
        --numberChr {params.number_chr}"""


rule generate_global_summary:
    input:
        tedb="results/tedb.sql",
        inframe="results/te_metadatas/all_te_summary.csv.gz",
        exonic="results/exonic_te.csv",
    output:
        summary=report(
            "results/te_global_summary.csv",
            caption="../../results/te_global_summary.csv",
            category="Stats",
        ),
    conda:
        "../envs/generate_te_stats.yaml"
    shell:
        """python {scripts}/generate_global_summary.py \
        --tedb {input.tedb} \
        --inframe {input.inframe} \
        --exonic {input.exonic} \
        --summary {output.summary}"""
