import argparse
from tqdm import tqdm
from pandarallel import pandarallel
import yaml
import pandas as pd
import numpy as np
from typing import Dict, List, Optional, Set, Tuple
import sys
import re


def load_json(jsonPath: str) -> Dict:
  with open(jsonPath) as f:
    jsonFile = json.load(f)
  return jsonFile


# %%


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--genes",
      help="Path to genes gff model",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--tes",
      help="Path to transposable elements gff model",
      type=str,
      required=True)
  parser.add_argument(
      "-c",
      "--chromosomes",
      help="Path to file listing chromosomes files",
      type=str,
      required=True)
  parser.add_argument(
      "-d",
      "--duplicates",
      help="Path to new SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to transposable elements stats",
      type=str,
      required=True)
  parser.add_argument(
      "-p",
      "--partials",
      help="Path to partials transposable elements stats",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def get_gene_name(dfRow: pd.DataFrame) -> pd.DataFrame:
  """This function aims to get gene names from attribute column

  Parameters
  ----------
  dfRow : pd.DataFrame
      This is a row from gene's gff file

  Returns
  -------
  pd.DataFrame
      A rwo with a name column added, storing gene ID
  """
  attribute = dfRow["attribute"]
  id_pattern = re.compile('^ID=(mRNA:)?(?P<id>[^;]*?)(_v2\.0\.a1)?(;|$)')
  results = re.search(id_pattern, attribute)
  dfRow["name"] = results.group("id")
  return dfRow


def get_neighbors(geneRow: pd.DataFrame, teGff: pd.DataFrame, chromosomeSize: pd.DataFrame) -> pd.DataFrame:
  """This function aims to get all TEs in and around gene,
  computing size of total TEs and number of TEs

  Parameters
  ----------
  geneRow : pd.DataFrame
      This is a row storing gene informations
  teGff : pd.DataFrame
      Store TE gff file in a dataframe
  chromosomeSize: pd.DataFrame
      Store chromosome size

  Returns
  -------
  pd.DataFrame
      This is a row storing gene informations with two more columns:
        - numberTE
        - TElength
  """
  teGff = teGff[teGff["seqname"] == geneRow["geneChr"]]
  windowStart = max(geneRow["geneStart"] - 2000, 0)
  windowEnd = min(geneRow["geneEnd"] + 2000,
                  chromosomeSize[chromosomeSize["seqname"] == geneRow["geneChr"]]["size"].values[0])
  neighbors = teGff.loc[teGff["start"].between(
    windowStart, windowEnd) | teGff["end"].between(windowStart, windowEnd)]
  neighbors = neighbors.drop_duplicates().reset_index()
  geneRow["numberTE"] = neighbors.shape[0]

  te_positions = []
  for i, te in neighbors.iterrows():
    if te["end"] > windowEnd:
      te_positions.append([te["start"], windowEnd])
      report_te(geneRow["name"],
                te,
                windowEnd,
                False)
    elif te["start"] < windowStart:
      te_positions.append([windowStart, te["end"]])
      report_te(geneRow["name"],
                te,
                windowStart,
                True)
    else:
      te_positions.append([te["start"],
                           te["end"]])

  geneRow["TElength"] = merged_intervals_length(te_positions)
  return geneRow


def compute_TE_stats(geneRow: pd.DataFrame,
                     chromosomeSize: pd.DataFrame) -> pd.DataFrame:
  """This functions aims to compute differetns stat about TE includind coverage and density for each gene row

  Parameters
  ----------
  geneRow : pd.DataFrame
      [description]
  chromosomeSize : pd.DataFrame
      A dataframe storing size of each chromosome like:
        Chr00 52728359
        Chr01 32625452
        Chr02 37577729
        Chr03 37524076

  Returns
  -------
  pd.DataFrame
      Gene row with coverage and gene density modified
  """
  windowStart = max(geneRow["geneStart"] - 2000, 0)
  windowEnd = min(geneRow["geneEnd"] + 2000,
                  chromosomeSize[chromosomeSize["seqname"] == geneRow["geneChr"]]["size"].values[0])
  windowSize = (windowEnd - windowStart) + 1
  geneRow["TEcoverage"] = geneRow["TElength"] / (windowSize)

  if windowSize - geneRow["TElength"] == 0:
    geneRow["TEdensity"] = geneRow["numberTE"]
  else:
    geneRow["TEdensity"] = geneRow["numberTE"] / \
        (windowSize - geneRow["TElength"])

  return geneRow


def merged_intervals_length(intervalsCoord: List) -> int:
  """[summary]

  Parameters
  ----------
  intervalsCoord : List
      [description]

  Returns
  -------
  int
      [description]
  """
  # Based on https://www.geeksforgeeks.org/merging-intervals/
  intervalsCoord.sort(key=lambda x: x[0])
  mergedIntervals = []
  start = -1000
  max = -1000
  for i in range(len(intervalsCoord)):
    interval = intervalsCoord[i]
    if interval[0] > max:
      if i != 0:
        mergedIntervals.append([start, max])
      max = interval[1]
      start = interval[0]
    else:
      if interval[1] >= max:
        max = interval[1]
  if max != -1000 and [start, max] not in mergedIntervals:
    mergedIntervals.append([start, max])

  combinedSize = 0
  # Compute total size
  for interval in mergedIntervals:
    combinedSize += interval[1] - interval[0] + 1

  return combinedSize


def report_te(seqname: str, teData: pd.DataFrame, windowLimit: int, atStart: int) -> None:
  """This functions aims to store in dataframes differents informations on TE that are partially in watched window

  Parameters
  ----------
  seqname: str
      gene ID
  teData : pd.DataFrame
      A df storing TE datas from gff files
  windowLimit : int
      Size of watched window
  atStart : int
      Window start coordinate
  """
  teSize = teData["end"] - teData["start"] + 1
  if atStart:
    baseIn = teData["end"] - windowLimit + 1
  else:
    baseIn = windowLimit - teData["start"] + 1
  percentIn = baseIn / teSize
  reportedTe = pd.DataFrame({"seqname": [seqname],
                             "size": [teSize],
                             "baseIn": [baseIn],
                             "percentIn": [percentIn],
                             "atStart": [atStart]})
  global partialTe
  partialTe = partialTe.append(reportedTe, ignore_index=True)


def main():
  args = get_arguments()
  pandarallel.initialize()
  tqdm.pandas(desc="Computing on all genes")

  gffColumnsNames = ["seqname",
                     "source",
                     "feature",
                     "start",
                     "end",
                     "score",
                     "strand",
                     "frame",
                     "attribute"]

  teGff = pd.read_csv(args.tes,
                      compression="infer",
                      sep="\t",
                      comment="#",
                      names=gffColumnsNames)
  # Monkey patch, because it load first line of comment in file
  teGff = teGff.iloc[1:]

  genesGff = pd.read_csv(args.genes,
                         compression="infer",
                         sep="\t",
                         comment="#",
                         names=gffColumnsNames)

  chromosomeSize = pd.read_csv(args.chromosomes, sep="\s+",
                               names=["seqname",
                                      "size"])

  ohnologousGenes = pd.read_csv(args.duplicates,
                                compression="infer",
                                sep="\s+")

  partialTe = pd.DataFrame(columns=["seqname",
                                    "size",
                                    "baseIn",
                                    "percentIn",
                                    "atStart"])

  teGff = teGff[teGff["seqname"] != "Chr00"]
  teGff = teGff[teGff["feature"] == "match"]

  genesGff = genesGff[genesGff["feature"] == "mRNA"]
  genesGff = genesGff[genesGff["seqname"] != "Chr00"]
  genesGff = genesGff.progress_apply(get_gene_name, axis=1)

  TEMetadata = pd.DataFrame(
    columns=["name",
             "numberTE",
             "duplicate",
             "TElength",
             "geneStart",
             "geneEnd",
             "geneChr",
             "TEdensity",
             "TEcoverage"])
  TEMetadata["name"] = genesGff["name"]
  TEMetadata["geneStart"] = genesGff["start"]
  TEMetadata["geneEnd"] = genesGff["end"]
  TEMetadata["geneChr"] = genesGff["seqname"]
  TEMetadata = TEMetadata.reset_index(drop=True)
  ohnologousGenesSerie = pd.concat([ohnologousGenes["gene_x"],
                                    ohnologousGenes["gene_y"]],
                                   axis=0)
  ohnologousGenesSerie = ohnologousGenesSerie.reset_index(drop=True)

  TEMetadata["duplicate"] = TEMetadata["name"].isin(ohnologousGenesSerie)

  TEMetadata = TEMetadata.progress_apply(get_neighbors,
                                         axis=1,
                                         args=(teGff, chromosomeSize))
  TEMetadata = TEMetadata.progress_apply(compute_TE_stats,
                                         axis=1,
                                         args=(chromosomeSize,))

  TEMetadata.to_csv(args.output, index=False)
  partialTe.to_csv(args.partials, index=False)


if __name__ == "__main__":
  main()
