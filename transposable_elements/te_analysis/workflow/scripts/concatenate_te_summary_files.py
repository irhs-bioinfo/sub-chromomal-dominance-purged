import argparse
import glob
import os

import dask.dataframe as dd
from dask.diagnostics import ProgressBar


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-b",
      "--basedir",
      help="Path to tmp files",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to concanated files",
      type=str,
      required=True)
  parser.add_argument(
      "-n",
      "--numberChr",
      help="Number of chr in genome",
      type=int,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()

  columnNames = ["geneName",
                 "geneChr",
                 "TE_name",
                 "LTE",
                 "TEStart",
                 "TEStop",
                 "TEid",
                 "TEclass",
                 "TEorder",
                 "TEtype",
                 "TElast",
                 "confidence",
                 "annotationType"]
  # Add Dask progress bar
  ProgressBar().register()

  ddf = dd.read_csv(args.basedir + "*.csv",
                    header=None,
                    names=columnNames)
  groups = ddf.groupby(["geneChr"])

  for name in range(0, args.numberChr + 1, 1):
    groups.get_group(name).to_csv(args.output.replace("number", str(name)),
                                  header=True,
                                  index=False,
                                  single_file=True,
                                  compression="infer")
  ddf.to_csv(args.output.replace("number", "all"),
             header=True,
             index=False,
             single_file=True,
             compression="infer")
  # drop all tmp csv files
  for file in glob.glob("results/tmp/*.csv"):
    os.remove(file)


if __name__ == "__main__":
  main()
