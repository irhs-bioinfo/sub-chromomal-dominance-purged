# %%

import argparse

import numpy as np
import pandas as pd
from sklearn_extra.cluster import KMedoids
from sklearn.cluster import MiniBatchKMeans, DBSCAN

# %%


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      help="Path to genes with associated TEs dataframe",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to transposable elements stats",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  pd.options.mode.chained_assignment = None

  args = get_arguments()
  TEMetadata = pd.read_csv(args.input)
  TEMetadata["geneChr"] = TEMetadata["geneChr"].str.replace(r'Chr', '')

  TEMetadata = TEMetadata.loc[TEMetadata["TEcoverage"] < 1]
  TEMetadata.index = TEMetadata["name"]
  TEMetadata = TEMetadata.drop(columns=["name"])

  TEMetadata0Clusters = TEMetadata[TEMetadata["numberTE"] == 0]
  TEMetadata0Clusters["cluster"] = 7

  TEMetadataAll = TEMetadata[TEMetadata["numberTE"] != 0]

  X = TEMetadataAll[["TEdensity", "TEcoverage"]]

  # Best number of clusters seems to be 4
  kmedoids = KMedoids(n_clusters=4,
                      metric='manhattan',
                      #                    method='pam',
                      #                    init='heuristic',
                      # max_iter=100,
                      random_state=42)
  yhat = kmedoids.fit_predict(X)
  TEMetadataAll["cluster"] = yhat

  TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])
  TEMetadata.to_csv(args.output)
  print("K-Medoids completed")
  # Define the model
  model = MiniBatchKMeans(n_clusters=4)
  # Assign a cluster to each example
  yhat = model.fit_predict(X)
  TEMetadataAll["cluster"] = yhat

  TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])
  TEMetadata.to_csv(args.output.replace("kmedoids", "mini"))
  print("MiniBatchKMeans completed")

  # Define the model
  model = DBSCAN(eps=0.5, min_samples=2)
  # Assign a cluster to each example
  yhat = model.fit_predict(X)
  TEMetadataAll["cluster"] = yhat

  TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])
  TEMetadata.to_csv(args.output.replace("kmedoids", "DBScan"))
  print("DBSCAN completed")

  X = TEMetadataAll[["TEdensity"]]

  # Define the model
  model = MiniBatchKMeans(n_clusters=4)
  # Assign a cluster to each example
  yhat = model.fit_predict(X)
  TEMetadataAll["cluster"] = yhat

  TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])
  TEMetadata.to_csv(args.output.replace("kmedoids", "mini.onlyTEdensity"))

  X = TEMetadataAll[["TEdensity", "numberTE"]]

  # Define the model
  model = MiniBatchKMeans(n_clusters=4)
  # Assign a cluster to each example
  yhat = model.fit_predict(X)
  TEMetadataAll["cluster"] = yhat

  TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])
  TEMetadata.to_csv(args.output.replace("kmedoids", "mini.numberTE."))


if __name__ == "__main__":
  main()
