import argparse

import pandas as pd
import swifter
from tqdm import tqdm


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      help="Path to clustered genes",
      type=str,
      required=True)
  parser.add_argument(
      "-c",
      "--ohnoGenes",
      help="Path to ohnologous genes couples",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to newly recosntructed ohnologous genes couples",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def reorder_genes(row: pd.DataFrame) -> pd.DataFrame:
  """This function aims to reorder genes in order to have in gene x
  a gene with a chromosome number inferior to gene y one

  Parameters
  ----------
  row : pd.DataFrame
      Ohnologous genes dataframe row

  Returns
  -------
  pd.DataFrame
      Ohnologous genes dataframe row with reodered genes
  """
  if int(row["gene_x"][2:4]) > int(row["gene_y"][2:4]):
    row["gene_x"], row["gene_y"] = row["gene_y"], row["gene_x"]
  return row


def reconstruct_couple(row: pd.DataFrame, TEMetadata: pd.DataFrame) -> pd.DataFrame:
  """This function aims to annnotate each duplicated genes with his duplicated pair

  Parameters
  ----------
  row : pd.DataFrame
      Ohnologus genes pairs
  TEMetadata : pd.DataFrame
      Transposable elements dataframe

  Returns
  -------
  pd.DataFrame
      Transposable elements row with annotated gene couple
  """
  first = TEMetadata[TEMetadata["name"] == row["gene_x"]]
  second = TEMetadata[TEMetadata["name"] == row["gene_y"]]
  reconstructedCouple = pd.concat([first, second])
  if reconstructedCouple.shape[0] == 2:
    reconstructedCouple["gene_couple"] = reconstructedCouple["name"].iloc[0] + "-" + \
        reconstructedCouple["name"].iloc[1]
    reconstructedCouple["couple"] = reconstructedCouple["name"].str[2:4].iloc[0] + "-" + \
        reconstructedCouple["name"].str[2:4].iloc[1]
  return reconstructedCouple


def main():
  args = get_arguments()
  TEMetadata = pd.read_csv(
   args.input)

  TEMetadata["geneChr"] = TEMetadata["geneChr"].replace(r'Chr', '')
  TEMetadata = TEMetadata[TEMetadata["duplicate"] == True]
  ohnologousGenes = pd.read_csv(args.ohnoGenes,
                                sep=" ")

  ohnologousGenes = ohnologousGenes.swifter.apply(reorder_genes,
                                                  axis=1)
  ohnologousGenes["couple"] = ohnologousGenes["gene_x"].str[2:4] + "-"
  + ohnologousGenes["gene_y"].str[2:4]
  reconstructedCouplesList = ohnologousGenes.swifter.apply(reconstruct_couple,
                                                           axis=1,
                                                           args=(TEMetadata,))
  reconstructedCouples = pd.DataFrame()

  for listCouple in (tqdm(reconstructedCouplesList)):
    reconstructedCouples = reconstructedCouples.append(
      listCouple, ignore_index=True)
  reconstructedCouples.to_csv(args.output)


if __name__ == "__main__":
  main()
