# This script will only work with our dataset of gff made by TEdenovo
# Specifically beacause of the regex pattern use to find the classification
import re
import argparse
import gffutils
import pandas as pd
from tqdm import tqdm


def get_arguments():
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "--tedb",
      help="Path to transposable elements SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "--inframe",
      help="Path to transposable elements found in gene frames",
      type=str,
      required=True)
  parser.add_argument(
      "--exonic",
      help="Path to transposable elements found in exons",
      type=str,
      required=True)
  parser.add_argument(
      "--summary",
      help="Path to global summary frequency table",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def get_te_classification(attributes):
  classt = ''
  order = ''
  sfamily = ''
  family = ''
  percent = 0
  pattern = r"(?P<id>[\S^:]+):(?P<class>[\S^:]+):(?P<order>[\S^:]+):(?P<superfamily>[\S^:]+):(?P<family>[\S^:]+): (?P<percent>\d{1,2}\.\d{2})%"

  if 'TargetDescription' not in attributes:
    return ['Unidentified'] * 4

  desc = attributes['TargetDescription'][0]

  if re.search(pattern, desc) == None:
    return ['Unidentified'] * 4

  homologs = re.finditer(pattern, desc)

  for match in homologs:
    mpercent = float(match.group('percent'))

    if mpercent > percent:
      classt = match.group('class')
      order = match.group('order')
      sfamily = match.group('superfamily')
      family = match.group('family')
      percent = mpercent

  return [classt, order, sfamily, family]


def main():
  args = get_arguments()

  tedb = gffutils.FeatureDB(args.tedb, keep_order=True)
  inFrameTe = pd.read_csv(args.inframe, compression="infer")
  inFrameId = pd.unique(inFrameTe['TE_name'])
  exonicTe = pd.read_csv(args.exonic)
  exonicId = pd.unique(exonicTe['te'])
  allSummary = []

  for feature in tqdm(tedb.all_features(),
                      desc='Getting classification of all transposable elements',
                      total=len(list(tedb.all_features()))):
    summary = []
    classification = get_te_classification(feature.attributes)
    size = [feature.end - feature.start + 1]

    type = ''
    inFrame = feature.attributes['ID'] in inFrameId
    inExon = feature.attributes['ID'] in exonicId
    if inFrame and inExon:
      type = 'inframe_exonic'
    elif inFrame:
      type = 'inframe'
    elif inExon:
      type = 'exonic'
    else:
      type = 'intergenic'

    summary.extend([type])
    summary.extend(classification)
    summary.extend(size)
    allSummary.append(summary)

  freq = pd.DataFrame(allSummary, columns=[
                      'type', 'class', 'order', 'superfamily', 'family', 'size'])
  freq.to_csv(args.summary)


if __name__ == "__main__":
  main()
