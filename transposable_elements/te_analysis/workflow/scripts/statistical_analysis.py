# %%
from pandas_profiling import ProfileReport
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
# %%
genesClustersByTe = pd.read_csv(
    "../../results/tables/genes_clusters_by_TE.csv")
TEMetadata = pd.read_csv("../../results/te_stats_data.csv", index_col=0)

TEMetadata["TEcoverage"].median()
# %%

profile = ProfileReport(
    genesClustersByTe, title='Pandas Profiling Report', explorative=True)
profile.to_file("../../results/pandas_report/genesClustersByTe.html")

# %%
fig = sns.displot(TEMetadata["TEcoverage"])
fig.savefig("../../results/plots/TEcoverage.png")
fig = sns.displot(TEMetadata["TEdensity"])
fig.savefig("../../results/plots/TEdensity.png")
fig = sns.displot(TEMetadata["numberTE"])
fig.savefig("../../results/plots/numberTE.png")


stats.spearmanr(TEMetadata["TEdensity"], TEMetadata["TEcoverage"])

TEMetadata.loc[TEMetadata["TEcoverage"] > 0.419]

# %%
# Classification file is tabulated on 12 colomns as the classification table:

#  Consensus name in 'Seq_name',
#  Consensus length(bp) in 'length',
#  Consensus orientation in 'strand' values + or -,
#  Is consensus confused? in 'confused' values True or False,
#  Consensus class in 'class' values I or II if consensus classified as TE, value NA if consensus classified as not TE,
#  Consensus order in 'order' values all order from Class I and class II if consensus classified as TE, value NA if consensus classified as not TE,
#  Wicker code attributed to consensus in 'Wcode' values see in Wicker et al., Nat.Rev.Genet., 2007 if consensus classified as TE, value PHG / SSR / PrDNA if consensus classified as not TE,
#  Consensus super family in 'sFamily' values see in Wicker et al., Nat.Rev.Genet., 2007 if consensus classified as TE, value NA if consensus classified as not TE,
#  Confidence index in 'CI' value calculated by PASTEC using the decision rules from DecisionRule.yaml,
#  Features found by homology in 'coding',
#  Structural features in 'struct',
#  Features from profiles tagged as 'other' and / or from other classification of confused consensus in 'other'
# "NA" means "Not Available"

teGff = pd.read_csv("../../resources/GDDH13_1-1_TE.tar.xz",
                    compression="infer",
                    sep="\t",
                    comment="#",
                    names=["seqname",
                           "source",
                           "feature",
                           "start",
                           "end",
                           "score",
                           "strand",
                           "frame",
                           "attribute"])
# Monkey patch, because it load first line of comment in file
teGff = teGff.iloc[1:]
teGff = teGff[teGff["feature"] == "match"]
display(teGff.iloc[4]["attribute"].split('='))

# %%
