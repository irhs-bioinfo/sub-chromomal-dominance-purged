# %%

import argparse

import gffutils
import pandas as pd
from interval import interval
from pandarallel import pandarallel
from tqdm import tqdm


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--genes",
      help="Path to genes SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--tes",
      help="Path to transposable elements SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-c",
      "--chromosomes",
      help="Path to dataframe with chromosomes size",
      type=str,
      required=True)
  parser.add_argument(
      "-f",
      "--frameSize",
      help="Size of the frame to consider before and after genes",
      type=int,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to new SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "--coverage",
      help="Path to chromosomes coverage output",
      type=str,
      required=True)
  parser.add_argument(
      "--exonic",
      help="Path to exonic transposable elements output",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def get_interval_size(interval):
  intervalSize = 0
  for component in interval.components:
    componentSize = component[0].sup - component[0].inf + 1
    intervalSize += componentSize
  return intervalSize


def main():
  args = get_arguments()

  chromosomes = pd.read_table(
    args.chromosomes, sep="\s+", names=["seqname", "size"], index_col="seqname"
  )

  densityDf = []
  exonicTe = []

  db = gffutils.FeatureDB(args.genes, keep_order=True)
  teDb = gffutils.FeatureDB(args.tes, keep_order=True)

  coverage = {}
  for chr in chromosomes.index:
    coverage[chr] = {"window": interval(), "exon": interval()}

  for gene in tqdm(db.features_of_type('gene'),
                   desc='Computing density on each genes',
                   total=len(list(db.features_of_type('gene')))):
    windowUp = interval[max(0, gene.start - args.frameSize), gene.start - 1]
    windowDown = interval[gene.end + 1,
                          min(chromosomes.at[gene.seqid, "size"], gene.end + args.frameSize)]
    windowPos = windowUp | windowDown

    intronsPos = interval()
    for intron in db.children(gene,
                              order_by='start',
                              featuretype='intron'):
      intronPos = interval([intron.start, intron.end])
      intronsPos = intronsPos | intronPos

    windowPos = windowPos | intronsPos
    coverage[gene.seqid]["window"] = coverage[gene.seqid]["window"] | windowPos

    teRegion = teDb.region(seqid=gene.seqid,
                           start=windowPos[0].inf,
                           end=windowPos[len(windowPos) - 1].sup,
                           completely_within=False)
    nTes = 0
    tesPos = interval()

    for te in teRegion:
      tePos = interval[te.start, te.end]
      tesPos = tesPos | tePos
      nTes += 1

    windowWithTe = windowPos & tesPos

    windowLength = get_interval_size(windowPos)
    tesLength = get_interval_size(windowWithTe)

    for exon in db.children(gene,
                            order_by='start',
                            featuretype='exon'):
      exonPos = interval([exon.start, exon.end])
      coverage[gene.seqid]["exon"] = coverage[gene.seqid]["exon"] | exonPos
      teExon = teDb.region(seqid=gene.seqid,
                           start=exonPos[0].inf,
                           end=exonPos[0].sup,
                           completely_within=True)
      # Substract te exclusively in exon from te count
      for te in teExon:
        exonicTe.append({"gene": gene['Name'][0],
                         "te": te.attributes['ID'][0]})
        nTes -= 1

    dic = {"geneName": gene['Name'][0],
           "geneChr": gene.seqid,
           "LGene": windowLength,
           "LTE": tesLength,
           "nTE": nTes}
    densityDf.append(dic)

  chromosomes[["window", "exon", "both"]] = 0
  for chr in chromosomes.index:
    chromosomes.at[chr, "window"] = get_interval_size(coverage[chr]["window"])
    chromosomes.at[chr, "exon"] = get_interval_size(coverage[chr]["exon"])
    chromosomes.at[chr, "both"] = get_interval_size(
      coverage[chr]["window"] & coverage[chr]["exon"])

  chromosomes.to_csv(args.coverage)

  exonicTe = pd.DataFrame.from_dict(exonicTe)
  exonicTe.to_csv(args.exonic, index=False)

  densityDf = pd.DataFrame.from_dict(densityDf)
  densityDf = densityDf.set_index("geneName")
  densityDf.to_csv(args.output)


if __name__ == "__main__":
  main()
