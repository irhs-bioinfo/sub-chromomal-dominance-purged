# %%
import argparse

import numpy as np
import pandas as pd
# from pandarallel import pandarallel
from tqdm import tqdm


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      help="Path to genes with associated TEs dataframe",
      type=str,
      required=True)
  parser.add_argument(
      "-d",
      "--duplicates",
      help="Path to new SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "--density",
      help="Method to use to calculate tes density",
      choices=['substract', 'keep'],
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to transposable elements stats",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  # pandarallel.initialize()
  tqdm.pandas(desc="Computing on all genes")

  ohnologousGenes = pd.read_csv(args.duplicates,
                                compression="infer",
                                sep="\s+")

  TEMetadata = pd.read_csv(args.input, sep=",")
  TEMetadata = TEMetadata.rename(columns={'geneName': 'name',
                                          'LTE': 'TElength',
                                          'nTE': 'numberTE'})
  ohnologousGenesSerie = pd.concat([ohnologousGenes["gene_x"],
                                    ohnologousGenes["gene_y"]],
                                   axis=0)
  ohnologousGenesSerie = ohnologousGenesSerie.reset_index(drop=True)

  TEMetadata = TEMetadata.assign(
    duplicate=lambda x: x.name.isin(ohnologousGenesSerie))
  TEMetadata = TEMetadata.assign(TEcoverage=lambda x: x.TElength / x.LGene)
  if args.density == 'substract':
    TEMetadata = TEMetadata.assign(
      TEdensity=lambda x: x.numberTE / (x.LGene - x.TElength))
  elif args.density == 'keep':
    TEMetadata = TEMetadata.assign(
      TEdensity=lambda x: x.numberTE / x.LGene)


  TEMetadata.to_csv(args.output, index=False)


if __name__ == "__main__":
  main()
