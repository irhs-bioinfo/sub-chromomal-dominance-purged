import argparse

import pandas as pd


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--gff",
      help="Path to genes gff model",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to filtered file",
      type=str,
      required=True)
  parser.add_argument(
      "-r",
      "--rePattern",
      help="Regex to use as a filter",
      type=str,
      required=False)
  parser.add_argument(
      "-t",
      "--type",
      help="gff type stored (TE or genes)",
      type=str,
      required=True)
  parser.add_argument(
      "-s",
      "--size",
      help="TE Size to filter",
      type=int)
  parser.add_argument(
      "--stats",
      help="Path to stats report file",
      type=str)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()

  gff = pd.read_table(args.gff,
                      header=None,
                      names=["seqid",
                             "source",
                             "type",
                             "start",
                             "end",
                             "score",
                             "strand",
                             "phase",
                             "attributes"],
                      comment="#")
  print("Cleaning Chr00 in gff file")
  filtered_gff = gff.loc[~gff["seqid"].str.contains("Chr00", regex=True)]
  if args.type == "TE":
    filtered_gff = gff.loc[gff["type"] == "match"]
    if args.size:
      print("Initial number of TE: {}".format(filtered_gff.shape[0]))
      filtered_gff["size"] = filtered_gff["end"] - filtered_gff["start"] + 1
      filtered_gff = filtered_gff.loc[filtered_gff["size"] > args.size]
      print("Number of TE after size ({} length) filter: {}".format(
        args.size, filtered_gff.shape[0]))
      filtered_gff = filtered_gff.drop(columns=["size"])

  filtered_gff.to_csv(args.output, sep='\t', header=None, index=False)

  stats = """\
  Number of elements in gff before and after cleaning.
  Before: {before}
  After: {after}
  """.format(before=gff.shape[0], after=filtered_gff.shape[0])

  with open(args.stats, "w") as f:
    f.write(stats)


if __name__ == "__main__":
  main()
