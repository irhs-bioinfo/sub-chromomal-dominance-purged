# %%
from __future__ import annotations

import argparse
import re
from typing import Dict, List, Optional, Set, Tuple

import gffutils
import numpy as np
import pandas as pd
from interval import interval
from tqdm import tqdm


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-g",
      "--genes",
      help="Path to genes SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--tes",
      help="Path to transposable elements SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-c",
      "--chromosomes",
      help="Path to dataframe with chromosomes size",
      type=str,
      required=True)
  parser.add_argument(
      "-f",
      "--frameSize",
      help="Size of the frame to consider before and after genes",
      type=int,
      required=True)
  parser.add_argument(
      "-b",
      "--basedir",
      help="Path to tmp files",
      type=str,
      required=True)
  parser.add_argument(
      "-a",
      "--annotationType",
      help="Annotation type",
      nargs='+',
      required=True)
  args = parser.parse_args()
  return args


def get_interval_size(interval):
  intervalSize = 0
  for component in interval.components:
    componentSize = component[0].sup - component[0].inf + 1
    intervalSize += componentSize
  return intervalSize


def parse_confidence(targetDescParsed):
  """
  This function parses the confidence of the annotation

  Parameters
  ----------
  targetDescParsed : str
      Description of the target

  Returns
  -------
  confidence : float
      Confidence of the annotation
  """

  if len(targetDescParsed.split(":")) == 7:
    parsedConf = targetDescParsed.split(":")[
        6]
  else:
    parsedConf = targetDescParsed.split(":")[
        5]

  if parsedConf == "?":
    parsedConf = np.nan
  else:
    parsedConf = parsedConf.replace("%", "").strip()
  return float(parsedConf)


def extract_max_confidence_annotation(targetDescParsed):
  """
  This function extracts the most confident annotation from a target description

  Parameters
  ----------
  targetDescParsed : str
      Description of the target

  Returns
  -------
  TEid : str
      Transposable element ID
  TEclass : str
      Transposable element class
  TEorder : str
      Transposable element order
  TEtype : str
      Transposable element type
  confidence : float
      Confidence of the annotation
  """

  targetDescParsedSplitted = targetDescParsed.split("|")
  confArr = []
  for targetDescParsedSplittedItem in targetDescParsedSplitted:
    parsedConf = parse_confidence(targetDescParsedSplittedItem)
    confArr.append(parsedConf)

  maxConf = confArr.index(max(confArr))
  maxConfTarget = targetDescParsedSplitted[maxConf]

  TEid = maxConfTarget.split(":")[0].strip()
  TEclass = maxConfTarget.split(":")[1].strip()
  TEorder = maxConfTarget.split(":")[2].strip()
  TEtype = maxConfTarget.split(":")[3].strip()
  TElast = maxConfTarget.split(":")[4].strip()
  confidence = parse_confidence(maxConfTarget)

  return confidence, TEid, TEclass, TEorder, TEtype, TElast


def output_data_and_reset__if_needed(i, computedGenesBatch1000, densityDf, columnNames, outputFileBasename):
  """
  This function checks if the reinitialisation of the densityDf is needed.
  If True, the densityDf is reinitialized and returned.

  Parameters
    ----------
  i : int
      number of computed genes
  computedGenesBatch1000 : int
      current number of dataframe
  densityDf : DataFrame
      DataFrame containing the datas of each TE
  columnNames : list
      list of column names
  outputFileBasename : str
      basename of the output file
  Returns
  -------
  densityDf : DataFrame
      DataFrame containing the datas of each TE
  j : int
      New current number of dataframe
  """
  if i // 1000 != computedGenesBatch1000:
    densityDf.to_csv(outputFileBasename + str(computedGenesBatch1000) + ".csv",
                     mode='a',
                     header=False,
                     index=False)
    densityDf = pd.DataFrame(columns=columnNames)
    computedGenesBatch1000 = i // 1000
  return densityDf, computedGenesBatch1000


def construct_regex(annotationTypeItem: str) -> str:
  """
  This function constructs the regex to match the annotation

  Parameters
  ----------
  annotationTypeItem : str
      annotation type
  Returns
  -------
  regex : str
      regex to match the annotation
  """
  if annotationTypeItem == "TE_BLRx":
    regex = r"TE_BLRx: (.*?)$"
  elif annotationTypeItem == "TE_BLRtx":
    regex = r"TE_BLRtx: (.*?)$"
  return regex


def clean_annotation_string(regexResult: str) -> str:
  """
  This function cleans the annotation string

  Parameters
  ----------
  regexResult : str
      annotation string
  Returns
  -------
  regexResult : str
      cleaned annotation string
  """
  if re.search(r'(.*?) profiles: ', regexResult) is not None:
    regexResult = re.search(r'(.*?) profiles: ', regexResult)
    regexResult = regexResult.group(1)
  if re.search(r'(.*?)\%\sTE_BLRt{0,1}x:\s', regexResult) is not None:
    regexResult = re.search(r'(.*?)\%\sTE_BLRt{0,1}x:\s', regexResult)
    regexResult = regexResult.group(1)
  return regexResult


def get_te_data(te, annotationTypeList: List[str]):
  """
  This function retrieves the data from a given transposable element

  Parameters
  ----------
  te : gffutils.Feature
      Transposable element to retrieve data from
  annotationType : List[str]
      List of annotation types to retrieve

  Returns
    -------
  TEid : str
      Transposable element ID
  TEclass : str
      Transposable element class
  TEorder : str
      Transposable element order
  TEtype : str
      Transposable element type
  confidence : float
      Confidence of the annotation
  annotationType : str
      Annotation type TE_BLRx or TE_BLRtx
  """
  TEclass = np.nan
  TEorder = np.nan
  TEtype = np.nan
  TEid = np.nan
  TElast = np.nan
  annotationType = np.nan
  confidence = 0
  if 'TargetDescription' in te.attributes:
    targetDesc = te.attributes['TargetDescription'][0]
    targetDescParsed = re.search(r'\((.*?)\)', targetDesc)
    if targetDescParsed is not None:
      targetDescParsed = targetDescParsed.group(1)
      for annotationTypeItem in annotationTypeList:
        regex = construct_regex(annotationTypeItem)
        regexResult = re.search(regex,
                                targetDescParsed)
        if regexResult is not None:
          regexResult = regexResult.group(1)
          regexResult = clean_annotation_string(regexResult)
          confidenceTmp, TEidTmp, TEclassTmp, TEorderTmp, TEtypeTmp, TElastTmp = extract_max_confidence_annotation(
              regexResult)
          if confidenceTmp > confidence:
            confidence = confidenceTmp
            TEid = TEidTmp
            TEclass = TEclassTmp
            TEorder = TEorderTmp
            TEtype = TEtypeTmp
            TElast = TElastTmp
            annotationType = annotationTypeItem
  return TEid, TEclass, TEorder, TEtype, TElast, confidence, annotationType


def main():
  columnNames = ["geneName",
                 "geneChr",
                 "TE_name",
                 "LTE",
                 "TEStart",
                 "TEStop",
                 "TEid",
                 "TEclass",
                 "TEorder",
                 "TEtype",
                 "TElast",
                 "confidence",
                 "annotationType"]
  args = get_arguments()

  chromosomes = pd.read_table(args.chromosomes,
                              sep="\s+",
                              names=["seqname", "size"],
                              index_col="seqname")

  densityDf = pd.DataFrame(columns=columnNames)
  db = gffutils.FeatureDB(args.genes, keep_order=True)
  teDb = gffutils.FeatureDB(args.tes, keep_order=True)
  computedGenes = 0
  computedGenesBatch1000 = 0
  for gene in tqdm(db.features_of_type('gene'),
                   desc='Computing annotation of each genes',
                   total=len(list(db.features_of_type('gene')))):
    densityDf, computedGenesBatch1000 = output_data_and_reset__if_needed(computedGenes,
                                                                         computedGenesBatch1000,
                                                                         densityDf,
                                                                         columnNames,
                                                                         args.basedir)
    windowUp = interval[max(0, gene.start - args.frameSize),
                        gene.start - 1]
    windowDown = interval[gene.end + 1,
                          min(chromosomes.at[gene.seqid, "size"],
                              gene.end + args.frameSize)]
    windowPos = windowUp | windowDown

    intronsPos = interval()
    for intron in db.children(gene,
                              order_by='start',
                              featuretype='intron'):
      intronPos = interval([intron.start, intron.end])
      intronsPos = intronsPos | intronPos

    windowPos = windowPos | intronsPos

    teRegion = teDb.region(seqid=gene.seqid,
                           start=windowPos[0].inf,
                           end=windowPos[len(windowPos) - 1].sup,
                           completely_within=False)
    for te in teRegion:
      # If gff is well cleaned this test is useless
      # if te.featuretype == 'match':
      TEid, TEclass, TEorder, TEtype, TElast, confidence, annotationType = get_te_data(
        te, args.annotationType)

      dic = {"geneName": gene['Name'][0],
             "geneChr": gene['Name'][0][2:4],
             "TE_name": te.id,
             "LTE": te.end - te.start + 1,
             "TEStart": te.start,
             "TEStop": te.end,
             "TEid": TEid,
             "TEclass": TEclass,
             "TEorder": TEorder,
             "TEtype": TEtype,
             "TElast": TElast,
             "confidence": confidence,
             "annotationType": annotationType}
      densityDf = densityDf.append(dic, ignore_index=True)
    # Drop tmp csv files
    computedGenes += 1


if __name__ == "__main__":
  main()

# %%
