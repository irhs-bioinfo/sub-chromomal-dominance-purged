import os

import goatools
import pandas as pd
from goatools.goea.go_enrichment_ns import GOEnrichmentStudyNS


def generate_filename_and_select_go_dag(filename: str,
                                        ontologyDAG: goatools.obo_parser.GODag,
                                        ontologySlimDAG: goatools.obo_parser.GODag) -> tuple[str, str]:
  if "slim" in filename:
    filenameOutput = "enrichmentSlim"
    oboSchema = ontologySlimDAG
  else:
    filenameOutput = "enrichment"
    oboSchema = ontologyDAG

  return filenameOutput, oboSchema


def instanciate_enrichment_object(oboSchema: goatools.obo_parser.GODag,
                                  interproOutput: pd.DataFrame,
                                  alphaTreshold: float,
                                  associationGO: dict[str, str]):
  proteinCodingGenes = interproOutput["Protein_accession"]
  goeaobj = GOEnrichmentStudyNS(
    # List of protein-coding genes
    proteinCodingGenes,
    # geneid/GO associations
    associationGO,
    # Ontologies
    oboSchema,
    propagate_counts=False,
    # default significance cut-off
    alpha=alphaTreshold,
    # default multiple test correction method
    methods=['fdr_bh'])
  return goeaobj


def compute_enrichment_analysis(dfGenes: pd.DataFrame,
                                goeaobj: goatools.goea.go_enrichment_ns.GOEnrichmentStudyNS,
                                nameExp: str) -> tuple[pd.DataFrame, dict[str, str]]:
  """[summary]

  Parameters
  ----------
  dfGenes : pd.DataFrame
      [description]
  goeaobj : goatools.goea.go_enrichment_ns.GOEnrichmentStudyNS
      [description]
  nameExp : str
      Experience name

  Returns
  -------
  tuple[pd.DataFrame, dict[str, str]]
      [description]
  """
  enrichmentResults = pd.DataFrame()
  resultsDic = {}
  # for boolDuplicate in [True, False]:
  #   geneids_study = dfGenes["name"][dfGenes["duplicate"]
  #                                   == boolDuplicate].to_list()
  geneids_study = dfGenes["name"][dfGenes["duplicate"]
                                  == nameExp.split("-")[0]]
  if len(geneids_study) > 50:
    print(geneids_study)
    goea_results_all = goeaobj.run_study(geneids_study, prt=None)
    goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < 0.05]
    resultsDic[nameExp] = goea_results_all
    if len(goea_results_all):
      goeaobj.wr_xlsx(str(nameExp) + "_results.xlsx", goea_results_sig)
      if os.path.exists(str(nameExp) + "_results.xlsx"):
        tmp = pd.read_excel(str(nameExp) + "_results.xlsx")
        tmp["cluster"] = nameExp
        tmp["duplicate"] = boolDuplicate
        enrichmentResults = enrichmentResults.append(tmp, ignore_index=True)
        os.remove(str(nameExp) + "_results.xlsx")
  return enrichmentResults, resultsDic
