import argparse
import os

import dask.dataframe as dd
import goatools
import pandas as pd
from goatools.anno.idtogos_reader import IdToGosReader
from goatools.base import download_go_basic_obo
from goatools.goea.go_enrichment_ns import GOEnrichmentStudyNS
from goatools.obo_parser import GODag
from tqdm import tqdm

tqdm.pandas()


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-s",
      "--goSlim",
      help="Path to GO Slim annotation file",
      type=str,
      required=True)
  parser.add_argument(
      "-g",
      "--go",
      help="Path to GO annotation",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--outputGoSlim",
      help="Path to output enrichment on GO Slim",
      type=str,
      required=True)
  parser.add_argument(
      "-p",
      "--outputGo",
      help="Path to output enrichment on GO",
      type=str,
      required=True)
  parser.add_argument(
      "-r",
      "--reconstructedCouples",
      help="Path to reconstructedCouples",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--teMetadata",
      help="Path to Transposable Element metadata",
      type=str,
      required=True)

  args = parser.parse_args()
  return args

# %%


def dataframe_clean_strings(geneColumns: list[str], df: pd.DataFrame) -> pd.DataFrame:
  """This function aims to clean symbols to make annotations columns clean

  Parameters
  ----------
  geneColumns : String
        Column names to use
  df : pandas.Dataframe
      Dataframe storing ohnologous genes couples, their GO and GO Slim annotations

  Returns
  -------
  pandas.Dataframe
      Dataframe storing ohnologous genes couples, their GO and GO Slim annotations.
      GO annotations are now cleaned for symbols
  """
  for geneColumn in geneColumns:
    df[geneColumn] = df[geneColumn].str.replace(
        ",", ";")
    df[geneColumn] = df[geneColumn].str.replace(
        "]", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "[", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "nan", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "'", "")
  df = df.replace(to_replace='^; ',
                  value='',
                  regex=True)
  return df


def generate_filename_and_select_go_dag(filename: str,
                                        ontologyDAG: goatools.obo_parser.GODag,
                                        ontologySlimDAG: goatools.obo_parser.GODag) -> tuple[str, str]:
  if "slim" in filename:
    filenameOutput = "enrichmentSlim"
    oboSchema = ontologySlimDAG
  else:
    filenameOutput = "enrichment"
    oboSchema = ontologyDAG

  return filenameOutput, oboSchema


# %%
def instanciate_enrichment_object(oboSchema: goatools.obo_parser.GODag,
                                  interproOutput: pd.DataFrame,
                                  alphaTreshold: float,
                                  associationGO: dict[str, str]):
  proteinCodingGenes = interproOutput["Protein_accession"]
  goeaobj = GOEnrichmentStudyNS(
    # List of protein-coding genes
    proteinCodingGenes,
    # geneid/GO associations
    associationGO,
    # Ontologies
    oboSchema,
    propagate_counts=False,
    # default significance cut-off
    alpha=alphaTreshold,
    # default multiple test correction method
    methods=['fdr_bh'])
  return goeaobj


def compute_enrichment_analysis(dfGenes: pd.DataFrame,
                                goeaobj: goatools.goea.go_enrichment_ns.GOEnrichmentStudyNS,
                                nameExp: str) -> tuple[pd.DataFrame, dict[str, str]]:
  """[summary]

  Parameters
  ----------
  dfGenes : pd.DataFrame
      [description]
  goeaobj : goatools.goea.go_enrichment_ns.GOEnrichmentStudyNS
      [description]
  nameExp : str
      Experience name

  Returns
  -------
  tuple[pd.DataFrame, dict[str, str]]
      [description]
  """
  enrichmentResults = pd.DataFrame()
  resultsDic = {}
  for boolDuplicate in [True, False]:
    geneids_study = dfGenes["name"][dfGenes["duplicate"]
                                    == boolDuplicate].to_list()
    if len(geneids_study) > 150:
      goea_results_all = goeaobj.run_study(geneids_study, prt=None)
      goea_results_sig = [r for r in goea_results_all if r.p_fdr_bh < 0.05]
      resultsDic[nameExp] = goea_results_all
      if len(goea_results_all):
        goeaobj.wr_xlsx(str(nameExp) + "_results.xlsx", goea_results_sig)
        if os.path.exists(str(nameExp) + "_results.xlsx"):
          tmp = pd.read_excel(str(nameExp) + "_results.xlsx")
          tmp["cluster"] = nameExp
          tmp["duplicate"] = boolDuplicate
          enrichmentResults = enrichmentResults.append(tmp, ignore_index=True)
          os.remove(str(nameExp) + "_results.xlsx")
  return enrichmentResults, resultsDic


def main():
  args = get_arguments()

  reconstructedCouples = pd.read_csv(
      args.reconstructedCouples,
      index_col=0)

  reconstructedCouplesPurged = pd.DataFrame()
  for name, group in reconstructedCouples.groupby("gene_couple"):
    if group.shape[0] == 2:
      reconstructedCouplesPurged = reconstructedCouplesPurged.append(
        group.iloc[0], ignore_index=True)

  TEMetadata = pd.read_csv(
      args.teMetadata)

  TEMetadata = TEMetadata[TEMetadata["duplicate"] == False]
  reconstructedCouplesPurged = reconstructedCouplesPurged.append(TEMetadata,
                                                                 ignore_index=True)
  # Get obo file http://geneontology.org/ontology/go-basic.obo
  oboHandler = download_go_basic_obo()
  ontologyDAG = GODag("go-basic.obo")
  # Get obo file http://current.geneontology.org/ontology/subsets/goslim_plant.obo
  oboHandler = download_go_basic_obo(obo="goslim_plant.obo")
  ontologySlimDAG = GODag("goslim_plant.obo")

  for filename in [args.goSlim,
                   args.go]:
    filenameOutput, oboSchema = generate_filename_and_select_go_dag(filename,
                                                                    ontologyDAG,
                                                                    ontologySlimDAG)
    goAnnotations = pd.read_csv(args.go,
                                sep="\t")
    geneIdToGO = IdToGosReader(filename,
                               godag=oboSchema)
    associationGO = geneIdToGO.get_ns2assc()
    goeaobj = instanciate_enrichment_object(oboSchema,
                                            goAnnotations,
                                            0.05,
                                            associationGO)

    for nameCluster, groupCluster in tqdm(reconstructedCouplesPurged.groupby(by=("cluster"))):

      enrichmentResults, resultsDic = compute_enrichment_analysis(groupCluster,
                                                                  goeaobj,
                                                                  nameCluster)
      enrichmentResults.to_csv("results/GO/" +
                               filenameOutput +
                               "/" +
                               str(nameCluster) +
                               ".csv")

  df = dd.read_csv(
      "results/GO/enrichment/*.csv").compute()
  df.to_csv(args.outputGo,
            compression="infer")

  df = dd.read_csv(
      "results/GO/enrichmentSlim/*.csv").compute()
  df.to_csv(args.outputGoSlim,
            compression="infer")


if __name__ == "__main__":
  main()
