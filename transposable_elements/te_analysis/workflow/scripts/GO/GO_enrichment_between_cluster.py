import argparse

import dask.dataframe as dd
import pandas as pd
from goatools.anno.idtogos_reader import IdToGosReader
from goatools.base import download_go_basic_obo
from goatools.obo_parser import GODag
from lib.enrichment_functions import (compute_enrichment_analysis,
                                      generate_filename_and_select_go_dag,
                                      instanciate_enrichment_object)
from tqdm import tqdm

tqdm.pandas()


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-s",
      "--goSlim",
      help="Path to GO Slim annotation file",
      type=str,
      required=True)
  parser.add_argument(
      "-g",
      "--go",
      help="Path to GO annotation",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--outputGoSlim",
      help="Path to output enrichment on GO Slim",
      type=str,
      required=True)
  parser.add_argument(
      "-p",
      "--outputGo",
      help="Path to output enrichment on GO",
      type=str,
      required=True)
  parser.add_argument(
      "-r",
      "--reconstructedCouples",
      help="Path to reconstructedCouples",
      type=str,
      required=True)
  parser.add_argument(
      "-t",
      "--teMetadata",
      help="Path to Transposable Element metadata",
      type=str,
      required=True)

  args = parser.parse_args()
  return args


def dataframe_clean_strings(geneColumns: list[str], df: pd.DataFrame) -> pd.DataFrame:
  """This function aims to clean symbols to make annotations columns clean

  Parameters
  ----------
  geneColumns : String
        Column names to use
  df : pandas.Dataframe
      Dataframe storing ohnologous genes couples, their GO and GO Slim annotations

  Returns
  -------
  pandas.Dataframe
      Dataframe storing ohnologous genes couples, their GO and GO Slim annotations.
      GO annotations are now cleaned for symbols
  """
  for geneColumn in geneColumns:
    df[geneColumn] = df[geneColumn].str.replace(
        ",", ";")
    df[geneColumn] = df[geneColumn].str.replace(
        "]", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "[", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "nan", "")
    df[geneColumn] = df[geneColumn].str.replace(
        "'", "")
  df = df.replace(to_replace='^; ',
                  value='',
                  regex=True)
  return df


def main():
  args = get_arguments()

  reconstructedCouples = pd.read_csv(
      args.reconstructedCouples,
      index_col=0)

  reconstructedCouplesPurged = pd.DataFrame()
  for name, group in tqdm(reconstructedCouples.groupby("gene_couple"),
                          desc="Keeping one random gene from each couple"):
    if group.shape[0] == 2:
      reconstructedCouplesPurged = reconstructedCouplesPurged.append(
        group.iloc[0], ignore_index=True)

  TEMetadata = pd.read_csv(
      args.teMetadata)

  TEMetadata = TEMetadata[TEMetadata["duplicate"] == False]
  reconstructedCouplesPurged = reconstructedCouplesPurged.append(TEMetadata,
                                                                 ignore_index=True)
  # Get obo file http://geneontology.org/ontology/go-basic.obo
  oboHandler = download_go_basic_obo()
  ontologyDAG = GODag("go-basic.obo")
  # Get obo file http://current.geneontology.org/ontology/subsets/goslim_plant.obo
  oboHandler = download_go_basic_obo(obo="goslim_plant.obo")
  ontologySlimDAG = GODag("goslim_plant.obo")

  for filename in [args.goSlim,
                   args.go]:
    filenameOutput, oboSchema = generate_filename_and_select_go_dag(filename,
                                                                    ontologyDAG,
                                                                    ontologySlimDAG)
    goAnnotations = pd.read_csv(args.go,
                                sep="\t")

    for nameCluster, groupCluster in tqdm(reconstructedCouplesPurged.groupby(by=("cluster"))):
      for nameCluster1, groupCluster1 in tqdm(reconstructedCouplesPurged.groupby(by=("cluster"))):
        if nameCluster != nameCluster1:
          test = pd.DataFrame()
          test = test.append(groupCluster, ignore_index=True)
          test = test.append(groupCluster1, ignore_index=True)
          test1 = goAnnotations[goAnnotations["Protein_accession"].isin(
            test["name"])]
          test1.to_csv("tmp.tsv", sep="\t", index=False)
          geneIdToGO = IdToGosReader("tmp.tsv",
                                     godag=oboSchema)
          associationGO = geneIdToGO.get_ns2assc()
          goeaobj = instanciate_enrichment_object(oboSchema,
                                                  goAnnotations,
                                                  0.05,
                                                  associationGO)
          enrichmentResults, resultsDic = compute_enrichment_analysis(groupCluster,
                                                                      goeaobj,
                                                                      str(nameCluster) +
                                                                      "-" +
                                                                      str(nameCluster1))

          enrichmentResults.to_csv("results/GO/" +
                                   filenameOutput +
                                   "/" +
                                   "against_clusters" +
                                   str(nameCluster) +
                                   "-" +
                                   str(nameCluster1) +
                                   ".csv")

  df = dd.read_csv(
      "results/GO/enrichment/*.csv").compute()
  df.to_csv(args.outputGo,
            compression="infer")

  df = dd.read_csv(
      "results/GO/enrichmentSlim/*.csv").compute()
  df.to_csv(args.outputGoSlim,
            compression="infer")


if __name__ == "__main__":
  main()
