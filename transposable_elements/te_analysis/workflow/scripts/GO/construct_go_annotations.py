# %%
import argparse

import numpy as np
import pandas as pd


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i",
      "--input",
      help="Path to annotation file",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to GO annotation",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  interproColumns = ["Protein_accession",
                     "Sequence_MD5_digest",
                     "Sequence_length",
                     "Analysis",
                     "Signature_accession",
                     "Signature_description",
                     "Start_location",
                     "Stop_location",
                     "Score",
                     "Status",
                     "Date",
                     "InterPro_annotations",
                     "Pathways_annotations",
                     "GoTerms"]
  #################### Get functionnal annotation of each genes ########################################
  interproOutput = pd.read_csv(args.input,
                               sep="\t",
                               header=None,
                               names=interproColumns,
                               index_col=0,
                               compression="infer")
  interproOutput["GoTerms"] = interproOutput["GoTerms"].str.replace("|", ";")
  NaN = np.nan

  interproOutput["GoTerms"] = interproOutput["GoTerms"].replace(
    "-", NaN)

  interproOutput[["GoTerms"]].to_csv(args.output, sep="\t")


if __name__ == "__main__":
  main()

# %%
