import argparse
import os

import gffutils


def get_arguments():
  """This function retrieves arguments invoked with the script

  Returns
  -------
  ArgumentParser object
      Store all usable arguments
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-d",
      "--database",
      help="Path to genes SQL database",
      type=str,
      required=True)
  parser.add_argument(
      "-o",
      "--output",
      help="Path to new SQL database",
      type=str,
      required=True)
  args = parser.parse_args()
  return args


def main():
  args = get_arguments()
  # Important to read before breaking this part!
  # https://github.com/daler/gffutils/issues/111
  # A big fail
  db = gffutils.FeatureDB(args.database, keep_order=True)
  introns = list(db.create_introns())
  db.update(
      introns,
      disable_infer_transcripts=True,
      disable_infer_genes=True,
      verbose=True,
      merge_strategy="create_unique"
  )
  os.rename(args.database, args.output)


if __name__ == "__main__":
  main()
