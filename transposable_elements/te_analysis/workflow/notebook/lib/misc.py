from typing import Dict, List, Optional, Set, Tuple
import errno
import os


def create_dir(directories: List, verbose: bool = False):
  """
  Create directories from a list of names passed as an argument

  Parameters
  ----------
  directories : array
      List of directories names
  verbose : int, optional
      Ask function to be verbose, by default False
  """
  for directory in directories:
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise
    if verbose:
      print('Directory ' + directory + 'created')
