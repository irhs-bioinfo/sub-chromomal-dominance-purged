import plotly.express as px


def density_heatmap_fixed(df, columnValue: str, yValue: str):
  """
  Creates a density heatmap with fixed x and y values.

  Parameters
  ----------
  df : pandas.DataFrame
      Dataframe with the data.
  columnValue : str
      Column name of the x-axis.
  yValue : str
      Column name of the y-axis.

  Returns
  -------
  plotly.express.figures.Figure
      Figure with the density heatmap.
  """
  fig = px.density_heatmap(df,
                           x="couple",
                           y=yValue,
                           z=columnValue,
                           histfunc="avg",
                           range_color=[0, 1],
                           color_continuous_scale=[(0.00, "rgba(12,9,135,255)"),
                                                   (0.05, "rgba(12,9,135,255)"),
                                                   (0.051, "rgba(240,249,33,255)"),
                                                   (1, "rgba(240,249,33,255)")])
  fig.update_layout(coloraxis_colorbar=dict(
                    title=columnValue,
                    tickvals=[0.04, 0.5],
                    ticktext=["Significative", "Not Significative"],
                    ))
  fig.update_layout(xaxis_type='category',
                    width=1080)
  return fig
