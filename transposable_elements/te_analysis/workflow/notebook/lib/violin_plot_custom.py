import errno
import os
from typing import Dict, List, Optional, Set, Tuple

import pandas as pd
import plotly
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def color(text: str, sizeOfGroup: float) -> str:
  """This function returns colored axis following splitting. Add also size of group

  Parameters
  ----------
  text : str
      couple name separated by a "-". Like: 01-07

  Returns
  -------
  str
      html formated text with added colors
  """
  splited = text.split("-")
  return "<span style='color: {}'> {} </span>".format("blue", splited[0]) + \
         "<span style='color: {}'> {} </span>".format("black", "-") + \
         "<span style='color: {}'> {} </span>".format("orange", splited[1]) + \
         "<span style='color: {}'> (size: {}) </span>".format("black",
                                                              int(sizeOfGroup))


def format_tick_text(fig, df: pd.DataFrame, columnName: str) -> plotly.graph_objects.Figure:
  """This function is used to format the tick text of the x axis.

  fig : plotly.graph_objects.Figure
      [description]
  df : pd.DataFrame
      [description]
  columnName : str
      [description]

  Returns
  -------
  [type]
      [description]
  """
  ticktext = []
  for name, group in df.groupby("couple"):
    ticktext.append(color(name, group.shape[0] / 2))

  fig.update_traces(meanline_visible=True,
                    scalemode='count')
  fig.update_layout(violingap=0,
                    xaxis=dict(tickmode='array',
                               ticktext=ticktext,
                               tickvals=df["couple"].unique()),
                    xaxis_type='category',
                    title="Violin plot of distributions of " + columnName)
  return fig


def generate_array_show_legend(df: pd.DataFrame) -> List[bool]:
  """This function generate an array of boolean to show the legend of the plot without duplicated entries.

  Parameters
  ----------
  df : pd.DataFrame
      [description]

  Returns
  -------
  List[bool]
      Array of boolean to show the legend of the plot without duplicated entries.

  """
  showLegend = [True]

  showLegend += [False] * \
      (len(df["couple"].unique()) - 1)
  return showLegend


def format_tick_text_subplots(fig, df: pd.DataFrame, row: int):
  """[summary]

  Parameters
  ----------
  fig : [type]
      [description]
  df : pd.DataFrame
      [description]
  row : int
      [description]


  Returns
  -------
  [type]
      [description]
  """
  ticktext = []
  for name, group in df.groupby("couple"):
    ticktext.append(color(name, group.shape[0] / 2))
  fig.update_xaxes(dict(tickmode='array',
                        ticktext=ticktext,
                        tickvals=df["couple"].unique()),
                   row=row,
                   col=1)
  return fig


def generate_subplot_violin(df, columnName) -> plotly.graph_objects.Figure:
  fig = make_subplots(rows=5,
                      cols=1,
                      subplot_titles=(
                          "Violin plot of distributions of " + columnName + " for cluster " + "TE poor",
                          "Violin plot of distributions of " + columnName + " for cluster " + "TE rich",
                          "Violin plot of distributions of " +
                          columnName + " for cluster " + "TE very-poor",
                          "Violin plot of distributions of " + columnName + " for cluster " + "TE very-rich"),
                      shared_xaxes=True,
                      vertical_spacing=0.05)
  for name, group in df.groupby("cluster"):
    if name != "TE Free":
      fig = generate_violin_plot(group, columnName, name, fig)
  fig.update_traces(meanline_visible=True,
                    scalemode='count')
  fig.update_xaxes(type='category')
  fig.update_layout(violingap=0,
                    width=1200,
                    height=1200,
                    showlegend=False)

  for i in range(4):
    xaxis_name = 'xaxis' if i == 0 else f'xaxis{i + 1}'
    getattr(fig.layout, xaxis_name).showticklabels = True
  return fig


def save_violin_plot(fig, columnName: str) -> None:
  """[summary]

  Parameters
  ----------
  fig : [type]
      [description]
  columnName : str
      [description]
  """
  create_dir(
    ["../../results/plots/descriptive_viz/cluster_violin/" + columnName + "/"])
  for formated in ["png", "jpg", "pdf", "svg", "webp"]:
    fig.write_image(
      "../../results/plots/descriptive_viz/cluster_violin/" + columnName + "/" + columnName + "." + formated)

  fig.write_html("../../results/plots/descriptive_viz/cluster_violin/" + columnName + "/" +
                 columnName + ".html")


def generate_violin_plot(df, columnName, clusterName, fig):
  showLegend = generate_array_show_legend(df)
  filterdDf = pd.DataFrame()

  i = 0
  for name, group in df.groupby("couple"):
    if group.shape[0] > df.shape[0] / 20:
      filterdDf = filterdDf.append(group, ignore_index=True)
      fig.append_trace(go.Violin(x=group["couple"],
                                 y=group[columnName][group["geneChr"]
                                                     == int(name.split("-")[0])],
                                 legendgroup='On first chromosome of pair',
                                 scalegroup='On first chromosome of pair',
                                 name='On first chromosome of pair',
                                 side='negative',
                                 line_color='blue',
                                 showlegend=showLegend[i]
                                 ),
                       row=get_number_of_row_of_current_cluster(clusterName),
                       col=1
                       )
      fig.append_trace(go.Violin(x=group["couple"],
                                 y=group[columnName][group["geneChr"]
                                                     == int(name.split("-")[1])],
                                 legendgroup='On second chromosome of pair',
                                 scalegroup='On second chromosome of pair',
                                 name='On second chromosome of pair',
                                 side='positive',
                                 line_color='orange',
                                 showlegend=showLegend[i]
                                 ),
                       row=get_number_of_row_of_current_cluster(clusterName),
                       col=1
                       )

      fig = format_tick_text_subplots(fig,
                                      filterdDf,
                                      get_number_of_row_of_current_cluster(clusterName))
      i += 1
  return fig


def get_number_of_row_of_current_cluster(clusterName: str) -> int:
  """
  Returns the number of the row of the current cluster

  Parameters
  ----------
  clusterName : str
      Name of the cluster
  Returns
  -------
  int
      Number of the row of the current cluster

  """
  if clusterName == "TE poor":
    return 1
  elif clusterName == "TE rich":
    return 2
  elif clusterName == "TE very-poor":
    return 3
  elif clusterName == "TE very-rich":
    return 4
  else:
    return 0


def create_dir(directories: List, verbose: bool = False):
  """
  Create directories from a list of names passed as an argument

  Parameters
  ----------
  directories : array
      List of directories names
  verbose : int, optional
      Ask function to be verbose, by default False
  """
  for directory in directories:
    try:
      os.makedirs(directory)
    except OSError as e:
      if e.errno != errno.EEXIST:
        raise
    if verbose:
      print('Directory ' + directory + 'created')


def add_pvalue_annotation(fig, couple, cat_order, y_range, symbol='', pvalue_th=0.05, aggregated_pvalue=0.05):
    """This function add annotation for plotly boxplot

    Parameters
    ----------
    couple : str
        couple name
    y_range : list
        list of y_range
    symbol : str
        symbol to add
    pvalue_th : float
        pvalue threshold
    aggregated_pvalue : float
        aggregated pvalue threshold
    """
    if aggregated_pvalue < pvalue_th:
        ## for bars, there is a direct mapping from the bar number to 0, 1, 2...
        bar_xcoord_map = {x: idx for idx, x in enumerate(cat_order)}
        x_coordinate = bar_xcoord_map[couple]
        x_start, x_end = x_coordinate - 0.2, x_coordinate + 0.2
        symbol = '*'

        ## add text at the correct x, y coordinates
        fig.add_annotation(dict(font=dict(color="black", size=14),
                                x=x_coordinate,
                                y=y_range[1] * 1.08,
                                showarrow=False,
                                text=symbol,
                                textangle=0,
                                xref="x",
                                yref="paper",
                                hovertext="p-value: {}".format(
            aggregated_pvalue),
        ))
