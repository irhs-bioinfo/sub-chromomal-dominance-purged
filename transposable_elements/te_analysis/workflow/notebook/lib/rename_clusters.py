from typing import Dict, List, Optional, Set, Tuple
import pandas as pd


def replace_cluster_value(value: int) -> str:
  """
  This function aims to replace cluster value with a string
  """
  if value == 0:
    return "TE very-rich"
  elif value == 1:
    return "TE rich"
  elif value == 2:
    return "TE very-poor"
  elif value == 3:
    return "TE poor"
  elif value == 7:
    return "TE Free"


def replace_value_cluster(value: str) -> int:
  """
  This function aims to replace cluster value with a string
  """
  if value == "TE very-rich":
    return 0
  elif value == "TE rich":
    return 1
  elif value == "TE very-poor":
    return 2
  elif value == "TE poor":
    return 3
  elif value == "TE Free":
    return 7


def reorder_genes(row: pd.DataFrame) -> pd.DataFrame:
  """This function aims to reorder genes in order to have in gene x
  a gene with a chromosome number inferior to gene y one

  Parameters
  ----------
  row : pd.DataFrame
      Ohnologous genes dataframe row

  Returns
  -------
  pd.DataFrame
      Ohnologous genes dataframe row with reodered genes
  """
  if int(row["gene_x"][2:4]) > int(row["gene_y"][2:4]):
    row["gene_x"], row["gene_y"] = row["gene_y"], row["gene_x"]
  return row
