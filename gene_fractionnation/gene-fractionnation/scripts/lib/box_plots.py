import plotly.graph_objects as go


def color(text: str, sizeOfGroup: str) -> str:
  """This function returns colored axis following splitting. Add also size of group

  Parameters
  ----------
  text : str
      couple name separated by a "-". Like: 01-07

  Returns
  -------
  str
      html formated text with added colors
  """
  splited = text.split("-")
  return "<span style='color: {}'>{}</span>".format("#3D9970", splited[0]) + \
         "<span style='color: {}'> {} </span>".format("black", "-") + \
         "<span style='color: {}'>{}</span>".format("#D95F02", splited[1]) + \
         "<br>" + \
         "<span style='color: {}'> (size: {} )</span>".format("black",
                                                              str(sizeOfGroup))


def add_pvalue_annotation(fig, couple, cat_order, y_range, symbol='*', pvalue_th=0.05, aggregated_pvalue=0.05):
  """This function add annotation for plotly boxplot

  Parameters
  ----------
  couple : str
      couple name
  y_range : list
      list of y_range
  symbol : str
      symbol to add
  pvalue_th : float
      pvalue threshold
  aggregated_pvalue : float
      aggregated pvalue threshold
  """
  if aggregated_pvalue < pvalue_th:
    # for bars, there is a direct mapping from the bar number to 0, 1, 2...
    bar_xcoord_map = {x: idx for idx, x in enumerate(cat_order)}
    x_coordinate = bar_xcoord_map[couple]
    x_start, x_end = x_coordinate - 0.2, x_coordinate + 0.2
    fig.add_shape(type="line",
                  xref="x",
                  yref="paper",
                  x0=x_start,
                  y0=y_range[0],
                  x1=x_start,
                  y1=y_range[1],
                  line=dict(
                      color="black",
                      width=2,
                  )
                  )
    fig.add_shape(
        type="line",
        xref="x",
        yref="paper",
        x0=x_start,
        y0=y_range[1],
        x1=x_end,
        y1=y_range[1],
        line=dict(
            color="black",
            width=2,
        )
    )
    fig.add_shape(
        type="line",
        xref="x",
        yref="paper",
        x0=x_end,
        y0=y_range[1],
        x1=x_end,
        y1=y_range[0],
        line=dict(
            color="black",
            width=2,
        )
    )
    # add text at the correct x, y coordinates
    fig.add_annotation(dict(font=dict(color="black", size=14),
                            x=x_coordinate,
                            y=y_range[1] * 1.08,
                            showarrow=False,
                            text=symbol,
                            textangle=0,
                            xref="x",
                            yref="paper",
                            hovertext="p-value: {}".format(aggregated_pvalue),
                            ))
