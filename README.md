# sub-chromomal-dominance

## Description
>Data and workflows relative to the article "Insights into the evolution of ohnologous sequences and their epigenetic marks post WGD in *Malus domestica*". 

Meta-analyse of multi-omics data to explore the genome evolution of apple post-whole genome duplication.

## Folder structure
Each workflow analysis is in a specific folder

- identification of syntenic blocks : 

`sub-chromomal-dominance-purged/syntenic_blocks_identification`
- gene fractionation : 

`sub-chromomal-dominance-purged/gene_fractionnation`
- quantitative trait loci : 

`sub-chromomal-dominance-purged/phenotypic_particpation`
- estimation of evolutionary patterns using Ka/Ks : `sub-chromomal-dominance-purged/selective_pressure/ka-ks-comparison`

- expression analysis (RNAseq) : 

`sub-chromomal-dominance-purged/expression/differential-expression-analysis`

- transposable elements analysis : 

`sub-chromomal-dominance-purged/transposable_elements/te_analysis`

- methylome (BS-DNAseq) : 

`sub-chromomal-dominance-purged/dna_methylation/methylome-imbalance`

- integrative analysis : 

`sub-chromomal-dominance-purged/integration`


- other utilities :

    - automatic extraction of SRA RNA-seq : 
    
    `sub-chromomal-dominance-purged/downloads/get-bisulfite-seq`


    - automatic extraction of SRA bisulfite-seq : 
    
    `sub-chromomal-dominance-purged/downloads/get-bisulfite-seq`



## Authors and acknowledgment
Tanguy Lallemand (and Martin Leduc for the TE analysis) - PhD @ IRHS - UMR 1345 UA-INRAE-IA-RA