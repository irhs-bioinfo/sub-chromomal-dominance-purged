rule construct_blast_database:
  input:
    expand("resources/genomes/sequences/protein_sequences/{genomeToBlast}", genomeToBlast=config["genomes"]["proteic"]),
  output:
    expand("results/blast/db/{databaseTitle}.{ext}", databaseTitle=config["blast"]["title"], ext=["pdb", "phr", "pin", "pot", "psq", "ptf", "pto"]),
  params:
    databaseTitle = config["blast"]["title"],
    dbPath = config['blast']['pathDb']+config["blast"]["title"]
  threads: 20
  message:
    "Builds a blast db with {input} genome. Construct {output} files"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    "makeblastdb -in {input} -out {params.dbPath} -title {params.databaseTitle} -dbtype prot"


rule blast_all_agains_all:
  input:
    expand("results/blast/db/{databaseTitle}.{ext}", databaseTitle=config["blast"]["title"], ext=["pdb", "phr", "pin", "pot", "psq", "ptf", "pto"]),
    genomePath=expand("resources/genomes/sequences/protein_sequences/{genomeToBlast}", genomeToBlast=config["genomes"]["proteic"]),
  output:
    expand("results/blast/malus_malus_{maxTargetSeqs}.blast", maxTargetSeqs=config['blast']['maxTargetSeqs']),
  params:
    dbPath = "results/blast/db/GDDH13",
    eValue = config['blast']['eValue'],
    maxTargetSeqs = config['blast']['maxTargetSeqs'],
  message:
    "Make a blast all against all between {input.genomePath} genome using {input} database"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    "blastp -query {input.genomePath} -db {params.dbPath} -num_threads 20 -evalue {params.eValue} -max_target_seqs {params.maxTargetSeqs} -outfmt 6 -out {output}"


rule blast_to_iadhore_input_file:
  input:
    "results/blast/malus_malus_{maxTargetSeqs}.blast"
  output:
    "results/iadhore/blast/{maxTargetSeqs}_blast_input.blast",
  message:
    "Parse blast result purging blast against himself"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    "awk -F'\t' '$1!=$2' {input} | cut --fields=1,2 > {output}"


rule process_gff:
  input:
    annotations=config['genomes']['annotations'],
  output:
    directory(config['iadhore']['iadhoreBaseDir']+"lstFiles"),
  params:
    purgeScaffold=config["iadhore"]["purgeScaffold"]
  message:
    "Process all gff of {input.annotations} directory to construct lst files, needed for iadhore input"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    "workflow/scripts/process_gff.py {input.annotations} {output} {params.purgeScaffold}"
