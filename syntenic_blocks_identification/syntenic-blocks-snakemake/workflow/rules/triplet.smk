rule construct_blast_prunus_database:
  input:
    "resources/genomes/sequences/protein_sequences/Prunus_persica_v2.0.a1.primaryTrs.pep.fa"
  output:
    expand("results/temp/blastDb/prunus.{ext}", ext=["pdb", "phr", "pin", "pot", "psq", "ptf", "pto"]),
  params:
    databaseTitle = "prunus",
    dbPath = "results/temp/blastDb/prunus"
  threads: 20
  message:
    "Builds a blast db with {input} genome. Construct {output} files"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    "makeblastdb -in {input} -out {params.dbPath} -title {params.databaseTitle} -dbtype prot"


rule construct_triplet:
  input:
    rules.construct_blast_prunus_database.output,
    ohnologousGenes= "results/tables/ohnologousGenes.csv"
  output:
    "results/tables/triplets.csv"
  params:
    configFile = "config/construct_triplet.yaml"
  conda:
    "../envs/Reciprocal_Best_Blast_Hit.yaml"
  shell:
    """
    python workflow/scripts/construct_triplet.py \
    --config {params.configFile} \
    --ohnologusGenes {input.ohnologousGenes}
    """