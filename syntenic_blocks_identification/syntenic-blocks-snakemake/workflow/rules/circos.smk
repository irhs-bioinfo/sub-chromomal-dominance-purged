rule generate_gene_data:
  input:
    "resources/genomes/genes/{gffFile}.gff3"
  output:
    "results/circos/gene_data_{gffFile}.csv"
  params:
    specieName=str("{gffFile}"),
  shell:
    # Cut on ; to get first part of file line:
    # Pp01	JGI	gene	179494	183844	.	+	.	Prupe.1G001100_v2.0.a1;Name=Prupe.1G001100;ancestorIdentifier=ppa004964m.g.v1.0
    # Keep only line tagged as gene
    # Cut on tab and keep only needed column Gene ID, feature (here always gene), chromosome number, specie, strand, start, stop
    # Replace space by commas
    # remove  '_v2.0.a1' string from gene name to handle with prunus gene name that are suffixed by '_v2.0.a1'
    # Patch on awk which does not handle with name spcie to reintegrate it with a last sed
    # Output it in a file a had a header to file using a sed -i
    """
    sed -e 's/ID=//g' {input} | grep -e 'IRHS-2017[[:blank:]]gene' -e 'JGI[[:blank:]]gene' | \
    cut -d';' -f 1 | sed -e 's/gene://g' | \
    awk -F'\t' '{{print $9, $3, $1, {params.specieName}, $7, $4, $5}}' | \
    sed 's/\s/,/g' | sed 's/_v2.0.a1//g' | sed 's/,,/,{params.specieName},/g' > {output} && \
    sed -i '1i \gene_id,feat,chrom,sp,strand,start,stop' {output}
     """


rule aggregate_gene_data:
  input:
    expand("results/circos/gene_data_{file}.csv", file=["malus_domestica", "prunus_persica"]),
  output:
    "results/circos/gene_data.csv"
  shell:
    """
    tail -n+2 {input} >> {output} && \
    sed -in-place '/==>/d' {output} && \
    if grep -Fxq "gene_id,feat,chrom,sp,strand,start,stop" {output}
    then
        echo ''
    else
        sed -i '1i \gene_id,feat,chrom,sp,strand,start,stop' {output}
    fi
    """


rule construct_circos:
  input:
    "results/circos/gene_data.csv",
    "results/iadhore/result_computation/multiplicon_pairs.txt"
  output:
    expand("results/plots/circos.{ext}", ext=["png", "svg"]),
    ohnologousGenes= report('results/tables/ohnologousGenes.csv',category="ohnologousGenes",caption='../report/construct_circos.rst')
  params:
    baseDirCircos = "results/circos/",
    baseDirIadhore = config['iadhore']['iadhoreBaseDir']+"result_computation/",
  message:
    "Parsing i-adhore result and constructing associated circos file"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    """
    python workflow/scripts/iadhore.py \
    --config ./config/parse_iadhore.yaml \
    --input {params.baseDirIadhore} \
    --output {params.baseDirCircos} \
    --ohnologusGenes {output.ohnologousGenes} && \
    mv {params.baseDirCircos}/circos.png results/plots/circos.png && \
    mv {params.baseDirCircos}/circos.svg results/plots/circos.svg
    """