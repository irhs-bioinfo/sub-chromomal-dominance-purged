rule run_iadhore:
  input:
    "results/iadhore/blast/50_blast_input.blast",
    "results/iadhore/lstFiles/malus_domestica/Chr00.lst",
  output:
    multipliconPairs = "results/iadhore/result_computation/multiplicon_pairs.txt",
    AlignmentMultiplicon = directory("results/iadhore/result_computation/alignment_multiplicon/"),
    dotMatrices = directory("results/iadhore/result_computation/dot_matrices/")
  params:
    configFile = config['iadhore']['iniFile']
  log:
    "logs/iadhore_computation.log"
  message:
    "Construct syntenic blocks using i-adhore with {params.configFile} as configuration file"
  conda:
    "../envs/pipeline_env.yaml"
  shell:
    """i-adhore {params.configFile} > {log} && \
    sed -i 's/\t\t/\t/g' {output.multipliconPairs} && \
    mkdir -p {output.AlignmentMultiplicon} && \
    mv results/iadhore/result_computation/*.svg {output.AlignmentMultiplicon} &&
    mkdir -p {output.dotMatrices} && \
    mv results/iadhore/result_computation/*.png {output.dotMatrices}
    """
