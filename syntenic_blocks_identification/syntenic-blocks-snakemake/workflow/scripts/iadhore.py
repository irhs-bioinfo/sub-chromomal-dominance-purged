# %%
import os

import pandas as pd
from lib.circos_wrapper import (write_circos_conf,
                                write_circos_conf_imbalanced,
                                write_karyotype_circos, write_ribbons)
from lib.config_parser import (config_parser, get_arguments,
                               override_config_with_args)
from lib.misc import change_directory, output_dataframe
from lib.parsing_functions import (get_ohnologous_genes_on_whole_genome,
                                   purge_patterns, segments_filter)

# %%
###################### Set up confinguration and dataframes ######################

# Get argument parser
args = get_arguments()
# Load configuration
conf = config_parser(args.config)
conf = override_config_with_args(conf, args)
# Determine if intra specific relationships need to be kept
if len(conf['NeededSpecie']) == 2:
  interSpecificRelationships = True
else:
  interSpecificRelationships = False


os.makedirs(os.path.dirname(conf['configCircos']['circosDir']), exist_ok=True)

# Open i-adhore output file
# Segments.txt contains all syntenic blocks identifed by i-adhore
segments = pd.read_csv(conf['iAdhoreOutput']
                       ['segments'], sep="\t", index_col=0)
# A gff like file with following informations:
# Gene_ID, feat, chrom, sp, strand, start, stop
geneData = pd.read_csv(conf['genome']['geneData'], index_col=0)

multipliconPairs = pd.read_csv(
    conf['iAdhoreOutput']['multipliconPairs'], sep="\t", index_col=0)
multipliconsDf = pd.read_csv(
    conf['iAdhoreOutput']['multiplicons'], sep="\t", index_col=0)
genesDf = pd.read_csv(
    conf['iAdhoreOutput']['genes'], sep="\t", index_col=0)
# Initialize a dataframe to handle results
ribbons = pd.DataFrame(
  columns=['chr1', 'start1', 'end1', 'chr2', 'start2', 'end2'])
ribbonsImbalanced = pd.DataFrame(
  columns=['chr1', 'start1', 'end1', 'chr2', 'start2', 'end2'])
# %% [markdown]
"""
  # Load input files
  # I-adhore output
  Load segments.txt storing syntenic genes, Here is a header of file:

  | id | multiplicon | genome | list  | first          | last           | order |
  |----|-------------|--------|-------|----------------|----------------|-------|
  | 1  | 1           | malus  | Chr03 | MD03G1000100   | MD03G1297400   | 0     |
  | 2  | 1           | malus  | Chr11 | MD11G1000100   | MD11G1317200   | 1     |
  | 3  | 2           | malus  | Chr03 | MD03G1204000   | MD03G1230700   | 0     |
  | 4  | 2           | malus  | Chr11 | MD11G1221100   | MD11G1251900   | 1     |
  | 5  | 2           | prunus | Pp04  | Prupe.4G177100 | Prupe.4G210900 | 2     |

  # Gene data
  A csv file coming from gff file. Here is header of file:

  | gene_id      | feat | chrom | sp              | strand | start | stop  |
  |--------------|------|-------|-----------------|--------|-------|-------|
  | MD15G1000600 | gene | Chr15 | Malus_domestica | +      | 39515 | 42267 |
  | MD15G1000700 | gene | Chr15 | Malus_domestica | +      | 52282 | 53659 |
  | MD15G1000800 | gene | Chr15 | Malus_domestica | +      | 60293 | 61611 |
  | MD15G1000900 | gene | Chr15 | Malus_domestica | +      | 68428 | 72055 |
  | MD15G1001100 | gene | Chr15 | Malus_domestica | -      | 81995 | 84759 |
  | MD15G1001300 | gene | Chr15 | Malus_domestica | -      | 88460 | 90315 |
  | MD15G1001500 | gene | Chr15 | Malus_domestica | -      | 93414 | 95139 |
"""
# %%
###### Generation of Circos conf and files to visualize I-ADHoRe results ######

############################### Clean data #####################################
# Filter syntenic blocks df by order of the multiplicons
segments = segments_filter(segments=segments, order=2)
# Remove all segments involving chr00 in different species
for pattern in ["MD00", "Pp0", "RC0"]:
  segments = purge_patterns(segments, pattern, ['first', 'last'], False)

# If need only data from one specie, purge others
if not interSpecificRelationships:
  genome = conf['NeededSpecie'][0].split("_")
  genome = genome[0].lower()
  # MONKEY PAATCH
  genome = "md"

  segments = purge_patterns(segments, genome, ['genome'], True)

malusOhnologousGenes = get_ohnologous_genes_on_whole_genome(
  df=multipliconPairs, specieId="MD", order=2)

isTandem = genesDf.loc[genesDf["is_tandem"] == -1]
tandemDuplicatedBool = pd.DataFrame(columns=["gene_x", "gene_y"])
tandemDuplicatedBool["gene_x"] = malusOhnologousGenes["gene_x"].isin(
  isTandem.index)
tandemDuplicatedBool["gene_y"] = malusOhnologousGenes["gene_y"].isin(
  isTandem.index)
tandemDuplicatedBoolDf = tandemDuplicatedBool[tandemDuplicatedBool.gene_x == True]
tandemDuplicatedBoolDf = tandemDuplicatedBoolDf[tandemDuplicatedBoolDf.gene_y == True]
output_dataframe(path=conf['dataFrames']['ohnologousGenes'],
                 dataframe=malusOhnologousGenes,
                 separator=' ',
                 verbose=True)
# %% [markdown]
"""
  # Construct input files for Circos
  # Ribbons.txt
  A file storing relationships

  | Chr1  | Begin    | End      | Chr2 | Begin    | End      |
  |-------|----------|----------|------|----------|----------|
  | Chr03 | 27858025 | 31592642 | Pp04 | 10505281 | 13159211 |
  | Chr11 | 32402552 | 36429642 | Pp04 | 10505281 | 13159211 |
  | Chr03 | 25624914 | 27470322 | Pp04 | 13266899 | 15534119 |
  | Chr11 | 29306989 | 31877533 | Pp04 | 13266899 | 15534119 |

  # karyotype.txt

  A file storing chromosome caracteristics

  | chr | dash | chrom | label | start | stop     | color |
  |-----|------|-------|-------|-------|----------|-------|
  | chr | -    | Chr01 | Chr01 | 0     | 32620872 | green |
  | chr | -    | Chr02 | Chr02 | 0     | 37543143 | green |
  | chr | -    | Chr03 | Chr03 | 0     | 37520020 | green |
"""
# %%
########################## Construct input for Circos ##########################
# Construct ribbons dataframe

ribbons = write_ribbons(
    ribbons,
    segments,
    geneData,
    interSpecificRelationships,
    conf['dropSmallSegments'])

ohnologousCouplesArr = []
for couple in conf["ohnologousCouples"]:
  ohnologousCouplesArr.append(couple)
  splittedCouple = couple.split("-")
  ohnologousCouplesArr.append(splittedCouple[1] + "-" + splittedCouple[0])

imbalancedGenesArr = []
for couple in conf["imbalancedGenes"]:
  imbalancedGenesArr.append(couple)
  splittedCouple = couple.split("-")
  imbalancedGenesArr.append(splittedCouple[1] + "-" + splittedCouple[0])

ribbons["couple"] = ribbons["chr1"] + "-" + ribbons["chr2"]
ribbons = ribbons[ribbons["couple"].isin(ohnologousCouplesArr)]
ribbonsImbalanced = ribbons[ribbons["couple"].isin(imbalancedGenesArr)]
ribbons = ribbons[~ribbons["couple"].isin(imbalancedGenesArr)]
ribbonsImbalanced = ribbonsImbalanced.drop(columns=["couple"])
ribbons = ribbons.drop(columns=["couple"])

# Reload gene datas
geneData = pd.read_csv(conf['genome']['geneData'])
# Clean data, by purging Chr00 and possible scaffolds
for pattern in conf['parsing']['pattern_to_purge']:
  geneData = purge_patterns(geneData, pattern, ['gene_id'], 0)
# If need only data from one specie, purge others
if len(conf['NeededSpecie']) == 1:
  geneData = purge_patterns(geneData, conf['NeededSpecie'][0], ['sp'], 1)
# Construct karyotype dataframe
karyotypeTowrite = write_karyotype_circos(geneData)

# %% [markdown]
"""
  # Output files for Circos
  # Ribbons.txt
  A file storing relationships

  | Chr1  | Begin    | End      | Chr2 | Begin    | End      |
  |-------|----------|----------|------|----------|----------|
  | Chr03 | 27858025 | 31592642 | Pp04 | 10505281 | 13159211 |
  | Chr11 | 32402552 | 36429642 | Pp04 | 10505281 | 13159211 |
  | Chr03 | 25624914 | 27470322 | Pp04 | 13266899 | 15534119 |
  | Chr11 | 29306989 | 31877533 | Pp04 | 13266899 | 15534119 |

  # karyotype.txt

  A file storing chromosome caracteristics

  | chr | dash | chrom | label | start | stop     | color |
  |-----|------|-------|-------|-------|----------|-------|
  | chr | -    | Chr01 | Chr01 | 0     | 32620872 | green |
  | chr | -    | Chr02 | Chr02 | 0     | 37543143 | green |
  | chr | -    | Chr03 | Chr03 | 0     | 37520020 | green |

  # circos.conf

  A file storing path to data file, colors and aesthetic different configuration
  Here is a sample


  <<include etc/colors_fonts_patterns.conf>>
  <image>
  <<include etc/image.conf>>
  </image>
  chromosomes_units       = 1000000
  chromosomes_display_default = yes
  karyotype = /data/karyotype.txt
  <links>
    <link>
      ribbon = yes
      file = /data/ribbons.txt
      bezier_radius = 0.1r
      thickness = 1
      <rules>
        <rule>
            condition  = between(Chr01,Pp01)
            color      = 141,211,199,0.65
            thickness  = 4
            z          = 15
        </rule>
      </rules>
        radius = 0.95r
    </link>
  </links>
  <ideogram>
    <spacing>
    default = 0.005r
    </spacing>
    radius    = 0.98r
    thickness = 20p
    fill      = yes
    show_label       = yes
    label_font       = default
    label_radius     = dims(ideogram,radius_outer) + 0.02r
    label_with_tag   = yes
    label_size       = 36
    label_parallel   = yes
    label_color = black
  </ideogram>
  <<include etc/housekeeping.conf>>
  """
# %%
########################## Output file for Circos ##########################
karyotypeTowrite.to_csv(conf['configCircos']['circosDir']
                        + "/karyotype.txt", sep=" ", index=None, header=None)
ribbons.to_csv(conf['configCircos']['circosDir']
               + "/ribbons.txt", sep=" ", index=None, header=None)
ribbonsImbalanced.to_csv(conf['configCircos']['circosDir']
                         + "/ribbons_imbalanced.txt", sep=" ", index=None, header=None)
# Construct config file
write_circos_conf(
    ribbons,
    conf['configCircos']['karyotypePath'],
    ribbonsPath=conf['configCircos']['ribbonsPath'],
    fname=conf['configCircos']['circosDir'] + "/circos.conf")

# write_circos_conf_imbalanced(
#     conf['configCircos']['karyotypePath'],
#     ribbonsPath=conf['configCircos']['ribbonsPath'],
#     ribbonsImbalancedPath=conf['configCircos']['circosDir']
#     + "/ribbons_imbalanced.txt",
#     fname=conf['configCircos']['circosDir'] + "/circos.conf")

# %% [markdown]
"""Construct circos using a docker container
"""
# %%
#################### Launch circos into a docker container ####################
stdout = os.system(
    'circos -conf ' + conf["configCircos"]["circosDir"] + 'circos.conf' ' -outputdir ' + conf["configCircos"]["circosDir"])
if stdout == 0:
  print('Circos visualisation constructed')
####################


# %%
