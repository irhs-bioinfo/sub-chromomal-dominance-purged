import yaml
import argparse


def config_parser(configPath):
  """
  A function to open a configuration file and load it in a dictionnary

  Parameters
  ----------
  configPath : string
      Path of config file

  Returns
  -------
  dictionnary
      dictionnary storing configuration
  """
  with open(configPath) as configFile:
    configObject = yaml.safe_load(configFile)
  return configObject


def get_arguments():
  """[summary]

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "-i", "--input",
      help="Path to iadhore ouput base directory")
  parser.add_argument(
      "-o", "--output",
      help="Ouput base directory for circos")
  parser.add_argument(
      "-g", "--ohnologusGenes",
      help="Ouput path for ohnologus genes file storing all anchors computed from i-adhore")
  parser.add_argument(
      "-c", "--config",
      help="Path to config file",
      required=True)

  args = parser.parse_args()
  return args

def override_config_with_args(conf, args):
  """[summary]

  Parameters
  ----------
  conf : [type]
      [description]
  args : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  if args.input:
    conf["iAdhoreOutput"]["segments"] = args.input + "segments.txt"
    conf["iAdhoreOutput"]["genes"] = args.input + "genes.txt"
    conf["iAdhoreOutput"]["multipliconPairs"] = args.input + "multiplicon_pairs.txt"
    conf["iAdhoreOutput"]["multiplicons"] = args.input + "multiplicons.txt"
    conf["iAdhoreOutput"]["baseDir"] = args.input
  if args.output:
    conf["configCircos"]["karyotypePath"] = args.output + "karyotype.txt"
    conf["configCircos"]["ribbonsPath"] = args.output + "ribbons.txt"
    conf["configCircos"]["circosDir"] = args.output

  return conf