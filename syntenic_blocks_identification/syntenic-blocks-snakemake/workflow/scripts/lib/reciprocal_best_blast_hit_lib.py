import os

import pandas as pd
from Bio.Blast.Applications import (NcbiblastpCommandline,
                                    NcbimakeblastdbCommandline)


def set_up_blast_command(conf, querySubject, db, maxTargetSeqs, outputPath, nbrThreads=1):
  """
  A function to construct blast command

  Parameters
  ----------
  conf : dictionnary
      dictionnary storing configuration
  querySubject : string
      Path to input file
  db : string
      database path
  maxTargetSeqs : integer
      Number of max sequence
  outputPath : string
      Path to output file
  nbrThreads : int, optional
      Number of threds used for compute blast, by default 1

  Returns
  -------
  NcbiblastpCommandline
      Object stroring BlastP command line
  """
  return NcbiblastpCommandline(
      query=querySubject,
      db=db,
      out=outputPath + ".tab",
      outfmt='6 qseqid sseqid pident qcovs qlen slen length bitscore evalue',
      max_target_seqs=maxTargetSeqs,
      num_threads=20)


def load_blast_results(resultFile):
  """
  Load blast results from blast output file into a pandas dataframe

  Parameters
  ----------
  resultFile : string
      A string with beginning of filename (malus_malus or malus_prunus file)
  conf : string
      Path to blast directory

  Returns
  -------
  pandas.DataFrame
      Blast results in a pandas dataframe
  """

  try:
    # Load the BLAST results into Pandas dataframes
    blastResults = pd.read_csv(resultFile, sep="\t", header=None)
    
  except:
    blastResults = pd.DataFrame(columns=["query", "subject", "identity", "coverage",
                                         "qlength", "slength", "alength",
                                         "bitscore", "E-value"])
    print("Cannot open this file: " + blastResults)

  # Add columns names
  blastResults.columns = ["query", "subject", "identity", "coverage",
                          "qlength", "slength", "alength",
                          "bitscore", "E-value"]
  return blastResults


def compute_blast(queryFastaFile, conf):
  """[summary]

  Parameters
  ----------
  queryFastaFile : [type]
      [description]
  conf : [type]
      [description]
  """

  outputPath = os.path.join(
      conf["computePath"]["blast"], os.path.basename(queryFastaFile))
  db = conf["computePath"]['blastDb'] + conf["computePath"]['dbTitle']
  # Create BLAST command-lines for forward and reverse BLAST searches
  blastp = set_up_blast_command(
      conf,
      queryFastaFile,
      db,
      maxTargetSeqs=5,
      outputPath=outputPath,
      nbrThreads=1)
  # Run BLAST searches
  stdout, stderr = blastp()
