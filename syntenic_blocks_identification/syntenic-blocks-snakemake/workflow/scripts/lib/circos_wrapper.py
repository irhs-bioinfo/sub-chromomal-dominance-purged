"""
Functions to parse I-ADHoRe output (genes_data.csv and segments.txt) to output them following Circos' requirements. Generate also a config file for Circos
"""
import numpy as np
import pandas as pd

pd.set_option('mode.chained_assignment', None)


def write_karyotype_circos(gdata):
  """
  This function aims to load genes_data.csv file and parse it to follow construct a file gathering name and size of each chromsomes.
  Circos requirements:
  karyotype.txt needs following columns:
  "chr", "dash", "chrom", "label", "start", "stop", "color"


  With Malus generate following file:
  chr - Chr01 Chr01 0 32620872 green
  chr - Chr02 Chr02 0 37543143 green
  chr - Chr03 Chr03 0 37520020 green
  chr - Chr04 Chr04 0 32286110 green
  chr - Chr05 Chr05 0 47948029 green
  chr - Chr06 Chr06 0 37129969 green
  chr - Chr07 Chr07 0 36672612 green
  chr - Chr08 Chr08 0 31591472 green
  chr - Chr09 Chr09 0 37599190 green
  chr - Chr10 Chr10 0 41747934 green
  chr - Chr11 Chr11 0 43027924 green
  chr - Chr12 Chr12 0 33049726 green
  chr - Chr13 Chr13 0 44235857 green
  chr - Chr14 Chr14 0 32505971 green
  chr - Chr15 Chr15 0 54925877 green
  chr - Chr16 Chr16 0 41384270 green
  chr - Chr17 Chr17 0 34732781 green

  Parameters
  ----------
  gdata : pandas.DataFrame
      A dataframe storing gene following informations:
      gene_id, feature, chromosomeNumber, strand, start and stop coordinates
  """
  colors = ["green", "blue", "orange"]
  df = gdata.groupby(["chrom"])[["stop", "sp"]].max()
  df["start"] = 0
  df["chrom"] = df["label"] = df.index
  df["color"] = colors[0]
  for (i, sp) in enumerate(df["sp"].unique()):
    df["color"][df["sp"] == sp] = colors[i]
  df["chr"] = "chr"
  df["dash"] = "-"
  colorder = ["chr", "dash", "chrom", "label", "start", "stop", "color"]
  return df[colorder]


def write_ribbons(ribbons, seg, gdata, keepInterSpecies, thresholdForBlocks=100):
  """
  This function aims to construct and output as a file ribbons.txt:

  Chr03 1515 37318225 Chr11 1 42886494

  Parameters
  ----------
  ribbons : pandas.DataFrame
      A dataframe to store ribbons results with following columns:
      'chr1', 'start1', 'end1', 'chr2', 'start2', 'end2'
  seg : pandas.DataFrame
      Dataframe from segments.txt coming from i-Adhore output. Gather syntenic blocks with following informations
      id	multiplicon	genome	list	first	last	order
  gdata : pandas.DataFrame
      A dataframe storing gene following informations:
      gene_id, feature, chromosomeNumber, strand, start and stop coordinates
  keepInterSpecies : bool
      Ask to drop intra specie relation ship or not
  thresholdForBlocks : integer, optional
      A threshold to drop too small blocks, by default 100
  """

  first = seg.groupby("multiplicon")["first"].apply(list)
  last = seg.groupby("multiplicon")["last"].apply(list)
  df = pd.concat([first, last], axis=1)
  for mp in df.index:
    n = len(df.loc[mp]["first"])
    for i in range(n):
      chr1 = gdata.loc[df.loc[mp]["first"][i]]["chrom"]
      start1 = gdata.loc[df.loc[mp]["first"][i]]["start"]
      end1 = gdata.loc[df.loc[mp]["last"][i]]["stop"]
      specie1 = gdata.loc[df.loc[mp]["first"][i]]["sp"]
      for j in range(i, n):
        chr2 = gdata.loc[df.loc[mp]["first"][j]]["chrom"]
        start2 = gdata.loc[df.loc[mp]["first"][j]]["start"]
        end2 = gdata.loc[df.loc[mp]["last"][j]]["stop"]
        specie2 = gdata.loc[df.loc[mp]["last"][j]]["sp"]
        # Avoid links between same chromosome
        if [chr1, start1, end1] != [chr2, start2, end2]:
          # Drop too small blocks
          if (end1 - start1 > thresholdForBlocks and end2 - start2 > thresholdForBlocks) or (start1 - end1 > thresholdForBlocks and start2 - end2 > thresholdForBlocks):
            ribbons.loc[len(ribbons)] = [chr1, start1,
                                         end1, chr2, start2, end2]

  return ribbons


def write_circos_conf_imbalanced(karyotypePath, ribbonsPath, ribbonsImbalancedPath, fname):
  """
  This functions aims to generate and output a config file for circos.

  Parameters
  ----------
  karyotype : pandas.DataFrame

  karyotypePath : string
      path to karyotype.txt file
  ribbonsPath : string
      path to ribbons.txt file
  fname : string
      Path to output file
  """

  color1 = "color = niceblue2"
  color2 = "color = niceorange2"
  conf_str = """
  <<include etc/colors_fonts_patterns.conf>>
  # this will append your definitions to the <colors> block
  <colors>
  # <<include mycolors.conf>>
    niceblue2 = 61, 190, 255, 0.25
    niceorange2 = 255, 126, 61, 0.25
    green=61, 255, 174, 0.25
  </colors>
  # <<include ideogram.conf>>
  <image>
  <<include etc/image.conf>>
  </image>
  chromosomes_units       = 1000000
  chromosomes_display_default = yes
  karyotype = {}
  <links>
    <link>
      ribbon = yes
      file = {}
      bezier_radius = 0.1r
      thickness = 1
      {}
      radius = 0.88r
    </link>
    <link>
      ribbon = yes
      file = {}
      bezier_radius = 0.1r
      thickness = 1
      {}
      radius = 0.88r
    </link>
  </links>
  <ideogram>
    <spacing>
    default = 0.005r
    </spacing>
    radius    = 0.90r
    thickness = 20p
    fill      = yes
    show_label       = yes
    label_font       = default
    label_radius     = dims(ideogram,radius_outer) + 0.02r
    label_with_tag   = yes
    label_size       = 36
    label_parallel   = yes
    #label_case       = lower
    label_color = black
    </ideogram>
      <<include etc/housekeeping.conf>>
  """.format(karyotypePath, ribbonsPath, color1, ribbonsImbalancedPath, color2)
  with open(fname, "w") as f:
    f.write(conf_str)


def write_circos_conf(ribbons, karyotypePath, ribbonsPath, fname):
  """
  This functions aims to generate and output a config file for circos.

  Parameters
  ----------
  ribbons : pandas.DataFrame
      All existing links between syntenic blocks
  karyotypePath : string
      path to karyotype.txt file
  ribbonsPath : string
      path to ribbons.txt file
  fname : string
      Path to output file
  """
  ribbons["couple"] = ribbons.apply(
    lambda x: x["chr1"] + "-" + x["chr2"], axis=1)
  color = generate_color_rules(ribbons["couple"].unique())

  conf_str = """
  <<include etc/colors_fonts_patterns.conf>>
  # this will append your definitions to the <colors> block
  # <<include ideogram.conf>>
  <image>
  <<include etc/image.conf>>
  </image>
  chromosomes_units       = 1000000
  chromosomes_display_default = yes
  karyotype = {}
  <links>
    <link>
      ribbon = yes
      file = {}
      bezier_radius = 0.1r
      thickness = 1
      radius = 0.95r
      {}
    </link>
  </links>
  <ideogram>
    <spacing>
    default = 0.005r
    </spacing>
    radius    = 0.90r
    thickness = 20p
    fill      = yes
    show_label       = yes
    label_font       = default
    label_radius     = dims(ideogram,radius_outer) + 0.02r
    label_with_tag   = yes
    label_size       = 36
    label_parallel   = yes
    #label_case       = lower
    label_color = black
    </ideogram>
      <<include etc/housekeeping.conf>>
  """.format(karyotypePath, ribbonsPath, color)
  with open(fname, "w") as f:
    f.write(conf_str)


def generate_color_rules(uniqueChrCouple):
  """
  This function aims to generate a color rules for circos.

  Parameters
  ----------
  uniqueChrCouple : list
      list of unique couple of chromosomes

  Returns
  -------
  color : string
      color rules
  """

  colorRules = """ <rules>
  </rules>"""
  pastelPalette, materialPalette = color_palette(str(0.65))

  for chrCouple, color in zip(uniqueChrCouple, materialPalette):
    newRule = """
      <rule>
          condition  = between({},{})
          color      = {}
          thickness  = 4
          z          = 15
      </rule>
      """.format(chrCouple.split("-")[0], chrCouple.split("-")[1], color)
    idx = colorRules.index("</rules>")
    colorRules = colorRules[:idx] + newRule + colorRules[idx:]
  return colorRules


def generate_color_rules_two_species(karyotype):

  colorRules = """ <rules>
  </rules>"""
  pastelPalette, materialPalette = color_palette(str(0.65))
  notMalus = karyotype["chrom"][~karyotype["chrom"].str.startswith(
      'Chr')]
  malus = karyotype["chrom"][karyotype["chrom"].str.startswith(
      'Chr')]

  for malusValue in malus.iteritems():
    for notMalusValue, color in zip(notMalus.iteritems(), pastelPalette):
      newRule = """
        <rule>
            condition  = between({},{})
            color      = {}
            thickness  = 4
            z          = 15
        </rule>
        """.format(malusValue[1], notMalusValue[1], color)
      idx = colorRules.index("</rules>")
      colorRules = colorRules[:idx] + newRule + colorRules[idx:]
  return colorRules


def color_palette(transparency):
  pastelPalette = ['141,211,199,' + transparency,
                   '216,216,151,' + transparency,
                   '190,186,218,' + transparency,
                   '251,128,114,' + transparency,
                   '128,177,211,' + transparency,
                   '253,180,98,' + transparency,
                   '179,222,105,' + transparency,
                   '252,205,229,' + transparency,
                   '217,217,217,' + transparency,
                   '188,128,189,' + transparency,
                   '204,235,197,' + transparency,
                   '255,237,111,' + transparency]

  materialPalette = [
      '2, 63, 165,' + transparency,
      '125, 135, 185,' + transparency,
      '190, 193, 212,' + transparency,
      '214, 188, 192,' + transparency,
      '187, 119, 132,' + transparency,
      '142, 6, 59,' + transparency,
      '74, 111, 227,' + transparency,
      '133, 149, 225,' + transparency,
      '181, 187, 227,' + transparency,
      '230, 175, 185,' + transparency,
      '224, 123, 145,' + transparency,
      '211, 63, 106,' + transparency,
      '17, 198, 56,' + transparency,
      '141, 213, 147,' + transparency,
      '198, 222, 199,' + transparency,
      '234, 211, 198,' + transparency,
      '240, 185, 141,' + transparency,
      '239, 151, 8,' + transparency,
      '15, 207, 192,' + transparency,
      '156, 222, 214,' + transparency,
      '213, 234, 231,' + transparency,
      '243, 225, 235,' + transparency,
      '246, 196, 225,' + transparency,
      '247, 156, 212,' + transparency,


      # material
      # '244, 67, 54, ' + transparency,
      # '232, 30, 99, ' + transparency,
      # '156, 39, 176, ' + transparency,
      # '103, 58, 183, ' + transparency,
      # '63, 81, 181, ' + transparency,
      # '33, 150, 243, ' + transparency,
      # '3, 169, 244, ' + transparency,
      # '0, 188, 212, ' + transparency,
      # '0, 150, 136, ' + transparency,
      # '76, 175, 80, ' + transparency,
      # '139, 195, 74, ' + transparency,
      # '205, 220, 57, ' + transparency,
      # '255, 235, 59, ' + transparency,
      # '255, 193, 7, ' + transparency,
      # '255, 152, 0, ' + transparency,
      # '255, 87, 34, ' + transparency,
      # '121, 85, 72, ' + transparency,
      # '158, 158, 158, ' + transparency,
      # '96, 125, 139, ' + transparency
  ]

  # '166,206,227,' + transparency,
  # '31,120,180,' + transparency,
  # '178,223,138,' + transparency,
  # '51,160,44,' + transparency,
  # '251,154,153,' + transparency,
  # '227,26,28,' + transparency,
  # '253,191,111,' + transparency,
  # '255,127,0,' + transparency,
  # '202,178,214,' + transparency,
  # '106,61,154,' + transparency,
  # '255,255,153,' + transparency,
  # '177,89,40,' + transparency,
  return pastelPalette, materialPalette
