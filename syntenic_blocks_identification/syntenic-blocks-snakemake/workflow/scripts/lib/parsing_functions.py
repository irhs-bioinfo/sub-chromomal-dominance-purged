def drop_duplicated_rows_by_check_string(df, columns):
  """[summary]

  Parameters
  ----------
  df : [type]
      [description]
  columns : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  df['check_string'] = df.apply(
      lambda row: ''.join(sorted([row[columns[0]], row[columns[1]]])), axis=1)
  # Drop duplicated check string
  df.drop_duplicates(
      'check_string', inplace=True)
  df.drop('check_string', 1, inplace=True)
  return df


def get_ohnologous_genes_on_whole_genome(df, specieId, order=2):
  """[summary]

  Parameters
  ----------
  df : [type]
      [description]
  specieId : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """
  df = df[df["code"]==order]
  df = df[['gene_x', 'gene_y', 'multiplicon']]
  df = purge_patterns(
      df, 'Pr', ['gene_x', 'gene_y'], 0)
  df = purge_patterns(
      df, specieId + "00", ['gene_x', 'gene_y'], 0)
#   df = df.sort_values(by=['gene_x', 'gene_y'])
  df = drop_duplicated_rows_by_check_string(
      df, ['gene_x', 'gene_y'])
  return df


def segments_filter(segments, order=2):
  """
  Filter the segments file by order of the multiplicons

  Parameters
  ----------
  segments : pandas.DataFrame
      Dataframe from segments.txt coming from i-Adhore output. Gather syntenic blocks with following informations
      id	multiplicon	genome	list	first	last	order
  order : integer
      Multiplicon order used for filtering

  Returns
  -------
  pandas.DataFrame
      segments dataframes filtered by minimal multiplicon order
  """
  s = segments.groupby("multiplicon").count()["genome"]
  idx = s[s >= order].index
  return segments[segments["multiplicon"].isin(idx)]


def purge_patterns(df, pattern, columns, keep):
  """
  This function aims to delete from a dataframe all rows where columns starts with a given string pattern

  Parameters
  ----------
  df : pandas.DataFrame
      A dataframe
  pattern : string
      Searched pattern
  columns : array
      Columns where search for pattern
  keep : bool
      A bool to indicate if a purge or a keep is needed

  Returns
  -------
  pandas.DataFrame
      Dataframe without row storing asked pattern
  """
  for columnName in columns:
    if keep:
      # Keep row from dataframe that are starting with given pattern
      df = df[df[columnName].astype(str).str.startswith(pattern)]
    else:
      # Get row from dataframe that are not starting with given pattern
      df = df[~df[columnName].astype(str).str.startswith(pattern)]
  return df
