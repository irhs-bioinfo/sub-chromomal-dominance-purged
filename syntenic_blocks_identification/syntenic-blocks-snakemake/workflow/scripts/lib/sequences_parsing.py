import pandas as pd
import numpy as np
from Bio import SeqIO
from tqdm import tqdm


def load_sequences_file(files):
  """
  This function aims to load a fasta file

  Parameters
  ----------
  files : array
      Array of path to fasta file

  Returns
  -------
  array
      Array of SeqRecord iterator
  """
  sequences = []
  # Iterate on path array
  for file in files:
    # Load fasta file and add SeqRecord iterator in result array
    sequences.append(SeqIO.parse(
        file,
        "fasta"))
  return sequences


def find_multiplicon_of_couple(ohnologousGenes, gene1, gene2):
  """
  This function aims to get multiplicon number of a gene couple

  Parameters
  ----------
  ohnologousGenes : pandas.DataFrame
      A dataframe with three column: gene1, gene2 and associated multiplicon
  gene1 : string
      Gene name
  gene2 : string
      Gene name

  Returns
  -------
  integer
      Multiplicon ID
  """
  row = ohnologousGenes[(ohnologousGenes["gene_x"] == gene1) &
                        (ohnologousGenes["gene_y"] == gene2)]
  if row.empty:
    row = ohnologousGenes[(ohnologousGenes["gene_x"] == gene2) &
                          (ohnologousGenes["gene_y"] == gene1)]

  return row["multiplicon"].values[0]


def get_gene_ids_of_a_ohnologous(ohnologousRow, recordDict, columns):
  """
  This function aims to get SeqRecord from an orthogroup family ID

  Parameters
  ----------
  ohnologousRow : pandas.DataFrame
      A row of orthogroup Dataframe
  recordDict : Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  columns : string
      Column Name storing gene ID

  Returns
  -------
  Dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  ohnoFamily = {}
  ids = []
  for column in columns:
    if len(ids)>2:
      ids = []
    if not pd.isnull(ohnologousRow[column]):
      ids.append(ohnologousRow[column])
      if len(ids) == 2:
        ohnoFamily[ohnologousRow['index']] = find_sequence(ids, recordDict)
  return ohnoFamily


def find_sequence(ids, records):
  """
  This function aims to get SeqRecord from sequence ID

  Parameters
  ----------
  ids : array
      Array of ID of sequence
  records : dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key

  Returns
  -------
  dictionnary
      Dictionnary of SeqRecord iterator as value and Sequence's ID as key
  """
  rec = {}
  for ID in ids:
    ID = ID.replace(" ", "")
    if ID.startswith('Pr') and not ID.endswith('.1'):
      ID = ID + '.1'
    if records.get(ID).id not in rec:
      rec[records.get(ID).id] = records.get(ID)
  return rec


def construct_fasta_of_orthogroup(orthoGroups, conf, type):
  """
  This function aims to construct a fasta file with all sequence of an orthogroup

  Parameters
  ----------
  orthoGroups : dictionary
    Dictionnary of SeqRecord iterator as value and Orthogroup's ID as key
  conf : dictionnary
      A dictionnary storing configuration variable
  type : string
      Indicate type of computed sequence, it is used to access key in conf dictionnary
  """
  for specie in tqdm(orthoGroups, desc="Constructing Fasta Files"):
    for i in specie.keys():
      for orthoFamily in specie[i].keys():
        with open(conf['computePath'][type] + str(orthoFamily) + ".fasta", "a") as output_handle:
          for j in specie[i][orthoFamily].values():
              SeqIO.write(j, output_handle, "fasta")
