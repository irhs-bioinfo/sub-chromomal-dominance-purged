# %% [markdown]
"""
## Reciprocal Best Blast Hit
This pipeline is splitted in three steps:

- Construction of blast databases for both genomes (prunus and malus)
- Run a forward and a reverse blast between malus and malus and malus and prunus
- Parse results, merge into a single panda dataframe and save it in csv file

## Few specifities

- Build a blast database is usefull to blast using multithread fashion
- In case of blast malus vs malus, best blast hit with one max target sequence,
 genes only match with themselves.
 So put max target to two and awk to delete genes matching with themselves, keeping second best blast hit
"""
# %%


import argparse
import multiprocessing
import os
from functools import partial

import pandas as pd
from Bio import SeqIO
from tqdm import tqdm

from lib.config_parser import config_parser
from lib.misc import get_list_of_file_with_extension, output_dataframe
from lib.reciprocal_best_blast_hit_lib import (compute_blast,
                                               load_blast_results)
from lib.sequences_parsing import (construct_fasta_of_orthogroup,
                                   find_multiplicon_of_couple,
                                   get_gene_ids_of_a_ohnologous,
                                   load_sequences_file)

# %%
def get_arguments():
  """[summary]

  Returns
  -------
  [type]
      [description]
  """
  parser = argparse.ArgumentParser()

  parser.add_argument(
      "-g", "--ohnologusGenes",
      help="Ouput path for ohnologus genes file storing all anchors computed from i-adhore")
  parser.add_argument(
      "-c", "--config",
      help="Path to config file",
      required=True)

  args = parser.parse_args()
  return args


def override_config_with_args(conf, args):
  """[summary]

  Parameters
  ----------
  conf : [type]
      [description]
  args : [type]
      [description]

  Returns
  -------
  [type]
      [description]
  """

  if args.ohnologusGenes:
    conf["iadhore"]["ohnologousGenes"] = args.ohnologusGenes
  
  return conf

args = get_arguments()
# Load config file
conf = config_parser(args.config)
conf = override_config_with_args(conf, args)
# %%

ohnologousGenes = pd.read_csv(conf['iadhore']['ohnologousGenes'], sep=' ')
# Reset index to get a column named index storing an unique index
ohnologousGenes = ohnologousGenes.reset_index()

# Load sequences as an array of SeqIO handle
sequences = load_sequences_file(
    [conf['genomicData']['proteinSequences']['malusDomestica'],
     conf['genomicData']['cdsSequences']['malusDomestica'],
     conf['genomicData']['proteinSequences']['prunusPersica'],
     conf['genomicData']['cdsSequences']['prunusPersica']])

# Store it in different variables
recordProteinsMalus = sequences[0]
recordNucleicMalus = sequences[1]
recordProteinsPrunus = sequences[2]
recordNucleicPrunus = sequences[3]

recordProteinsMalusDict = SeqIO.to_dict(recordProteinsMalus)
recordNucleicMalusDict = SeqIO.to_dict(recordNucleicMalus)
recordProteinsPrunusDict = SeqIO.to_dict(recordProteinsPrunus)
recordNucleicPrunusDict = SeqIO.to_dict(recordNucleicPrunus)

for directory in [conf['computePath']['proteinFasta'], conf['computePath']['nucleicFasta'], conf['computePath']['blast']]:
  os.makedirs(os.path.dirname(directory), exist_ok=True)


# Get Seq.record of each genes associated to each orthogroups
orthoProteinsMalus = ohnologousGenes.apply(
    get_gene_ids_of_a_ohnologous, axis=1, args=(
        recordProteinsMalusDict,
        ['gene_x', 'gene_y']))
orthoNucleicMalus = ohnologousGenes.apply(
    get_gene_ids_of_a_ohnologous, axis=1, args=(
        recordNucleicMalusDict,
        ['gene_x', 'gene_y']))

# Creates a set of fasta files named after the orthogroup identifier, and the associated protein sequences therein
construct_fasta_of_orthogroup(
    [orthoProteinsMalus],
    conf,
    'proteinFasta')

# Creates a set of fasta files named after the orthogroup identifier, and
# the associated nucleic sequences therein
construct_fasta_of_orthogroup(
    [orthoNucleicMalus],
    conf,
    'nucleicFasta')

# Store name of created files
listOfNucleicFile = get_list_of_file_with_extension(
    conf['computePath']['nucleicFasta'], "*.fasta")
listOfProteinFile = get_list_of_file_with_extension(
    conf['computePath']['proteinFasta'], "*.fasta")
# %%

# for queryFastaFile in tqdm(listOfProteinFile):
#   compute_blast(queryFastaFile, conf)

# %%
# Get all blast results
listOfBlastFile = get_list_of_file_with_extension(
    conf['computePath']['blast'], "*.tab")
# Init result variables
tripletDf = pd.DataFrame(columns=["gene1", "gene2", "gene3", "multiplicon"])
tempsTriplet = {}

allblastFileResult = pd.DataFrame()
tripletsblastFileResult = pd.DataFrame()

# Iterate on blast files
for blastFileResult in tqdm(listOfBlastFile):
  # Load blast results in pandas Dataframe
  blastResult = load_blast_results(blastFileResult)
  # Sort by queried sequence and by identity
  blastResult = blastResult.sort_values(
      by=['query', 'identity'], ascending=False)
  # Drop duplciated results
  blastResult = blastResult.drop_duplicates(subset=['query', 'subject'])
  allblastFileResult = allblastFileResult.append(blastResult, ignore_index=True)
  # Groupby queried sequence
  blastResultGrouped = blastResult.groupby("query")
  # Get first results of each group
  firstRowOfEachGroup = blastResultGrouped.head(n=1)
  # At this state each groups looks like:
  if firstRowOfEachGroup.shape[0] == 2:
    # If best blast hit on each MD gene is the same Prunus gene
    if firstRowOfEachGroup.iloc[0]["subject"] == firstRowOfEachGroup.iloc[1]["subject"] and firstRowOfEachGroup.iloc[0]["query"] != firstRowOfEachGroup.iloc[1]["query"]:
      tempsTriplet["gene1"] = firstRowOfEachGroup.iloc[0]["query"]
      tempsTriplet["gene2"] = firstRowOfEachGroup.iloc[1]["query"]
      tempsTriplet["gene3"] = firstRowOfEachGroup.iloc[0]["subject"]
      tempsTriplet["multiplicon"] = find_multiplicon_of_couple(ohnologousGenes,
                                                              firstRowOfEachGroup.iloc[0]["query"],
                                                              firstRowOfEachGroup.iloc[1]["query"])
      tripletDf = tripletDf.append(tempsTriplet, ignore_index=True)
      tripletsblastFileResult = tripletsblastFileResult.append(firstRowOfEachGroup, ignore_index=True)

output_dataframe(path=conf["triplet"]["dataFrameBlast"], dataframe=allblastFileResult, separator=",", verbose=True)
output_dataframe(path=conf["triplet"]["dataFrameBlastTriplets"], dataframe=tripletsblastFileResult, separator=",", verbose=True)

print("Triplet construct: ",
      tripletDf.shape[0], " upon ", str(len(listOfBlastFile))+ \
      " files. Some triplet cannot be reconstructed with reliable results: ")

output_dataframe(path=conf["triplet"]["dataFrame"], dataframe=tripletDf, separator=",", verbose=True)


# %%
