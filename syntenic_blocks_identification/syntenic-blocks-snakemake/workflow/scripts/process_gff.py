#!/usr/bin/env python3
import sys
import os
import re
import pandas as pd

args = iter(sys.argv)
next(args)  # Skip the script name
inputPath = next(args)
outputPath = next(args)
purgeScaffold = next(args)
allGff = {}

for file in os.listdir(inputPath):
    if file.endswith(('.gff', '.gff3')):
        filePath = os.path.join(inputPath, file)
        df = pd.read_csv(filePath, sep='\t', header=None, comment='#')

        species = os.path.splitext(file)[0]
        allGff[species] = df

for species in list(allGff.keys()):
    gff = allGff[species]

    # Keep only gene feature and list all unique lg name
    gff = gff[gff.iloc[:, 2] == 'gene']
    lg_name = gff.iloc[:, 0].unique()

    for lg in lg_name:
        # Create new df with all genes of the lg then sort by start position
        lg_list = gff[gff.iloc[:, 0] == lg]
        lg_list = lg_list.sort_values(by=[3], ascending=True)
        # Keep only strand and attributes, extract the name from attributes
        # and merge both values
        lg_list = lg_list.iloc[:, [6, 8]]
        lg_list.iloc[:, 1] = lg_list.iloc[:, 1].apply(lambda x:
          re.search('ID=(gene:){0,1}(?P<id>[^;]*)', x).group('id'))
        lg_list = lg_list.iloc[:, 1] + lg_list.iloc[:, 0]

        dirPath = os.path.join(outputPath, species)
        os.makedirs(dirPath, exist_ok=True)
        filePath = os.path.join(outputPath, species, lg + '.lst')
        if purgeScaffold and not lg.startswith('scaffold'):
          lg_list.to_csv(filePath, header=False, index=False)
