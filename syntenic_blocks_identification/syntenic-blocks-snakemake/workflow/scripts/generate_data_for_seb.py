#%%

import pandas as pd
from tqdm import tqdm
tqdm.pandas()

ohnologousGenes = pd.read_csv("../../results/tables/ohnologousGenes.csv",delimiter=" ")
geneDataMalus = pd.read_csv("../../results/circos/gene_data_malus_domestica.csv",delimiter=",")


def get_localisation_of_gene(geneID, geneDataMalus, column):
  return int(geneDataMalus.loc[geneDataMalus["gene_id"] == geneID][column])


for column in ["start_x",
               "stop_x",
               "start_y",
               "stop_y",]:
  ohnologousGenes[column] = 0


ohnologousGenes["start_x"] = ohnologousGenes["gene_x"].progress_apply(
    get_localisation_of_gene, args=(geneDataMalus, "start"))
ohnologousGenes["stop_x"] = ohnologousGenes["gene_x"].progress_apply(
    get_localisation_of_gene, args=(geneDataMalus, "stop"))
ohnologousGenes["start_y"] = ohnologousGenes["gene_y"].progress_apply(
    get_localisation_of_gene, args=(geneDataMalus, "start"))
ohnologousGenes["stop_y"] = ohnologousGenes["gene_y"].progress_apply(
    get_localisation_of_gene, args=(geneDataMalus, "stop"))



# ohnologousGenes = ohnologousGenes.drop_duplicates(
#     subset=['gene_x'], keep='first')
ohnologousGenes = ohnologousGenes.sort_values(by=["multiplicon"])

MD13MD16 = ohnologousGenes.loc[(ohnologousGenes["gene_x"].str.startswith("MD13")) & 
                               (ohnologousGenes["gene_y"].str.startswith("MD16"))]


MD13MD16 = MD13MD16.loc[(MD13MD16["start_x"].between(2900000, 10000000)) &
                        (MD13MD16["start_y"].between(2900000, 10000000))]


MD13MD16[["gene_x", "gene_y", "multiplicon"]].to_csv(
    "../../results/tables/ohnologousGenesSebMD13MD16.csv", sep=" ", index=False)
ohnologousGenes[["gene_x", "gene_y", "multiplicon"]].to_csv(
    "../../results/tables/ohnologousGenesSeb.csv", sep=" ", index=False)
# %%
