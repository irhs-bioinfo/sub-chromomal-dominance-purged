import os
import sys

import subprocess as sp
from tempfile import TemporaryDirectory
import shutil
from pathlib import Path, PurePosixPath

sys.path.insert(0, os.path.dirname(__file__))

import common


def test_construct_blast_database():

    with TemporaryDirectory() as tmpdir:
        workdir = Path(tmpdir) / "workdir"
        data_path = PurePosixPath(".tests/unit/construct_blast_database/data")
        expected_path = PurePosixPath(".tests/unit/construct_blast_database/expected")

        # Copy data to the temporary workdir.
        shutil.copytree(data_path, workdir)

        # dbg
        print("results/blast/db/GDDH13.pdb results/blast/db/GDDH13.phr results/blast/db/GDDH13.pin results/blast/db/GDDH13.pot results/blast/db/GDDH13.psq results/blast/db/GDDH13.ptf results/blast/db/GDDH13.pto", file=sys.stderr)

        # Run the test job.
        sp.check_output([
            "python",
            "-m",
            "snakemake", 
            "results/blast/db/GDDH13.pdb results/blast/db/GDDH13.phr results/blast/db/GDDH13.pin results/blast/db/GDDH13.pot results/blast/db/GDDH13.psq results/blast/db/GDDH13.ptf results/blast/db/GDDH13.pto",
            "-f", 
            "-j1",
            "--keep-target-files",
    
            "--directory",
            workdir,
        ])

        # Check the output byte by byte using cmp.
        # To modify this behavior, you can inherit from common.OutputChecker in here
        # and overwrite the method `compare_files(generated_file, expected_file), 
        # also see common.py.
        common.OutputChecker(data_path, expected_path, workdir).check()
