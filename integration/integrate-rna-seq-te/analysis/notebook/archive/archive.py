from sklearn_extra.cluster import KMedoids
test1Head = test1.head(10000)
test1Head = test1Head.loc[test1Head["TEcoverage"] < 1]
test1Head.index = test1Head["name"]
test1Head = test1Head.drop(columns=["name"])
TEMetadata0Clusters = test1Head[test1Head["numberTE"] == 0]
TEMetadata0Clusters["cluster"] = 7
TEMetadataAll = test1Head[test1Head["numberTE"] != 0]
X = TEMetadataAll[["TEcoverage", "logFC"]]
# Best number of clusters seems to be 4
kmedoids = KMedoids(n_clusters=4,
                    metric='manhattan',
                    #                    method='pam',
                    #                    init='heuristic',
                    max_iter=100,
                    random_state=42)
yhat = kmedoids.fit_predict(X)
TEMetadataAll["cluster"] = yhat
TEMetadata = pd.concat([TEMetadataAll, TEMetadata0Clusters])


px.scatter_matrix(TEMetadata,color='cluster', dimensions=["TEcoverage", "logFC", "rawCountMean"])



###### regression KFold


from sklearn.compose import TransformedTargetRegressor
from sklearn.preprocessing import QuantileTransformer, MinMaxScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
import plotly.graph_objects as go

regressor = LinearRegression()


model = TransformedTargetRegressor(regressor=regressor, transformer=MinMaxScaler())
# evaluate model
cv = KFold(n_splits=10, shuffle=True, random_state=1)
scores = cross_val_score(model, X, y, scoring='neg_mean_absolute_error', cv=cv, n_jobs=-1)
# convert scores to positive
scores = np.absolute(scores)
# summarize the result
s_mean = np.mean(scores)
print('Mean MAE: %.3f' % (s_mean))



###### regfression statsmodels


import statsmodels.api as sm
from statsmodels.formula.api import logit

cols = [["LGene","TElength","geneChr","numberTE","duplicate","TEcoverage","TEdensity","rawCountMean","logFC","log_rawCountMean"]]
X = test1[cols]
y = test1["cluster"]
## fit a OLS model with intercept on TV and Radio
X = sm.add_constant(X)
est = sm.Logit(test1["TEcoverage"], test1[["rawCountMean", "logFC"]]).fit()
est.summary()


# generate dummies columns


testDummies = pd.DataFrame()
for name, group in test1.groupby("couple"):
  if group.shape[0] > 800:
    testDummies = testDummies.append(group, ignore_index=True)

testDummies = testDummies[["LGene","TElength","geneChr","numberTE","duplicate","TEcoverage","TEdensity","rawCountMean","logFC","log_rawCountMean", "couple"]]

testDummies = pd.get_dummies(data=testDummies, drop_first=True)
testDummies


# LGene	TElength	geneChr	numberTE	duplicate	TEcoverage	TEdensity	rawCountMean	logFC	log_rawCountMean	couple_02-15	couple_03-11	couple_04-12	couple_05-10	couple_06-14	couple_08-15	couple_09-17	couple_13-16
# 0	5521.0	1426.0	1	3.0	True	0.258287	0.000733	1470.545861	7.313097	10.522136	0	0	0	0	0	0	0	0
# 1	4868.0	2202.0	7	11.0	True	0.452342	0.004126	37.366890	7.313097	5.223689	0	0	0	0	0	0	0	0
# 2	5214.0	1101.0	1	7.0	True	0.211162	0.001702	101.722222	-2.486390	6.668491	0	0	0	0	0	0	0	0
# 3	5336.0	1490.0	7	5.0	True	0.279235	0.001300	524.416667	-2.486390	9.034570	0	0	0	0	0	0	0	0
# 4	6737.0	1915.0	1	10.0	True	0.284251	0.002074	271.578089	4.397316	8.085223	0	0	0	0	0	0	0	0
# ...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...
# 17083	4000.0	1021.0	16	12.0	True	0.255250	0.004028	218.939815	3.007741	7.774391	0	0	0	0	0	0	0	1
# 17084	5553.0	2944.0	13	14.0	True	0.530164	0.005366	241.211712	9.401673	7.914156	0	0	0	0	0	0	0	1
# 17085	4117.0	470.0	16	8.0	True	0.114161	0.002194	0.961712	9.401673	-0.056324	0	0	0	0	0	0	0	1
# 17086	11610.0	4072.0	13	22.0	True	0.350732	0.002919	27.222222	-2.219723	4.766713	0	0	0	0	0	0	0	1
# 17087	7600.0	3446.0	16	9.0	True	0.453421	0.002167	125.777778	-2.219723	6.974733	0	0	0	0	0	0	0	1